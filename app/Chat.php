<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Chat extends Model {

	public $timestamps = false;

	protected $table = 'chatbox';

	public function user()
    {
        return $this->belongsTo('App\User');
    }

}
