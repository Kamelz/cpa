<?php namespace App\Commands;

// Can run for 10 minutes maximum
set_time_limit(600);

use App\Commands\Command;

use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Bus\SelfHandling;

use Cache;

class FetchOffers extends Command implements SelfHandling {

	use InteractsWithQueue, SerializesModels;

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */

	public $network;


	public function __construct($network)
	{
		$this->network = $network;
	}

	/**
	 * Execute the command.
	 *
	 * @return void
	 */

	public function handle()
	{
		// Turn off all offers for this network. We'll activate later
		\App\Offer::whereNetwork($this->network)->update(['active' => 0]);

		switch ($this->network) {
			case 'Adworkmedia':
				$this->getAdworkOffers();
				break;
			case 'Adgatemedia':
				$this->getAdgateOffers();
				break;
			case 'Adscendmedia':
				$this->getAdscendOffers();
				break;
			case 'CPAGrip':
				$this->getCPAGripoffers();
				break;
			case 'CPALead':
				$this->getCPALeadOffers();
				break;
			case 'Adludum':
				$this->getAdludumOffers();
				break;
		}
	}

	public function getAdworkOffers() {

		$networkdt = \App\Network::whereName($this->network)->first();

		$url = $networkdt->offer_url;
		$pubid = $networkdt->pubid;
		$apikey = $networkdt->apikey;

		$url = str_replace('%pubid%',$pubid,$url);
		$url = str_replace('%apikey%',$apikey,$url);

		$result = file_get_contents($url);
        $xml = simplexml_load_string($result);

        $total = count($xml->campDetails);

        echo 'Total:'.$total;

        $n = 1;

        foreach ($xml->campDetails as $offer) {

    	if (!preg_match('/^[0-9]+(\.[0-9]{0,2})?$/',$offer->cr)) { continue; }
    	if (str_replace('$','',$offer->epc) < $networkdt->minepc) { continue; }

    	$count = \App\Offer::whereCampid($offer->campaign_id)->whereNetwork($this->network)->count();

    	if ($count != 0) {
    		\App\Offer::whereCampid($offer->campaign_id)->whereNetwork($this->network)->update(['active' => 1,'updated_at' => date('Y-m-d H:i:s'),'network_epc' => str_replace('$','',$offer->epc), 'payout' => str_replace('$','',$offer->payout)]);
    	} else {
    		$noffer = new \App\Offer;
    		$noffer->campid = $offer->campaign_id;
    		$noffer->offer = $offer->campaign_name;
    		$noffer->title = $this->formatstr($offer->teaseText);
    		$noffer->network_epc = str_replace('$','',$offer->epc);
    		$noffer->payout = str_replace('$','',$offer->payout);
    		$noffer->url = $this->formatstr($offer->url).'%subid%';
    		$noffer->countries = $offer->countries;
			$noffer->network = $this->network;
			$noffer->created_at = date('Y-m-d H:i:s');
			$noffer->updated_at = date('Y-m-d H:i:s');
			$noffer->device = $this->findevice($offer->device_type);
			$noffer->save();
    	}

		Cache::put('fetch_'.$this->network,'Processing '.$this->network.' Offers: '.$n.' of '.$total,20);
    	$n++;
    	usleep(1000);

        } 

        Cache::put('fetch_'.$this->network,'Processing Complete',20);

        $numOffers = \App\Offer::whereNetwork($this->network)->whereActive(1)->count();
        \App\Network::whereName($this->network)->update(['numOffers' => $numOffers, 'updated_at' => date('Y-m-d H:i:s')]);

	}

	public function getAdgateOffers() {
		$networkdt = \App\Network::whereName($this->network)->first();

		$url = $networkdt->offer_url;
		$pubid = $networkdt->pubid;
		$apikey = $networkdt->apikey;

		$url = str_replace('%pubid%',$pubid,$url);
		$url = str_replace('%apikey%',$apikey,$url);

		$result = file_get_contents($url);
        $offers = json_decode($result,true);

        $total = count($offers);

        $n = 1;

        foreach ($offers as $offer) {

    	if ($offer['epc'] < $networkdt->minepc) { continue; }

    	$count = \App\Offer::whereCampid($offer['id'])->whereNetwork($this->network)->count();

    	if ($count != 0) {
    		\App\Offer::whereCampid($offer['id'])->whereNetwork($this->network)->update(['active' => 1,'updated_at' => date('Y-m-d H:i:s'), 'network_epc' => $offer['epc'], 'payout' => $offer['payout']]);
    	} else {
    		$noffer = new \App\Offer;
    		$noffer->campid = $offer['id'];
    		$noffer->offer = $offer['name'];
    		$noffer->title = $this->formatstr($offer['anchor']);
    		$noffer->network_epc = $offer['epc'];
    		$noffer->payout = $offer['payout'];
    		$noffer->url = $this->formatstr($offer['tracking_url']).'%subid%';
    		$noffer->countries = $offer['country'];
			$noffer->network = $this->network;
			$noffer->created_at = date('Y-m-d H:i:s');
			$noffer->updated_at = date('Y-m-d H:i:s');
			$noffer->device = $this->findevice($offer['ua']);
			$noffer->save();
    	}

		Cache::put('fetch_'.$this->network,'Processing '.$this->network.' Offers: '.$n.' of '.$total,20);
    	$n++;
    	usleep(1000);

        } 

        Cache::put('fetch_'.$this->network,'Processing Complete',20);

        $numOffers = \App\Offer::whereNetwork($this->network)->count();
        \App\Network::whereName($this->network)->update(['numOffers' => $numOffers, 'updated_at' => date('Y-m-d H:i:s')]);
	}

	public function getAdscendOffers() {
			$networkdt = \App\Network::whereName($this->network)->first();

			$url = $networkdt->offer_url;
			$pubid = $networkdt->pubid;
			$apikey = $networkdt->apikey;

			$url = str_replace('%pubid%',$pubid,$url);
			$url = str_replace('%apikey%',$apikey,$url);

			$result = file_get_contents($url);
	        $offers = json_decode($result,true);

	        $total = count($offers);

	        echo $total;

	        $n = 1;

	        foreach ($offers as $offer) {

    		if ($offer['perf_score'] < $networkdt->minepc) { continue; }
    		if ($offer['perf_score'] > 1) { continue; }

	    	$count = \App\Offer::whereCampid($offer['id'])->whereNetwork($this->network)->count();

	    	if ($count != 0) {
	    		\App\Offer::whereCampid($offer['id'])->whereNetwork($this->network)->update(['active' => 1,'updated_at' => date('Y-m-d H:i:s'), 'network_epc' => $offer['perf_score'], 'payout' => $offer['payout']]);
	    	} else {
	    		$noffer = new \App\Offer;
	    		$noffer->campid = $offer['id'];
	    		$noffer->offer = $offer['name'];
	    		$noffer->title = $this->formatstr($offer['description']);
	    		$noffer->network_epc = $offer['perf_score'];
	    		$noffer->payout = $offer['payout'];
	    		$noffer->url = $this->formatstr($offer['url']).'%subid%';
	    		$noffer->countries = $offer['country'];
				$noffer->network = $this->network;
				$noffer->created_at = date('Y-m-d H:i:s');
				$noffer->updated_at = date('Y-m-d H:i:s');
				$noffer->device = $this->findevice($offer['target_platform']);
				$noffer->save();
	    	}

			Cache::put('fetch_'.$this->network,'Processing '.$this->network.' Offers: '.$n.' of '.$total,20);
	    	$n++;
	    	usleep(1000);

	        } 

	        Cache::put('fetch_'.$this->network,'Processing Complete',20);

	        $numOffers = \App\Offer::whereNetwork($this->network)->count();
        	\App\Network::whereName($this->network)->update(['numOffers' => $numOffers, 'updated_at' => date('Y-m-d H:i:s')]);
		}


	public function getCPAGripOffers() {

		$networkdt = \App\Network::whereName($this->network)->first();

		$url = $networkdt->offer_url;
		$pubid = $networkdt->pubid;
		$apikey = $networkdt->apikey;

		$url = str_replace('%pubid%',$pubid,$url);
		$url = str_replace('%apikey%',$apikey,$url);

		$checkurls = [];

		array_push($checkurls,$url.'&ua=firefox');
		array_push($checkurls,$url.'&ua=android');
		array_push($checkurls,$url.'&ua=ios');
		array_push($checkurls,$url.'&ua=iphone');
		array_push($checkurls,$url.'&ua=ipad');

		foreach ($checkurls as $checkurl) {

			$result = utf8_encode(file_get_contents($checkurl));
	        $offers = simplexml_load_string($result);

	        $total = count($offers);

	        $n = 1;

	        foreach ($offers->offers->offer as $offer) {

    		if ($offer->netepc < $networkdt->minepc) { continue; }

	    	$count = \App\Offer::whereCampid($offer->offer_id)->whereNetwork($this->network)->count();

	    	if ($count != 0) {
	    		\App\Offer::whereCampid($offer->offer_id)->whereNetwork($this->network)->update(['active' => 1,'updated_at' => date('Y-m-d H:i:s'), 'network_epc' => $offer->netepc, 'payout' => $offer->payout]);
	    	} else {
	    		$noffer = new \App\Offer;
	    		$noffer->campid = $offer->offer_id;
	    		$noffer->offer = utf8_decode(trim(strip_tags($offer->title)));
	    		$noffer->title = utf8_decode(($offer->description));
	    		$noffer->network_epc = $offer->netepc;
	    		$noffer->payout = $offer->payout;
	    		$noffer->url = $this->formatstr($offer->offerlink).'&tracking_id=%subid%';
	    		$noffer->countries = $offer->accepted_countries;
				$noffer->network = $this->network;
				$noffer->created_at = date('Y-m-d H:i:s');
				$noffer->updated_at = date('Y-m-d H:i:s');
				$noffer->device = $this->findevice($offer->type);
				$noffer->save();
	    	}

			Cache::put('fetch_'.$this->network,'Processing '.$this->network.' Offers: '.$n.' of '.$total.' Parts',20);
	    	$n++;
	    	usleep(1000);

	        }
		}

        Cache::put('fetch_'.$this->network,'Processing Complete',20);

        $numOffers = \App\Offer::whereNetwork($this->network)->count();
        \App\Network::whereName($this->network)->update(['numOffers' => $numOffers, 'updated_at' => date('Y-m-d H:i:s')]);
	}

	public function getCPALeadOffers() {

		$networkdt = \App\Network::whereName($this->network)->first();

		$url = $networkdt->offer_url;
		$pubid = $networkdt->pubid;

		$url = str_replace('%pubid%',$pubid,$url);

		echo 'Starting to fetch '.$url."\n";

		$result = file_get_contents($url);
        $xml = simplexml_load_string($result, "SimpleXMLElement", LIBXML_NOCDATA);

        echo 'Fetch complete'."\n";

        $total = count($xml->channel->item);

        $n = 1;

        foreach ($xml->channel->item as $offer) {


    	// Get campaign info from name space
        $campaign = $offer->children("http://www.cpalead.com/feeds/campinfo.php");

    	if ($campaign->epc < $networkdt->minepc) { continue; }

    	$count = \App\Offer::whereCampid($campaign->campid)->whereNetwork($this->network)->count();


    	if ($count != 0) {
    		\App\Offer::whereCampid($campaign->campid)->whereNetwork($this->network)->update(['active' => 1,'updated_at' => date('Y-m-d H:i:s'),'network_epc' => $campaign->epc, 'payout' => $campaign->amount]);
    	} else {
    		$noffer = new \App\Offer;
    		$noffer->campid = $campaign->campid;
    		$noffer->offer = $this->formatstr($offer->title);
    		$noffer->title = $this->formatstr($offer->title);
    		$noffer->network_epc = $campaign->epc;
    		$noffer->payout = $campaign->amount;
    		$noffer->url = $this->formatstr($offer->link).'%subid%';
    		$noffer->countries = $campaign->country;
			$noffer->network = $this->network;
			$noffer->created_at = date('Y-m-d H:i:s');
			$noffer->updated_at = date('Y-m-d H:i:s');
			$noffer->device = $this->findevice($campaign->type);
			$noffer->save();
    	}

		Cache::put('fetch_'.$this->network,'Processing '.$this->network.' Offers: '.$n.' of '.$total,20);
    	$n++;
    	usleep(1000);

        }

        Cache::put('fetch_'.$this->network,'Processing Complete',20);

        $numOffers = \App\Offer::whereNetwork($this->network)->count();
        \App\Network::whereName($this->network)->update(['numOffers' => $numOffers, 'updated_at' => date('Y-m-d H:i:s')]);
	}

	public function getAdludumOffers() {
		$networkdt = \App\Network::whereName($this->network)->first();

        $url = $networkdt->offer_url;
        $pubid = $networkdt->pubid;
        $apikey = $networkdt->apikey;

        $url = str_replace('%pubid%',$pubid,$url);
        $url = str_replace('%apikey%',$apikey,$url);

        echo 'Starting to fetch '.$url."\n";

        $result = file_get_contents($url);
        $offers = json_decode($result,true);

        echo 'Fetch complete'."\n";

        $total = count($offers);

        $n = 1;

        foreach ($offers as $offer) {

        if ($offer['epc'] < $networkdt->minepc) { continue; }

        $count = \App\Offer::whereCampid($offer['id'])->whereNetwork($this->network)->count();

        if ($count != 0) {
            \App\Offer::whereCampid($offer['id'])->whereNetwork($this->network)->update(['active' => 1,'updated_at' => date('Y-m-d H:i:s'), 'network_epc' => $offer['epc'], 'payout' => $offer['payout']]);
        } else {
            $noffer = new \App\Offer;
            $noffer->campid = $offer['id'];
            $noffer->offer = $offer['name'];
            if(str_contains($offer['display_title'],'-')) { $stitle = $offer['description']; } else { $stitle = $offer['display_title']; }
            $noffer->title = html_entity_decode($stitle);
            $noffer->network_epc = $offer['epc'];
            $noffer->payout = $offer['payout'];
            $noffer->url = $this->formatstr($offer['url']).'%subid%';
            $noffer->countries = $offer['countries'];
            $noffer->network = $this->network;
            $noffer->created_at = date('Y-m-d H:i:s');
            $noffer->updated_at = date('Y-m-d H:i:s');
            $noffer->device = $this->findevice($offer['conversion_on']);
            $noffer->save();
        }

        Cache::put('fetch_'.$this->network,'Processing '.$this->network.' Offers: '.$n.' of '.$total,20);
        $n++;
        usleep(1000);

        } 

        Cache::put('fetch_'.$this->network,'Processing Complete',20);

        $numOffers = \App\Offer::whereNetwork($this->network)->count();
        \App\Network::whereName($this->network)->update(['numOffers' => $numOffers, 'updated_at' => date('Y-m-d H:i:s')]);
	}

	public function findevice($string) {
		switch ($string) {
			case "Only Mobile/WAP Traffic":
				return "mobile";
				break;
			case "All Devices (Mobile Friendly)":
				return "desktopmobile";
				break;
			case "iOS":
				return "ios";
				break;
			case "Desktop Only":
				return "desktop";
				break;
			case "Android":
				return "android";
				break;
			case "iOS - iPhones":
				return "ios";
				break;
			case "windows":
				return "desktop";
				break;
			case "ios":
				return "ios";
				break;
			case "iphone":
				return "ios";
				break;
			case "mobile":
				return "mobile";
				break;
			case "android":
				return "android";
				break;
			case "ipad":
				return "ios";
				break;
			case "mac os x":
				return "desktop";
				break;
			case "":
				return "desktop";
				break;
			case "0": //  All Platforms: All Browsers
				return "desktopmobile";
				break;
			case "10": // Windows
				return "desktop";
				break;
			case "20": // Mac OS
				return "desktop";
				break;
			case "30": //All mobile
				return "mobile";
				break;
			case "40": // Android
				return "android";
				break;
			case "50": //iOS
				return "ios";
				break;
			case "51": // iPhone
				return "ios";
				break;
			case "52": // iPad
				return "ios";
				break;
			case "mobile_droid":
				return "android";
				break;
			case "mobile_ios":
				return "ios";
				break;
			case "Install":
				return "mobile";
				break;
			default:
				return "desktop";
		}
	}

	public function formatstr($val) {
		  $var = html_entity_decode($val);
		  $var = str_replace("&#039;","'",$var);
		  $var = stripslashes($var);
							  
		  return $var;
	}

	public function isDesktop($val) {
		$device = $this->findevice($val);
		if (in_array($device,array('desktopmobile','desktop'))) { return true; } else { return false; }
	}

}
