<?php namespace App\Commands;

use App\Commands\Command;

use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Bus\SelfHandling;
use Illuminate\Contracts\Queue\ShouldBeQueued;

use DB;
use Cache;

class LeadManager extends Command implements SelfHandling, ShouldBeQueued {

	use InteractsWithQueue, SerializesModels;

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public $action,$statid,$campid,$payout,$gset;

	public function __construct($action,$statid,$campid = 0,$payout = 0)
	{
		$this->action = $action;
		$this->statid = $statid;
		$this->campid = $campid;
		$this->payout = $payout;
		$this->gset = \App\Settings::generalsettings();
	}

	/**
	 * Execute the command.
	 *
	 * @return void
	 */
	public function handle()
	{
		switch ($this->action) {
			case 'credit':
				$this->creditlead();
				break;
			case 'reverse':
				$this->reverselead();
				break;
			case 'test':
				$this->test();
				break;
		}
	}

	public function reverselead($statid,$campid,$payout,$sub) {

		// Reverse Statistics
		$stat = \App\Statistic::find($statid);
		$stat->status = $sub;
		$stat->save();

		// Subtract balance
		$user = \App\User::find($stat->user_id);
		$user->balance = $user->balance - $stat->payout;
		$user->save();

		// Delete admin earnings
		\App\AdminEarning::whereStatid($statid)->delete();

		// Delete referral earning
		\App\ReferLog::whereStatid($statid)->delete();

		// Recount offer stats
		$offerstats = \App\Statistic::select(\DB::raw('count(DISTINCT ip) as clicks, SUM(IF(status=1,1,0)) as leads, timestamp'))->whereCampid($stat->campid)->whereNetwork($stat->network)->first();

		\App\Offer::whereCampid($stat->campid)->whereNetwork($stat->network)->update(['clicks' => $offerstats->clicks, 'leads' => $offerstats->leads]);

		// Recount locker stats

		$lockerstats = \App\Statistic::select(\DB::raw('count(DISTINCT ip) as clicks, SUM(IF(status=1,payout,0)) as payout, SUM(IF(status=1,1,0)) as leads, timestamp'))->where('locker_id','=',$stat->locker_id)->first();

		if($stat->locker_type == 'file') {
			\App\File::find($stat->locker_id)->update(['clicks' => $lockerstats->clicks, 'leads' => $lockerstats->leads, 'earnings' => $lockerstats->payout]);
		} elseif($stat->locker_type == 'note') {
			\App\Note::find($stat->locker_id)->update(['clicks' => $lockerstats->clicks, 'leads' => $lockerstats->leads, 'earnings' => $lockerstats->payout]);
		} elseif($stat->locker_type == 'link') {
			\App\Link::find($stat->locker_id)->update(['clicks' => $lockerstats->clicks, 'leads' => $lockerstats->leads, 'earnings' => $lockerstats->payout]);
		} elseif($stat->locker_type == 'gateway') {
			\App\Gateway::find($stat->locker_id)->update(['clicks' => $lockerstats->clicks, 'leads' => $lockerstats->leads, 'earnings' => $lockerstats->payout]);
		}
	}

	public function test()
	{
		Cache::put('testqueue',1,20);
	}

}