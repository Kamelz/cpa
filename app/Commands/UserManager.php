<?php namespace App\Commands;

use App\Commands\Command;

use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Bus\SelfHandling;
use Illuminate\Contracts\Queue\ShouldBeQueued;

class UserManager extends Command implements SelfHandling, ShouldBeQueued {

	use InteractsWithQueue, SerializesModels;

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */

	public $userid, $action, $details;

	public function __construct($userid, $action, $details)
	{
		$this->userid = $userid;
		$this->action = $action;
		$this->details = $details;
	}

	/**
	 * Execute the command.
	 *
	 * @return void
	 */
	public function handle()
	{
		switch ($this->action) {
			case 'changechathandle':
				$this->changeChathandle();
				break;
			case 'delete':
				$this->deleteUser();
				break;
		}
	}

	public function changeChathandle()
	{
		foreach ($this->details as $dt) {
			$newname = $dt['newname'];
			$oldname = $dt['oldname'];
		}
		$userid = $this->userid;

		\App\User::whereId($userid)->whereChathandle($oldname)->update(['chathandle' => $newname]);
		\App\Settings::where('user_id','=',$userid)->whereUser($oldname)->update(['user' => $newname]);
		//\App\PaymentRequest::where('user_id','=',$userid)->whereUser($oldname)->update(['user' => $newname]);
		\App\TicketMessage::where('user_id','=',$userid)->whereUser($oldname)->update(['user' => $newname]);
		\App\DownloadLog::whereUser($oldname)->update(['user' => $newname]);
	}

}
