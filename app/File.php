<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class File extends Model {

	public $timestamps = false;

	protected $fillable = ['clicks','leads','earnings'];

	public function filereports()
    {
        return $this->hasOne('App\FileReport');
    }

    public function user()
    {
    	return $this->belongsTo('App\User');
    }

}
