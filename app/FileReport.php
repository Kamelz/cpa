<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class FileReport extends Model {

	public $timestamps = false;

	public function file()
    {
        return $this->belongsTo('App\File');
    }

}
