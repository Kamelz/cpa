<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Gateway extends Model {

	protected $fillable = ['clicks','leads','earnings'];

	public function user()
    {
    	return $this->belongsTo('App\User');
    }

}
