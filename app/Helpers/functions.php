<?php
/**
 * @param $number
 * @param int $decimals
 * @return string
 */
function formatnum($number, $decimals = 1)
{
    return number_format($number, $decimals);

}

/**
 * @param $dividend
 * @param $divisor
 * @return float|int
 */
function division($dividend, $divisor)
{
    // if($divisor == 0) throw new Exception("Error division by zero");
    if ($divisor == 0) return 0;
    return $dividend / $divisor;

}