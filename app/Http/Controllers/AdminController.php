<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

//use Illuminate\Http\Request;
use Cache;
use Queue;
use Validator;
use Input;
use Redirect;
use Auth;
use Session;
use Request;
use DB;
use View;
use App;
use Carbon\Carbon;

class AdminController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function __construct()
	{
		$this->middleware('domain');
		$this->middleware('auth');
		$this->middleware('admin');

		if (Auth::check()) {

		//$this->unreads = \App\Message::whereNotIn('id',unserialize(Auth::user()->read_msgs))->whereIn('receiver',array('Public',Auth::user()->chathandle))->get();
		$this->unreads = \App\Message::messages()->filter(function($message) {
			$reads = unserialize(Auth::user()->read_msgs);
			if(!in_array($message->id,$reads) && in_array($message->receiver,array('Public',Auth::user()->chathandle))) { return true; }
		});

		$gset = \App\Settings::generalsettings();

		if(Auth::user()->isAdmin()) { 
			$role = "admin"; 
		} else { $role = 'super_admin'; }

		$today = \App\Statistic::select(DB::raw('count(DISTINCT ip) as clicks, SUM(IF(status=1,payout,0)) as payout, SUM(IF(status=1,1,0)) as leads'))->whereTimestamp(Carbon::today()->toDateString())->first();

		View::share(['unreads' => $this->unreads,'gset' => $gset, 'today' => $today, 'role' => $role]);

		}
	}

	public function index()
	{
		// Move announcements to cache if not exist
		$announcements = Cache::rememberForever('announcements', function()
		{
		    return App\Announcement::orderBy('timestamp','desc')->get();
		});

		$pendingcashouts = App\PaymentRequest::whereStatus('Pending')->count();

		$newusers = App\User::where('register_date','>',new \DateTime('yesterday'))->count();

		$pendingtickets = App\Ticket::whereStatus(1)->count();

		$mtd = App\Statistic::select(DB::raw('count(DISTINCT ip) as clicks, SUM(IF(status=1,payout,0)) as payout, SUM(IF(status=1,1,0)) as leads, timestamp'))->whereBetween('timestamp',[Carbon::now()->startofMonth()->toDateString(),Carbon::now()->toDateString()])->groupBy('timestamp')->get();

		$lastmonth = App\Statistic::select(DB::raw('count(DISTINCT ip) as clicks, SUM(IF(status=1,payout,0)) as payout, SUM(IF(status=1,1,0)) as leads, timestamp'))->whereBetween('timestamp',[Carbon::today()->subMonth()->firstofMonth()->toDateString(),Carbon::today()->subMonth()->lastofMonth()->toDateString()])->first();

		// Cache reults before last month for performance
		$alltime = Cache::remember('admin_'.Auth::user()->id.'_alltime',1440, function() {
			 return App\Statistic::select(DB::raw('count(DISTINCT ip) as clicks, SUM(IF(status=1,payout,0)) as payout, SUM(IF(status=1,1,0)) as leads, timestamp'))->where('timestamp','<',Carbon::today()->subMonth()->firstofMonth()->toDateString())->first();
		});

		$latest = App\Statistic::whereStatus(1)->orderBy('timestamp','DESC')->take(6)->get();

		$mtd->payout = $mtd->sum('payout');
		$mtd->leads = $mtd->sum('leads');
		$mtd->clicks = $mtd->sum('clicks');

		$graph = $mtd->filter(function($mtd) { if ($mtd->timestamp > Carbon::now()->subDay(8)) {  return true; } });

		$yesterday = $mtd->filter(function($mtd) { if ($mtd->timestamp == Carbon::yesterday()->toDateString()) {  return true; } })->first();

		if (is_null($yesterday)) { $yesterday = (object) array('clicks' => 0, 'leads' => 0, 'payout' => 0.00); }

		$countries = App\Statistic::select(DB::raw('SUM(IF(status=1,payout,0)) as payout,country'))->whereTimestamp(Carbon::today()->toDateString())->groupBy('country')->get();


		$topoffers = Cache::remember('topoffers', 1440, function()
		{
		    return \App\Offer::whereActive(1)->orderBy('network_epc','DESC')->take(5)->get();
		});

		$recentoffers = Cache::remember('recentoffers',1440, function()
		{
			return \App\Offer::whereActive(1)->orderBy('created_at','DESC')->take(5)->get();
		});

		$expiredoffers = Cache::remember('expiredoffers',1440, function()
		{
			return \App\Offer::whereActive(0)->orderBy('updated_at','DESC')->take(5)->get();
		});

		$disabled = 0;

		return view('admin.aindex')->with(compact('announcements','pendingcashouts','newusers','pendingtickets','topoffers','recentoffers','expiredoffers','countries','graph','latest','yesterday','mtd','lastmonth','alltime','disabled'));
	}

	public function getEarnings() {
		$today = App\Statistic::select(DB::raw('count(id) as clicks, SUM(IF(status=1,payout,0)) as payout, SUM(IF(status=1,1,0)) as leads, timestamp'))->whereTimestamp(Carbon::today())->first();

		return '{"clicks": '.$today->clicks.', "leads": '.formatnum($today->leads).', "earnings": "$'.formatnum($today->payout,2).'", "epc": "$'.round2d(division($today->payout,$today->clicks)).'"}';
	}

	public function confirmdelete($scope,$id) {
		return view('admin.ajax.deleteconfirmation')->with(compact('scope','id'));
	}

	public function messages() {

		$users = \App\User::lists('chathandle');
		$recepient = Input::get('to','Public');
		$type = Input::get('type','general');

		$messages = \App\Message::all();

		$readmsgs = Auth::user()->read_msgs;
		$readmsgs = unserialize($readmsgs);

		$successmsg = Session::get('success');
		$infomsg = Session::get('info');
		return view('admin/amessages')->with(compact('users','recepient','type','successmsg','messages','readmsgs','infomsg'));
	}

	public function sendmessage() {

		$data = Input::all();

		$rules = array(
			'subject' => 'required',
			'message' => 'required|min:25'
		);

	    $validator = Validator::make($data, $rules);

	    if ($validator->passes()) {
    		$msg = new \App\Message;
    		$msg->user_id = Auth::user()->id;
    		$msg->title = Input::get('subject');
    		$msg->message = Input::get('message');
    		$msg->receiver = Input::get('recepient');
    		$msg->type = Input::get('type');
    		$msg->timestamp = date('Y-m-d H:i:s');
    		$msg->save();

    		Cache::forget('messages');

    		return Redirect::to('admin/messages')->with('success','Your message has been sent!');	
	    } else {
    		return Redirect::to('admin/messages')->withErrors($validator->errors());
	    }
	}

	public function getmessage($id)
	{	
		$readmsgs = Auth::user()->read_msgs;
		$readmsgs = unserialize($readmsgs);

		if (!in_array($id,$readmsgs)) {
		array_push($readmsgs,$id);
		\App\User::where('id','=',Auth::user()->id)->update(['read_msgs' => serialize($readmsgs)]);
		}

		$message = \App\Message::whereId($id)->first();

		return view('admin.ajax.viewmessage')->with(compact('message'));
	}

	public function deletemessage($id)
	{
		\App\Message::whereId($id)->delete();

		Cache::forget('messages');

		return Redirect::to('admin/messages')->with('info','Message has been deleted');
	}

	public function announcements()
	{
		$announcements = Cache::rememberForever('announcements', function()
		{
		    return \App\Announcement::orderBy('timestamp','desc')->get();
		});

		$successmsg = Session::get('success');
		$infomsg = Session::get('info');
		return view('admin.aannouncements')->with(compact('announcements','successmsg','infomsg'));
	}

	public function createannouncement() {
		$data = Input::all();

		$rules = array(
			'subject' => 'required',
			'message' => 'required|min:25'
		);

	    $validator = Validator::make($data, $rules);

	    if ($validator->passes()) {
    		$ann = new \App\Announcement;
    		$ann->user_id = Auth::user()->id;
    		$ann->title = Input::get('subject');
    		$ann->message = Input::get('message');
    		$ann->timestamp = date('Y-m-d H:i:s');
    		$ann->save();

    		Cache::forget('announcements');

    		return Redirect::to('admin/announcements')->with('success','Announcement has been created');	
	    } else {
    		return Redirect::to('admin/announcements')->withErrors($validator->errors());
	    }
	}

	public function getanmt($id) {
		$anmt = \App\Announcement::whereId($id)->first();
		return view('admin.ajax.viewanmt')->with(compact('anmt'));
	}

	public function deleteanmt($id) {

		\App\Announcement::whereId($id)->delete();

		Cache::forget('announcements');

		return Redirect::to('admin/announcements')->with('info','Announcement has been deleted');
	}

	public function payments() {
		$payments = \App\PaymentRequest::with('user')->orderBy('request_date','DESC')->paginate(15);
		return view('admin.apayments')->with(compact('payments'));
	}

	public function postpayments() {
		
		\App\PaymentRequest::whereId(Input::get('pk'))->update([Input::get('name') => Input::get('value')]);

		if (Input::get('value') == 'Complete' && Input::get('name') == 'status') {
			\App\PaymentRequest::whereId(Input::get('pk'))->update(['payment_date' => date('Y-m-d H:i:s')]);
		}

		if (Input::get('value') == 'Pending' && Input::get('name') == 'status') {
			\App\PaymentRequest::whereId(Input::get('pk'))->update(['payment_date' => NULL]);
		}

		return 'success';
	}

	public function viewpayment($id) {
		$payment = \App\PaymentRequest::whereId($id)->first();

		if ($payment->payment_method != 'Wire Transfer') { $detail = $payment->payment_detail; } else {
			$detail = unserialize($payment->payment_details);
			//$data = unserialize($payment->payment_details); $detail = 'Name on Account:'.$data['Name on Account']."\r\n".'<br /><br>Bank Name:'.$data['Bank Name'].'<br><br>Bank Address:'.$data['Bank Address'].'<br><br>Bank Country:'.$data['Bank Country'].'<br><br>Account Type:'.$data['Account Type'].'<br><br>Account / IBAN #:'.$data['Account / IBAN #'].'<br><br>Routing / ABA #:'.$data['Routing / ABA #'].'<br><br>SWIFT Code'.$data['SWIFT Code'];
		}

		return view('admin.ajax.viewpayment')->with(compact('payment','detail'));
	}

	public function deletepayment($id) {

		$pr = \App\PaymentRequest::whereId($id)->first();
		$amount = $pr->amount + $pr->fee;
		$user = \App\User::find($pr->user_id);
		$user->balance = $user->balance + $amount;
		$user->save();
		$pr->delete();
		return Redirect::to('admin/payments');
	}

	public function publishers() {

		if (Input::has('status')) {
			$users = \App\User::whereRole('publisher')->orderBy('register_date','DESC')->whereStatus(Input::get('status'))->with('files')->paginate(15);
		} elseif (Input::has('username')) {
			$users = \App\User::whereRole('publisher')->orderBy('register_date','DESC')->whereChathandle(Input::get('username'))->with('files')->paginate(15);
		} else {
			$users = \App\User::whereRole('publisher')->orderBy('register_date','DESC')->whereIn('status',array('active','pending'))->with('files')->paginate(15);
		}

		$users->setPath('publishers');

		$successmsg = Session::get('success');
		return view('admin.apublishers')->with(compact('users','successmsg'));
	}

	public function viewpublisher($id) {
		$user = \App\User::whereId($id)->whereRole('publisher')->with('user_profile')->first();
		$countries = \App\Settings::countries();

		if (\App\Settings::isChatDisabled($id)) { $disabled = 1; } else { $disabled = 0; }

		$customrate = \App\Settings::getCustomPayout($id);

		return view('admin.ajax.viewpublisher')->with(compact('user','countries','disabled','customrate'));
	}

	public function editpublisher() {
		if(Input::get('name') == 'disablechat') {
			\App\Settings::changeChatDisabled(Input::get('pk'),Input::get('value'));
			return;
		} elseif(Input::get('name') == 'balance') {
			if(!preg_match('/^[0-9]+(\.[0-9]{0,2})?$/',Input::get('value'))) {
				return response('Invalid Amount',500);
			}
			\App\User::whereId(Input::get('pk'))->update(['balance' => Input::get('value')]);
			return;
		} elseif(Input::get('name') == 'customrate') {
			if(Input::get('value') < 1 || Input::get('value') > 100) {
				return response('Invalid Number',500);
			}
			\App\Settings::setCustomPayout(Input::get('pk'),Input::get('value'));
			return;
		} elseif(Input::get('name') == 'chathandle') {

			$user = \App\User::whereChathandle(Input::get('value'))->first();
			if(count($user) > 0) { return response('Name already taken',500); }

			$muser = \App\User::find(Input::get('pk'));

			//$details = array(['oldname' => $userid->chathandle, 'newname' => Input::get('value')]);
			//Queue::push(new \App\Commands\UserManager($userid->id,'changechathandle',$details));

			$oldname = $muser->chathandle;
			$newname = Input::get('value');
			$userid = $muser->id;

			\App\User::whereId($userid)->whereChathandle($oldname)->update(['chathandle' => $newname]);
			\App\Settings::where('user_id','=',$userid)->whereUser($oldname)->update(['user' => $newname]);
			\App\PaymentRequest::where('user_id','=',$userid)->whereUser($oldname)->update(['user' => $newname]);
			\App\TicketMessage::where('user_id','=',$userid)->whereUser($oldname)->update(['user' => $newname]);
			\App\DownloadLog::whereUser($oldname)->update(['user' => $newname]);

			return;
		}

		if(in_array(Input::get('name'),array('payment_cycle','address','city','state','zip','country','skype'))) {
			\App\User_Profile::where('user_id','=',Input::get('pk'))->update([Input::get('name') => Input::get('value')]);
		} else {
			\App\User::whereId(Input::get('pk'))->update([Input::get('name') => Input::get('value')]);
		}
	}

	public function accessaccount($id) {
		$user = \App\User::whereId($id)->first();
		if ($user->role == 'publisher') {
			Auth::loginUsingId($id);
			$role = \Crypt::encrypt('publisher');
			Session::put('pmd',$role);
			return Redirect::to('dashboard');
		} else {
			$title = "403";
			$message = "Oh snap, you couldn't access admin accounts";
		    return view('block')->with(compact('title','message'));
		}
	}

	public function deletepublisher($id) {
		$user = \App\User::find($id);

		if($user->role == 'admin' || $user->role == 'super_admin') {
			return Redirect::to('admin/publishers');
		}

		$user->delete();
		\App\User_Profile::where('user_id','=',$id)->delete();

		return Redirect::to('admin/publishers')->with('success','User has been deleted');
	}

	public function changepublisherstatus() {
		\App\User::find(Input::get('pk'))->update(['status' => Input::get('value')]);
		return 'status changed';
	}

	public function downloadlog() {
		$dls = \App\DownloadLog::orderBy('timestamp','DESC')->paginate(30);

		return view('admin.adownloadlog')->with(compact('dls'));
	}

	public function leadslog() {

		$status = Input::get('filter','');
		$userid = Input::get('user','');

		$from = Input::get('fromDate', Carbon::today()->subDays(7)->toDateString());
		$to = Input::get('toDate', Carbon::today());

		$networks = \App\Network::getNetworks();

		$leads = \App\Statistic::ofUser($userid)->ofStatus($status)->whereBetween('timestamp',[$from,$to])->orderby('timestamp','DESC')->with('user')->paginate(30);
		$leads->appends(Input::except('page'));

		$leads->setPath('leadslog');

		return view('admin.aleadslog')->with(compact('leads','networks','from','to'));
	}

	public function toggleleadstatus() {

		$stat = \App\Statistic::find(Input::get('pk'));

		$pubrate = \App\Settings::getCustomPayout($stat->user_id);

		$payout = ($stat->payout * 100) / $pubrate;

		if (Input::get('value') == 2) {
			$this->reverselead($stat->id,23,$stat->payout,2);
		} elseif(Input::get('value') == 1) {
			return response('Reversed lead cannot be credited back',500);
		} else {
			return response('Reversed lead cannot be credited back',500);
		}
	}

	public function sitereferrer() {

		$current = Input::get('page',0);

		$refs = \App\Statistic::whereStatus(1)->select('site_referrer_id',DB::raw('count(status) as leads'))->groupBy('site_referrer_id')->skip($current * 30)->take(30)->get();

		$urls = \App\SiteReferrer::whereIn('id',$refs->lists('site_referrer_id'))->get();

		return view('admin.asitereferrers')->with(compact('refs','urls','current'));
	}

	public function overview()
	{
		$from = Input::get('fromDate',Carbon::today()->subDays(7)->toDateString());
		$to = Input::get('toDate',Carbon::today());

		$users = \App\Statistic::select(DB::raw('count(DISTINCT ip) as clicks, SUM(IF(status=1,1,0)) as leads, SUM(IF(status=1,payout,0)) as payout'))->whereBetween('timestamp',[$from,$to])->first();

		$days = \App\Statistic::select(DB::raw('count(DISTINCT ip) as clicks, SUM(IF(status=1,1,0)) as leads, SUM(IF(status=1,payout,0)) as payout, timestamp'))->whereBetween('timestamp',[$from,$to])->groupBy('timestamp')->get();

		$adminearns = \App\AdminEarning::whereBetween('timestamp',[$from,$to])->get();

		$adminchart = \App\AdminEarning::select(DB::raw('SUM(payout) as payout, timestamp'))->whereBetween('timestamp',[$from,$to])->groupBy('timestamp')->get();

		return view('admin.aoverview')->with(compact('users','days','from','to','adminearns','adminchart'));
	}

	public function files() {

		if (Input::has('user')) {
			$user = \App\User::whereChathandle(Input::get('user'))->first();
			if (count($user) == 0) {
				$files = \App\File::whereArchive(0)->orderBy('created_at')->with('user')->paginate(25);
				return view('admin.afiles')->with(compact('files'))->withErrors("The user doesn't exist");
			} else {
				$files = \App\File::where('user_id','=',$user->id)->whereArchive(0)->orderBy('created_at')->with('user')->paginate(25);
			}
		} elseif (Input::has('filter') && Input::get('filter') == 'active') {
			$files = \App\File::where('leads','>',0)->whereArchive(0)->orderBy('created_at')->with('user')->paginate(25);
		} elseif (Input::has('filter') && Input::get('filter') == 'inactive') {
			$files = \App\File::whereLeads(0)->whereArchive(0)->orderBy('created_at')->with('user')->paginate(25);
		} else {
			$files = \App\File::whereArchive(0)->orderBy('created_at')->with('user')->paginate(25);
		}

		$files->appends(Input::except('page'));
		$files->setPath('files');
		return view('admin.afiles')->with(compact('files'));
	}

	public function downloadfile($code) {

		$file = \App\File::whereCode($code)->first();

		// Copy file to temp
		$contents = \Storage::disk('files')->get($file->hashname);
		\Storage::disk('tmp')->put($file->hashname,$contents);

		$filedl = storage_path()."/tmp/".$file->hashname;

		// Send the file and delete from temp
		return response()->download($filedl,$file->filename.".".$file->ext)->deleteFileAfterSend(true);
	}

	public function deletefile($code) {

		$file = \App\File::whereCode($code)->first();

		// Preserve record in db for statistics
		\App\File::whereCode($code)->where('user_id','=',Auth::user()->id)->update(['archive' => 1]);
		// Delete file to save space
		\Storage::disk('files')->delete($file->hashname);

		return Redirect::to('admin/files')->with('success','File deleted');
	}

	public function gateways() {

		if (Input::has('user')) {
			$user = \App\User::whereChathandle(Input::get('user'))->first();
			if (count($user) == 0) {
				$gateways = \App\Gateway::whereArchive(0)->orderBy('created_at')->with('user')->paginate(25);	
				return view('admin.agateways')->with(compact('gateways'))->withErrors("The user doesn't exist");
			} else {
				$gateways = \App\Gateway::where('user_id','=',$user->id)->whereArchive(0)->orderBy('created_at')->with('user')->paginate(25);
			}
		} elseif (Input::has('filter') && Input::get('filter') == 'active') {
			$gateways = \App\Gateway::where('leads','>',0)->whereArchive(0)->orderBy('created_at')->with('user')->paginate(25);
		} elseif (Input::has('filter') && Input::get('filter') == 'inactive') {
			$gateways = \App\Gateway::whereLeads(0)->whereArchive(0)->orderBy('created_at')->with('user')->paginate(25);
		} else {
			$gateways = \App\Gateway::whereArchive(0)->orderBy('created_at')->with('user')->paginate(25);
		}

		$gateways->appends(Input::except('page'));
		$gateways->setPath('gateways');
		return view('admin.agateways')->with(compact('gateways'));
	}

	public function deletegateway($id) {

		\App\Gateway::whereId($id)->update(['archive' => 1]);

		return Redirect::to('admin/gateways');
	}

	public function links() {

		if (Input::has('user')) {
			$user = \App\User::whereChathandle(Input::get('user'))->first();
			if (count($user) == 0) {
				$links = \App\Link::whereArchive(0)->orderBy('created_at')->with('user')->paginate(25);	
				return view('admin.alinks')->with(compact('links'))->withErrors("The user doesn't exist");
			} else {
				$links = \App\Link::where('user_id','=',$user->id)->whereArchive(0)->orderBy('created_at')->with('user')->paginate(25);	
			}
		} elseif (Input::has('filter') && Input::get('filter') == 'active') {
			$links = \App\Link::where('leads','>',0)->whereArchive(0)->orderBy('created_at')->with('user')->paginate(25);
		} elseif (Input::has('filter') && Input::get('filter') == 'inactive') {
			$links = \App\Link::whereLeads(0)->whereArchive(0)->orderBy('created_at')->with('user')->paginate(25);
		} else {
			$links = \App\Link::whereArchive(0)->orderBy('created_at')->with('user')->paginate(25);
		}

		$links->appends(Input::except('page'));
		$links->setPath('links');
		return view('admin.alinks')->with(compact('links'));
	}

	public function deletelink($id) {
		\App\Link::whereId($id)->update(['archive' => 1]);
		return Redirect::to('admin/links');
	}

	public function notes() {

		if (Input::has('user')) {
			$user = \App\User::whereChathandle(Input::get('user'))->first();
			if (count($user) == 0) {
				$notes = \App\Note::whereArchive(0)->orderBy('created_at')->with('user')->paginate(25);
				return view('admin.anotes')->with(compact('notes'))->withErrors("The user doesn't exist");
			} else {
				$notes = \App\Note::where('user_id','=',$user->id)->whereArchive(0)->orderBy('created_at')->with('user')->paginate(25);
			}
		} elseif (Input::has('filter') && Input::get('filter') == 'active') {
			$notes = \App\Note::where('leads','>',0)->whereArchive(0)->orderBy('created_at')->with('user')->paginate(25);
		} elseif (Input::has('filter') && Input::get('filter') == 'inactive') {
			$notes = \App\Note::whereLeads(0)->whereArchive(0)->orderBy('created_at')->with('user')->paginate(25);
		} else {
			$notes = \App\Note::whereArchive(0)->orderBy('created_at')->with('user')->paginate(25);
		}

		$notes->setPath('notes');
		return view('admin.anotes')->with(compact('notes'));
	}

	public function viewnote($id) {
		$note = \App\Note::whereId($id)->first();

		return view('admin.ajax.viewnote')->with(compact('note'));
	}

	public function deletenote($id) {
		\App\Note::whereId($id)->update(['archive' => 1]);
		return Redirect::to('admin/notes');
	}

	public function contentreports() {
		$reports = \App\FileReport::orderBy('timestamp','DESC')->paginate(15);

		return view('admin.acontentreports')->with(compact('reports'));
	}

	public function viewcontentreport($id) {
		$report = \App\FileReport::whereId($id)->first();
		return view('admin.ajax.viewcontentreport')->with(compact('report'));
	}

	public function mirrors() {
		$mirrors = \App\Mirror::all();
		$successmsg = Session::get('success');
		return view('admin.amirrors')->with(compact('mirrors','successmsg'));
	}

	public function togglemirrorstatus() {
		\App\Mirror::whereId(Input::get('pk'))->update(['status' => Input::get('value')]);
		return 'success';
	}

	public function deletemirror($id) {
		\App\Mirror::whereId($id)->delete();
		return Redirect::to('admin/mirrors');
	}

	public function addmirror() {
		$data = Input::all();
		$rules = array('domain' => 'required|unique:mirrors');
		$validator = Validator::make($data,$rules);

		if ($validator->passes()) {
			$mirror = new \App\Mirror;
			$mirror->domain = Input::get('domain');
			$mirror->status = 1;
			$mirror->save();

			$msg[] = array('title' => 'Success', 'message' => 'Added mirror domain'); return $msg;
		}

		$messages = $validator->messages();

	    foreach($messages->all() as $message) {
	    	$msg[] = array('title' => 'xhr', 'message' => str_replace('_',' ',$message));
	    }
	    return $msg;
	}

	public function promotions() {

		$promotions = \App\Promotion::all();
		$successmsg = Session::get('success');
		$globalpromo = Cache::get('global_promotion',array('promo_id' => 0, 'promo_percent' => 0, 'activated' => Carbon::now()->toDateTimeString()));
		$timeago = Carbon::createFromFormat('Y-m-d H:i:s', $globalpromo['activated'])->diffForHumans();
		return view('admin.apromotions')->with(compact('promotions','successmsg','globalpromo','timeago'));
	}

	public function addpromotion() {
		$data = Input::all();
		$rules = array(
			'name' => 'required',
			'bonus' => 'required|numeric',
			'leads' => 'required|numeric',
			'hours' => 'required|numeric'
			);
		$validator = Validator::make($data,$rules);
		if($validator->passes()) {
			$promo = new \App\Promotion;
			$promo->name = Input::get('name');
			$promo->bonus_percentage = Input::get('bonus');
			$promo->leads = Input::get('leads');
			$promo->hours = Input::get('hours');
			$promo->timestamp = date('Y-m-d H:i:s');
			$promo->save();

			$msg[] = array('title' => 'Success', 'message' => 'Promotion created'); return $msg;
		}

		$messages = $validator->messages();

	    foreach($messages->all() as $message) {
	    	$msg[] = array('title' => 'xhr', 'message' => str_replace('_',' ',$message));
	    }
	    return $msg;
	}

	public function togglepromo() {
		\App\Promotion::whereId(Input::get('pk'))->update(['active' => Input::get('value')]);
		return 'success';
	}

	public function deletepromo($id) {
		\App\Promotion::whereId($id)->delete();
		Cache::forget('global_promotion');
		return Redirect::to('admin/promotions');
	}

	public function activatepromoconfirm($id) {
		$promo = \App\Promotion::whereId($id)->first();
		return view('admin.ajax.promoactivateconfirm')->with(compact('id','promo'));
	}

	public function activatepromo($id) {
		$promo = \App\Promotion::whereId($id)->first();
		$minutes = $promo->hours * 60;
		$data = array('promo_id' => $promo->id, 'promo_percent' => $promo->bonus_percentage, 'activated' => Carbon::now()->toDateTimeString());
		Cache::put('global_promotion',$data,$minutes);
		return Redirect::to('admin/promotions');
	}

	public function tickets() {

		$tickets = \App\Ticket::orderBy('timestamp','DESC')->with('user','ticketmessage')->paginate(25);

		return view('admin.atickets')->with(compact('tickets'));
	}

	public function viewticket($id) {
		$tmsgs = \App\TicketMessage::where('ticket_id','=',$id)->get();

		return view('admin.ajax.viewticket')->with(compact('tmsgs','id'));
	}

	public function addticketmsg($id) {

		if (Input::get('message') == "") { $msg[] = array('title' => 'Whoops!', 'message' => 'Message is empty'); return $msg; }
		$tmsg = new \App\TicketMessage;
		$tmsg->ticket_id = $id;
		$tmsg->user_id = Auth::user()->id;
		$tmsg->user = 'AdGodMedia';
		$tmsg->message = Input::get('message');
		$tmsg->timestamp = date('Y-m-d H:i:s');
		$tmsg->save();

		$msg[] = array('title' => 'Success', 'message' => 'Message Added!'); return $msg;
	}

	public function toggleticket() {
		\App\Ticket::whereId(Input::get('pk'))->update(['status' => Input::get('value')]);
		return 'success';
	}

	public function reverselead($statid,$campid,$payout,$sub) {

		// Reverse Statistics
		$stat = \App\Statistic::find($statid);
		$stat->status = $sub;
		$stat->save();

		// Subtract balance
		$user = \App\User::find($stat->user_id);
		$user->balance = $user->balance - $stat->payout;
		$user->save();

		// Delete admin earnings
		\App\AdminEarning::whereStatid($statid)->delete();

		// Delete referral earning
		\App\ReferLog::whereStatid($statid)->delete();

		// Recount offer stats
		$offerstats = \App\Statistic::select(\DB::raw('count(DISTINCT ip) as clicks, SUM(IF(status=1,1,0)) as leads, timestamp'))->whereCampid($stat->campid)->whereNetwork($stat->network)->first();

		\App\Offer::whereCampid($stat->campid)->whereNetwork($stat->network)->update(['clicks' => $offerstats->clicks, 'leads' => $offerstats->leads]);

		// Recount locker stats

		$lockerstats = \App\Statistic::select(\DB::raw('count(DISTINCT ip) as clicks, SUM(IF(status=1,payout,0)) as payout, SUM(IF(status=1,1,0)) as leads, timestamp'))->where('locker_id','=',$stat->locker_id)->first();

		if($stat->locker_type == 'file') {
			\App\File::find($stat->locker_id)->update(['clicks' => $lockerstats->clicks, 'leads' => $lockerstats->leads, 'earnings' => $lockerstats->payout]);
		} elseif($stat->locker_type == 'note') {
			\App\Note::find($stat->locker_id)->update(['clicks' => $lockerstats->clicks, 'leads' => $lockerstats->leads, 'earnings' => $lockerstats->payout]);
		} elseif($stat->locker_type == 'link') {
			\App\Link::find($stat->locker_id)->update(['clicks' => $lockerstats->clicks, 'leads' => $lockerstats->leads, 'earnings' => $lockerstats->payout]);
		} elseif($stat->locker_type == 'gateway') {
			\App\Gateway::find($stat->locker_id)->update(['clicks' => $lockerstats->clicks, 'leads' => $lockerstats->leads, 'earnings' => $lockerstats->payout]);
		}
	}

	/*
	public function misctask() {
		$stats = \App\Statistic::select(SUM(IF(payout,1,0), COUNT(status))->get();
	}
	*/


	public function confirmbulkreverse() {

		$data = Input::all();

		$rules = array(
			'userid' => 'required',
			'campid' => 'required',
			'network' => 'required',
			'no' => 'required'
			);

		$validator = Validator::make($data,$rules);

		if($validator->passes()) {

			$stats = \App\Statistic::whereNetwork(Input::get('network'))->whereCampid(Input::get('campid'))->whereUserId(Input::get('userid'))->whereStatus(1)->orderBy('timestamp','DESC')->take(Input::get('no'))->get();

			if(count($stats) == 0) {

				$msg[] = array('title' => 'Whoops!', 'message' => 'There are 0 leads matching this criteria'); return $msg;

			} else {

				foreach ($stats as $stat) {
					$this->reverselead($stat->id,$stat->campid,$stat->payout,2);
				}

				$msg[] = array('title' => 'Success', 'message' => count($stats).' lead(s) have been reversed'); return $msg;

			}

		}

		$messages = $validator->messages();

	    foreach($messages->all() as $message) {
	    	$msg[] = array('title' => 'xhr', 'message' => str_replace('_',' ',$message));
	    }
	    return $msg;
	}

	public function offers() {

		$disabled = \App\Settings::disabledoffers();
		$favorite = \App\Settings::favoriteoffers();

		$countries = \App\Settings::countries();
		$networks = \App\Network::lists('name');

		$snetwork = Input::get('network','');
		$scountry = Input::get('country','');

		$offers = \App\Offer::whereActive(1)->ofNetwork($snetwork)->ofCountry($scountry)->ofOrder(Input::get('sort',''))->paginate(25);

		$offers->appends(Input::except('page'));
		$offers->setPath('offers');

		$sort = Input::get('sort','');
		return view('admin.aoffers')->with(compact('offers','disabled','favorite','countries','networks','snetwork','scountry','sort'));
	}

	public function toggleoffer() {
		if(Input::get('status') == 0) {
			\App\Settings::firstOrCreate(['user_id' => 20, 'scope' => 'offers', 'option' => 'disabled', 'value' => Input::get('id')]);
		} else {
			\App\Settings::whereUserId(20)->whereScope('offers')->whereOption('disabled')->whereValue(Input::get('id'))->delete();
		}
		Cache::forget('disabled_offers');
	}

	public function favoriteoffer() {
		if(Input::get('status') == 1) {
			\App\Settings::firstOrCreate(['user_id' => 20, 'scope' => 'offers', 'option' => 'favorite', 'value' => Input::get('id')]);
		} else {
			\App\Settings::whereUserId(20)->whereScope('offers')->whereOption('favorite')->whereValue(Input::get('id'))->delete();
		}
		Cache::forget('favorite_offers');
	}

	public function resetoptimization() {
		\App\Settings::whereScope('offers')->whereOption('disabled')->whereUserId(20)->delete();
		\App\Settings::whereScope('offers')->whereOption('favorite')->whereUserId(20)->delete();

		Cache::forget('disabled_offers');
		Cache::forget('favorite_offers');

		return Redirect::to('admin/offers')->with('success','All optimizations have been reset');
	}

	public function expiredoffers() {
		$countries = \App\Settings::countries();
		$networks = \App\Network::lists('name');

		$snetwork = Input::get('network','');
		$scountry = Input::get('country','');

		$offers = \App\Offer::whereActive(0)->ofNetwork($snetwork)->ofCountry($scountry)->orderBy("updated_at","DESC")->paginate(25);

		$offers->appends(Input::except('page'));
		$offers->setPath('offers');
		return view('admin.aexpiredoffers')->with(compact('offers','countries','networks','snetwork','scountry'));
	}

 }