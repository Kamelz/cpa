<?php namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Contracts\Auth\Registrar;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;

use Input,App\User,Redirect,Auth,Cache;
use PragmaRX\Google2FA\Contracts\Google2FA;

class AuthController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Registration & Login Controller
	|--------------------------------------------------------------------------
	|
	| This controller handles the registration of new users, as well as the
	| authentication of existing users. By default, this controller uses
	| a simple trait to add these behaviors. Why don't you explore it?
	|
	*/

	use AuthenticatesAndRegistersUsers;

	protected $redirectTo = 'dashboard';

	/**
	 * Create a new authentication controller instance.
	 *
	 * @param  \Illuminate\Contracts\Auth\Guard  $auth
	 * @param  \Illuminate\Contracts\Auth\Registrar  $registrar
	 * @return void
	 */
	public function __construct(Guard $auth, Registrar $registrar)
	{
		$this->auth = $auth;
		$this->registrar = $registrar;

		$this->middleware('guest', ['except' => 'getLogout']);
	}


	// Verify two factor token for admin users
	public function verifytoken(Google2FA $google2fa) 
	{
		$userid = Input::get('_user');
		$secret = Input::get('secret');


		$user = User::find($userid);
		if($google2fa->verifyKey($user->google2fa_secret, $secret)) {

			Auth::login($user);

			$log = new \App\LoginLog;
			$log->status = 1;
			$log->role = $user->role;
			$log->user = $user->email;
			$log->timestamp = date('Y-m-d H:i:s');
			$log->save();

			return Redirect::to('admin');
		}
		else {
			return Redirect::back()->withErrors(['Authorization code is invalid']);
		}
	}

	// Show QR Code and instruction to setup for new admin
	public function welcomeadmin($id)
	{
		Auth::logout();
		$url = Cache::get($id);
		return view('auth.welcomeadmin')->with('url',$url);
	}

}
