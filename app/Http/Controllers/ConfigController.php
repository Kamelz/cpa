<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

use App\User,Cache;
use PragmaRX\Google2FA\Contracts\Google2FA;
use Settings;
use App;
use Crypt;

class ConfigController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		return Crypt::encrypt('publisher');
	}

	public function stats() {
			//return Stats::find(1)->with('user')->get();
		//$msg = array('id' => 23, 'reason' => array('ban', 'spam', 'cheat'));
		//Cache::store('redis')->forever('msg', $msg);
		$get = Cache::store('redis')->get('msg');
		foreach ($get['reason'] as $reason) { echo $reason; }
		return $get['reason'];
	}

	public function gfa(Google2FA $google2fa) {


		/* Save secret code to database */
		//$user = User::find(27);
		//$user->google2fa_secret = $google2fa->generateSecretKey(32);
		//$user->save();

		/* Put QR image to cache for 2 hours */
		$user = User::find(27);
		$google2fa_url = $google2fa->getQRCodeGoogleUrl('AdGodMedia',$user->email,$user->google2fa_secret);
		Cache::put('6471',$google2fa_url,120);
	}

}
