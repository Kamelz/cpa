<?php namespace App\Http\Controllers;

use Cache;
use App;
use Crypt;
use Session;
use Input;
use Validator;
use Redirect;
use Auth;
use DB;
use Request;
use Carbon\Carbon;
use Redis;
use View;

class DashboardController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Home Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders your application's "dashboard" for users that
	| are authenticated. Of course, you are free to change or remove the
	| controller as you wish. It is just here to get your app started!
	|
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */

	private $minutes = 15;

	public function __construct()
	{
		$this->middleware('domain');
		$this->middleware('auth');
		$this->middleware('banuser');

		if (Auth::check()) {
		//$this->unreads = \App\Message::whereNotIn('id',unserialize(Auth::user()->read_msgs))->whereIn('receiver',array('Public',Auth::user()->chathandle))->get();
		$this->unreads = \App\Message::messages()->filter(function($message) {
			$reads = unserialize(Auth::user()->read_msgs);
			if(!in_array($message->id,$reads) && in_array($message->receiver,array('Public',Auth::user()->chathandle))) { return true; }
		});
		$gset = \App\Settings::generalsettings();
		$this->gset = $gset;

		$today = App\Statistic::where('user_id','=',Auth::user()->id)->select(DB::raw('count(DISTINCT ip) as clicks, SUM(IF(status=1,payout,0)) as payout, SUM(IF(status=1,1,0)) as leads'))->whereTimestamp(Carbon::today()->toDateString())->first();

		View::share(['unreads' => $this->unreads, 'gset' => $gset, 'today' => $today]);
		}
	}

	public function confirmdelete($scope,$id) {
		return view('dashboard.ajax.deleteconfirmation')->with(compact('scope','id'));
	}

	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */
	public function index()
	{

		// Move announcements to cache if not exist
		$announcements = Cache::rememberForever('announcements', function()
		{
		    return \App\Announcement::orderBy('timestamp','desc')->get();
		});

		$mtd = App\Statistic::where('user_id','=',Auth::user()->id)->select(DB::raw('count(DISTINCT ip) as clicks, SUM(IF(status=1,payout,0)) as payout, SUM(IF(status=1,1,0)) as leads, timestamp'))->whereBetween('timestamp',[Carbon::now()->startofMonth()->toDateString(),Carbon::now()->toDateString()])->groupBy('timestamp')->get();

		$lastmonth = App\Statistic::where('user_id','=',Auth::user()->id)->select(DB::raw('count(DISTINCT ip) as clicks, SUM(IF(status=1,payout,0)) as payout, SUM(IF(status=1,1,0)) as leads, timestamp'))->whereBetween('timestamp',[Carbon::today()->subMonth()->firstofMonth()->toDateString(),Carbon::today()->subMonth()->lastofMonth()->toDateString()])->first();

		// Cache reults before last month for performance
		$alltime = Cache::remember('publisher_'.Auth::user()->id.'_alltime',1440, function() {
			 return App\Statistic::where('user_id','=',Auth::user()->id)->select(DB::raw('count(DISTINCT ip) as clicks, SUM(IF(status=1,payout,0)) as payout, SUM(IF(status=1,1,0)) as leads, timestamp'))->where('timestamp','<',Carbon::today()->subMonth()->firstofMonth()->toDateString())->first();
		});

		$latest = App\Statistic::where('user_id','=',Auth::user()->id)->whereStatus(1)->orderBy('timestamp','DESC')->take(6)->get();

		$mtd->payout = $mtd->sum('payout');
		$mtd->leads = $mtd->sum('leads');
		$mtd->clicks = $mtd->sum('clicks');

		$graph = $mtd->filter(function($mtd) { if ($mtd->timestamp > Carbon::now()->subDay(8)) {  return true; } });

		$yesterday = $mtd->filter(function($mtd) { if ($mtd->timestamp == Carbon::yesterday()->toDateString()) {  return true; } })->first();

		if (is_null($yesterday)) { $yesterday = (object) array('clicks' => 0, 'leads' => 0, 'payout' => 0.00); }

		$countries = App\Statistic::where('user_id','=',Auth::user()->id)->select(DB::raw('SUM(IF(status=1,payout,0)) as payout,country'))->whereTimestamp(Carbon::today()->toDateString())->groupBy('country')->get();

		//dd($today);

		$topoffers = Cache::remember('topoffers', 1440, function()
		{
		    return \App\Offer::whereActive(1)->orderBy('network_epc','DESC')->take(5)->get();
		});

		$recentoffers = Cache::remember('recentoffers',1440, function()
		{
			return \App\Offer::whereActive(1)->orderBy('created_at','DESC')->take(5)->get();
		});

		$expiredoffers = Cache::remember('expiredoffers',1440, function()
		{
			return \App\Offer::whereActive(0)->orderBy('updated_at','DESC')->take(5)->get();
		});

		$refcount = \App\ReferLog::where('user_id','=',Auth::user()->id)->groupBy('referral_id')->count();

		if(\App\Settings::isChatDisabled(Auth::user()->id)) { $disabled = 1; } else { $disabled = 0; }

		return view('dashboard.index')->with(compact('mtd','graph','yesterday','lastmonth','alltime','announcements','countries','topoffers','recentoffers','expiredoffers','latest','refcount','disabled'));
	}

	public function referralsidebar() {
		$today = \App\ReferLog::where('user_id','=',Auth::user()->id)->whereTimestamp(Carbon::today())->sum('payout');
		$mtd = \App\ReferLog::where('user_id','=',Auth::user()->id)->whereBetween('timestamp',[Carbon::now()->startofMonth()->toDateString(),Carbon::today()])->sum('payout');
		$alltime = \App\ReferLog::where('user_id','=',Auth::user()->id)->sum('payout');

		$referrals = \App\ReferLog::where('user_id','=',Auth::user()->id)->select(DB::raw('SUM(payout) as pay, referral_id'))->groupBy('referral_id')->get();

		$me = \Hashids::encode(Auth::user()->id);

		return view('dashboard.referralsidebar')->with(compact('referrals','today','mtd','alltime','me'));
	}

	public function getEarnings() {
		$today = App\Statistic::where('user_id','=',Auth::user()->id)->select(DB::raw('count(DISTINCT ip) as clicks, SUM(IF(status=1,payout,0)) as payout, SUM(IF(status=1,1,0)) as leads, timestamp'))->whereTimestamp(Carbon::today())->first();

		return '{"clicks": '.$today->clicks.', "leads": '.formatnum($today->leads).', "earnings": "$'.formatnum($today->payout,2).'", "epc": "$'.formatnum(division($today->payout,$today->clicks),2).'"}';
	}

	public function overview()
	{
		$from = Input::get('fromDate',Carbon::today()->subDays(7)->toDateString());
		$to = Input::get('toDate',Carbon::today());

		$days = App\Statistic::where('user_id','=',Auth::user()->id)->select(DB::raw('count(DISTINCT ip) as clicks, SUM(IF(status=1,payout,0)) as payout, timestamp, locker_type'))->whereBetween('timestamp',[$from,$to])->groupBy('timestamp')->get();

		$ref = \App\ReferLog::where('user_id','=',Auth::user()->id)->whereBetween('timestamp',[$from,$to])->sum('payout');

		$total = 0;$download=0;$gateway=0;$links=0;$notes=0;

		foreach ($days as $day) {
			$total = $total + $day->payout;
			if ($day->locker_type == 'file') { $download = $download + $day->payout; } elseif ($day->locker_type == 'gateway') { $gateway = $gateway + $day->payout; } elseif ($day->locker_type == 'link') { $links = $links + $day->payout; } elseif ($day->locker_type == 'note') { $notes = $notes + $day->payout; }
		}
		return view('dashboard.overview')->with(compact('days','total','download','gateway','links','from','to','ref','notes'));
	}

	public function timescalebreakdown()
	{
		$from = Input::get('fromDate',Carbon::today()->subDays(7)->toDateString());
		$to = Input::get('toDate',Carbon::today());

		$days = App\Statistic::where('user_id','=',Auth::user()->id)->select(DB::raw('count(id) as clicks, SUM(IF(status=1,1,0)) as leads, SUM(IF(status=1,payout,0)) as payout, timestamp'))->whereBetween('timestamp',[$from,$to])->groupBy('timestamp')->get();

		return view('dashboard.timescale')->with(compact('days','from','to'));
	}

	public function lockerbreakdown()
	{
		$from = Input::get('fromDate',Carbon::today()->subDays(7)->toDateString());
		$to = Input::get('toDate',Carbon::today());

		$lockers = App\Statistic::where('user_id','=',Auth::user()->id)->select(DB::raw('count(DISTINCT ip) as clicks, SUM(IF(status=1,1,0)) as leads, SUM(IF(status=1,payout,0)) as payout, timestamp,locker_id,locker_type'))->whereBetween('timestamp',[$from,$to])->groupBy('locker_id','locker_type')->get();

		foreach ($lockers as $locker) {
			if($locker->locker_type == 'file') {
				$query = \App\File::whereId($locker->locker_id)->first();
				$locker->name = $query->filename;
			} elseif($locker->locker_type == 'gateway') {
				$query = \App\Gateway::whereId($locker->locker_id)->first();
				$locker->name = $query->name;
			} elseif($locker->locker_type == 'link') {
				$query = \App\Link::whereId($locker->locker_id)->first();
				$locker->name = $query->title;
			} elseif($locker->locker_type == 'note') {
				$query = \App\Note::whereId($locker->locker_id)->first();
				$locker->name = $query->title;
			}
		}

		return view('dashboard.locker')->with(compact('lockers','from','to'));
	}

	public function countrybreakdown()
	{
		$from = Input::get('fromDate',Carbon::today()->subDays(7)->toDateString());
		$to = Input::get('toDate',Carbon::today());

		$countries = App\Statistic::where('user_id','=',Auth::user()->id)->select(DB::raw('count(DISTINCT ip) as clicks, SUM(IF(status=1,1,0)) as leads, SUM(IF(status=1,payout,0)) as payout, timestamp, country'))->whereBetween('timestamp',[$from,$to])->groupBy('country')->get();
		$codes = App\Settings::countries();

		return view('dashboard.country')->with(compact('countries','codes','from','to'));
	}

	public function files()
	{
		$successmsg = Session::get('success');
		$files = \App\File::where('user_id','=',Auth::user()->id)->whereArchive(0)->get();
		return view('dashboard/files')->with(compact('files','successmsg'));
	}

	public function fileupload()
	{
		return view('dashboard.fileupload');
	}

	public function postfileupload()
	{

			$file = array('file' => Input::file('file'));

			$rules = array(
		        'file' => 'mimes:txt,csv,doc,docx,xls,xlsx,rtf,ppt,pptx,pdf,swf,flv,psd,avi,wmv,mov,jpg,jpeg,gif,png,zip,tif,rar|max:15000'
		    );

		    $validator = Validator::make($file, $rules);

	    if ($validator->passes()) {

	    	$file = Input::file('file');

			$fname = $file->getClientOriginalName();
			$ext = $file->getClientOriginalExtension();

			$nfile = new \App\File;
			$nfile->user_id = Auth::user()->id;
			$nfile->filename = preg_replace('/\\.[^.\\s]{3,4}$/', '', $fname);
			$nfile->ext = $ext;
			$nfile->hashname = str_random(25).".".$ext;
			$nfile->code = str_random(8);
			$nfile->filesize = $file->getSize();
			$nfile->fakesize = get_file_size($file->getSize());
			$nfile->created_at = date('Y-m-d H:i:s');

			if($nfile->save()) {
				\Storage::disk('files')->put($nfile->hashname,  \File::get($file));
				return array('title' => 'Success','message' => 'Your file has been succesfully saved!');
			} else {
				return array('title' => 'Whoops!','message' => 'There was a problem saving your file, please try again.');
			}
		}
			return array('title' => 'Whoops!','message' => 'Direct file upload not allowed');
	}

	public function fileedit($code) {
		$file = \App\File::whereCode($code)->where('user_id','=',Auth::user()->id)->first();

		return view('dashboard/ajax/filedit')->with(compact('file','code'));
	}

	public function savefileedit($code) {
			$data = Input::all();

		    $rules = array(
		        'filename' => 'required',
		        'fakesize' => 'required',
	        	'fakeleads' => 'numeric'
		    );

		    $validator = Validator::make($data, $rules);

		    if ($validator->passes()) {
		    	$fname = Input::get('filename');
		    	$fsize = Input::get('fakesize');
		    	$fleads = Input::get('fakeleads');
		        \App\File::whereCode($code)->where('user_id','=',Auth::user()->id)->update(['filename' => $fname,'fakesize' => $fsize,'fake_leads' => $fleads]);

	        	return Redirect::to('dashboard/files')->with('success','File has been edited');
		    }

		    return Redirect::to('dashboard/files')->withErrors($validator->errors());
	}

	public function filelink($code)
	{
		$file = \App\File::whereCode($code)->where('user_id','=',Auth::user()->id)->first();
		$templates = \App\Template::all();
		$mirrors = \App\Mirror::whereStatus(1)->get();

		return view('dashboard.ajax.filelink')->with(compact('file','templates','code','mirrors'));
	}

	public function savefiletemplate($code)
	{
		$data = Input::all();

		    $rules = array(
		        'template' => 'required',
		    );

		    $validator = Validator::make($data, $rules);

		    if ($validator->passes()) {
		    	$template = Input::get('template');
		        \App\File::whereCode($code)->where('user_id','=',Auth::user()->id)->update(['template' => $template]);

	        	return Redirect::to('dashboard/files')->with('success','Settings have been saved');
		    }

		    return Redirect::to('dashboard/files')->withErrors($validator->errors());
	}

	public function downloadfile($code)
	{
		$file = \App\File::whereCode($code)->where('user_id','=',Auth::user()->id)->first();

		// Copy file to temp
		$contents = \Storage::disk('files')->get($file->hashname);
		\Storage::disk('tmp')->put($file->hashname,$contents);

		$filedl = storage_path()."/tmp/".$file->hashname;

		// Send the file and delete from temp
		return response()->download($filedl,$file->filename.".".$file->ext)->deleteFileAfterSend(true);
	}

	public function deletefileconfirm($code)
	{
		return view('dashboard.ajax.confirmfiledelete')->with(compact('code'));
	}

	public function deletefile($code)
	{
		
		$file = \App\File::whereCode($code)->where('user_id','=',Auth::user()->id)->first();

		// Preserve record in db for statistics
		\App\File::whereCode($code)->where('user_id','=',Auth::user()->id)->update(['archive' => 1]);
		// Delete file to save space
		\Storage::disk('files')->delete($file->hashname);

		return Redirect::to('dashboard/files')->with('success','File deleted');
	}

	public function createnote()
	{
		$note = new \App\Note;
		$action = url('dashboard/note/create');
		return view('dashboard.createnote')->with(compact('note','action'));
	}
	public function postcreatenote()
	{
			$data = Input::all();

		    $rules = array(
		        'title' => 'required',
		        'note' => 'required',
		        'tos' => 'accepted'
		    );

		    $validator = Validator::make($data, $rules);

		    if ($validator->passes()) {

		    	$note = new \App\Note;
		    	$note->user_id = Auth::user()->id;
		    	$note->code = str_random(8);
		    	$note->title = Input::get('title');
		    	$note->summary = Input::get('summary');
		    	$note->note = Input::get('note');
		    	$note->created_at = date('Y-m-d H:i:s');
		    	$note->updated_at = date('Y-m-d H:i:s');
		    	$note->save();

	    		$msg[] = array('title' => 'Success', 'message' => 'Note saved!'); return $msg;
		    }

	    $messages = $validator->messages();

	    foreach($messages->all() as $message) {
	    	$msg[] = array('title' => 'xhr', 'message' => str_replace('_',' ',$message));
	    }
	    return $msg;
	}

	public function posteditnote()
	{
		$data = Input::all();

		    $rules = array(
		        'title' => 'required',
		        'note' => 'required',
		        'tos' => 'accepted'
		    );

		    $validator = Validator::make($data, $rules);

		    if ($validator->passes()) {

		    	$note = \App\Note::where('user_id','=',Auth::user()->id)->whereId(Input::get('note_id'))->first();
		    	$note->title = Input::get('title');
		    	$note->summary = Input::get('summary');
		    	$note->note = Input::get('note');
		    	$note->updated_at = date('Y-m-d H:i:s');
		    	$note->save();

	    		$msg[] = array('title' => 'Success', 'message' => 'Note saved!'); return $msg;
		    }

	    $messages = $validator->messages();

	    foreach($messages->all() as $message) {
	    	$msg[] = array('title' => 'xhr', 'message' => str_replace('_',' ',$message));
	    }
	    return $msg;
	}

	public function editnote($id)
	{
		$note = \App\Note::where('user_id','=',Auth::user()->id)->whereId($id)->first();
		$action = url('dashboard/note/edit');
		return view('dashboard.createnote')->with(compact('note','action'));
	}

	public function getnote($id)
	{
		$note = \App\Note::where('user_id','=',Auth::user()->id)->whereId($id)->first();
		return view('dashboard.ajax.getnote')->with(compact('note'));
	}

	public function confirmnotedelete($id)
	{
		$note = \App\Note::where('user_id','=',Auth::user()->id)->whereId($id)->first();
		return view('dashboard.ajax.confirmnotedelete')->with(compact('note'));
	}

	public function notedelete($id)
	{
		\App\Note::where('user_id','=',Auth::user()->id)->whereId($id)->update(['archive' => 1]);
		return Redirect::to('dashboard/notes-links')->with('success','Note Deleted.');
	}

	public function createlink()
	{
		$link = new \App\Link;
		$action = url('dashboard/link/create');
		return view('dashboard.createlink')->with(compact('link','action'));
	}

	public function postcreatelink()
	{
		$data = Input::all();

		    $rules = array(
		        'title' => 'required',
		        'link' => 'required|url',
		        'tos' => 'accepted'
		    );

		    $validator = Validator::make($data, $rules);

		    if ($validator->passes()) {

	    		$link = new \App\Link;
		    	$link->user_id = Auth::user()->id;
		    	$link->code = str_random(8);
		    	$link->title = Input::get('title');
		    	$link->description = Input::get('description');
		    	$link->link = Input::get('link');
		    	$link->created_at = date('Y-m-d H:i:s');
		    	$link->updated_at = date('Y-m-d H:i:s');
		    	$link->save();

	    		$msg[] = array('title' => 'Success', 'message' => 'Link saved!'); return $msg;
		    }

		    $messages = $validator->messages();

		    foreach($messages->all() as $message) {
		    	$msg[] = array('title' => 'xhr', 'message' => str_replace('_',' ',$message));
		    }
		    return $msg;
	}

	public function posteditlink()
	{
		$data = Input::all();

		    $rules = array(
		        'title' => 'required',
		        'link' => 'required|active_url',
		        'tos' => 'accepted'
		    );

		    $validator = Validator::make($data, $rules);

		    if ($validator->passes()) {

	    		$link = \App\Link::where('user_id','=',Auth::user()->id)->whereId(Input::get('link_id'))->first();
		    	$link->title = Input::get('title');
		    	$link->description = Input::get('description');
		    	$link->link = Input::get('link');
		    	$link->updated_at = date('Y-m-d H:i:s');
		    	$link->save();

	    		$msg[] = array('title' => 'Success', 'message' => 'Link saved!'); return $msg;
		    }

		    $messages = $validator->messages();

		    foreach($messages->all() as $message) {
		    	$msg[] = array('title' => 'xhr', 'message' => str_replace('_',' ',$message));
		    }
		    return $msg;
	}

	public function getlink($id)
	{
		$link = \App\Link::where('user_id','=',Auth::user()->id)->whereId($id)->first();
		return view('dashboard.ajax.getlink')->with(compact('link'));
	}

	public function editlink($id)
	{
		$link = \App\Link::where('user_id','=',Auth::user()->id)->whereId($id)->first();
		$action = url('dashboard/link/edit');
		return view('dashboard.createlink')->with(compact('link','action'));
	}

	public function confirmlinkdelete($id)
	{
		$link = \App\Link::where('user_id','=',Auth::user()->id)->whereId($id)->first();
		return view('dashboard.ajax.confirmlinkdelete')->with(compact('link'));
	}

	public function linkdelete($id)
	{
		\App\Link::where('user_id','=',Auth::user()->id)->whereId($id)->update(['archive' => 1]);
		return Redirect::to('dashboard/notes-links')->with('success','Link Deleted');
	}

	public function noteslinks()
	{
		$notes = \App\Note::where('user_id','=',Auth::user()->id)->whereArchive(0)->get();
		$links = \App\Link::where('user_id','=',Auth::user()->id)->whereArchive(0)->get();

		$num = count($notes) + count($links);

		$successmsg = Session::get('success');
		return view('dashboard.noteslinks')->with(compact('notes','links','successmsg','num'));
	}

	public function leaderboard()
	{
		$toptoday = \App\Statistic::select(DB::raw('SUM(payout) as pay, user_id'))->whereTimestamp(Carbon::today())->groupBy('user_id')->whereStatus(1)->orderBy('pay','DESC')->take(10)->get();

		$topmtd = \App\Statistic::select(DB::raw('SUM(payout) as pay, user_id'))->whereBetween('timestamp',[Carbon::today()->startOfMonth()->toDateString(),Carbon::today()])->whereStatus(1)->orderBy('pay','DESC')->groupBy('user_id')->take(10)->get();

		$userids = [];

		foreach($toptoday as $top) { array_push($userids, $top->user_id); }
		foreach ($topmtd as $top) { array_push($userids, $top->user_id); }

		array_push($userids,Auth::user()->id);

		$userids = array_unique($userids);

		$users = \App\User::whereIn('id',$userids)->get();

		$settings = \App\Settings::whereScope('leaderboard')->whereIn('user_id',$userids)->get();

		if(count($settings->where('user_id',Auth::user()->id)->where('option','hide_username')->first()) > 0) {
			$hideusername = 1;
		} else { $hideusername = 0; }

		if(count($settings->where('user_id',Auth::user()->id)->where('option','hide_earnings')->first()) > 0) {
			$hidearnings = 1;
		} else { $hidearnings = 0; }

		$myid = Auth::user()->id;

		return view('dashboard.leaderboard')->with(compact('toptoday','topmtd','users','settings','myid','hideusername','hidearnings'));
	}

	public function saveleaderboardsettings()
	{
		if (Input::get('status') == 1) {
			\App\Settings::firstOrCreate(['user_id' => Auth::user()->id, 'scope' => 'leaderboard', 'option' => Input::get('field'), 'value' => 1]);
		} else {
			\App\Settings::whereUserId(Auth::user()->id)->whereScope('leaderboard')->whereOption(Input::get('field'))->delete();
		}
	}

	public function gateways()
	{
		$successmsg = Session::get('success');
		$gateways = \App\Gateway::where('user_id','=',Auth::user()->id)->whereArchive(0)->get();
		return view('dashboard.gateways')->with(compact('gateways','successmsg'));
	}

	public function creategateway()
	{
		$gateway = new \App\Gateway;
		$gateway->name = '';
		$gateway->redirect = '';
		$gateway->title = 'Download';
		$gateway->inst = 'Please Complete One Offer Below to Continue';
		$gateway->load_method = 'onclick';
		$gateway->bgimg = 'http://i.imgur.com/R5UHBqR.gif';
		$gateway->redirect = '';
		$gateway->customcss = 'false';
		$gateway->allow_close = 0;
		$gateway->numOffers = 5;

		$css = 'a:16:{s:5:"width";s:3:"539";s:6:"height";s:3:"265";s:13:"border_radius";s:1:"0";s:10:"title_font";s:11:"Arial Black";s:11:"title_color";s:4:"#fff";s:15:"title_font_size";s:0:"";s:16:"title_top_margin";s:2:"30";s:16:"instruction_font";s:6:"Tahoma";s:17:"instruction_color";s:4:"#fff";s:21:"instruction_font_size";s:0:"";s:22:"instruction_top_margin";s:0:"";s:11:"offers_font";s:5:"Arial";s:12:"offers_color";s:7:"#4a89dc";s:16:"offers_font_size";s:2:"17";s:17:"offers_top_margin";s:0:"";s:16:"offers_alignment";s:6:"center";}';
		$css = unserialize($css);

		$gateway_id = 0;
		return view('dashboard.creategateway')->with(compact('gateway','css','gateway_id'));
	}

	public function postcreategateway()
	{
		$data = Input::all();

		$messages = [
		    'redirect_url.required_if' => 'Choose no redirect or enter redirect url',

		];

	    $rules = array(
	    	'gateway_name' => 'required|alphadot',
	    	'redirect_url' => 'required_if:noredirect,false|url',
	    	'num_offers' => 'integer',
	    	'width' => 'integer',
	    	'height' => 'integer',
	    	'border_radius' => 'integer',
	    	'title_font' => 'alphanumspace',
	    	'title_color' => 'colorhex',
	    	'title_font_size' => 'integer',
	    	'title_top_margin' => 'integer',
	    	'instruction_font' => 'alphanumspace',
    		'instruction_color' => 'colorhex',
    		'instruction_font_size' => 'integer',
    		'instruction_top_margin' => 'integer',
    		'offers_font' => 'alphanumspace',
    		'offers_color' => 'colorhex',
    		'offers_font_size' => 'integer',
    		'offers_top_margin' => 'integer',
    		'offers_alignment' => 'alpha',
	        'title' => 'alphadot',
	        'instruction' => 'alphadot',
	        'bgimage' => 'url',
	    );

	    $validator = Validator::make($data, $rules,$messages);

	    if ($validator->passes()) {

    		// Break if custom css is not formatted well
	    	if(Input::get('custom') == 'true') { if (Input::get('validcss') == 'false') { $msg[] = array('title' => 'Error', 'message' => 'Please check your custom css and try again'); return $msg; } }

	    	if (Input::get('gateway_id') == 0) {
    			$gate = new \App\Gateway;
    			$gate->code = str_random(10);
			} else {
				$gate = \App\Gateway::where('user_id','=',Auth::user()->id)->whereId(Input::get('gateway_id'))->first();
			}
			$gate->user_id = Auth::user()->id;
			$gate->name = Input::get('gateway_name');
			if (Input::get('noredirect') == true) {$gate->redirect = '';} else { $gate->redirect = Input::get('redirect_url');}
			$gate->load_method = Input::get('load_method');
			$gate->title = Input::get('title');
			$gate->inst = Input::get('instruction');
			$gate->bgimg = Input::get('bgimage');
			if (Input::get('allow_close') == 'true') { $gate->allow_close = 1; } else { $gate->allow_close = 0; }
			$gate->numOffers = Input::get('num_offers');

			if (Input::get('custom') == 'true') {
				$css = Input::get('custom_css');
			} else {
				$css = array(
					'width' => Input::get('width'),
					'height' => Input::get('height'),
					'border_radius' => Input::get('border_radius'),
					'title_font' => Input::get('title_font'),
					'title_color' => Input::get('title_color'),
					'title_font_size' => Input::get('title_font_size'),
					'title_top_margin' => Input::get('title_top_margin'),
					'instruction_font' => Input::get('instruction_font'),
					'instruction_color' => Input::get('instruction_color'),
					'instruction_font_size' => Input::get('instruction_font_size'),
					'instruction_top_margin' => Input::get('instruction_top_margin'),
					'offers_font' => Input::get('offers_font'),
					'offers_color' => Input::get('offers_color'),
					'offers_font_size' => Input::get('offers_font_size'),
					'offers_top_margin' => Input::get('offers_top_margin'),
					'offers_alignment' => Input::get('offers_alignment'),
					);
				$css = serialize($css);
			}

			$gate->css = $css;
			$gate->customcss = Input::get('custom');
			if (Input::get('gateway_id') == 0) { $gate->created_at = date('Y-m-d H:i:s'); }
			$gate->updated_at = date('Y-m-d H:i:s');

			if($gate->save()) {
				$msg[] = array('title' => 'Success', 'message' => 'Gateway have been successfully saved!'); return $msg;
			} else {
				$msg[] = array('title' => 'Whoops!', 'message' => 'Gateway have been successfully saved!'); return $msg;
			}
	    }

	    $messages = $validator->messages();

	    foreach($messages->all() as $message) {
	    	$msg[] = array('title' => 'Whoops!', 'message' => str_replace('_',' ',$message));
	    }
	    return $msg;
	}

	public function editgateway($id)
	{
		$gateway = \App\Gateway::whereUserId(Auth::user()->id)->whereId($id)->first();
		$css = unserialize($gateway->css);
		$gateway_id = $id;

		$fonts = array($css['title_font'],$css['instruction_font'],$css['offers_font']);
		$gfonts = [];

		foreach($fonts as $font) {
			if(!in_array($font,["Georgia","Palatino Linotype","Times New Roman","Arial","Arial Black","Comic Sans MS","Impact","Lucida Sans Unicode","Tahoma","Trebuchet MS","Verdana","Courier New","Lucida Console"])) {
				array_push($gfonts,$font);
			}
		}

		$gfonts = implode("|", $gfonts);

		return view('dashboard.creategateway')->with(compact('gateway','css','gateway_id','gfonts'));
	}

	public function changeloadmethod() {
		\App\Gateway::where('user_id','=',Auth::user()->id)->whereId(Input::get('pk'))->update(['load_method' => Input::get('value')]);
		return 'success';
	}

	public function integrategateway($id) {
		$gateway = \App\Gateway::where('user_id','=',Auth::user()->id)->whereId($id)->first();
		return view('dashboard.ajax.integrategateway')->with(compact('gateway'));
	}

	public function deletegateway($id) {
		\App\Gateway::where('user_id','=',Auth::user()->id)->whereId($id)->update(['archive' => 1]);
		return Redirect::to('dashboard/gateways');
	}

	public function account()
	{
		$role = Auth::user()->role;
		$user = Auth::user();
		$profile = Auth::user()->user_profile;
		if ($profile->payment_method == 'Wire Transfer') { $wire = unserialize($profile->payment_detail); } else { $wire = array(
					'Name on Account' => "",
					'Bank Name' => "",
					'Bank Address' => "",
					'Bank Country' => "",
					'Account Type' => "",
					'Account / IBAN #' => "",
					'Routing / ABA #' => "",
					'SWIFT Code' => "",
					); }
		$countries = App\Settings::countries();
		return view('dashboard.account')->with(compact('role','profile','user','wire','countries'));
	}

	public function postaccount()
	{
		$data = Input::all();

		$rules = array(
			'old_password' => 'required_with:new_password',
			'confirm_new_password' => 'required_with:new_password|same:new_password',
			'paypal' => 'required_if:payment_method,PayPal',
			'payza' => 'required_if:payment_method,Payza',
			'moneybookers' => 'required_if:payment_method,Moneybookers / Skrill',
			'account_name' => 'required_if:payment_method,Wire Transfer',
			'bank_name' => 'required_if:payment_method,Wire Transfer',
			'bank_address' => 'required_if:payment_method,Wire Transfer',
			'bank_country' => 'required_if:payment_method,Wire Transfer',
			'account_type' => 'required_if:payment_method,Wire Transfer',
			'account_iban' => 'required_if:payment_method,Wire Transfer',
			'routing_aba' => 'required_if:payment_method,Wire Transfer',
			'westernunion' => 'required_if:payment_method,Western Union',
			'bitcoin_address' => 'required_if:payment_method,Bitcoin',
			'profileimg' => 'url',
			);

		$validator = Validator::make($data, $rules);

		if ($validator->passes()) {

			$user = Auth::user();
			$user->profileimg = Input::get('profileimg');
			$user->chatsound = Input::get('chatsound');

			if ((Input::has('old_password'))) {
				if (\Hash::check(Input::get('old_password'), $user->password)) { $user->password = \Hash::make(Input::get('new_password')); } else {
					$msg[] = array('title' => 'Whoops!', 'message' => 'Password credentials do not match our records'); return $msg;
				}
			}

			$user->save();

			$profile = Auth::user()->user_profile;
			$profile->payment_method = Input::get('payment_method');

			if (Input::get('payment_method') == 'PayPal') {
				$profile->payment_detail = Input::get('paypal');
			} elseif (Input::get('payment_method') == 'Payza') {
				$profile->payment_detail = Input::get('payza');
			} elseif (Input::get('payment_method') == 'Moneybookers / Skrill') {
				$profile->payment_detail = Input::get('moneybookers');
			} elseif (Input::get('payment_method') == 'Wire Transfer') {
				$detail = array(
					'Name on Account' => Input::get('account_name'),
					'Bank Name' => Input::get('bank_name'),
					'Bank Address' => Input::get('bank_address'),
					'Bank Country' => Input::get('bank_country'),
					'Account Type' => Input::get('account_type'),
					'Account / IBAN #' => Input::get('account_iban'),
					'Routing / ABA #' => Input::get('routing_aba'),
					'SWIFT Code' => Input::get('swift_code'),
					);
				$profile->payment_detail = serialize($detail);
			} elseif (Input::get('payment_method') == 'Western Union') {
				$profile->payment_detail = Input::get('westernunion');
			} else {
				$profile->payment_detail = Input::get('bitcoin_address');
			}

			$profile->skype = Input::get('skype');
			$profile->phone = Input::get('phone');

			$profile->save();

			$msg[] = array('title' => 'Success', 'message' => 'Account settings saved!'); return $msg;
		}

		$messages = $validator->messages();

	    foreach($messages->all() as $message) {
	    	$msg[] = array('title' => 'xhr', 'message' => str_replace('_',' ',$message));
	    }
	    return $msg;

	}

	public function payments()
	{
		$payments = \App\PaymentRequest::whereUserId(Auth::user()->id)->orderBy('request_date','desc')->get();

		$pending = $payments->filter(function($payments) { if ($payments->status == 'Pending') {  return true; } });
		$paid = $payments->filter(function($payments) { if ($payments->status == 'Complete') {  return true; } });

		$pending = $pending->sum('amount');
		$paid = $paid->sum('amount');

		$profile = Auth::user()->user_profile;
		$schedules = \App\Settings::paymentschedules();
		$myschedule = $schedules->where('option',$profile->payment_cycle)->first();
		$successmsg = Session::get('success');
		$error = Session::get('error');

		$dates = $this->getPaymentDates($profile->payment_cycle);
		$balancedate = $dates['balancedate'];
		$paydate = $dates['paydate'];

		$alltime = $paid + $pending + Auth::user()->balance;

		$balancearn = Auth::user()->balance;

		$request = $pending + $paid;

		return view('dashboard.payments')->with(compact('payments','profile','myschedule','paydate','successmsg','pending','paid','alltime','balancearn','error'));
	}

	public function postpayment() {
		$data = Input::all();

		$rules = array(
				'amount' => 'required|payout',
				'date' => 'required'
			);

		$validator = Validator::make($data,$rules);

		$profile = Auth::user()->user_profile;
		$dates = $this->getPaymentDates($profile->payment_cycle);
		$balancedate = $dates['balancedate'];
		$paydate = $dates['paydate'];

		$balancearn = Auth::user()->balance;

		$pendingpayments = \App\PaymentRequest::whereUserId(Auth::user()->id)->whereStatus('Pending')->first();

		if (empty($profile->payment_method)) {
			return Redirect::to('dashboard/payments')->with('error','Please complete your payment details first');
		}

		if (count($pendingpayments) > 0) {
			return Redirect::to('dashboard/payments')->with('error','You already have a payment pending');
		}

		if($balancearn < $this->gset->mincashout) {
			return Redirect::to('dashboard/payments')->with('error','You have insufficient balance to make a request');
		}


		if ($validator->passes()) {

			$profile = Auth::user()->user_profile;

			$cashout = new \App\PaymentRequest;
			$cashout->user_id = Auth::user()->id;
			$cashout->amount = Input::get('amount');
			$cashout->status = 'Pending';
			$cashout->payment_method = $profile->payment_method;
			$cashout->schedule = $profile->payment_cycle;
			$cashout->request_date = date('Y-m-d H:i:s');
			$cashout->payment_details = $profile->payment_detail;
			$cashout->priority = 'Normal';
			$cashout->fee = '0.00';
			$cashout->save();

			$pub = \App\User::find(Auth::user()->id);
			$pub->balance = $pub->balance - Input::get('amount');
			$pub->save();

			return Redirect::to('dashboard/payments')->with('success','Payment request sent!');
		}

		return Redirect::to('dashboard/payments')->withErrors($validator->errors());
	}

	public function referrals()
	{
		$refearn = \App\ReferLog::where('user_id','=',Auth::user()->id)->select(DB::raw('SUM(payout) as pay, referral_id'))->groupBy('referral_id')->get();

		$users = \App\User::where('referred_by','=',Auth::user()->id)->get();

		$me = \Hashids::encode(Auth::user()->id);

		return view('dashboard.referrals')->with(compact('refearn','me','users'));
	}

	public function optimizeoffers()
	{
		$disabled = \App\Settings::whereScope('offers')->whereOption('disabled')->whereUserId(Auth::user()->id)->lists('value');
		$favorite = \App\Settings::whereScope('offers')->whereOption('favorite')->whereUserId(Auth::user()->id)->lists('value');

		$country = Input::get('country','US');

		$activenetworks = \App\Network::getActiveNetworks();
		$nenabled = \App\Settings::getNetworkEnabled(Auth::user()->id);
		$nenabled = array_merge($activenetworks,$nenabled);

		$offers = \App\Offer::ofCountry($country)->ofNetworkIn($nenabled)->ofOrder(Input::get('sort',''))->paginate(15);

		$offers->appends(Input::except('page'));
		$offers->setPath('optimize');
		$countries = \App\Settings::countries();

		$scountry = Input::get('country','');
		$sort = Input::get('sort','');

		return view('dashboard.optimizeoffers')->with(compact('offers','countries','disabled','favorite','scountry','sort'));
	}
	
	public function offers()
	{
		$disabled = \App\Settings::whereScope('offers')->whereOption('disabled')->whereUserId(Auth::user()->id)->lists('value');
		$favorite = \App\Settings::whereScope('offers')->whereOption('favorite')->whereUserId(Auth::user()->id)->lists('value');

		$country = Input::get('country','US');

		$activenetworks = \App\Network::getActiveNetworks();
		$nenabled = \App\Settings::getNetworkEnabled(Auth::user()->id);
		$nenabled = array_merge($activenetworks,$nenabled);

		$offers = \App\Offer::ofCountry($country)->ofNetworkIn($nenabled)->ofOrder(Input::get('sort',''))->paginate(15);

		$offers->appends(Input::except('page'));
		$offers->setPath('offers');
		$countries = \App\Settings::countries();

		$scountry = Input::get('country','');
		$sort = Input::get('sort','');

		return view('dashboard.offers')->with(compact('offers','countries','scountry','sort'));
	}	

	public function offerdetails($id)
	{

		$offer_details = \App\Offer::OfId($id);

		return view('dashboard.ajax.offerdetails')->with(compact('offer_details','id'));
	}	
	
	public function toggleoffer() {

		if(Input::get('status') == 0) {
			\App\Settings::firstOrCreate(['user_id' => Auth::user()->id, 'scope' => 'offers', 'option' => 'disabled', 'value' => Input::get('id')]);
		} else {
			\App\Settings::whereUserId(Auth::user()->id)->whereScope('offers')->whereOption('disabled')->whereValue(Input::get('id'))->delete();
		}

	}

	public function favoriteoffer() {

		if(Input::get('status') == 1) {
			\App\Settings::firstOrCreate(['user_id' => Auth::user()->id, 'scope' => 'offers', 'option' => 'favorite', 'value' => Input::get('id')]);
		} else {
			\App\Settings::whereUserId(Auth::user()->id)->whereScope('offers')->whereOption('favorite')->whereValue(Input::get('id'))->delete();
		}

	}

	public function resetoptimization() {
		\App\Settings::whereScope('offers')->whereOption('disabled')->whereUserId(Auth::user()->id)->delete();
		\App\Settings::whereScope('offers')->whereOption('favorite')->whereUserId(Auth::user()->id)->delete();
		return Redirect::to('dashboard/offers/optimize');
	}

	public function countryminpayout()
	{
		$countries = App\Settings::countries();

		$rules = App\Settings::where('user_id','=',Auth::user()->id)->whereScope('minpayout')->get();

		return view('dashboard.countryminpayout')->with(compact('countries','rules'));
	}

	public function tickets()
	{
		$tickets = App\Ticket::where('user_id','=',Auth::user()->id)->get();
		return view('dashboard.tickets')->with(compact('tickets'));
	}

	public function promotions()
	{
		$leads = \App\Statistic::where('user_id','=',Auth::user()->id)->whereStatus(1)->whereBetween('timestamp',[Carbon::today()->startOfMonth()->toDateString(),Carbon::today()])->count();

		$current = \App\PromotionUser::where('user_id','=',Auth::user()->id)->where('timestamp','>',Carbon::today()->startOfMonth()->toDateString())->lists('promo_id');

		$promos = \App\Promotion::whereActive(1)->get();

		$successmsg = Session::get('success');
		$error = Session::get('error');

		return view('dashboard.promotions')->with(compact('leads','promos','current','successmsg','error'));
	}

	public function activatepromotion($id) {

		$promo = \App\Promotion::find($id);

		//$leads = \App\Statistic::where('user_id','=',Auth::user()->id)->whereStatus(1)->whereBetween('timestamp',[Carbon::today()->startOfMonth()->toDateString(),Carbon::today()])->count();

		$leads = 200;

		$current = \App\PromotionUser::where('user_id','=',Auth::user()->id)->where('timestamp','>',Carbon::today()->startOfMonth()->toDateString())->lists('promo_id');

		if($leads >= $promo->leads && !in_array($promo->id,$current)) {
			$pro = new \App\PromotionUser;
			$pro->user_id = Auth::user()->id;
			$pro->promo_id = $promo->id;
			$pro->save();

			Cache::put('promotion_'.Auth::user()->id, $promo->bonus_percentage, 60 * $promo->hours);
			return Redirect::to('dashboard/promotions')->with('success','Promotion activated successfully!');
		} else {
			return Redirect::to('dashboard/promotions')->with('error','Insufficient leads or promotion already used');
		}
	}

	public function reversals()
	{
		$from = Input::get('fromDate',Carbon::today()->subDays(7)->toDateString());
		$to = Input::get('toDate',Carbon::today());

		$reversed = App\Statistic::where('user_id','=',Auth::user()->id)->whereStatus(2)->whereBetween('timestamp',[$from,$to])->get();

		return view('dashboard/reversedleads')->with(compact('reversed','from','to'));
	}

	public function exportreversals()
	{
		$from = Input::get('from');
		$to = Input::get('to');

		$reversed = App\Statistic::where('user_id','=',Auth::user()->id)->whereStatus(2)->whereBetween('timestamp',[$from,$to])->get();

	    $output = implode(",", array('Date', 'Offer ID', 'Offer', 'Country', 'Amount'));
	    $output .= "\r\n";

	    foreach ($reversed as $reverse) {
	        $output .=  implode(",", array($reverse['timestamp'], $reverse['campid'], $reverse['offname'], $reverse['country'], $reverse['payout'])); // append each row
	        $output .= "\r\n";
	    }

	    $headers = array(
	        'Content-Type' => 'text/csv',
	        'Content-Disposition' => 'attachment; filename="reversedleads.csv"',
	        );

	    return \Response::make(rtrim($output, "\n"), 200, $headers);
	}

	public function livestats() {
		return view('dashboard.livestats');
	}

	public function livedata() {

		$codes = App\Settings::countries();

		$countries = [];
		$content = [];

		$onlinevisitors = $this->getvisitors();
		$onlinecontent = $this->getcontent();

		foreach($onlinevisitors as $visitor) {
			$visitor = json_decode($visitor,true);
			array_push($countries,$visitor['country']);
		}

		foreach($onlinecontent as $contente) {
			$contente = json_decode($contente,true);
			array_push($content,$contente['content']);
		}

		$countries = array_count_values($countries);
		$content = array_count_values($content); 

		$data = '{"visitors": [';

		foreach ($countries as $key => $value) {
			$data .= '{"c":"'.$key.'","v":'.$value.',"t":"'.$codes->where('option',$key)->first()->value.'"},';
		}

		$data = rtrim($data,",");

		$data .= '], "content": [';

		foreach ($content as $key => $value) {
			$data .= '{"c":"'.$key.'","v":'.$value.'},';
		}

		$data = rtrim($data,",");

		$data .= ']}';

		return $data;


		/*
		$num = rand(1,10);
		$countries = array('US','BE','DE','BR','IN','CA','ZA','FR');
		$content = array('Clash of Clans','Password.txt','MP3 Collection');

		$codes = App\Settings::countries();

		$data = '{"visitors": [';

		for ($i=0;$i<=$num;$i++) {
			$rand = array_rand($countries,1);
			$data .= '{"c":"'.$countries[$rand].'","v":'.rand(1,5).',"t":"'.$codes->where('option',$countries[$rand])->first()->value.'"},';
		}

		$data = rtrim($data,",");
		$data .= '], "content": [';

		for ($i=0;$i<=$num;$i++) {
			$data .= '{"c":"'.$content[array_rand($content,1)].'","v":'.rand(1,5).'},';
		}

		$data = rtrim($data,",");

		$data .= ']}';

		return $data; */

	}

	public function newticket() {

		    $data = Input::all();

		    $rules = array(
		        'subject' => 'required',
		        'message' => 'required'
		    );

		    $validator = Validator::make($data, $rules);

		    if ($validator->passes()) {
		        $ticket = new App\Ticket;
		        $ticket->user_id = Auth::user()->id;
		        $ticket->subject = Input::get('subject');
		        $ticket->department = Input::get('department');
		        $ticket->priority = Input::get('priority');
		        $ticket->timestamp = date('Y-m-d H:i:s');
		        $ticket->save();

		        $ticketmsg = new App\TicketMessage;
		        $ticketmsg->ticket_id = $ticket->id;
	        	$ticketmsg->user_id = Auth::user()->id;
	        	$ticketmsg->user = Auth::user()->chathandle;
	        	$ticketmsg->message = Input::get('message');
	        	$ticketmsg->timestamp = date('Y-m-d H:i:s');
	        	$ticketmsg->save();
		    }

		    return Redirect::to('dashboard/tickets')->withErrors($validator->errors());
	}

	public function viewticket($id) {

		$ticket = App\Ticket::whereId($id)->first();

		if ($ticket->user_id != Auth::user()->id) {
			return Redirect::to('dashboard/tickets');
		}

		$msgs = App\TicketMessage::where('ticket_id','=',$id)->get();

		return view('dashboard.ticket')->with(compact('msgs','ticket'));

	}

	public function addticketmsg($id) {
			    $data = Input::all();

			    $rules = array(
			        'message' => 'required'
			    );

			    $validator = Validator::make($data, $rules);

			    if ($validator->passes()) {

			        $ticketmsg = new App\TicketMessage;
			        $ticketmsg->ticket_id = $id;
		        	$ticketmsg->user_id = Auth::user()->id;
		        	$ticketmsg->user = Auth::user()->chathandle;
		        	$ticketmsg->message = Input::get('message');
		        	$ticketmsg->timestamp = date('Y-m-d H:i:s');
		        	$ticketmsg->save();
			    }

			    return Redirect::to('dashboard/ticket/'.$id)->withErrors($validator->errors());
	}

	public function savecountryminpayout() {
		$data = Input::all();

		$rules = array(
			'country' => 'required',
			'payout' => 'required|payout'
		);

	    $validator = Validator::make($data, $rules);

	    if ($validator->passes()) {
    		$setting = new App\Settings;
    		$setting->user_id = Auth::user()->id;
    		$setting->scope = 'minpayout';
    		$setting->option = Input::get('country');
    		$setting->value = Input::get('payout');
    		$setting->save();

    		return Redirect::to('dashboard/countries/minpayout');
	    }

	    return Redirect::to('dashboard/countries/minpayout')->withErrors($validator->errors());
	}

	public function deletecountryminpayout($id) {
		$target = App\Settings::whereScope('minpayout')->where('user_id','=',Auth::user()->id)->where('id','=',$id)->first();
		$target->delete();
		return Redirect::to('dashboard/countries/minpayout');
	}

	public function messages() {
		$messages = \App\Message::whereIn('receiver',array('Public',Auth::user()->chathandle))->get();

		$readmsgs = unserialize(Auth::user()->read_msgs);

		return view('dashboard.messages')->with(compact('messages','readmsgs'));
	}

	public function getmessage($id)
	{	
		$readmsgs = Auth::user()->read_msgs;
		$readmsgs = unserialize($readmsgs);

		if (!in_array($id,$readmsgs)) {
		array_push($readmsgs,$id);
		\App\User::where('id','=',Auth::user()->id)->update(['read_msgs' => serialize($readmsgs)]);
		}

		$message = \App\Message::whereIn('receiver',array('Public',Auth::user()->chathandle))->whereId($id)->first();

		return view('dashboard.ajax.viewmessage')->with(compact('message'));
	}

	public function getvisitors() {
		 /* current hour and minute */
        $now = time();
        $min = date("i",$now);
        $hor = date("G",$now);

        /* redis keys to union, based on last $minutes */
        $keys = array();
        for($i = $min ; $i >= $min - $this->minutes; $i--) {
            $keys[] = "visitor".Auth::user()->id.":".$hor.":".$i; // define the key
        }

        $redis = Redis::connection(); // connect to redis at localhost
        $scmd = $redis->createCommand("sunion",$keys); // create the union with desired keys
        $online = $redis->executeCommand($scmd); // issue the sunion and grab the result
        
        return $online ; // array of online usernames
	}

	public function getcontent() {
		 /* current hour and minute */
        $now = time();
        $min = date("i",$now);
        $hor = date("G",$now);

        /* redis keys to union, based on last $minutes */
        $keys = array();
        for($i = $min ; $i >= $min - $this->minutes; $i--) {
            $keys[] = "content".Auth::user()->id.":".$hor.":".$i; // define the key
        }

        $redis = Redis::connection(); // connect to redis at localhost
        $scmd = $redis->createCommand("sunion",$keys); // create the union with desired keys
        $online = $redis->executeCommand($scmd); // issue the sunion and grab the result
        
        return $online ; // array of online usernames
	}

	public function getPaymentDates($schedule) {

		if ($schedule == 'Net30') {
			$balance = Carbon::parse('last day of previous month')->toDateString();
			$paydate = Carbon::today()->lastOfMonth()->toDateString();
		} elseif ($schedule == 'Net15') {
			$balance = Carbon::parse('last day of previous month')->toDateString();
			$paydate = Carbon::createFromDate(null,null,15)->toDateString();
		} elseif ($schedule == 'Net7') {
			$balance = Carbon::parse('last day of previous month')->toDateString();
			$paydate = Carbon::createFromDate(null,null,7)->toDateString();
		} elseif ($schedule == 'Net0') {
			$balance = Carbon::parse('last day of previous month')->toDateString();
			$paydate = Carbon::createFromDate(null,null,3)->toDateString();
		} elseif ($schedule == 'Bi-Weekly') {
			if( date('d') <= 15) { 
				$balance = Carbon::parse('last day of previous month')->toDateString();
				$paydate = Carbon::createFromDate(null,null,15)->toDateString();
			} else { 
				$balance = Carbon::createFromDate(null,null,15)->toDateString();
				$paydate = Carbon::parse('last day of this month')->toDateString();
			}
		} elseif ($schedule == 'Weekly') {
			$balance = Carbon::parse('last friday')->toDateString();
			$paydate = Carbon::parse('next friday')->toDateString();
		} else {
			$balance = Carbon::parse('last day of previous month')->toDateString();
			$paydate = Carbon::today()->lastOfMonth()->toDateString();
		}

		return array('balancedate' => $balance, 'paydate' => $paydate);
	}
}
