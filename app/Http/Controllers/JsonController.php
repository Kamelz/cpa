<?php namespace App\Http\Controllers;

use Auth;
use App\Chat;
use Cache;
use App\User;
use Input;
use Crypt;
use Session;
use Redirect;
use Carbon\Carbon;
use Illuminate\Support\Facades\Redis;

class JsonController extends Controller {

	/**
	 * Show the application welcome screen to the user.
	 *
	 * @return Response
	 */

	private $minutes = 15;

	public function chat($timestamp)
	{
		if(!Auth::check()) {
			\App::abort(403, 'Unauthorized.');
		} else {

			$public = 10;

			$bots = array(20);

			$admins = \App\User::getAdmins();

			$role = Auth::user()->role;

			if (str_contains($role,'admin')) { $role = 'admin'; } else { $role = 'user'; }

			$this->ping('{"id": '.Auth::user()->id.', "name": "'.Auth::user()->chathandle.'", "role": "'.$role.'"}');

			$date = date('Y-m-d H:i:s',$timestamp);

			$output = '{"messages": [';

			// Get Public Messages

			$publicmsg = Chat::where('receiver','=',$public)->where('timestamp','>',$date)->with('user')->get();

			$privatemsg = Chat::whereNested(function($query) {
				$query->where('receiver','=',Auth::user()->id);
				$query->orWhere('user_id','=',Auth::user()->id);
			})->where('timestamp','>',$date)->where('receiver','!=',$public)->where('checked','NOT LIKE','%['.Auth::user()->id.']%')->with('user')->get();

			foreach ($publicmsg as $message) {

					$id = $public;

					$scope = 'Public';

					$name = $message->user->chathandle;

					$msg = $message->message;

					if (in_array($message->user->id,$admins)) {
							$role = 'admin';
						} elseif (in_array($message->user->id,$bots)) {
							$role = 'system';
						} else { $role = 'user'; }

					$output .= '{"id":'.$id.',"name":"'.$name.'","scope":"'.$scope.'","msg":"'.$msg.'","role":"'.$role.'","timestamp":"'.strtotime($message->timestamp).'","msgid":"'.$message->id.'"},';
			}
			

			foreach ($privatemsg as $message) {

				if ($message->user_id != Auth::user()->id) {
				$id = $message->user_id;
				} else {
				$id = $message->receiver;
				}

				$name = $message->user->chathandle;

				if ($message->user_id == Auth::user()->id) {
				$scope = User::find($message->receiver)->chathandle;
				} else {
				$scope = $message->user->chathandle;
				}

				$msg = $message->message;

				if (in_array($message->user->id,$admins)) {
							$role = 'admin';
						} elseif (in_array($message->user->id,$bots)) {
							$role = 'system';
						} else { $role = 'user'; }

				$output .= '{"id":'.$id.',"name":"'.$name.'","scope":"'.$scope.'","msg":"'.$msg.'","role":"'.$role.'","timestamp":"'.strtotime($message->timestamp).'"},';
			}

			$users = $this->whosonline();

			$newuser = "[";
			foreach ($users as $user) {
				$newuser .= $user.',';
			}
			$newuser = rtrim($newuser,",");
			$newuser .= "]";

			$output = rtrim($output,",").'], "curtime": '.time().', "users": '.$newuser.'}';
			return $output;
		}
	}

	public function closechat($closeid) {
		$tables = Chat::where(function($query) use ($closeid) {
			$query->where('user_id','=',Auth::user()->id);
			$query->where('receiver','=',$closeid);
		})->orWhere(function($query) use ($closeid) {
			$query->where('user_id','=',$closeid);
			$query->where('receiver','=',Auth::user()->id);
		})->get();

		foreach ($tables as $table) {
			$table->checked = $table->checked."[".Auth::user()->id."]";
			$table->save();
		}
		return $tables;
	}

	public function sendmsg() {

		if(\App\Settings::isChatDisabled(Auth::user()->id)) { return; }

		if(Input::get('msg') == '/today') {
			$data = \App\Statistic::where('user_id','=',Auth::user()->id)->select(\DB::raw('count(DISTINCT ip) as clicks, SUM(IF(status=1,payout,0)) as payout, SUM(IF(status=1,1,0)) as leads'))->whereTimestamp(Carbon::today()->toDateString())->first();
			$clicks = formatnum($data->clicks);
			$leads = formatnum($data->leads);
			$payout = formatnum($data->payout,2);
		} elseif (Input::get('msg') == '/yesterday') {
			$data = \App\Statistic::where('user_id','=',Auth::user()->id)->select(\DB::raw('count(DISTINCT ip) as clicks, SUM(IF(status=1,payout,0)) as payout, SUM(IF(status=1,1,0)) as leads'))->whereTimestamp(Carbon::yesterday()->toDateString())->first();
			$clicks = formatnum($data->clicks);
			$leads = formatnum($data->leads);
			$payout = formatnum($data->payout,2);
		} elseif (Input::get('msg') == '/mtd') {
			$data = \App\Statistic::where('user_id','=',Auth::user()->id)->select(\DB::raw('count(DISTINCT ip) as clicks, SUM(IF(status=1,payout,0)) as payout, SUM(IF(status=1,1,0)) as leads'))->whereBetween('timestamp',[Carbon::today()->startOfMonth()->toDateString(),Carbon::today()->toDateString()])->first();
			$clicks = formatnum($data->clicks);
			$leads = formatnum($data->leads);
			$payout = formatnum($data->payout,2);
		} elseif (Input::get('msg') == '/alltime') {
			$data = \App\Statistic::where('user_id','=',Auth::user()->id)->select(\DB::raw('count(DISTINCT ip) as clicks, SUM(IF(status=1,payout,0)) as payout, SUM(IF(status=1,1,0)) as leads'))->first();
			$clicks = formatnum($data->clicks);
			$leads = formatnum($data->leads);
			$payout = formatnum($data->payout,2);
		}

		if (in_array(Input::get('msg'),array('/today','/yesterday','/mtd','/alltime'))) {
			$msg = new Chat;
		    $msg->user_id = 20;
		    $msg->message = "<b>".Auth::user()->chathandle."</b> has earned <b>$".$payout."</b> ".str_replace("/","",Input::get('msg'))." with <b>".$clicks."</b> clicks and <b>".$leads."</b> leads!";
		    $msg->receiver = 10;
		    $msg->timestamp = date('Y-m-d H:i:s',time());
		    $msg->save();

		} else {

		$msg = new Chat;
	    $msg->user_id = Auth::user()->id;
	    $msg->message = str_replace('"','\"',Input::get('msg'));
	    $msg->receiver = Input::get('to');
	    $msg->timestamp = date('Y-m-d H:i:s',time());
	    $msg->save();

	    }
	    return $this->chat(Input::get('curtime'));
	}

	public function whosonline() {
		 /* current hour and minute */
        $now = time();
        $min = date("i",$now);
        $hor = date("G",$now);

        /* redis keys to union, based on last $minutes */
        $keys = array();
        for($i = $min ; $i >= $min - $this->minutes; $i--) {
            $keys[] = "online:".$hor.":".$i; // define the key
        }

        $redis = Redis::connection(); // connect to redis at localhost
        $scmd = $redis->createCommand("sunion",$keys); // create the union with desired keys
        $online = $redis->executeCommand($scmd); // issue the sunion and grab the result
        
        return $online ; // array of online usernames
	}

	public function ping($user) {
		/* current hour:minute to make up the redis key */
        $now  = time();
        $min  = date("G:i",$now);
        $key  = "online:".$min;

        $redis = Redis::connection(); // connect to redis at localhost

        $redis->sadd($key,$user); // add the user to the set
        $ttl = $redis->ttl($key) ; // check if key has an expire
        if($ttl == -1) { // if it do not have, set it to $minutes + 1
            $redis->expire($key, 60);
        }
        
        return $this ;
	}

	public function deletechat($id) {
		Chat::find($id)->delete();
		return Redirect::to('admin');
	}

}
