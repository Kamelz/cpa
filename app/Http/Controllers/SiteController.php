<?php namespace App\Http\Controllers;
use Cache;
use View;
use Validator;
use Mail;
use Session;
use Input;
use Redirect;
use Hashids;
use Cookie;
use Carbon\Carbon;
use App\Settings;

class SiteController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Welcome Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders the "marketing page" for the application and
	| is configured to only allow guests. Like most of the other sample
	| controllers, you are free to modify or remove it as you desire.
	|
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		//$this->middleware('guest');
		$this->middleware('domain');
		$this->middleware('banipcountry');

		$gset = \App\Settings::generalsettings();

		$this->gset = $gset;

		View::share(['gset' => $gset]);
	}

	/**
	 * Show the application welcome screen to the user.
	 *
	 * @return Response
	 */
	public function index()
	{
		$users = \App\User::whereStatus('active')->whereRole('publisher')->count();
		$days = Carbon::create('2013', '11', '08', '0')->diffInDays();
		$offers = \App\Offer::whereActive(1)->count();
		return view('index')->with(compact('users','days','offers'));
	}

	public function video()
	{
		return view('_video');
	}

	public function publishers()
	{
		return view('publishers');
	}

	public function advertisers()
	{
		return view('advertisers');
	}

	public function faq()
	{
		return view('faq');
	}

	public function contact()
	{
		$successmsg = Session::get('success');
		return view('contactus')->with(compact('successmsg'));
	}

	public function terms()
	{
		return view('terms');
	}

	public function dmca() {
		return view('dmca');
	}

	public function about() {
		return view('about');
	}

	public function blog() {
		$posts = \App\Blog::wherePublished(1)->orderBy('created_at','DESC')->paginate(6);
		$recentposts = \App\Blog::wherePublished(1)->orderBy('created_at','DESC')->take(5)->get();
		return view('blog')->with(compact('posts','recentposts'));
	}

	public function viewblogpost($slug) {
		$post = \App\Blog::whereSlug($slug)->first();
		$recentposts = \App\Blog::wherePublished(1)->orderBy('created_at','DESC')->take(5)->get();
		return view('viewpost')->with(compact('post','recentposts'));
	}

	public function sendcontact() {
		$data = Input::all();

		$rules = array(
			'name' => 'required',
			'email' => 'required|email',
			'subject' => 'required',
			'message' => 'required',
			);

		$validator = Validator::make($data,$rules);

		if ($validator->passes()) {
				Mail::send('emails.contactus', array('name' => Input::get('name'), 'email' => Input::get('email'), 'subject' => Input::get('subject'), 'msg' => Input::get('message')), function($message)
				{
				    $message->to($this->gset->adminemail, 'AdGodMedia')->subject('New Contact Us Message');
				});

				return Redirect::to('contactus')->with('success','Your message has been sent.');
		}

		return Redirect::to('contactus')->withErrors($validator->errors())->withInput($data);
	}

	public function setreferralcookie($code) {

		$id = Hashids::decode($code);

		$cookie = Cookie::forever('ucref90', $id[0]);

		return Redirect::to('/')->withCookie($cookie);
	}

}
