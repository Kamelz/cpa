<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth;
use Input;
use Cache;
use Redirect;
use Session;
use View;
use Mail;
use Validator;
use Queue;
use DB;
use Carbon\Carbon;
use PragmaRX\Google2FA\Contracts\Google2FA;

use Illuminate\Http\Request;

class SuperAdminController extends Controller {

	public function __construct()
	{
		$this->middleware('domain');
		$this->middleware('auth');
		$this->middleware('superadmin');

		if (Auth::check()) {

		//$this->unreads = \App\Message::whereNotIn('id',unserialize(Auth::user()->read_msgs))->whereIn('receiver',array('Public',Auth::user()->chathandle))->get();
		$this->unreads = \App\Message::messages()->filter(function($message) {
			$reads = unserialize(Auth::user()->read_msgs);
			if(!in_array($message->id,$reads) && in_array($message->receiver,array('Public',Auth::user()->chathandle))) { return true; }
		});
		$gset = \App\Settings::generalsettings();
		$this->gset = $gset;

		if(Auth::user()->isAdmin()) { 
			$role = "admin"; 
		} else { $role = 'super_admin'; }
		
		$today = \App\Statistic::select(DB::raw('count(DISTINCT ip) as clicks, SUM(IF(status=1,payout,0)) as payout, SUM(IF(status=1,1,0)) as leads'))->whereTimestamp(Carbon::today()->toDateString())->first();

		View::share(['unreads' => $this->unreads,'gset' => $gset, 'today' => $today, 'role' => $role]);
		}
	}

	public function administrators() {
		$admins = \App\User::whereIn('role',array('admin','super_admin'))->get();

		return view('admin.aadministrators')->with(compact('admins'));
	}

	public function viewadmin($id) {
		$user = \App\User::whereId($id)->whereIn('role',array('admin','super_admin'))->with('user_profile')->first();
		$countries = \App\Settings::countries();

		return view('admin.ajax.viewadmin')->with(compact('user','countries'));
	}

	public function editadmin() {
		if(Input::get('name') == 'disablechat') {
			\App\Settings::changeChatDisabled(Input::get('pk'),Input::get('value'));
			return;
		} elseif(Input::get('name') == 'customrate') {
			if(Input::get('value') < 1 || Input::get('value') > 100) {
				return response('Invalid Number',500);
			}
			\App\Settings::setCustomPayout(Input::get('pk'),Input::get('value'));
			return;
		} elseif(Input::get('name') == 'chathandle') {

			$user = \App\User::whereChathandle(Input::get('value'))->first();
			if(count($user) > 0) { return response('Name already taken',500); }

			$muser = \App\User::find(Input::get('pk'));

			//$details = array(['oldname' => $userid->chathandle, 'newname' => Input::get('value')]);
			//Queue::push(new \App\Commands\UserManager($userid->id,'changechathandle',$details));

			$oldname = $muser->chathandle;
			$newname = Input::get('value');
			$userid = $muser->id;

			\App\User::whereId($userid)->whereChathandle($oldname)->update(['chathandle' => $newname]);
			\App\Settings::where('user_id','=',$userid)->whereUser($oldname)->update(['user' => $newname]);
			\App\PaymentRequest::where('user_id','=',$userid)->whereUser($oldname)->update(['user' => $newname]);
			\App\TicketMessage::where('user_id','=',$userid)->whereUser($oldname)->update(['user' => $newname]);
			\App\DownloadLog::whereUser($oldname)->update(['user' => $newname]);

			return;
		}

		if(in_array(Input::get('name'),array('payment_cycle','address','city','state','zip','country','skype'))) {
			\App\User_Profile::where('user_id','=',Input::get('pk'))->update([Input::get('name') => Input::get('value')]);
		} else {
			\App\User::whereId(Input::get('pk'))->update([Input::get('name') => Input::get('value')]);
		}
	}

	public function createadmin() {

		if(Input::get('secret_token') != $this->gset->secrettoken) {
			$msg[] = array('title' => 'Whoops!', 'message' => 'Wrong secret token'); return $msg;
		}

		$user = \App\User::whereId(Input::get('user_id'))->whereChathandle(Input::get('user'))->first();

		if (count($user) == 0) { $msg[] = array('title' => 'Whoops!', 'message' => 'There is no user in this criteria'); return $msg; }

		$user->assignRole(101);
		$user->role = 'admin';
		$user->save();

		Cache::forget('admins');

		$msg[] = array('title' => 'Success', 'message' => Input::get('user').' is now admin'); return $msg;
	}

	public function removeadmin() {
		$user = \App\User::whereId(Input::get('user_id'))->first();

		$user->revokeRole(101);
		$user->role = 'publisher';
		$user->save();

		Cache::forget('admins');

		$msg[] = array('title' => 'Success', 'message' => $user->chathandle.' removed from admin'); return $msg;
	}

	public function sendqrcode(Google2FA $google2fa) {

		if(Input::get('secret_token') != $this->gset->secrettoken) {
			$msg[] = array('title' => 'Whoops!', 'message' => 'Wrong secret token'); return $msg;
		}

		$user = \App\User::whereId(Input::get('user_id'))->whereChathandle(Input::get('user'))->whereRole('admin')->first();

		if (count($user) == 0) { $msg[] = array('title' => 'Whoops!', 'message' => 'User is not admin or details incorrect'); return $msg; }

		if(empty($user->google2fa_secret)) {
			$user->google2fa_secret = $google2fa->generateSecretKey(32);
			$user->save();
		}

		$google2fa_url = $google2fa->getQRCodeGoogleUrl('AdGodMedia',$user->email,$user->google2fa_secret);

		$rand = str_random(6);

		Cache::put($rand,$google2fa_url,120);

		Mail::send('emails.twofactor', ['url' => url('welcome/'.$rand), 'name' => $user->name, 'user' => $user], function($message) use ($user)
		{
		    $message->to($user->email)->subject('You\'ve been promoted as AdGodMedia Admin');
		});

		$msg[] = array('title' => 'Success', 'message' => 'QR Code Sent to user'); return $msg;

	}

	public function createblogpost() {
		$post = new \App\Blog;
		$post->created_at = date('Y-m-d H:i:s');
		return view('admin.acreateblogpost')->with(compact('post'));
	}

	public function postcreateblogpost() {
		$data = Input::all();
		$rules = array(
			'title' =>'required',
			'slug' => 'required',
			'date' => 'required',
			'content' => 'required',
			'tags' => 'required'
			);

		$validator = \Validator::make($data,$rules);

		if ($validator->passes()) {

			if(Input::has('post_id')) {
				$post = \App\Blog::whereId(Input::get('post_id'))->first();			
			} else {
				$post = new \App\Blog;
			}
			$post->title = Input::get('title');
			$post->slug = Input::get('slug');
			$post->author = Auth::user()->chathandle;
			$post->content = Input::get('content');
			$post->tags = Input::get('tags');
			$post->created_at = date('Y-m-d H:i:s', strtotime(Input::get('date')));
			$post->save();

			$msg[] = array('title' => 'Success', 'message' => 'Post saved.'); return $msg;
		}

		$messages = $validator->messages();

	    foreach($messages->all() as $message) {
	    	$msg[] = array('title' => 'xhr', 'message' => str_replace('_',' ',$message));
	    }
	    return $msg;
	}

	public function blogposts() {

		$posts = \App\Blog::orderBy('created_at','DESC')->paginate(25);
		return view('admin.ablogposts')->with(compact('posts'));
	}

	public function editpost($id) {
		$post = \App\Blog::find($id);
		return view('admin.acreateblogpost')->with(compact('post'));
	}

	public function togglepoststatus($id) {
		$post = \App\Blog::find($id);
		$post->published = 1 - $post->published;
		$post->save();

		return Redirect::to('admin/blogposts');
	}

	public function deleteblogpost($id) {
		\App\Blog::find($id)->delete();
		return Redirect::to('admin/blogposts');
	}

	public function loginlog() {
		$logs = \App\LoginLog::orderBy('timestamp','DESC')->paginate(25);

		return view('admin.aloginlogs')->with(compact('logs'));
	}

	public function generalsettings() {
		$successmsg = Session::get('success');
		return view('admin.ageneralsettings')->with(compact('successmsg'));
	}

	public function editgeneralsettings() {

		$data = Input::all();
		$rules = array(
			'description' => 'required|alphanumspace',
			'keywords' => 'required',
			'admin_email' => 'required|email',
			'paypal_email' => 'required|email',
			'min_cashout' => 'required|numeric',
			'quick_payout_fee' => 'required|numeric',
			'offer_rate' => 'required|numeric',
			'referral_rate' => 'required|numeric',
			'secret_token' => 'required|in:'.$this->gset->secrettoken,
			'total_payments' => 'required|numeric'
			);

		$validator = Validator::make($data,$rules);

		if($validator->passes()) {
			\App\Settings::whereScope('generalsettings')->whereOption('description')->update(['value' => Input::get('description')]);
			\App\Settings::whereScope('generalsettings')->whereOption('keywords')->update(['value' => Input::get('keywords')]);
			\App\Settings::whereScope('generalsettings')->whereOption('paypalemail')->update(['value' => Input::get('paypal_email')]);
			\App\Settings::whereScope('generalsettings')->whereOption('mincashout')->update(['value' => Input::get('min_cashout')]);
			\App\Settings::whereScope('generalsettings')->whereOption('quickpayout')->update(['value' => Input::get('quick_payout')]);
			\App\Settings::whereScope('generalsettings')->whereOption('quickpayoutfee')->update(['value' => Input::get('quick_payout_fee')]);
			\App\Settings::whereScope('generalsettings')->whereOption('offerrate')->update(['value' => Input::get('offer_rate')]);
			\App\Settings::whereScope('generalsettings')->whereOption('referralrate')->update(['value' => Input::get('referral_rate')]);
			\App\Settings::whereScope('generalsettings')->whereOption('autoapprove')->update(['value' => Input::get('auto_approve')]);
			\App\Settings::whereScope('generalsettings')->whereOption('totalpayments')->update(['value' => Input::get('total_payments')]);

			Cache::forget('gset');

			return Redirect::to('admin/settings/general')->with('success','Settings Saved!');

		}

		return Redirect::to('admin/settings/general')->withErrors($validator->errors());
	}

	public function apisettings() {
		$networks = \App\Network::all();
		$successmsg = Session::get('success');
		return view('admin.aapisettings')->with(compact('networks','successmsg'));
	}

	public function editapisettings() {

		$networks = \App\Network::all();

		$data = Input::all();

		$rules = array();
		$rules['secret_token'] = 'required|in:'.$this->gset->secrettoken;
		foreach ($networks as $network) {
			$rules[strtolower($network->name).'_pub_id'] = 'required';
			$rules[strtolower($network->name).'_api_key'] = 'required';
			$rules[strtolower($network->name).'_offer_url'] = 'required';
		}

		$validator = Validator::make($data,$rules);

		if($validator->passes()) {
			foreach($networks as $network) {
				\App\Network::whereName($network->name)->update(['pubid' => Input::get(strtolower($network->name).'_pub_id'), 'apikey' => Input::get(strtolower($network->name).'_api_key'), 'offer_url' => Input::get(strtolower($network->name).'_offer_url'), 'active' => Input::get(strtolower($network->name).'_status')]);
			}
			return Redirect::to('admin/settings/api')->with('success','Settings Saved!');
		}

		return Redirect::to('admin/settings/api')->withErrors($validator->errors());
	}

	public function foffers() {
		$networks = \App\Network::all();
		return view('admin.afetchoffers')->with(compact('networks'));
	}

	public function fetchoffers($network) {
		Cache::put('fetch_'.$network,'Starting Up..',20);

		$this->dispatch(
        new \App\Commands\FetchOffers($network)
 		);

		//$queue = Queue::push(new \App\Commands\FetchOffers($network));
		return $queue;
	}

	public function getfetch($network) {
		return Cache::get('fetch_'.$network);
	}

	public function setminepc() {
		\App\Network::whereId(Input::get('pk'))->update(['minepc' => Input::get('value')]);
		return 'success';
	}

	public function deleteoffers($network) {
		\App\Offer::whereNetwork($network)->delete();
		\App\Network::whereName($network)->update(['numOffers' => 0]);
		return Redirect::to('admin/fetchoffers');
	}

	public function togglenetwork() {
		\App\Network::whereId(Input::get('id'))->update(['active' => Input::get('status')]);

		Cache::forget('activenetworks');
	}

	public function banusers() {

		$successmsg = Session::get('success');
		$error = Session::get('error');

		$bans = \App\Settings::whereScope('ban_user')->get();

		return view('admin.abanusers')->with(compact('bans','successmsg','error'));
	}

	public function banuser() {

		$user = \App\User::whereChathandle(Input::get('user'))->first();

		if(count($user) == 0) {
			return Redirect::to('admin/banusers')->with('error','This user doesn\'t exist');
		}

		if(in_array($user->role,array('bot','super_admin','admin'))) {
			return Redirect::to('admin/banusers')->with('error','This user cannot be banned.');
		}

		$ban = new \App\Settings;
		$ban->scope = 'ban_user';
		$ban->user_id = $user->id;
		$ban->user = $user->chathandle;
		$ban->option = Input::get('reason');
		$ban->value = date('Y-m-d H:i:s');
		$ban->save();

		$user->status = 'banned';
		$user->save();

		Cache::forget('banneduserids');

		return Redirect::to('admin/banusers')->with('success','User added to ban list');
	}

	public function removebanuser($id) {
		$set = \App\Settings::whereScope('ban_user')->whereId($id)->first();
		\App\User::whereId($set->user_id)->update(['status' => 'active']);
		$set->delete();
		Cache::forget('banneduserids');
		return Redirect::to('admin/banusers');
	}

	public function bancountries() {
		$successmsg = Session::get('success');
		$banc = \App\Settings::whereScope('countryban')->get();
		$countries = \App\Settings::countries();
		return view('admin.abancountries')->with(compact('banc','countries','successmsg'));
	}

	public function bancountry() {

		$countries = \App\Settings::countries();

		$ban = new \App\Settings;
		$ban->scope = 'countryban';
		$ban->option = Input::get('country');
		$ban->value = $countries->where('option',Input::get('country'))->first()->value;
		$ban->alpha = date('Y-m-d H:i:s');
		$ban->save();

		Cache::forget('countryban');

		return Redirect::to('admin/bancountries')->with('success','Country added to block list');
	}

	public function removebancountry($id) {
		\App\Settings::whereScope('countryban')->whereId($id)->delete();

		Cache::forget('countryban');

		return Redirect::to('admin/bancountries');
	}

	public function banips() {
		$successmsg = Session::get('success');
		$banips = \App\Settings::whereScope('ipban')->get();
		return view('admin.abanip')->with(compact('banips','successmsg'));
	}

	public function banip() {
		$ban = new \App\Settings;
		$ban->scope = 'ipban';
		$ban->option = Input::get('ip');
		$ban->value = date('Y-m-d H:i:s');
		$ban->save();

		Cache::forget('ipban');

		return Redirect::to('admin/banip')->with('success','IP added to ban list');
	}

	public function removebanip($id) {

		\App\Settings::whereScope('ipban')->whereId($id)->delete();

		Cache::forget('ipban');

		return Redirect::to('admin/banip');
	}

	public function networkenable() {
		$networks = \App\Network::all();
		$error = Session::get('error');
		$successmsg = Session::get('success');
		$enabled = \App\Settings::whereScope('network_enable')->get();
		return view('admin.anetworkenable')->with(compact('networks','enabled','error','successmsg'));
	}

	public function postnetworkenable() {

		$user = \App\User::whereChathandle(Input::get('user'))->first();

		if(count($user) == 0) {
			$msg[] = array('title' => 'Whoops!', 'message' => 'This user doesn\'t exist'); return $msg;
		}

		\App\Settings::firstOrCreate(['scope' => 'network_enable', 'user_id' => $user->id, 'user' => Input::get('user'), 'option' => Input::get('network')]);

		$msg[] = array('title' => 'Success', 'message' => 'Network Enabled'); return $msg;

	}

	public function deletenetworkenable($id)
	{
		\App\Settings::whereScope('network_enable')->whereId($id)->delete();

		return Redirect::to('admin/networkenable');
	}

	public function subidblock() {
		$successmsg = Session::get('success');
		$error = Session::get('error');
		$networks = \App\Network::all();
		$blocks = \App\Settings::whereScope('subid_block')->get();
		return view('admin.asubidblock')->with(compact('blocks','networks','successmsg','error'));
	}

	public function postsubidblock() {

		$data = Input::all();

		$rules = array(
			'offer_id' => 'required|numeric'
			);

		$validator = Validator::make($data,$rules);

		$user = \App\User::whereChathandle(Input::get('user'))->first();

		if(count($user) == 0) {
			$msg[] = array('title' => 'Whoops!', 'message' => 'This user doesn\'t exist'); return $msg;
		}

		if ($validator->passes()) {
			\App\Settings::firstOrCreate(['scope' => 'subid_block', 'user_id' => $user->id, 'user' => Input::get('user'), 'option' => Input::get('offer_id'), 'value' => Input::get('network')]);
			$msg[] = array('title' => 'Success', 'message' => 'Subid Blocked'); return $msg;
		}

		$messages = $validator->messages();

	    foreach($messages->all() as $message) {
	    	$msg[] = array('title' => 'xhr', 'message' => str_replace('_',' ',$message));
	    }
	    return $msg;
	}

	public function deletesubidblock($id)
	{
		\App\Settings::whereScope('subid_block')->whereId($id)->delete();

		return Redirect::to('admin/subidblock');
	}

	public function healthcheck()
	{
		$postmax = ini_get('post_max_size');
		$uploadmax = ini_get('upload_max_filesize');
		Cache::put('testqueue',0,20);
		$queue = Queue::push(new \App\Commands\LeadManager('test',0));
		return view('admin.ahealthcheck')->with(compact('postmax','uploadmax','queue'));
	}

	public function testqueue()
	{
		Queue::push(new \App\Commands\LeadManager('test',0));
		return Cache::get('testqueue');
	}

}
