<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Input;
use Redis;
use Location;
use Request;
use Hashids;
use Session;
use Redirect;
use Config;
use Queue;
use Agent;
use Validator;
use Cache;
use Carbon\Carbon;

class TemplateController extends Controller {

	public function __construct() {
		$this->gset = \App\Settings::generalsettings();
	}

	public function offers($userid,$location,$num,$skip = 0) {

		$disabled = \App\Settings::disabledoffers();
		$disabled_user = \App\Settings::whereUserId($userid)->whereScope('offers')->whereOption('disabled')->lists('value');
		$subidblocks = \App\Settings::getSubIDBlocks($userid);
		$disabled = array_merge($disabled,$disabled_user);
		$disabled = array_merge($disabled,$subidblocks);

		$favorite = \App\Settings::favoriteoffers();
		$favorite_user = \App\Settings::where('user_id','=',$userid)->whereScope('offers')->whereOption('favorite')->lists('value');
		$favorite = array_merge($favorite,$favorite_user);
		$favorite = array_merge($favorite,[0]);
		$favorite = implode (",", $favorite);

		$activenetworks = \App\Network::getActiveNetworks();
		$nenabled = \App\Settings::getNetworkEnabled($userid);
		$nenabled = array_merge($activenetworks,$nenabled);

		$minpayout = \App\Settings::where('user_id','=',$userid)->whereScope('minpayout')->whereOption($location)->first();

		if(!is_null($minpayout)) { $minpayout = $minpayout->value + ($minpayout->value * ((100 - $this->gset->offerrate) / 100)); } else { $minpayout = ''; }

		if(Agent::isAndroidOS()) {
			$ua = array('mobile','android');
		} elseif(Agent::isIOS()) {
			$ua = array('ios','mobile');
		} elseif(Agent::isMobile() || Agent::isTablet()) {
			$ua = array('mobile');
		} else {
			$ua = array('desktop');
		}

		$ua = array('mobile');

		$ua = implode(',',$ua);

		$offers = \App\Offer::ofNotID($disabled)->ofNetworkIn($nenabled)->ofMinPayout($minpayout)->ofCountry($location)->whereActive(1)->orderBy(\DB::raw('FIELD (ID,'.$favorite.')'), 'DESC')->orderBy(\DB::raw('FIELD (device,"'.$ua.'","desktopmobile")'), 'DESC')->skip($skip)->take($num)->get();

		return $offers;
	}

	public function moreoffers() {

		$offers = $this->offers(Input::get('user'),Input::get('country'),Input::get('num'),Input::get('skip') * Input::get('num'));

		return response($offers,200)->header('Access-Control-Allow-Origin','*');
	}

	// Common handler for files, links & notes
	public function index($code)
	{
		$uri = Request::segment(1);
		if ($uri == 'file') {
			$content = \App\File::whereCode($code)->first();
			if (count($content) == 0) {
				$title = "404";
				$message = "The specified file cannot be found";
			    return view('block')->with(compact('title','message'));
			}
			$content->scope = 'file';
			$cname = $content->filename.".".$content->ext;
		} elseif($uri == 'link') {
			$content = \App\Link::whereCode($code)->first();
			if (count($content) == 0) {
				$title = "404";
				$message = "The specified link cannot be found";
			    return view('block')->with(compact('title','message'));
			}
			$content->scope = 'link';
			$cname = $content->title;
		} elseif($uri == 'note') {
			$content = \App\Note::whereCode($code)->first();
			if (count($content) == 0) {
				$title = "404";
				$message = "The specified note cannot be found";
			    return view('block')->with(compact('title','message'));
			}
			$content->scope = 'note';
			$cname = $content->title;
		} else {
			return Redirect::to('');
		}

		if(isset($_SERVER['HTTP_REFERER'])) {

		$nr = \App\SiteReferrer::whereUrl($_SERVER['HTTP_REFERER'])->first();

		if (count($nr) == 0 && parse_url($_SERVER['HTTP_REFERER'], PHP_URL_HOST) !=  $_SERVER['HTTP_HOST']) {
		$nre = new \App\SiteReferrer;
		$nre->url = $_SERVER['HTTP_REFERER'];
		$nre->save();
		$site_referrer = $nre->id;
		} elseif (count($nr) > 0) {
		$site_referrer = $nr->id;
		} else {
			$site_referrer = 0;
		}

		} else { $site_referrer = 0; }

		$location = Location::get();
		$sessid = Session::getId();

		$ip = ip2long($location->ip);

		$session = \App\Session::whereSessid($sessid)->whereCtype($uri)->whereCid($content->id)->first();

		if(count($session) < 1) {
			$session = new \App\Session;
			$session->sessid = $sessid;
			$session->ctype = $uri;
			$session->cid = $content->id;
			$session->ip = ip2long($location->ip);
			$session->timestamp = date('Y-m-d H:i:s');
			$session->save();
		}

		$offers = $this->offers($content->user_id,$location->isoCode,5);

		if ($uri == 'file') { $template = $content->template; } else { $template = 'default'; }

		$template = Input::get('template',$template);

		return view('templates.'.$template.'.index')->with(compact('content','cname','offers','site_referrer','location','sessid','ip'));
	}

	public function gotooffer() {
		$offer = \App\Offer::whereCampid(Input::get('offer'))->first();

		$stat = \App\Statistic::where('user_id','=',Input::get('pub'))->whereCampid(Input::get('offer'))->whereNetwork(Input::get('nt'))->whereIp(Input::get('ip'))->where('locker_type','=',Input::get('locker'))->where('locker_id','=',Input::get('lid'))->whereTimestamp(Carbon::today())->first();

		$ua = $_SERVER['HTTP_USER_AGENT'];

		if(count($stat) == 0) {
			$st = new \App\Statistic;
			$st->user_id = Input::get('pub');
			$st->campid = Input::get('offer');
			$st->offname = $offer->title;
			$st->network = Input::get('nt');
			$st->country = Input::get('cnty');
			$st->locker_type = Input::get('locker');
			$st->locker_id = Input::get('lid');
			$st->payout = $offer->payout * ($this->gset->offerrate / 100);
			$st->ip = Input::get('ip');
			$st->ua = $ua;
			$st->site_referrer_id = Input::get('site_referrer');
			$st->timestamp = Carbon::now();
			$st->save();
			$id = $st->id;
		} else { $id = $stat->id; }

		\App\Session::whereSessid(Input::get('session'))->whereCtype(Input::get('locker'))->whereCid(Input::get('lid'))->update(['statid' => $id]);

		$url = str_replace('%subid%',Input::get('session'),$offer->url);

		return Redirect::to($url);
	}
	
	public function go($offer, $pub) {
		$offer = \App\Offer::whereCampid($offer)->first();
		//var_dump($offer);
		$stat = \App\Statistic::where('user_id','=',$pub)->whereCampid($offer)->whereNetwork(Input::get('nt'))->whereIp(Input::get('ip'))->where('locker_type','=',Input::get('locker'))->where('locker_id','=',Input::get('lid'))->whereTimestamp(Carbon::today())->first();

		$ua = $_SERVER['HTTP_USER_AGENT'];
		$ip = $_SERVER["HTTP_CF_CONNECTING_IP"];

		if(count($stat) == 0) {
			$st = new \App\Statistic;
			$st->user_id = $pub;
			$st->campid = $offer;
			$st->offname = $offer->title;
			$st->network = $offer->network;
			$st->country = $offer->countries;
			$st->locker_type = "direct";
			$st->locker_id = 0;
			$st->payout = $offer->payout * ($this->gset->offerrate / 100);
			$st->ip = $ip;
			$st->ua = $ua;
			$st->site_referrer_id = 0;
			$st->timestamp = Carbon::now();
			$st->save();
			$id = $st->id;
		} else { $id = $stat->id; }

		\App\Session::whereSessid(Input::get('session'))->whereCtype(Input::get('locker'))->whereCid(Input::get('lid'))->update(['statid' => $id]);

		$url = str_replace('%subid%',Input::get('session'),$offer->url);

		return Redirect::to($url);
	}	

	public function gotocontent() {
		$session = \App\Session::whereSessid(Input::get('session'))->orderBy('timestamp','DESC')->first();

		if($session->complete == 1) {
			if($session->ctype == 'file') {

				$file = \App\File::whereId($session->cid)->first();

				// Copy file to temp
				$contents = \Storage::disk('files')->get($file->hashname);
				\Storage::disk('tmp')->put($file->hashname,$contents);

				$filedl = storage_path()."/tmp/".$file->hashname;

				$user = \App\User::find($file->user_id);

				$log = \App\DownloadLog::where('user_id','=',$file->user_id)->whereUser($user->chathandle)->where('file_id','=',$file->id)->whereIp(Input::get('ip'))->first();

				if(count($log) == 0) {
					$lg = new \App\DownloadLog;
					$lg->user_id = $file->user_id;
					$lg->user = $user->chathandle;
					$lg->file_id = $file->id;
					$lg->filename = $file->filename.".".$file->ext;
					$lg->ip = Input::get('ip');
					$lg->country = Input::get('country');
					$lg->timestamp = Carbon::now();
					$lg->save();
				}

				// Send the file and delete from temp
				return response()->download($filedl,$file->filename.".".$file->ext)->deleteFileAfterSend(true);

			} elseif ($session->ctype == 'link') {

				$link = \App\Link::whereId($session->cid)->first();

				return Redirect::to($link->link);

			} elseif ($session->ctype == 'note') {

				$note = \App\Note::whereId($session->cid)->first();

				return '<h4>'.$note->title.'</h4>'.$note->note;
			}
		} else {
			return 'You are not authorized to get this content';
		}
	}

	public function gateway($id)
	{
		$gateway = \App\Gateway::whereCode($id)->first();

		if(isset($_SERVER['HTTP_REFERER'])) {

		$nr = \App\SiteReferrer::whereUrl($_SERVER['HTTP_REFERER'])->first();

		if (count($nr) == 0 && parse_url($_SERVER['HTTP_REFERER'], PHP_URL_HOST) !=  $_SERVER['HTTP_HOST']) {
		$nre = new \App\SiteReferrer;
		$nre->url = $_SERVER['HTTP_REFERER'];
		$nre->save();
		$site_referrer = $nre->id;
		} elseif (count($nr) > 0) {
		$site_referrer = $nr->id;
		} else {
			$site_referrer = 0;
		}

		} else { $site_referrer = 0; }

		$location = Location::get();
		$sessid = Session::getId();

		$ip = ip2long($location->ip);

		$session = \App\Session::whereSessid($sessid)->whereCtype('gateway')->whereCid($gateway->id)->first();

		if(count($session) < 1) {
			$session = new \App\Session;
			$session->sessid = $sessid;
			$session->ctype = 'gateway';
			$session->cid = $gateway->id;
			$session->ip = ip2long($location->ip);
			$session->timestamp = date('Y-m-d H:i:s');
			$session->save();
		}

		$offers = $this->offers($gateway->user_id,$location->isoCode,$gateway->numOffers);

		$num = $gateway->numOffers;

		$contents = \View::make('gateway.index')->with(compact('gateway','location','sessid','ip','offers','site_referrer','num'));
		$response = \Response::make($contents);
		$response->header('Content-Type', 'application/javascript');
		return $response;
	}

	public function gatewaycss($id) {
		$gateway = \App\Gateway::whereId($id)->first();
		$css = unserialize($gateway->css);

		$safefonts = array('Georgia','Palatino Linotype','Times New Roman','Arial','Arial Black','Comic Sans MS','Impact','Lucida Sans Unicode','Tahoma','Trebuchet MS','Verdana','Courier New','Lucida Console');

		$gfonts = [];

		if (!in_array($css['title_font'],$safefonts)) { array_push($gfonts,$css['title_font']); }
		if (!in_array($css['instruction_font'],$safefonts)) { array_push($gfonts,$css['instruction_font']); }
		if (!in_array($css['offers_font'],$safefonts)) { array_push($gfonts,$css['offers_font']); }

		$contents = \View::make('gateway.css')->with(compact('gateway','css','gfonts'));
		$response = \Response::make($contents);
		$response->header('Content-Type', 'text/css');
		return $response;
	}

	public function completioncheck($session) {
		$this->pingvisitor(Input::get('user'),'{"country": "'.Input::get('country').'", "ip": "'.Input::get('ip').'"}');
		$this->pingcontent(Input::get('user'),'{"content": "'.Input::get('content').'", "ip": "'.Input::get('ip').'"}');

		$visitor = \App\Session::whereSessid($session)->whereCtype(Input::get('ctype'))->whereCid(Input::get('id'))->first();

		return response($visitor->complete,200)->header('Access-Control-Allow-Origin','*');
	}

	public function getvisitors() {
		 /* current hour and minute */
        $now = time();
        $min = date("i",$now);
        $hor = date("G",$now);

        /* redis keys to union, based on last $minutes */
        $keys = array();
        for($i = $min ; $i >= $min - $this->minutes; $i--) {
            $keys[] = "visitor".Auth::user()->id.":".$hor.":".$i; // define the key
        }

        $redis = Redis::connection(); // connect to redis at localhost
        $scmd = $redis->createCommand("sunion",$keys); // create the union with desired keys
        $online = $redis->executeCommand($scmd); // issue the sunion and grab the result
        
        return $online ; // array of online usernames
	}

	public function getcontent() {
		 /* current hour and minute */
        $now = time();
        $min = date("i",$now);
        $hor = date("G",$now);

        /* redis keys to union, based on last $minutes */
        $keys = array();
        for($i = $min ; $i >= $min - $this->minutes; $i--) {
            $keys[] = "content".Auth::user()->id.":".$hor.":".$i; // define the key
        }

        $redis = Redis::connection(); // connect to redis at localhost
        $scmd = $redis->createCommand("sunion",$keys); // create the union with desired keys
        $online = $redis->executeCommand($scmd); // issue the sunion and grab the result
        
        return $online ; // array of online usernames
	}

	public function pingvisitor($id, $user) {
		/* current hour:minute to make up the redis key */
        $now  = time();
        $min  = date("G:i",$now);
        $key  = "visitor".$id.":".$min;

        $redis = Redis::connection(); // connect to redis at localhost

        $redis->sadd($key,$user); // add the user to the set
        $ttl = $redis->ttl($key) ; // check if key has an expire
        if($ttl == -1) { // if it do not have, set it to $minutes + 1
            $redis->expire($key, 60);
        }
        
        return $this ;
	}

	public function pingcontent($id, $user) {
		/* current hour:minute to make up the redis key */
        $now  = time();
        $min  = date("G:i",$now);
        $key  = "content".$id.":".$min;

        $redis = Redis::connection(); // connect to redis at localhost

        $redis->sadd($key,$user); // add the user to the set
        $ttl = $redis->ttl($key) ; // check if key has an expire
        if($ttl == -1) { // if it do not have, set it to $minutes + 1
            $redis->expire($key, 60);
        }
        
        return $this ;
	}

	public function reportcontent() {

		$data = Input::all();

		$rules = array(
			'name' => 'required',
			'email' => 'required|email',
			'message' => 'required',
			);

		$validator = Validator::make($data,$rules);

		if($validator->passes()) {

			$report = new \App\FileReport;
			$report->locker_id = Input::get('lockerid');
			$report->locker_type = Input::get('lockertype');
			$report->locker_name = Input::get('lockername');
			$report->name = Input::get('name');
			$report->email = Input::get('email');
			$report->message = Input::get('message');
			$report->ip = Input::get('ip');
			$report->country = Input::get('country');
			$report->timestamp = Carbon::now();
			$report->save();

			$msg[] = array('title' => 'Success', 'message' => 'Your message has been sent.'); return $msg;

		}

		$messages = $validator->messages();

		foreach($messages->all() as $message) {
	    	$msg[] = array('title' => 'Whoops!', 'message' => $message);
	    }
	    return $msg;
	}

	public function postback($nid,$secret) {

		$network = \App\Network::find($nid);

		if ($secret == Config::get('postback.'.$network->name)) {

			if ($network->name == 'Adworkmedia') {
				$campid = Input::get('campaign_id');
				$payout = Input::get('commission');
				$sub = Input::get('sid');
				$status = Input::get('status',1);
			} elseif ($network->name == 'Adludum') {
				$campid = Input::get('oid');
				$payout = Input::get('payout');
				$sub = Input::get('sid');
				$status = Input::get('status',1);
			} else {
				$campid = Input::get('campid');
				$payout = Input::get('payout');
				$sub = Input::get('subx');
				$status = Input::get('status',1);
			}

			$session = \App\Session::whereSessid($sub)->first();
			if(count($session) == 0) { return 'Invalid Session'; }
			$session->complete = 1;
			$session->save();

		} else { return 'Invalid parameters'; }

		$payout = abs($payout);

		if($status == 1) {
			$this->creditlead($session->statid,$campid,$payout,$sub);
			return 200;
		} else {
			$this->reverselead($session->statid,$campid,$payout,$sub);
			return 200;
		}
	}

	public function creditlead($statid,$campid,$payout,$sub) {

		$gset = \App\Settings::generalsettings();

		// Add statistics
		$stat = \App\Statistic::find($statid);

		$pubrate = \App\Settings::getCustomPayout($stat->user_id) + Cache::get('promotion_'.$stat->user_id,0);

		$stat->status = 1;
		$stat->payout = $payout * ($pubrate / 100);
		$stat->save();

		// Add to balance
		$user = \App\User::find($stat->user_id);
		$user->balance = $user->balance + ($payout * ($pubrate / 100));
		$user->save();

		// Credit referral
		if($user->referred_by != 0) {

			$refer = \App\User::find($user->referred_by);

			$referlog = \App\ReferLog::whereStatid($stat->id)->get();

			if(count($referlog) == 0) {

				$rl = new \App\ReferLog;
				$rl->statid = $stat->id;
				$rl->user_id = $refer->id;
				$rl->referral_id = $user->id;
				$rl->payout = $stat->payout * ($gset->referralrate / 100);
				$rl->timestamp = date('Y-m-d H:i:s');
				$rl->save();
			}

			$adminrate = $gset->offerrate + $gset->referralrate;

		} else {
			$adminrate = $gset->offerrate;
		}

		// Credit admin
		$ae = \App\AdminEarning::whereStatid($stat->id)->get();

		if(count($ae) == 0) {
			$aen = new \App\AdminEarning;
			$aen->statid = $stat->id;
			$aen->user_id = $stat->user_id;
			$aen->payout = $payout * ((100 - $adminrate) / 100);
			$aen->campid = $stat->campid;
			$aen->offname = $stat->offname;
			$aen->network = $stat->network;
			$aen->locker_id = $stat->locker_id;
			$aen->locker_type = $stat->locker_type;
			$aen->country = $stat->country;
			$aen->timestamp = date('Y-m-d H:i:s');
			$aen->save();
		}

		// Recount offer stats

		$offerstats = \App\Statistic::select(\DB::raw('count(DISTINCT ip) as clicks, SUM(IF(status=1,payout,0)) as payout, SUM(IF(status=1,1,0)) as leads, timestamp'))->whereCampid($stat->campid)->whereNetwork($stat->network)->first();

		$soffer = \App\Offer::whereCampid($stat->campid)->whereNetwork($stat->network)->first();

		$conv = round2d(division($offerstats->clicks,$offerstats->leads),2);
		$epc = round2d(division(($soffer->payout * $offerstats->leads),$offerstats->clicks),2);

		$soffer->clicks = $offerstats->clicks;
		$soffer->leads = $offerstats->leads;
		$soffer->conv = $conv;
		$soffer->epc = $epc;
		$soffer->save();


		//\App\Offer::whereCampid($stat->campid)->whereNetwork($stat->network)->update(['clicks' => $offerstats->clicks, 'leads' => $offerstats->leads, 'conv' => $conv, 'epc' => $epc]);

		// Recount locker stats

		$lockerstats = \App\Statistic::select(\DB::raw('count(DISTINCT ip) as clicks, SUM(IF(status=1,payout,0)) as payout, SUM(IF(status=1,1,0)) as leads, timestamp'))->where('locker_id','=',$stat->locker_id)->first();

		if($stat->locker_type == 'file') {
			\App\File::find($stat->locker_id)->update(['clicks' => $lockerstats->clicks, 'leads' => $lockerstats->leads, 'earnings' => $lockerstats->payout]);
		} elseif($stat->locker_type == 'note') {
			\App\Note::find($stat->locker_id)->update(['clicks' => $lockerstats->clicks, 'leads' => $lockerstats->leads, 'earnings' => $lockerstats->payout]);
		} elseif($stat->locker_type == 'link') {
			\App\Link::find($stat->locker_id)->update(['clicks' => $lockerstats->clicks, 'leads' => $lockerstats->leads, 'earnings' => $lockerstats->payout]);
		} elseif($stat->locker_type == 'gateway') {
			\App\Gateway::find($stat->locker_id)->update(['clicks' => $lockerstats->clicks, 'leads' => $lockerstats->leads, 'earnings' => $lockerstats->payout]);
		}

	}

	public function reverselead($statid,$campid,$payout,$sub) {

		// Reverse Statistics
		$stat = \App\Statistic::find($statid);
		$stat->status = 2;
		$stat->save();

		// Subtract balance
		$user = \App\User::find($stat->user_id);
		$user->balance = $user->balance - $stat->payout;
		$user->save();

		// Delete admin earnings
		\App\AdminEarning::whereStatid($statid)->delete();

		// Delete referral earning
		\App\ReferLog::whereStatid($statid)->delete();

		// Recount offer stats
		$offerstats = \App\Statistic::select(\DB::raw('count(DISTINCT ip) as clicks, SUM(IF(status=1,1,0)) as leads, timestamp'))->whereCampid($stat->campid)->whereNetwork($stat->network)->first();

		\App\Offer::whereCampid($stat->campid)->whereNetwork($stat->network)->update(['clicks' => $offerstats->clicks, 'leads' => $offerstats->leads]);

		// Recount locker stats

		$lockerstats = \App\Statistic::select(\DB::raw('count(DISTINCT ip) as clicks, SUM(IF(status=1,payout,0)) as payout, SUM(IF(status=1,1,0)) as leads, timestamp'))->where('locker_id','=',$stat->locker_id)->first();

		if($stat->locker_type == 'file') {
			\App\File::find($stat->locker_id)->update(['clicks' => $lockerstats->clicks, 'leads' => $lockerstats->leads, 'earnings' => $lockerstats->payout]);
		} elseif($stat->locker_type == 'note') {
			\App\Note::find($stat->locker_id)->update(['clicks' => $lockerstats->clicks, 'leads' => $lockerstats->leads, 'earnings' => $lockerstats->payout]);
		} elseif($stat->locker_type == 'link') {
			\App\Link::find($stat->locker_id)->update(['clicks' => $lockerstats->clicks, 'leads' => $lockerstats->leads, 'earnings' => $lockerstats->payout]);
		} elseif($stat->locker_type == 'gateway') {
			\App\Gateway::find($stat->locker_id)->update(['clicks' => $lockerstats->clicks, 'leads' => $lockerstats->leads, 'earnings' => $lockerstats->payout]);
		}
	}

}
