<?php namespace App\Http\Middleware;

use Closure;
use Crypt,Session;

class Admin {

	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @return mixed
	 */
	public function handle($request, Closure $next)
	{
		$role = Crypt::decrypt(Session::get('pmd'));

		if($role == 'admin' || $role == 'super_admin') {
			return $next($request);
		} else {
			$title = "403";
			$message = "Oh snap! You couldn't access this area";
		    return view('block')->with(compact('title','message'));
		}
	}

}
