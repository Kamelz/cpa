<?php namespace App\Http\Middleware;

use Closure;
use App\Settings;
use Cache;
use Auth;

class BanIPCountry {

	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @return mixed
	 */
	public function handle($request, Closure $next)
	{

		$location = \Location::get();

		$bancountry = Cache::rememberForever('countryban', function()
		{
		    return Settings::bannedcountries();
		});

		$banip = Cache::rememberForever('ipban', function()
		{
		    return Settings::bannedip();
		});

		$banusers = Settings::banneduserids();

		if (in_array($location->countryCode,$bancountry)) {

			$title = "403";
			$message = "Sorry, your country has been blocked from accessing this site";
		    return view('block')->with(compact('title','message'));

		} elseif (in_array($location->ip, $banip)) {

			$title = "403";
			$message = "Your IP has been blocked from accessing this site";
		    return view('block')->with(compact('title','message'));

		} else {
			return $next($request);
		}
	}

}
