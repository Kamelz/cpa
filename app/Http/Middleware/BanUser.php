<?php namespace App\Http\Middleware;

use Closure;
use App\Settings;
use Cache;
use Auth;

class BanUser {

	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @return mixed
	 */
	public function handle($request, Closure $next)
	{

		$location = \Location::get();

		$bancountry = Cache::rememberForever('countryban', function()
		{
		    return Settings::bannedcountries();
		});

		$banip = Cache::rememberForever('ipban', function()
		{
		    return Settings::bannedip();
		});

		if (in_array($location->countryCode,$bancountry)) {

			$title = "403";
			$message = "Sorry, your country has been blocked from accessing this site";
		    return view('block')->with(compact('title','message'));

		} elseif (in_array($location->ip, $banip)) {

			$title = "403";
			$message = "Your IP has been blocked from accessing this site";
		    return view('block')->with(compact('title','message'));

		} elseif (Auth::check() && Auth::user()->status == 'banned') {

			$title = "403";
			$message = "Sorry, this account has been banned.";
		    return view('block')->with(compact('title','message'));

		} elseif (Auth::check() && Auth::user()->status == 'pending') {

			$title = "Pending";
			$message = "Your account is pending activation. You will receive an email once activated.";
		    return view('block')->with(compact('title','message'));

		} else {
			return $next($request);
		}
	}

}