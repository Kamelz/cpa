<?php namespace App\Http\Requests;

use Illuminate\Validation\Validator;

class UCValidator extends Validator {

    public function validateAlphanumspace($attribute, $value, $parameters)
    {
        return preg_match('/^[\pL\s]+$/u', $value);
    }

    public function validateColorhex($attribute, $value, $parameters)
    {
    	return preg_match('/^#(?:[0-9a-fA-F]{3}){1,2}$/',$value);
    }

    public function validateAlphadot($attribute, $value, $parameters)
    {
    	return preg_match('/^[a-z0-9 .\-]+$/i', $value);
    }

    public function validatePayout($attribute, $value, $parameters)
    {
        return preg_match('/^[0-9]+(\.[0-9]{0,2})?$/',$value);
    }

} 