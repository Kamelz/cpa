<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('home', 'DashboardController@index');

/*
|--------------------------------------------------------------------------
| Site Routes
|--------------------------------------------------------------------------
|
| 
|
*/
Route::get('/', 'SiteController@index');
//Route::get('/', 'SiteController@maintenance');

Route::get('video', 'SiteController@video');
Route::get('about', 'SiteController@about');
Route::get('publishers', 'SiteController@publishers');
Route::get('advertisers', 'SiteController@advertisers');
Route::get('faq', 'SiteController@faq');
Route::get('contactus', 'SiteController@contact');
Route::post('contactus', 'SiteController@sendcontact');
Route::get('terms-and-conditions', 'SiteController@terms');
Route::get('dmca', 'SiteController@dmca');
Route::get('download-tos', 'SiteController@downloadtos');
Route::get('blog', 'SiteController@blog');
Route::get('blog/{slug}', 'SiteController@viewblogpost');

Route::get('ref/{code}','SiteController@setreferralcookie');

/*
|--------------------------------------------------------------------------
| Dashboard Routes
|--------------------------------------------------------------------------
|
| 
|
*/
Route::get('dashboard', 'DashboardController@index');

Route::get('dashboard/confirmdelete/{scope}/{id}','DashboardController@confirmdelete');

Route::get('dashboard/earnings','DashboardController@getEarnings');

Route::post('dashboard/referralsidebar','DashboardController@referralsidebar');

Route::get('dashboard/overview', 'DashboardController@overview');
Route::post('dashboard/overview', 'DashboardController@overview');

Route::get('dashboard/timescalebreakdown', 'DashboardController@timescalebreakdown');
Route::post('dashboard/timescalebreakdown', 'DashboardController@timescalebreakdown');

Route::get('dashboard/lockerbreakdown', 'DashboardController@lockerbreakdown');
Route::post('dashboard/lockerbreakdown', 'DashboardController@lockerbreakdown');

Route::get('dashboard/countrybreakdown', 'DashboardController@countrybreakdown');
Route::post('dashboard/countrybreakdown', 'DashboardController@countrybreakdown');

Route::get('dashboard/reversals', 'DashboardController@reversals');
Route::post('dashboard/reversals', 'DashboardController@reversals');

Route::get('dashboard/reversals/export','DashboardController@exportreversals');

Route::get('dashboard/live', 'DashboardController@livestats');
Route::get('dashboard/livedata','DashboardController@livedata');

Route::get('dashboard/files', 'DashboardController@files');

Route::get('dashboard/file/upload', 'DashboardController@fileupload');
Route::post('dashboard/file/upload', 'DashboardController@postfileupload');
Route::get('dashboard/file/edit/{code}','DashboardController@fileedit');
Route::post('dashboard/file/edit/{code}','DashboardController@savefileedit');
Route::get('dashboard/file/{code}','DashboardController@filelink');
Route::post('dashboard/file/{code}','DashboardController@savefiletemplate');
Route::get('dashboard/file/download/{code}','DashboardController@downloadfile');
Route::get('dashboard/file/delete/{code}','DashboardController@deletefileconfirm');
Route::post('dashboard/file/delete/{code}','DashboardController@deletefile');

Route::get('dashboard/note/create','DashboardController@createnote');
Route::post('dashboard/note/create','DashboardController@postcreatenote');

Route::get('dashboard/note/edit/{id}','DashboardController@editnote');
Route::post('dashboard/note/edit','DashboardController@posteditnote');

Route::get('dashboard/note/get/{id}','DashboardController@getnote');
Route::get('dashboard/note/delete/{id}','DashboardController@confirmnotedelete');
Route::post('dashboard/note/delete/{id}','DashboardController@notedelete');

Route::get('dashboard/link/create','DashboardController@createlink');
Route::post('dashboard/link/create','DashboardController@postcreatelink');
Route::get('dashboard/link/edit/{id}','DashboardController@editlink');
Route::post('dashboard/link/edit','DashboardController@posteditlink');

Route::get('dashboard/link/get/{id}','DashboardController@getlink');
Route::get('dashboard/link/delete/{id}','DashboardController@confirmlinkdelete');
Route::post('dashboard/link/delete/{id}','DashboardController@linkdelete');

Route::get('dashboard/notes-links', 'DashboardController@noteslinks');

Route::get('dashboard/leaderboard', 'DashboardController@leaderboard');
Route::post('leaderboard/settings','DashboardController@saveleaderboardsettings');
Route::get('dashboard/promotions', 'DashboardController@promotions');
Route::get('dashboard/promotion/activate/{id}','DashboardController@activatepromotion');

Route::get('dashboard/gateways','DashboardController@gateways');
Route::get('dashboard/gateway/create', 'DashboardController@creategateway');
Route::post('dashboard/gateway/create','DashboardController@postcreategateway');
Route::get('dashboard/gateway/edit/{id}','DashboardController@editgateway');
Route::post('dashboard/gateway/loadmethod','DashboardController@changeloadmethod');
Route::get('dashboard/gateway/integrate/{id}','DashboardController@integrategateway');

Route::get('dashboard/gateway/delete/{id}','DashboardController@deletegateway');

Route::get('dashboard/account', 'DashboardController@account');
Route::post('dashboard/account', 'DashboardController@postaccount');

Route::get('dashboard/payments', 'DashboardController@payments');
Route::post('dashboard/payment', 'DashboardController@postpayment');


Route::get('dashboard/referrals', 'DashboardController@referrals');
Route::get('dashboard/offers/optimize', 'DashboardController@optimizeoffers');
Route::get('dashboard/offers', 'DashboardController@offers');
Route::get('dashboard/offer/details/{id}','DashboardController@offerdetails');

Route::post('dashboard/offer/toggle','DashboardController@toggleoffer');
Route::post('dashboard/offer/favorite','DashboardController@favoriteoffer');
Route::get('offers/optimize/reset','DashboardController@resetoptimization');

Route::get('dashboard/countries/minpayout', 'DashboardController@countryminpayout');
Route::post('dashboard/countries/minpayout/add', 'DashboardController@savecountryminpayout');
Route::get('dashboard/countries/minpayout/delete/{id}', 'DashboardController@deletecountryminpayout');

Route::get('dashboard/tickets', 'DashboardController@tickets');
Route::get('dashboard/ticket/{id}', 'DashboardController@viewticket');
Route::post('dashboard/ticket/{id}/add', 'DashboardController@addticketmsg');
Route::post('dashboard/ticket/new','DashboardController@newticket');

Route::get('dashboard/messages','DashboardController@messages');
Route::get('dashboard/message/{id}','DashboardController@getmessage');


/*
|--------------------------------------------------------------------------
| Admin Routes
|--------------------------------------------------------------------------
|
| 
|
*/

Route::get('admin','AdminController@index');

Route::get('admin/earnings','AdminController@getEarnings');

Route::get('admin/confirmdelete/{scope}/{id}','AdminController@confirmdelete');

Route::get('admin/offers','AdminController@offers');
Route::post('admin/offer/toggle','AdminController@toggleoffer');
Route::post('admin/offer/favorite','AdminController@favoriteoffer');
Route::get('admin/optimization/delete/0','AdminController@resetoptimization');

Route::get('admin/expiredoffers','AdminController@expiredoffers');

Route::get('admin/fetchoffers','SuperAdminController@foffers');
Route::get('admin/fetchoffers/{network}','SuperAdminController@fetchoffers');
Route::get('admin/getfetch/{network}','SuperAdminController@getfetch');
Route::post('admin/offers/minepc','SuperAdminController@setminepc');
Route::get('admin/offers/delete/{network}','SuperAdminController@deleteoffers');

Route::post('admin/network/toggle','SuperAdminController@togglenetwork');

Route::get('admin/messages','AdminController@messages');
Route::post('admin/message/send','AdminController@sendmessage');
Route::get('admin/message/{id}','AdminController@getmessage');
Route::get('admin/message/delete/{id}','AdminController@deletemessage');

Route::get('admin/announcements','AdminController@announcements');
Route::post('admin/announcement/create','AdminController@createannouncement');
Route::get('admin/announcement/{id}','AdminController@getanmt');
Route::get('admin/announcement/delete/{id}','AdminController@deleteanmt');

Route::get('admin/subidblock','SuperAdminController@subidblock');
Route::post('admin/subidblock','SuperAdminController@postsubidblock');
Route::get('admin/subidblock/delete/{id}','SuperAdminController@deletesubidblock');

Route::get('admin/payments','AdminController@payments');
Route::post('admin/payments','AdminController@postpayments');
Route::get('admin/payments/view/{id}','AdminController@viewpayment');
Route::get('admin/paymentrequest/delete/{id}','AdminController@deletepayment');

Route::get('admin/banusers','SuperAdminController@banusers');
Route::post('admin/banusers','SuperAdminController@banuser');
Route::get('admin/ban/delete/{id}','SuperAdminController@removebanuser');

Route::get('admin/bancountries','SuperAdminController@bancountries');
Route::post('admin/bancountries','SuperAdminController@bancountry');
Route::get('admin/bancountry/delete/{id}','SuperAdminController@removebancountry');

Route::get('admin/banip','SuperAdminController@banips');
Route::post('admin/banip','SuperAdminController@banip');
Route::get('admin/banip/delete/{id}','SuperAdminController@removebanip');

Route::get('admin/publishers','AdminController@publishers');
Route::get('admin/publisher/view/{id}','AdminController@viewpublisher');
Route::post('admin/publisher/edit','AdminController@editpublisher');
Route::get('admin/accessaccount/{id}','AdminController@accessaccount');
Route::get('admin/publisher/delete/{id}','AdminController@deletepublisher');

Route::post('admin/publisher/status','AdminController@changepublisherstatus');

Route::get('admin/downloadlog','AdminController@downloadlog');
Route::get('admin/leadslog','AdminController@leadslog');
Route::post('admin/toggleleadstatus','AdminController@toggleleadstatus');

Route::post('admin/bulkreverse/confirm','AdminController@confirmbulkreverse');

Route::get('admin/sitereferrer','AdminController@sitereferrer');

Route::get('admin/overview','AdminController@overview');
Route::post('admin/overview','AdminController@overview');

Route::get('admin/files','AdminController@files');
Route::get('admin/file/download/{code}','AdminController@downloadfile');
Route::get('admin/file/delete/{code}','AdminController@deletefile');

Route::get('admin/gateways','AdminController@gateways');
Route::get('admin/gateway/delete/{id}','AdminController@deletegateway');

Route::get('admin/links','AdminController@links');
Route::get('admin/link/delete/{id}','AdminController@deletelink');

Route::get('admin/notes','AdminController@notes');
Route::get('admin/note/view/{id}','AdminController@viewnote');
Route::get('admin/note/delete/{id}','AdminController@deletenote');

Route::get('admin/contentreports','AdminController@contentreports');
Route::get('admin/contentreport/view/{id}','AdminController@viewcontentreport');

Route::get('admin/mirrors','AdminController@mirrors');
Route::post('admin/mirror/toggle','AdminController@togglemirrorstatus');
Route::get('admin/mirror/delete/{id}','AdminController@deletemirror');
Route::post('admin/mirror/add','AdminController@addmirror');

Route::get('admin/promotions','AdminController@promotions');
Route::post('admin/promotion/add','AdminController@addpromotion');
Route::post('admin/promo/toggle','AdminController@togglepromo');
Route::get('admin/promo/delete/{id}','AdminController@deletepromo');

Route::get('admin/activateconfirm/promo/{id}','AdminController@activatepromoconfirm');
Route::get('admin/promo/activate/{id}','AdminController@activatepromo');

Route::get('admin/tickets','AdminController@tickets');
Route::get('admin/ticket/view/{id}','AdminController@viewticket');
Route::post('admin/ticket/add/{id}','AdminController@addticketmsg');
Route::post('admin/ticket/toggle','AdminController@toggleticket');

//Route::get('admin/misctask','AdminController@misctask');


/*
|--------------------------------------------------------------------------
| Super Admin Routes
|--------------------------------------------------------------------------
|
|
*/
Route::get('admin/administrators','SuperAdminController@administrators');
Route::get('admin/admin/view/{id}','SuperAdminController@viewadmin');
Route::post('admin/admin/edit','SuperAdminController@editadmin');

Route::post('admin/createadmin','SuperAdminController@createadmin');
Route::post('admin/removeadmin','SuperAdminController@removeadmin');
Route::post('admin/sendqrcode','SuperAdminController@sendqrcode');

Route::get('admin/blog/createpost','SuperAdminController@createblogpost');
Route::post('admin/blog/createpost','SuperAdminController@postcreateblogpost');

Route::get('admin/blogposts','SuperAdminController@blogposts');
Route::get('admin/blog/edit/{id}','SuperAdminController@editpost');
Route::get('admin/blog/toggle/{id}','SuperAdminController@togglepoststatus');
Route::get('admin/blog/delete/{id}','SuperAdminController@deleteblogpost');

Route::get('admin/loginlog','SuperAdminController@loginlog');

Route::get('admin/settings/general','SuperAdminController@generalsettings');
Route::post('admin/settings/general/edit','SuperAdminController@editgeneralsettings');

Route::get('admin/settings/api','SuperAdminController@apisettings');

Route::post('admin/settings/api/edit','SuperAdminController@editapisettings');

Route::get('admin/healthcheck','SuperAdminController@healthcheck');
Route::post('admin/testqueue','SuperAdminController@testqueue');

Route::get('admin/networkenable','SuperAdminController@networkenable');
Route::post('admin/networkenable','SuperAdminController@postnetworkenable');
Route::get('admin/networkenable/delete/{id}','SuperAdminController@deletenetworkenable');

/*
|--------------------------------------------------------------------------
| Chat Controller Routes
|--------------------------------------------------------------------------
|
| 
|
*/

Route::get('json/chat/{timestamp}', 'JsonController@chat');

Route::post('json/chat', 'JsonController@sendmsg');

Route::get('json/chat/close/{id}', 'JsonController@closechat');

Route::get('json/test/{timestamp}', 'JsonController@test');

Route::get('json/chat/delete/{id}','JsonController@deletechat');


/*
|--------------------------------------------------------------------------
| Templates
|--------------------------------------------------------------------------
|
*/

Route::get('file/{code}','TemplateController@index');
Route::get('link/{code}','TemplateController@index');
Route::get('note/{code}','TemplateController@index');
Route::get('gateway/{id}','TemplateController@gateway');
Route::get('gateway/css/{id}','TemplateController@gatewaycss');

Route::post('check/{session}','TemplateController@completioncheck');

Route::get('gotocontent','TemplateController@gotocontent');

Route::get('gotooffer','TemplateController@gotooffer');

Route::get('go/{offer}/{pub}','TemplateController@go');

Route::get('postback/{nid}/{secret}','TemplateController@postback');

Route::post('content/report','TemplateController@reportcontent');

Route::get('moreoffers','TemplateController@moreoffers');


/*
|--------------------------------------------------------------------------
| Authentication Routes & Controllers
|--------------------------------------------------------------------------
|
| 
|
*/
Route::controllers([
	'auth' => 'Auth\AuthController',
	'password' => 'Auth\PasswordController',
]);
Route::post('verifytoken','Auth\AuthController@verifytoken');
Route::get('welcome/{id}','Auth\AuthController@welcomeadmin');

/*
|--------------------------------------------------------------------------
| Configuration Routes
|--------------------------------------------------------------------------
| Run: /config after db seed.
| Important: Comment this is production!
|
*/
//Route::get('config','ConfigController@index');
//Route::get('stats','ConfigController@stats');
//Route::get('gfa','ConfigController@gfa');
//Route::get('changelog', function() {return view('changelog');});