<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Link extends Model {

	protected $fillable = ['clicks','leads','earnings'];

	public function user()
    {
    	return $this->belongsTo('App\User');
    }

}
