<?php namespace App;
use Cache;

use Illuminate\Database\Eloquent\Model;

class Message extends Model {

	public $timestamps = false;

	protected $table = 'messages';

	public function user()
    {
        return $this->belongsTo('App\User');
    }

    public static function messages() {
    	$messages = Cache::rememberForever('messages', function()
        {
            return Message::all();
        });
        return $messages;
    }

}
