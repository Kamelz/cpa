<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Cache;

class Network extends Model {

	public $timestamps = false;

	public static function getNetworks() {
		$networks = Cache::rememberForever('networks', function()
        {
            $net = Network::all();
            $netarr = [];
            foreach ($net as $nt) {
            	$netarr[$nt->id] = $nt->name;
            }
            return $netarr;
        });
        return $networks;
	}

    public static function getActiveNetworks() {
        $networks = Cache::rememberForever('activenetworks', function()
        {
            $net = Network::whereActive(1)->lists('name');
            return $net;
        });
        return $networks;
    }

}
