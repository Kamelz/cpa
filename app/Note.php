<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Note extends Model {

	protected $fillable = ['clicks','leads','earnings'];

	public function user()
    {
    	return $this->belongsTo('App\User');
    }

}
