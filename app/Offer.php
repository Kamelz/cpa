<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Offer extends Model {

	public $timestamps = false;

	public function scopeOfCountry($query, $type)
    {
    	if(empty($type)) { return $query; }
        return $query->where('countries','LIKE','%'.$type.'%');
    }

    public function scopeOfNetwork($query, $type)
    {
    	if(empty($type)) { return $query; }
        return $query->whereNetwork($type);
    }
	
    public function scopeOfId($query, $offerid)
    {
		if(empty($offerid)) { return $query; }
        return $query->find($offerid);
    }	

    public function scopeOfMinPayout($query, $payout)
    {
        if(empty($payout)) { return $query; }
        return $query->where('payout','>',$payout);
    }

    public function scopeOfNotID($query, $disabled)
    {
        if(empty($disabled)) { return $query; }
        return $query->whereNotIn('id',$disabled);
    }

    public function scopeOfNetworkIn($query, $enabled)
    {
        if(empty($enabled)) { return $query; }
        return $query->whereIn('network',$enabled);
    }

    public function scopeOfDevice($query, $device)
    {
        if($device == 'mobile') {
            return $query->whereDevice('mobile');
        } else {
            return $query->whereIn('device',array('desktop','desktopmobile'));
        }
    }

    public function scopeofOrder($query, $order)
    {
        if(empty($order)) { return $query; }
        return $query->orderBy($order,'DESC');
    }
}
