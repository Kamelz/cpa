<?php namespace App\Services;

use App\User;
use App\User_Profile;
use Validator;
use Illuminate\Contracts\Auth\Registrar as RegistrarContract;

class Registrar implements RegistrarContract {

	/**
	 * Get a validator for an incoming registration request.
	 *
	 * @param  array  $data
	 * @return \Illuminate\Contracts\Validation\Validator
	 */
	public function validator(array $data)
	{
		return Validator::make($data, [
			'name' => 'required|max:255',
			'email' => 'required|email|max:255|unique:users',
			'password' => 'required|confirmed|min:6',
			'displayname' => 'required|max:255||unique:users,chathandle',
			'country' => 'required',
			'monthly_income' => 'required',
			'promo_methods' => 'required',
			'reason_toaccept' => 'required',
			'terms' => 'accepted'
		]);
	}

	/**
	 * Create a new user instance after a valid registration.
	 *
	 * @param  array  $data
	 * @return User
	 */
	public function create(array $data)
	{
		$gset = \App\Settings::generalsettings();

		if($gset->autoapprove == 1) { $status = 'active'; } else { $status = 'pending'; }

		$user = User::create([
			'name' => $data['name'],
			'email' => $data['email'],
			'password' => bcrypt($data['password']),
			'chathandle' => $data['displayname'],
			'status' => $status,
			'referred_by' => $data['referral'],
			'role' => 'publisher',
			'register_date' => date('Y-m-d H:i:s'),
			'read_msgs' => 'a:0:{}',
		]);

		User_Profile::create([
			'user_id' => $user->id,
			'address' => $data['address'],
			'city' => $data['city'],
			'state' => $data['state'],
			'zip' => $data['address'],
			'country' => $data['country'],
			'phone' => $data['phone'],
			'websites' => $data['websites'],
			'gender' => $data['gender'],
			'register_ip' => $data['ip'],
			'promo_methods' => $data['promo_methods'],
			'reason_toaccept' => $data['reason_toaccept'],
			'payment_cycle' => 'Net30',
			'monthly_income' => $data['monthly_income'],
			'skype' => $data['skype'],
			]);
		return $user;
	}

}
