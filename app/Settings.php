<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Cache;

class Settings extends Model {

	public $timestamps = false;

	protected $table = 'settings';

    protected $fillable = ['user_id','scope','option','value','user'];

	public static function bannedcountries()
    {
   		$countries = Settings::whereScope('countryban')->get();
		$banc = array();
        foreach ($countries as $country) {
        	$banc[] = $country->option;
        }
        return $banc;
    }

    public static function bannedip()
    {
   		$ips = Settings::whereScope('ipban')->get();
		$banip = array();
        foreach ($ips as $ip) {
        	$banip[] = $ip->option;
        }
        return $banip;
    }

    public static function countries() {
        $countries = Cache::rememberForever('countries', function()
        {
            return Settings::whereScope('cc')->get();
        });
        return $countries;
    }

    public static function paymentschedules() {
        $schedules = Cache::rememberForever('payment_schedules', function()
        {
            return Settings::whereScope('payment_schedule')->where('user_id','=',20)->get();
        });
        return $schedules;
    }

    public static function banneduserids() {
        $bans = Cache::rememberForever('banneduserids', function()
        {
            return Settings::whereScope('ban_user')->lists('user_id');
        });
        return $bans;
    }

    public static function generalsettings() {
        $settings = Cache::rememberForever('gset', function()
        {
            $rows = Settings::whereScope('generalsettings')->get();
            $arr = (object) [
            'description' => $rows->where('option','description')->first()->value,
            'keywords' => $rows->where('option','keywords')->first()->value,
            'adminemail' => $rows->where('option','adminemail')->first()->value,
            'paypalemail' => $rows->where('option','paypalemail')->first()->value,
            'quickpayout' => $rows->where('option','quickpayout')->first()->value,
            'quickpayoutfee' => $rows->where('option','quickpayoutfee')->first()->value,
            'offerrate' => $rows->where('option','offerrate')->first()->value,
            'referralrate' => $rows->where('option','referralrate')->first()->value,
            'autoapprove' => $rows->where('option','autoapprove')->first()->value,
            'mincashout' => $rows->where('option','mincashout')->first()->value,
            'secrettoken' => $rows->where('option','secrettoken')->first()->value,
            'totalpayments' => $rows->where('option','totalpayments')->first()->value,
            ];
            return $arr;

        });
        return $settings;
    }

    public static function disabledoffers() {
        $offers = Cache::rememberForever('disabled_offers', function()
        {
            $off = Settings::where('user_id','=',20)->whereScope('offers')->whereOption('disabled')->lists('value');
            return $off;
        });
        $offers[] = 0;
        return $offers;
    }

    public static function favoriteoffers() {
        $offers = Cache::rememberForever('favorite_offers', function()
        {
            $off = Settings::where('user_id','=',20)->whereScope('offers')->whereOption('favorite')->lists('value');
            return $off;
        });
        $offers[] = 0;
        return $offers;
    }

    public static function isChatDisabled($userid) { 
        $chatoff = Cache::rememberForever('chat_disabled', function()
        {
            $chatdis = Settings::whereScope('chat')->whereOption('disabled')->lists('user_id');
            return $chatdis;
        });
        // dd($chatoff);
        return in_array($userid,$chatoff->toArray());
    }

    public static function changeChatDisabled($userid,$state) {
        if($state == 0) {
            Settings::where('user_id','=',$userid)->whereScope('chat')->whereOption('disabled')->delete();
        } elseif($state == 1) {
            $st = new Settings;
            $st->user_id = $userid;
            $st->scope = 'chat';
            $st->option = 'disabled';
            $st->save();
        }
        Cache::forget('chat_disabled');
    }

    public static function getCustomPayout($userid) {
        $payout = Cache::rememberForever('custom_payouts', function()
        {
            $pyt = Settings::whereScope('payout')->whereOption('custom')->get();
            $arr = [];
            foreach ($pyt as $pt) {
                $arr[$pt->user_id] = $pt->value;
            }
            return $arr;
        });
        if(array_key_exists($userid,$payout)) { $pay = (int) $payout[$userid]; } else { $pay = Settings::generalsettings()->offerrate; }

        $globalpromo = Cache::get('global_promotion',array('promo_id' => 0, 'promo_percent' => 0));

        $pay = $globalpromo['promo_percent'] + $pay;

        return (int) $pay;
    }

    public static function setCustomPayout($userid,$rate) {
        if($rate == Settings::generalsettings()->offerrate) {
            \App\Settings::where('user_id','=',$userid)->whereScope('payout')->whereOption('custom')->delete();
        } else {
            $st = Settings::firstOrNew(['user_id' => $userid, 'scope' => 'payout', 'option' => 'custom']);
            $st->value = $rate;
            $st->save();
        }
        Cache::forget('custom_payouts');
    }

    public static function getSubIDBlocks($userid) {
        $blocks = Settings::whereUserId($userid)->whereScope('subid_block')->lists('option');
        return $blocks;
    }

    public static function getNetworkEnabled($userid) {
        $blocks = Settings::whereUserId($userid)->whereScope('network_enable')->lists('option');
        return $blocks;
    }

}
