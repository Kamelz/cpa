<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Statistic extends Model {

	public $timestamps = false;

	protected $table = 'statistics';

	protected $fillable = ['status'];

	public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function scopeOfUser($query, $user)
    {
    	if(empty($user)) { return $query; }
        return $query->whereUserId($user);
    }

    public function scopeOfStatus($query, $status)
    {
    	if(empty($status)) { return $query; }
        return $query->whereStatus($status);
    }
}
