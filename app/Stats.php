<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Stats extends Model {

	public $timestamps = false;

	protected $table = 'statistics';

	public function user()
    {
        return $this->belongsTo('App\User');
    }

}

