<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Ticket extends Model {

	public $timestamps = false;

	protected $table = 'tickets';

	public function user()
    {
        return $this->belongsTo('App\User')->select(['id','chathandle']);
    }

    public function ticketmessage()
    {
        return $this->hasMany('App\TicketMessage');
    }

}
