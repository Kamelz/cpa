<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class TicketMessage extends Model {

	public $timestamps = false;

	protected $table = 'ticket_messages';

	public function ticket()
    {
        return $this->belongsTo('App\Ticket');
    }

}
