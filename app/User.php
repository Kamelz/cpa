<?php namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

use Caffeinated\Shinobi\Traits\ShinobiTrait;

use DB;
use Cache;

class User extends Model implements AuthenticatableContract, CanResetPasswordContract {

	use Authenticatable, CanResetPassword, ShinobiTrait;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'users';

    public $timestamps = false;

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['name', 'email', 'password','chathandle','status','referred_by','role','register_date','read_msgs'];

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = ['password', 'remember_token'];

	public function user_profile()
    {
        return $this->hasOne('App\User_Profile');
    }

    public function chat()
    {
        return $this->hasMany('App\Chat');
    }

    public function stats()
    {
        return $this->hasMany('App\Stats');
    }

    public static function getBots() {
    	return DB::table('role_user')->where('role_id', 104)->lists('user_id');
    }

    public function tickets()
    {
        return $this->hasMany('App\Ticket');
    }

    public function files()
    {
        return $this->hasMany('App\File');
    }

    public function gateways()
    {
        return $this->hasMany('App\Gateway');
    }

    public function links()
    {
        return $this->hasMany('App\Link');
    }

    public function notes()
    {
        return $this->hasMany('App\Note');
    }

    public function statistics()
    {
        return $this->hasMany('\App\Statistic');
    }

    public static function getAdmins() {
        $admins = Cache::rememberForever('admins', function()
        {
            return User::whereIn('role',array('super_admin','admin'))->lists('id');
        });
        return $admins;
    }

}
