<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class User_Profile extends Model {

	protected $table = 'user_profiles';

	protected $fillable = ['user_id', 'address', 'state', 'city', 'zip', 'country', 'phone', 'websites', 'gender', 'register_ip', 'promo_methods', 'reason_toaccept', 'payment_cycle', 'monthly_income', 'skype'];

	public $timestamps = false;

	public function user()
    {
        return $this->belongsTo('App\User');
    }

}
