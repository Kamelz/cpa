<?php

return [

	// User id of Public user. Refer to database
	'public' => '4',
	'admins' => array(1,2),
	'bots' => array(5),
	'success' => '#70ca63',
	'error' => '#e9573f',
	'warning' => '#f7c65f',
	'info' => '#3bafda',
	'primary' => '#4a89dc'

];