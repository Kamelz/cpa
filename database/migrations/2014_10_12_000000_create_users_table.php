<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('users', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('name');
			$table->string('email')->unique();
			$table->string('password', 60);
			$table->string('chathandle')->unique();
			$table->string('chatsound',20);
			$table->string('profileimg');
			$table->integer('referred_by')->default(0);
			$table->string('status')->default('active');
			$table->string('role')->default('publisher');
			$table->decimal('balance')->default(0.00);
			$table->timestamp('register_date');
			$table->boolean('email_confirmed')->default(0);
            $table->string('confirmation_code')->default('');
            $table->string('google2fa_secret')->default('');
            $table->text('read_msgs');
			$table->rememberToken();
		});
		Schema::create('user_profiles', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('user_id')->unsigned();
			$table->foreign('user_id')->references('id')->on('users')->onUpdate('cascade')->onDelete('cascade');
			$table->string('address')->nullable();
			$table->string('city')->nullable();
			$table->string('state')->nullable();
			$table->integer('zip')->nullable();
			$table->string('country')->nullable();
			$table->string('phone')->nullable();
			$table->string('websites')->nullable();
			$allow = array('Male','Female');
    		$table->enum('gender', $allow);
			$table->integer('register_ip')->unsigned();
			$table->string('promo_methods')->nullable();
			$table->string('payment_cycle')->default('Net30');
			$table->string('payment_method')->nullabe();
			$table->text('payment_detail');
			$table->string('reason_toaccept')->nullable();
			$table->string('monthly_income');
			$table->string('skype')->nullable();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('users');
		Schema::drop('user_profile');
	}

}
