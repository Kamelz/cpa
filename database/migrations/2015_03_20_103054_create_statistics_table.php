<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStatisticsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('statistics', function(Blueprint $table)
		{
			$table->bigIncrements('id'); // Need to increment manually since primary keys create problems with partitions
			$table->integer('user_id')->unsigned();
			$table->foreign('user_id')->references('id')->on('users');
			$table->integer('campid');
			$table->string('offname');
			$table->string('network');
			$table->string('country');
			$table->string('locker_type');
			$table->integer('locker_id');
			$table->boolean('status')->default(0);
			$table->decimal('payout',10,2);
			$table->integer('ip');
			$table->string('ua');
			$table->integer('site_referrer_id');
			$table->date('timestamp');
		});
	}

	/* For stat performance, partition the table by month
	/* http://dev.mysql.com/doc/refman/5.1/en/partitioning-range.html
	ALTER TABLE statistics PARTITION BY RANGE ( UNIX_TIMESTAMP(timestamp) ) (
		PARTITION p0 VALUES LESS THAN (UNIX_TIMESTAMP('2014-01-01 00:00:00')),
		PARTITION p1 VALUES LESS THAN (UNIX_TIMESTAMP('2014-02-01 00:00:00')),
		PARTITION p2 VALUES LESS THAN (UNIX_TIMESTAMP('2014-03-01 00:00:00')),
		PARTITION p3 VALUES LESS THAN (UNIX_TIMESTAMP('2014-04-01 00:00:00')),
		PARTITION p4 VALUES LESS THAN (UNIX_TIMESTAMP('2014-05-01 00:00:00')),
		PARTITION p5 VALUES LESS THAN (UNIX_TIMESTAMP('2014-06-01 00:00:00')),
		PARTITION p6 VALUES LESS THAN (UNIX_TIMESTAMP('2014-07-01 00:00:00')),
		PARTITION p7 VALUES LESS THAN (UNIX_TIMESTAMP('2014-08-01 00:00:00')),
		PARTITION p8 VALUES LESS THAN (UNIX_TIMESTAMP('2014-09-01 00:00:00')),
		PARTITION p9 VALUES LESS THAN (UNIX_TIMESTAMP('2014-10-01 00:00:00')),
	    PARTITION p10 VALUES LESS THAN (UNIX_TIMESTAMP('2014-11-01 00:00:00')),
	    PARTITION p11 VALUES LESS THAN (UNIX_TIMESTAMP('2014-12-01 00:00:00')),
	    PARTITION p12 VALUES LESS THAN (UNIX_TIMESTAMP('2015-01-01 00:00:00')),
	    PARTITION p13 VALUES LESS THAN (UNIX_TIMESTAMP('2015-02-01 00:00:00')),
	    PARTITION p14 VALUES LESS THAN (UNIX_TIMESTAMP('2015-03-01 00:00:00')),
	    PARTITION p15 VALUES LESS THAN (UNIX_TIMESTAMP('2015-04-01 00:00:00')),
	    PARTITION p16 VALUES LESS THAN (UNIX_TIMESTAMP('2015-05-01 00:00:00')),
	    PARTITION p17 VALUES LESS THAN (UNIX_TIMESTAMP('2015-06-01 00:00:00')),
	    PARTITION p18 VALUES LESS THAN (UNIX_TIMESTAMP('2015-07-01 00:00:00')),
	    PARTITION p19 VALUES LESS THAN MAXVALUE
	);
	*/

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('statistics');
	}

}
