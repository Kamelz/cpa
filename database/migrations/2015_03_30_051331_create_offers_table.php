<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOffersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('offers', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('campid');
			$table->string('offer');
			$table->string('title');
			$table->boolean('active')->default(1);
			$table->decimal('network_epc',10,2);
			$table->decimal('payout',10,2);
			$table->string('url');
			$table->string('countries');
			$table->string('network');
			$table->integer('clicks');
			$table->integer('leads');
			$table->integer('limit');
			$table->string('device');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('offers');
	}

}
