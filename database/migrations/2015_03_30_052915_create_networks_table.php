<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNetworksTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('networks', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('name');
			$table->string('active')->default(1);
			$table->integer('numOffers');
			$table->decimal('minepc',8,2)->default(0.04);
			$table->integer('pubid');
			$table->string('apikey');
			$table->string('sub',30);
			$table->string('offer_url');
			$table->string('secret');
			$table->string('postback');
			$table->timestamp('updated_at');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('networks');
	}

}
