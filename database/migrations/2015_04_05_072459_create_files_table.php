<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFilesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('files', function(Blueprint $table)
		{
			$table->bigIncrements('id');
			$table->integer('user_id')->unsigned();
			$table->foreign('user_id')->references('id')->on('users');
			$table->string('filename');
			$table->string('ext',5);
			$table->string('hashname')->unique();
			$table->string('code')->unique();
			$table->integer('filesize');
			$table->string('fakesize',50);
			$table->integer('clicks');
			$table->integer('leads');
			$table->integer('fake_leads');
			$table->decimal('earnings',20,2);
			$table->string('template',20)->default('default');
			$table->boolean('archive')->default(0);
			$table->timestamps(); // created + last downloaded
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('files');
	}

}
