<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFileReportsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('file_reports', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('locker_id');
			$table->string('locker_type');
			$table->string('locker_name');
			$table->string('name');
			$table->string('email');
			$table->text('message');
			$table->integer('ip');
			$table->string('country');
			$table->timestamp('timestamp');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('file_reports');
	}

}
