<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGatewaysTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('gateways', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('user_id')->unsigned();
			$table->foreign('user_id')->references('id')->on('users');
			$table->string('code',10)->unique();
			$table->string('name');
			$table->string('redirect');
			$table->string('load_method',10);
			$table->string('title');
			$table->string('inst');
			$table->string('bgimg');
			$table->boolean('allow_close');
			$table->tinyInteger('numOffers');
			$table->text('css');
			$table->string('customcss',5);
			$table->integer('clicks');
			$table->integer('leads');
			$table->decimal('earnings');
			$table->boolean('archive')->default(0);
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('gateways');
	}

}
