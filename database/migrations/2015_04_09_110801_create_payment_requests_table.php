<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaymentRequestsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('payment_requests', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('user_id')->unsigned();
			$table->foreign('user_id')->references('id')->on('users');
			$table->decimal('amount',10,2);
			$table->string('status',20)->default('Pending');
			$table->string('payment_method');
			$table->string('schedule',20);
			$table->timestamp('request_date');
			$table->timestamp('payment_date')->nullable();
			$table->text('payment_details');
			$table->string('priority')->default('Normal');
			$table->decimal('fee',10,2);
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('payment_requests');
	}

}
