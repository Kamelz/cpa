<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdminEarningsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('admin_earnings', function(Blueprint $table)
		{
			$table->increments('id');
			$table->bigInteger('statid');
			$table->integer('user_id');
			$table->decimal('payout',10,2);
			$table->integer('campid');
			$table->string('offname');
			$table->string('network');
			$table->integer('locker_id');
			$table->string('locker_type');
			$table->string('country',2);
			$table->date('timestamp');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('admin_earnings');
	}

}
