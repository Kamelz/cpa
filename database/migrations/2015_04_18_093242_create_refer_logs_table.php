<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReferLogsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('refer_logs', function(Blueprint $table)
		{
			$table->increments('id');
			$table->bigInteger('statid');
			$table->integer('user_id');
			$table->integer('referral_id');
			$table->decimal('payout');
			$table->date('timestamp');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('refer_logs');
	}

}
