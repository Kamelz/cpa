<?php
 
use Illuminate\Database\Seeder;
 
class AnnouncementSeeder extends Seeder {
 
    public function run()
    {
        // Uncomment the below to wipe the table clean before populating
        DB::table('announcements')->delete();
 
        $msgs = array(
            ['user_id' => 27, 'title' => 'AdGodMedia Weekly Payment Released!', 'message' => 'Hi,
             We have added WEEKLY Payments on AdGodMedia, to get weekly payment please open a ticket in your AdGodMedia account.
             
             Weekly Payment requirements:
             
- Your account should be more than 30 days old.
- Your payment amount should be less than your pending balance.
- You MUST received at least one payment from AdGodMedia before requesting WEEKLY payment.', 'timestamp' => '2015-03-16 05:23:00'],
            ['user_id' => 27, 'title' => 'Earnings Bonus for Christmas', 'message' => 'Hello UC members,
            
             We are really happy to announce the new Earning bonus for 2 days on 24th and 25th December 2014 as a part of give back to our valued customers. For this 2 days, all users will be having 20% bonus earnings for every lead they earn. 
             
             
Happy Holidays and Happy Earnings!! 

 Best Regards, 
 AdGodMedia AM', 'timestamp' => '2014-12-16 05:23:00'],
            ['user_id' => 27, 'title' => 'Winter Affiliate Reward Program - Winners Announced!', 'message' => 'Hello UC members, 
            
             As of today we have shortlisted the members who have won their rewards in the WINTER Affiliate Reward Program. We would be contacting them shortly regarding their shipping details to send them the prizes. 
             
             Best Regards, 
             AdGodMedia AM', 'timestamp' => '2014-12-06 05:23:00'],
        );
 
        // Uncomment the below to run the seeder
        DB::table('announcements')->insert($msgs);
    }
 
}