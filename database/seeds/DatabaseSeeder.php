<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Model::unguard();

		$this->call('UserSeeder');
		$this->call('MessageSeeder');
		$this->call('UserProfileSeeder');
		$this->call('AnnouncementSeeder');
		$this->call('SettingSeeder');
		$this->call('RoleSeeder');
		$this->call('NetworkSeeder');
	}

}
