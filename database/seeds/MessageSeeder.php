<?php
 
use Illuminate\Database\Seeder;
 
class MessageSeeder extends Seeder {
 
    public function run()
    {
        // Uncomment the below to wipe the table clean before populating
        DB::table('messages')->delete();
 
        $msgs = array(
            ['user_id' => 27, 'title' => 'AdGodMedia Weekly Payment Released', 'message' => 'Hello All, We have added WEEKLY Payments in AdGodMedia , to get weekly payment please open a ticket in your AdGodMedia account.\n\n Weekly Payment requirements:\n - Your account should be more than 30 days old\n. - Your payment amount should be less than your pending balance.\n - You MUST received at least one payment from AdGodMedia before requesting WEEKLY payment.', 'receiver' => 4, 'timestamp' => '2015-03-16 05:05:00']
        );
 
        // Uncomment the below to run the seeder
        DB::table('messages')->insert($msgs);
    }
 
}