<?php
 
use Illuminate\Database\Seeder;
 
class NetworkSeeder extends Seeder {
 
    public function run()
    {
        // Uncomment the below to wipe the table clean before populating
        DB::table('networks')->delete();
 
        $msgs = array(
            ['name' => 'Adworkmedia', 'pubid' => 22467, 'apikey' => 'bowz4nr5g8xe5ym4xakz33zg3ur2yxdkaqvyvlwf', 'sub' => 'sid','offer_url' => 'https://www.adworkmedia.com/api/index.php?pubID=%pubid%&apiID=%apikey%&campDetails=true&optimize=EPC&oType=2'],
            ['name' => 'Adgatemedia', 'pubid' => 40428, 'apikey' => '49ddcb7917fdfd4ffe4eb0a62a94a778','sub' => 's1', 'offer_url' => 'https://api.adgatemedia.com/v1/offers?aff=%pubid%&api_key=%apikey%&minepc=.04'],
            ['name' => 'Adscendmedia', 'pubid' => 8565, 'apikey' => 1288872991, 'sub' => 'sub1', 'offer_url' => 'http://adscendmedia.com/api-get.php?pubid=%pubid%&key=%apikey%&mode=offers&simulate_country=all&include_completed=1&adlock_mode=1&only_instant=1&category=0&min_payout=0.10&sort=epc'],
            ['name' => 'CPAGrip', 'pubid' => 5436, 'apikey' => '2cb49a0034d026ae5485f4521ee4f8fc','sub' => 'tracking_id', 'offer_url' => 'https://www.cpagrip.com/common/offer_feed_rss.php?user_id=%pubid%&key=%apikey%&ip=&tracking_id=&showall=yes'],
            ['name' => 'CPALead', 'pubid' => 221271, 'apikey' => 'Not Required', 'sub' => 'subid', 'offer_url' => 'http://www.cpalead.com/dashboard/reports/campaign_rss.php?id=%pubid%&offer_type=survey,trial,purchase,pinsubmit,mobile,download'],
            ['name' => 'Adludum', 'pubid' => 331, 'apikey' => '8alWFJ9N7pbUKwEi8q8OOeA59kif7YXq', 'sub' => 'sid', 'offer_url' => 'http://www.adludum.com/apis/offers.php?pubid=%pubid%&apikey=%apikey%'],

        );
 
        // Uncomment the below to run the seeder
        DB::table('networks')->insert($msgs);
    }
 
}