<?php
 
use Illuminate\Database\Seeder;
 
class PromotionSeeder extends Seeder {
 
    public function run()
    {
        // Uncomment the below to wipe the table clean before populating
        DB::table('promotions')->delete();
 
        $msgs = array(
            ['name' => 'Beginner\'s Luck', 'bonus_percentage' => 2, 'leads' => 100, 'hours' => 2, 'active' => 1],
            ['name' => 'Launch', 'bonus_percentage' => 3, 'leads' => 125, 'hours' => 3, 'active' => 1],
            ['name' => 'Newbie', 'bonus_percentage' => 3, 'leads' => 175, 'hours' => 4, 'active' => 1],
            ['name' => 'Jazz', 'bonus_percentage' => 4, 'leads' => 200, 'hours' => 7, 'active' => 1],
            ['name' => 'Professional', 'bonus_percentage' => 5, 'leads' => 400, 'hours' => 5, 'active' => 1],
            ['name' => 'Friend', 'bonus_percentage' => 4, 'leads' => 500, 'hours' => 8, 'active' => 1],
            ['name' => 'Superhero', 'bonus_percentage' => 6, 'leads' => 1200, 'hours' => 9, 'active' => 1],
            ['name' => 'Party', 'bonus_percentage' => 7, 'leads' => 1600, 'hours' => 5, 'active' => 1],
            ['name' => 'Marketer', 'bonus_percentage' => 8, 'leads' => 2500, 'hours' => 10, 'active' => 1],
        );
 
        // Uncomment the below to run the seeder
        DB::table('promotions')->insert($msgs);
    }
 
}