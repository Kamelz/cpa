<?php
 
use Illuminate\Database\Seeder;
 
class RoleSeeder extends Seeder {
 
    public function run()
    {
        // Uncomment the below to wipe the table clean before populating
        DB::table('roles')->delete();
 
        $roles = array(
            ['id' => 100, 'name' => 'Super Administrator', 'slug' =>'super_admin', 'description' => 'Super administrator with all permissions', 'created_at' => new DateTime, 'updated_at' => new DateTime],
            ['id' => 101, 'name' => 'Administrator', 'slug' =>'admin', 'description' => 'Administrator with limited permissions', 'created_at' => new DateTime, 'updated_at' => new DateTime],
            ['id' => 102, 'name' => 'Publisher', 'slug' =>'publisher', 'description' => 'Network Affiliate', 'created_at' => new DateTime, 'updated_at' => new DateTime],
            ['id' => 103, 'name' => 'Advertiser', 'slug' =>'advertiser', 'description' => 'Advertiser', 'created_at' => new DateTime, 'updated_at' => new DateTime],
            ['id' => 104, 'name' => 'Bot', 'slug' =>'bot', 'description' => 'Chat Bot', 'created_at' => new DateTime, 'updated_at' => new DateTime],
        );
 
        // Uncomment the below to run the seeder
        DB::table('roles')->insert($roles);

        // Assign roles to users
        DB::table('role_user')->delete();

        $role_user = array(
            ['id' => 1, 'role_id' => 100, 'user_id' => 27, 'created_at' => new DateTime, 'updated_at' => new DateTime],
            ['id' => 3, 'role_id' => 104, 'user_id' => 10, 'created_at' => new DateTime, 'updated_at' => new DateTime],
            ['id' => 4, 'role_id' => 104, 'user_id' => 20, 'created_at' => new DateTime, 'updated_at' => new DateTime],
        );

        DB::table('role_user')->insert($role_user);
    }
 
}