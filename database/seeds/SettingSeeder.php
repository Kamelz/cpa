<?php
 
use Illuminate\Database\Seeder;
 
class SettingSeeder extends Seeder {
 
    public function run()
    {
        // Uncomment the below to wipe the table clean before populating
        DB::table('settings')->delete();
 
        $msgs = array(
            ['user_id' => 20, 'scope' => 'cc', 'option' => 'AF', 'value' => 'Afghanistan'],
            ['user_id' => 20, 'scope' => 'cc', 'option' => 'AX', 'value' => 'Aland Islands'],
            ['user_id' => 20, 'scope' => 'cc', 'option' => 'AL', 'value' => 'Albania'],
            ['user_id' => 20, 'scope' => 'cc', 'option' => 'DZ', 'value' => 'Algeria'],
            ['user_id' => 20, 'scope' => 'cc', 'option' => 'AS', 'value' => 'American Samoa'],
            ['user_id' => 20, 'scope' => 'cc', 'option' => 'AD', 'value' => 'Andorra'],
            ['user_id' => 20, 'scope' => 'cc', 'option' => 'AO', 'value' => 'Angola'],
            ['user_id' => 20, 'scope' => 'cc', 'option' => 'AI', 'value' => 'Anguilla'],
            ['user_id' => 20, 'scope' => 'cc', 'option' => 'AQ', 'value' => 'Antarctica'],
            ['user_id' => 20, 'scope' => 'cc', 'option' => 'AG', 'value' => 'Antigua and Barbuda'],
            ['user_id' => 20, 'scope' => 'cc', 'option' => 'AR', 'value' => 'Argentina'],
            ['user_id' => 20, 'scope' => 'cc', 'option' => 'AM', 'value' => 'Armenia'],
            ['user_id' => 20, 'scope' => 'cc', 'option' => 'AW', 'value' => 'Aruba'],
            ['user_id' => 20, 'scope' => 'cc', 'option' => 'AP', 'value' => 'Asia/Pacific Region'],
            ['user_id' => 20, 'scope' => 'cc', 'option' => 'AU', 'value' => 'Australia'],
            ['user_id' => 20, 'scope' => 'cc', 'option' => 'AT', 'value' => 'Austria'],
            ['user_id' => 20, 'scope' => 'cc', 'option' => 'AZ', 'value' => 'Azerbaijan'],
            ['user_id' => 20, 'scope' => 'cc', 'option' => 'BS', 'value' => 'Bahamas'],
            ['user_id' => 20, 'scope' => 'cc', 'option' => 'BH', 'value' => 'Bahrain'],
            ['user_id' => 20, 'scope' => 'cc', 'option' => 'BD', 'value' => 'Bangladesh'],
            ['user_id' => 20, 'scope' => 'cc', 'option' => 'BB', 'value' => 'Barbados'],
            ['user_id' => 20, 'scope' => 'cc', 'option' => 'BY', 'value' => 'Belarus'],
            ['user_id' => 20, 'scope' => 'cc', 'option' => 'BE', 'value' => 'Belgium'],
            ['user_id' => 20, 'scope' => 'cc', 'option' => 'BZ', 'value' => 'Belize'],
            ['user_id' => 20, 'scope' => 'cc', 'option' => 'BJ', 'value' => 'Benin'],
            ['user_id' => 20, 'scope' => 'cc', 'option' => 'BM', 'value' => 'Bermuda'],
            ['user_id' => 20, 'scope' => 'cc', 'option' => 'BT', 'value' => 'Bhutan'],
            ['user_id' => 20, 'scope' => 'cc', 'option' => 'BO', 'value' => 'Bolivia'],
            ['user_id' => 20, 'scope' => 'cc', 'option' => 'BQ', 'value' => 'Bonaire, Saint Eustatius and Saba'],
            ['user_id' => 20, 'scope' => 'cc', 'option' => 'BA', 'value' => 'Bosnia and Herzegovina'],
            ['user_id' => 20, 'scope' => 'cc', 'option' => 'BW', 'value' => 'Botswana'],
            ['user_id' => 20, 'scope' => 'cc', 'option' => 'BV', 'value' => 'Bouvet Island'],
            ['user_id' => 20, 'scope' => 'cc', 'option' => 'BR', 'value' => 'Brazil'],
            ['user_id' => 20, 'scope' => 'cc', 'option' => 'IO', 'value' => 'British Indian Ocean Territory'],
            ['user_id' => 20, 'scope' => 'cc', 'option' => 'BN', 'value' => 'Brunei Darussalam'],
            ['user_id' => 20, 'scope' => 'cc', 'option' => 'BG', 'value' => 'Bulgaria'],
            ['user_id' => 20, 'scope' => 'cc', 'option' => 'BF', 'value' => 'Burkina Faso'],
            ['user_id' => 20, 'scope' => 'cc', 'option' => 'BI', 'value' => 'Burundi'],
            ['user_id' => 20, 'scope' => 'cc', 'option' => 'KH', 'value' => 'Cambodia'],
            ['user_id' => 20, 'scope' => 'cc', 'option' => 'CM', 'value' => 'Cameroon'],
            ['user_id' => 20, 'scope' => 'cc', 'option' => 'CA', 'value' => 'Canada'],
            ['user_id' => 20, 'scope' => 'cc', 'option' => 'CV', 'value' => 'Cape Verde'],
            ['user_id' => 20, 'scope' => 'cc', 'option' => 'KY', 'value' => 'Cayman Islands'],
            ['user_id' => 20, 'scope' => 'cc', 'option' => 'CF', 'value' => 'Central African Republic'],
            ['user_id' => 20, 'scope' => 'cc', 'option' => 'TD', 'value' => 'Chad'],
            ['user_id' => 20, 'scope' => 'cc', 'option' => 'CL', 'value' => 'Chile'],
            ['user_id' => 20, 'scope' => 'cc', 'option' => 'CN', 'value' => 'China'],
            ['user_id' => 20, 'scope' => 'cc', 'option' => 'CX', 'value' => 'Christmas Island'],
            ['user_id' => 20, 'scope' => 'cc', 'option' => 'CC', 'value' => 'Cocos (Keeling) Islands'],
            ['user_id' => 20, 'scope' => 'cc', 'option' => 'CO', 'value' => 'Colombia'],
            ['user_id' => 20, 'scope' => 'cc', 'option' => 'KM', 'value' => 'Comoros'],
            ['user_id' => 20, 'scope' => 'cc', 'option' => 'CG', 'value' => 'Congo'],
            ['user_id' => 20, 'scope' => 'cc', 'option' => 'CD', 'value' => 'Congo, The Democratic Republic of the'],
            ['user_id' => 20, 'scope' => 'cc', 'option' => 'CK', 'value' => 'Cook Islands'],
            ['user_id' => 20, 'scope' => 'cc', 'option' => 'CR', 'value' => 'Costa Rica'],
            ['user_id' => 20, 'scope' => 'cc', 'option' => 'CI', 'value' => 'Cote D\'Ivoire'],
            ['user_id' => 20, 'scope' => 'cc', 'option' => 'HR', 'value' => 'Croatia'],
            ['user_id' => 20, 'scope' => 'cc', 'option' => 'CU', 'value' => 'Cuba'],
            ['user_id' => 20, 'scope' => 'cc', 'option' => 'CW', 'value' => 'Curacao'],
            ['user_id' => 20, 'scope' => 'cc', 'option' => 'CY', 'value' => 'Cyprus'],
            ['user_id' => 20, 'scope' => 'cc', 'option' => 'CZ', 'value' => 'Czech Republic'],
            ['user_id' => 20, 'scope' => 'cc', 'option' => 'DK', 'value' => 'Denmark'],
            ['user_id' => 20, 'scope' => 'cc', 'option' => 'DJ', 'value' => 'Djibouti'],
            ['user_id' => 20, 'scope' => 'cc', 'option' => 'DM', 'value' => 'Dominica'],
            ['user_id' => 20, 'scope' => 'cc', 'option' => 'DO', 'value' => 'Dominican Republic'],
            ['user_id' => 20, 'scope' => 'cc', 'option' => 'EC', 'value' => 'Ecuador'],
            ['user_id' => 20, 'scope' => 'cc', 'option' => 'EG', 'value' => 'Egypt'],
            ['user_id' => 20, 'scope' => 'cc', 'option' => 'SV', 'value' => 'El Salvador'],
            ['user_id' => 20, 'scope' => 'cc', 'option' => 'GQ', 'value' => 'Equatorial Guinea'],
            ['user_id' => 20, 'scope' => 'cc', 'option' => 'ER', 'value' => 'Eritrea'],
            ['user_id' => 20, 'scope' => 'cc', 'option' => 'EE', 'value' => 'Estonia'],
            ['user_id' => 20, 'scope' => 'cc', 'option' => 'ET', 'value' => 'Ethiopia'],
            ['user_id' => 20, 'scope' => 'cc', 'option' => 'EU', 'value' => 'Europe'],
            ['user_id' => 20, 'scope' => 'cc', 'option' => 'FK', 'value' => 'Falkland Islands (Malvinas)'],
            ['user_id' => 20, 'scope' => 'cc', 'option' => 'FO', 'value' => 'Faroe Islands'],
            ['user_id' => 20, 'scope' => 'cc', 'option' => 'FJ', 'value' => 'Fiji'],
            ['user_id' => 20, 'scope' => 'cc', 'option' => 'FI', 'value' => 'Finland'],
            ['user_id' => 20, 'scope' => 'cc', 'option' => 'FR', 'value' => 'France'],
            ['user_id' => 20, 'scope' => 'cc', 'option' => 'GF', 'value' => 'French Guiana'],
            ['user_id' => 20, 'scope' => 'cc', 'option' => 'PF', 'value' => 'French Polynesia'],
            ['user_id' => 20, 'scope' => 'cc', 'option' => 'TF', 'value' => 'French Southern Territories'],
            ['user_id' => 20, 'scope' => 'cc', 'option' => 'GA', 'value' => 'Gabon'],
            ['user_id' => 20, 'scope' => 'cc', 'option' => 'GM', 'value' => 'Gambia'],
            ['user_id' => 20, 'scope' => 'cc', 'option' => 'GE', 'value' => 'Georgia'],
            ['user_id' => 20, 'scope' => 'cc', 'option' => 'DE', 'value' => 'Germany'],
            ['user_id' => 20, 'scope' => 'cc', 'option' => 'GH', 'value' => 'Ghana'],
            ['user_id' => 20, 'scope' => 'cc', 'option' => 'GI', 'value' => 'Gibraltar'],
            ['user_id' => 20, 'scope' => 'cc', 'option' => 'GR', 'value' => 'Greece'],
            ['user_id' => 20, 'scope' => 'cc', 'option' => 'GL', 'value' => 'Greenland'],
            ['user_id' => 20, 'scope' => 'cc', 'option' => 'GD', 'value' => 'Grenada'],
            ['user_id' => 20, 'scope' => 'cc', 'option' => 'GP', 'value' => 'Guadeloupe'],
            ['user_id' => 20, 'scope' => 'cc', 'option' => 'GU', 'value' => 'Guam'],
            ['user_id' => 20, 'scope' => 'cc', 'option' => 'GT', 'value' => 'Guatemala'],
            ['user_id' => 20, 'scope' => 'cc', 'option' => 'GG', 'value' => 'Guernsey'],
            ['user_id' => 20, 'scope' => 'cc', 'option' => 'GN', 'value' => 'Guinea'],
            ['user_id' => 20, 'scope' => 'cc', 'option' => 'GW', 'value' => 'Guinea-Bissau'],
            ['user_id' => 20, 'scope' => 'cc', 'option' => 'GY', 'value' => 'Guyana'],
            ['user_id' => 20, 'scope' => 'cc', 'option' => 'HT', 'value' => 'Haiti'],
            ['user_id' => 20, 'scope' => 'cc', 'option' => 'HM', 'value' => 'Heard Island and McDonald Islands'],
            ['user_id' => 20, 'scope' => 'cc', 'option' => 'VA', 'value' => 'Holy See (Vatican City State)'],
            ['user_id' => 20, 'scope' => 'cc', 'option' => 'HN', 'value' => 'Honduras'],
            ['user_id' => 20, 'scope' => 'cc', 'option' => 'HK', 'value' => 'Hong Kong'],
            ['user_id' => 20, 'scope' => 'cc', 'option' => 'HU', 'value' => 'Hungary'],
            ['user_id' => 20, 'scope' => 'cc', 'option' => 'IS', 'value' => 'Iceland'],
            ['user_id' => 20, 'scope' => 'cc', 'option' => 'IN', 'value' => 'India'],
            ['user_id' => 20, 'scope' => 'cc', 'option' => 'ID', 'value' => 'Indonesia'],
            ['user_id' => 20, 'scope' => 'cc', 'option' => 'IR', 'value' => 'Iran, Islamic Republic of'],
            ['user_id' => 20, 'scope' => 'cc', 'option' => 'IQ', 'value' => 'Iraq'],
            ['user_id' => 20, 'scope' => 'cc', 'option' => 'IE', 'value' => 'Ireland'],
            ['user_id' => 20, 'scope' => 'cc', 'option' => 'IM', 'value' => 'Isle of Man'],
            ['user_id' => 20, 'scope' => 'cc', 'option' => 'IL', 'value' => 'Israel'],
            ['user_id' => 20, 'scope' => 'cc', 'option' => 'IT', 'value' => 'Italy'],
            ['user_id' => 20, 'scope' => 'cc', 'option' => 'JM', 'value' => 'Jamaica'],
            ['user_id' => 20, 'scope' => 'cc', 'option' => 'JP', 'value' => 'Japan'],
            ['user_id' => 20, 'scope' => 'cc', 'option' => 'JE', 'value' => 'Jersey'],
            ['user_id' => 20, 'scope' => 'cc', 'option' => 'JO', 'value' => 'Jordan'],
            ['user_id' => 20, 'scope' => 'cc', 'option' => 'KZ', 'value' => 'Kazakstan'],
            ['user_id' => 20, 'scope' => 'cc', 'option' => 'KE', 'value' => 'Kenya'],
            ['user_id' => 20, 'scope' => 'cc', 'option' => 'KI', 'value' => 'Kiribati'],
            ['user_id' => 20, 'scope' => 'cc', 'option' => 'KP', 'value' => 'Korea, Democratic People\'s Republic of'],
            ['user_id' => 20, 'scope' => 'cc', 'option' => 'KR', 'value' => 'Korea, Republic of'],
            ['user_id' => 20, 'scope' => 'cc', 'option' => 'KW', 'value' => 'Kuwait'],
            ['user_id' => 20, 'scope' => 'cc', 'option' => 'KG', 'value' => 'Kyrgyzstan'],
            ['user_id' => 20, 'scope' => 'cc', 'option' => 'LA', 'value' => 'Lao People\'s Democratic Republic'],
            ['user_id' => 20, 'scope' => 'cc', 'option' => 'LV', 'value' => 'Latvia'],
            ['user_id' => 20, 'scope' => 'cc', 'option' => 'LB', 'value' => 'Lebanon'],
            ['user_id' => 20, 'scope' => 'cc', 'option' => 'LS', 'value' => 'Lesotho'],
            ['user_id' => 20, 'scope' => 'cc', 'option' => 'LR', 'value' => 'Liberia'],
            ['user_id' => 20, 'scope' => 'cc', 'option' => 'LY', 'value' => 'Libyan Arab Jamahiriya'],
            ['user_id' => 20, 'scope' => 'cc', 'option' => 'LI', 'value' => 'Liechtenstein'],
            ['user_id' => 20, 'scope' => 'cc', 'option' => 'LT', 'value' => 'Lithuania'],
            ['user_id' => 20, 'scope' => 'cc', 'option' => 'LU', 'value' => 'Luxembourg'],
            ['user_id' => 20, 'scope' => 'cc', 'option' => 'MO', 'value' => 'Macau'],
            ['user_id' => 20, 'scope' => 'cc', 'option' => 'MK', 'value' => 'Macedonia'],
            ['user_id' => 20, 'scope' => 'cc', 'option' => 'MG', 'value' => 'Madagascar'],
            ['user_id' => 20, 'scope' => 'cc', 'option' => 'MW', 'value' => 'Malawi'],
            ['user_id' => 20, 'scope' => 'cc', 'option' => 'MY', 'value' => 'Malaysia'],
            ['user_id' => 20, 'scope' => 'cc', 'option' => 'MV', 'value' => 'Maldives'],
            ['user_id' => 20, 'scope' => 'cc', 'option' => 'ML', 'value' => 'Mali'],
            ['user_id' => 20, 'scope' => 'cc', 'option' => 'MT', 'value' => 'Malta'],
            ['user_id' => 20, 'scope' => 'cc', 'option' => 'MH', 'value' => 'Marshall Islands'],
            ['user_id' => 20, 'scope' => 'cc', 'option' => 'MQ', 'value' => 'Martinique'],
            ['user_id' => 20, 'scope' => 'cc', 'option' => 'MR', 'value' => 'Mauritania'],
            ['user_id' => 20, 'scope' => 'cc', 'option' => 'MU', 'value' => 'Mauritius'],
            ['user_id' => 20, 'scope' => 'cc', 'option' => 'YT', 'value' => 'Mayotte'],
            ['user_id' => 20, 'scope' => 'cc', 'option' => 'MX', 'value' => 'Mexico'],
            ['user_id' => 20, 'scope' => 'cc', 'option' => 'FM', 'value' => 'Micronesia, Federated States of'],
            ['user_id' => 20, 'scope' => 'cc', 'option' => 'MD', 'value' => 'Moldova, Republic of'],
            ['user_id' => 20, 'scope' => 'cc', 'option' => 'MC', 'value' => 'Monaco'],
            ['user_id' => 20, 'scope' => 'cc', 'option' => 'MN', 'value' => 'Mongolia'],
            ['user_id' => 20, 'scope' => 'cc', 'option' => 'ME', 'value' => 'Montenegro'],
            ['user_id' => 20, 'scope' => 'cc', 'option' => 'MS', 'value' => 'Montserrat'],
            ['user_id' => 20, 'scope' => 'cc', 'option' => 'MA', 'value' => 'Morocco'],
            ['user_id' => 20, 'scope' => 'cc', 'option' => 'MZ', 'value' => 'Mozambique'],
            ['user_id' => 20, 'scope' => 'cc', 'option' => 'MM', 'value' => 'Myanmar'],
            ['user_id' => 20, 'scope' => 'cc', 'option' => 'NA', 'value' => 'Namibia'],
            ['user_id' => 20, 'scope' => 'cc', 'option' => 'NR', 'value' => 'Nauru'],
            ['user_id' => 20, 'scope' => 'cc', 'option' => 'NP', 'value' => 'Nepal'],
            ['user_id' => 20, 'scope' => 'cc', 'option' => 'NL', 'value' => 'Netherlands'],
            ['user_id' => 20, 'scope' => 'cc', 'option' => 'NC', 'value' => 'New Caledonia'],
            ['user_id' => 20, 'scope' => 'cc', 'option' => 'NZ', 'value' => 'New Zealand'],
            ['user_id' => 20, 'scope' => 'cc', 'option' => 'NI', 'value' => 'Nicaragua'],
            ['user_id' => 20, 'scope' => 'cc', 'option' => 'NE', 'value' => 'Niger'],
            ['user_id' => 20, 'scope' => 'cc', 'option' => 'NG', 'value' => 'Nigeria'],
            ['user_id' => 20, 'scope' => 'cc', 'option' => 'NU', 'value' => 'Niue'],
            ['user_id' => 20, 'scope' => 'cc', 'option' => 'NF', 'value' => 'Norfolk Island'],
            ['user_id' => 20, 'scope' => 'cc', 'option' => 'MP', 'value' => 'Northern Mariana Islands'],
            ['user_id' => 20, 'scope' => 'cc', 'option' => 'NO', 'value' => 'Norway'],
            ['user_id' => 20, 'scope' => 'cc', 'option' => 'OM', 'value' => 'Oman'],
            ['user_id' => 20, 'scope' => 'cc', 'option' => 'O1', 'value' => 'Other'],
            ['user_id' => 20, 'scope' => 'cc', 'option' => 'PK', 'value' => 'Pakistan'],
            ['user_id' => 20, 'scope' => 'cc', 'option' => 'PW', 'value' => 'Palau'],
            ['user_id' => 20, 'scope' => 'cc', 'option' => 'PS', 'value' => 'Palestinian Territory'],
            ['user_id' => 20, 'scope' => 'cc', 'option' => 'PA', 'value' => 'Panama'],
            ['user_id' => 20, 'scope' => 'cc', 'option' => 'PG', 'value' => 'Papua New Guinea'],
            ['user_id' => 20, 'scope' => 'cc', 'option' => 'PY', 'value' => 'Paraguay'],
            ['user_id' => 20, 'scope' => 'cc', 'option' => 'PE', 'value' => 'Peru'],
            ['user_id' => 20, 'scope' => 'cc', 'option' => 'PH', 'value' => 'Philippines'],
            ['user_id' => 20, 'scope' => 'cc', 'option' => 'PN', 'value' => 'Pitcairn Islands'],
            ['user_id' => 20, 'scope' => 'cc', 'option' => 'PL', 'value' => 'Poland'],
            ['user_id' => 20, 'scope' => 'cc', 'option' => 'PT', 'value' => 'Portugal'],
            ['user_id' => 20, 'scope' => 'cc', 'option' => 'PR', 'value' => 'Puerto Rico'],
            ['user_id' => 20, 'scope' => 'cc', 'option' => 'QA', 'value' => 'Qatar'],
            ['user_id' => 20, 'scope' => 'cc', 'option' => 'RE', 'value' => 'Reunion'],
            ['user_id' => 20, 'scope' => 'cc', 'option' => 'RO', 'value' => 'Romania'],
            ['user_id' => 20, 'scope' => 'cc', 'option' => 'RU', 'value' => 'Russian Federation'],
            ['user_id' => 20, 'scope' => 'cc', 'option' => 'RW', 'value' => 'Rwanda'],
            ['user_id' => 20, 'scope' => 'cc', 'option' => 'BL', 'value' => 'Saint Barthelemy'],
            ['user_id' => 20, 'scope' => 'cc', 'option' => 'SH', 'value' => 'Saint Helena'],
            ['user_id' => 20, 'scope' => 'cc', 'option' => 'KN', 'value' => 'Saint Kitts and Nevis'],
            ['user_id' => 20, 'scope' => 'cc', 'option' => 'LC', 'value' => 'Saint Lucia'],
            ['user_id' => 20, 'scope' => 'cc', 'option' => 'MF', 'value' => 'Saint Martin'],
            ['user_id' => 20, 'scope' => 'cc', 'option' => 'PM', 'value' => 'Saint Pierre and Miquelon'],
            ['user_id' => 20, 'scope' => 'cc', 'option' => 'VC', 'value' => 'Saint Vincent and the Grenadines'],
            ['user_id' => 20, 'scope' => 'cc', 'option' => 'WS', 'value' => 'Samoa'],
            ['user_id' => 20, 'scope' => 'cc', 'option' => 'SM', 'value' => 'San Marino'],
            ['user_id' => 20, 'scope' => 'cc', 'option' => 'ST', 'value' => 'Sao Tome and Principe'],
            ['user_id' => 20, 'scope' => 'cc', 'option' => 'SA', 'value' => 'Saudi Arabia'],
            ['user_id' => 20, 'scope' => 'cc', 'option' => 'SN', 'value' => 'Senegal'],
            ['user_id' => 20, 'scope' => 'cc', 'option' => 'RS', 'value' => 'Serbia'],
            ['user_id' => 20, 'scope' => 'cc', 'option' => 'SC', 'value' => 'Seychelles'],
            ['user_id' => 20, 'scope' => 'cc', 'option' => 'SL', 'value' => 'Sierra Leone'],
            ['user_id' => 20, 'scope' => 'cc', 'option' => 'SG', 'value' => 'Singapore'],
            ['user_id' => 20, 'scope' => 'cc', 'option' => 'SX', 'value' => 'Sint Maarten (Dutch part)'],
            ['user_id' => 20, 'scope' => 'cc', 'option' => 'SK', 'value' => 'Slovakia'],
            ['user_id' => 20, 'scope' => 'cc', 'option' => 'SI', 'value' => 'Slovenia'],
            ['user_id' => 20, 'scope' => 'cc', 'option' => 'SB', 'value' => 'Solomon Islands'],
            ['user_id' => 20, 'scope' => 'cc', 'option' => 'SO', 'value' => 'Somalia'],
            ['user_id' => 20, 'scope' => 'cc', 'option' => 'ZA', 'value' => 'South Africa'],
            ['user_id' => 20, 'scope' => 'cc', 'option' => 'GS', 'value' => 'South Georgia and the S. Sandwich Isl.'],
            ['user_id' => 20, 'scope' => 'cc', 'option' => 'SS', 'value' => 'South Sudan'],
            ['user_id' => 20, 'scope' => 'cc', 'option' => 'ES', 'value' => 'Spain'],
            ['user_id' => 20, 'scope' => 'cc', 'option' => 'LK', 'value' => 'Sri Lanka'],
            ['user_id' => 20, 'scope' => 'cc', 'option' => 'SD', 'value' => 'Sudan'],
            ['user_id' => 20, 'scope' => 'cc', 'option' => 'SR', 'value' => 'Suriname'],
            ['user_id' => 20, 'scope' => 'cc', 'option' => 'SJ', 'value' => 'Svalbard and Jan Mayen'],
            ['user_id' => 20, 'scope' => 'cc', 'option' => 'SZ', 'value' => 'Swaziland'],
            ['user_id' => 20, 'scope' => 'cc', 'option' => 'SE', 'value' => 'Sweden'],
            ['user_id' => 20, 'scope' => 'cc', 'option' => 'CH', 'value' => 'Switzerland'],
            ['user_id' => 20, 'scope' => 'cc', 'option' => 'SY', 'value' => 'Syrian Arab Republic'],
            ['user_id' => 20, 'scope' => 'cc', 'option' => 'TW', 'value' => 'Taiwan'],
            ['user_id' => 20, 'scope' => 'cc', 'option' => 'TJ', 'value' => 'Tajikistan'],
            ['user_id' => 20, 'scope' => 'cc', 'option' => 'TZ', 'value' => 'Tanzania, United Republic of'],
            ['user_id' => 20, 'scope' => 'cc', 'option' => 'TH', 'value' => 'Thailand'],
            ['user_id' => 20, 'scope' => 'cc', 'option' => 'TL', 'value' => 'Timor-Leste'],
            ['user_id' => 20, 'scope' => 'cc', 'option' => 'TG', 'value' => 'Togo'],
            ['user_id' => 20, 'scope' => 'cc', 'option' => 'TK', 'value' => 'Tokelau'],
            ['user_id' => 20, 'scope' => 'cc', 'option' => 'TO', 'value' => 'Tonga'],
            ['user_id' => 20, 'scope' => 'cc', 'option' => 'TT', 'value' => 'Trinidad and Tobago'],
            ['user_id' => 20, 'scope' => 'cc', 'option' => 'TN', 'value' => 'Tunisia'],
            ['user_id' => 20, 'scope' => 'cc', 'option' => 'TR', 'value' => 'Turkey'],
            ['user_id' => 20, 'scope' => 'cc', 'option' => 'TM', 'value' => 'Turkmenistan'],
            ['user_id' => 20, 'scope' => 'cc', 'option' => 'TC', 'value' => 'Turks and Caicos Islands'],
            ['user_id' => 20, 'scope' => 'cc', 'option' => 'TV', 'value' => 'Tuvalu'],
            ['user_id' => 20, 'scope' => 'cc', 'option' => 'UG', 'value' => 'Uganda'],
            ['user_id' => 20, 'scope' => 'cc', 'option' => 'UA', 'value' => 'Ukraine'],
            ['user_id' => 20, 'scope' => 'cc', 'option' => 'AE', 'value' => 'United Arab Emirates'],
            ['user_id' => 20, 'scope' => 'cc', 'option' => 'GB', 'value' => 'United Kingdom'],
            ['user_id' => 20, 'scope' => 'cc', 'option' => 'US', 'value' => 'United States'],
            ['user_id' => 20, 'scope' => 'cc', 'option' => 'UM', 'value' => 'United States Minor Outlying Islands'],
            ['user_id' => 20, 'scope' => 'cc', 'option' => 'UY', 'value' => 'Uruguay'],
            ['user_id' => 20, 'scope' => 'cc', 'option' => 'UZ', 'value' => 'Uzbekistan'],
            ['user_id' => 20, 'scope' => 'cc', 'option' => 'VU', 'value' => 'Vanuatu'],
            ['user_id' => 20, 'scope' => 'cc', 'option' => 'VE', 'value' => 'Venezuela'],
            ['user_id' => 20, 'scope' => 'cc', 'option' => 'VN', 'value' => 'Vietnam'],
            ['user_id' => 20, 'scope' => 'cc', 'option' => 'VG', 'value' => 'Virgin Islands, British'],
            ['user_id' => 20, 'scope' => 'cc', 'option' => 'VI', 'value' => 'Virgin Islands, U.S.'],
            ['user_id' => 20, 'scope' => 'cc', 'option' => 'WF', 'value' => 'Wallis and Futuna'],
            ['user_id' => 20, 'scope' => 'cc', 'option' => 'EH', 'value' => 'Western Sahara'],
            ['user_id' => 20, 'scope' => 'cc', 'option' => 'YE', 'value' => 'Yemen'],
            ['user_id' => 20, 'scope' => 'cc', 'option' => 'ZM', 'value' => 'Zambia'],
            ['user_id' => 20, 'scope' => 'cc', 'option' => 'ZW', 'value' => 'Zimbabwe'],
            ['user_id' => 20, 'scope' => 'payment_schedule', 'option' => 'Net30', 'value' => 'Your payment schedule is Net30, which means you will get paid on 30st February for earnings generated until 31st January.'],
            ['user_id' => 20, 'scope' => 'payment_schedule', 'option' => 'Net15', 'value' => 'Your payment schedule is Net15, which means you will get paid on 15th April for earnings generated until 31st March.'],
            ['user_id' => 20, 'scope' => 'payment_schedule', 'option' => 'Net0', 'value' => 'Your payment schedule is Net0, which means you will get paid on 3rd April for earnings generated until 31st March.'],
            ['user_id' => 20, 'scope' => 'payment_schedule', 'option' => 'Bi-Weekly', 'value' => 'Your payment schedule is Bi-Weekly, which means you will get paid on 31st March for earnings generated until 15th March'],
            ['user_id' => 20, 'scope' => 'payment_schedule', 'option' => 'Weekly', 'value' => 'Your payment schedule is Weekly, which means you will get paid every friday for earnings generated previous week.'],
            ['user_id' => 20, 'scope' => 'generalsettings', 'option' => 'description', 'value' => 'Next Generation cpa network'],
            ['user_id' => 20, 'scope' => 'generalsettings', 'option' => 'keywords', 'value' => 'ppd, monetisation, content locking, affiliate marketing, cpa'],
            ['user_id' => 20, 'scope' => 'generalsettings', 'option' => 'adminemail', 'value' => 'adgodmedia@gmail.com'],
            ['user_id' => 20, 'scope' => 'generalsettings', 'option' => 'paypalemail', 'value' => 'paypal@adgodmedia.com'],
            ['user_id' => 20, 'scope' => 'generalsettings', 'option' => 'quickpayout', 'value' => 0],
            ['user_id' => 20, 'scope' => 'generalsettings', 'option' => 'quickpayoutfee', 'value' => 20],
            ['user_id' => 20, 'scope' => 'generalsettings', 'option' => 'offerrate', 'value' => 78],
            ['user_id' => 20, 'scope' => 'generalsettings', 'option' => 'referralrate', 'value' => 5],
            ['user_id' => 20, 'scope' => 'generalsettings', 'option' => 'autoapprove', 'value' => 0],
            ['user_id' => 20, 'scope' => 'generalsettings', 'option' => 'mincashout', 'value' => 10],
            ['user_id' => 20, 'scope' => 'generalsettings', 'option' => 'secrettoken', 'value' => 'Hiddenpassword'],
            ['user_id' => 20, 'scope' => 'generalsettings', 'option' => 'totalpayments', 'value' => '109586'],
        );
 
        // Uncomment the below to run the seeder
        DB::table('settings')->insert($msgs);
    }
 
}