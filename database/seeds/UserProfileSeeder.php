<?php
 
use Illuminate\Database\Seeder;
 
class UserProfileSeeder extends Seeder {
 
    public function run()
    {
        // Uncomment the below to wipe the table clean before populating
        //DB::table('user_profiles')->delete();
 
        // Uncomment the below to run the seeder
        DB::table('user_profiles')->insert(['user_id' => 27, 'address' => 'No. 5480804', 'city' => 'Raven Cir Wyoming', 'zip' => '19934', 'country' => 'Germany', 'websites' => 'adgodmedia.com', 'gender' => 'Male', 'register_ip' => ip2long('192.168.1.1'), 'skype' => 'adgodmedia.com']);
    }
 
}