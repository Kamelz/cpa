<?php
 
use Illuminate\Database\Seeder;
 
class UserSeeder extends Seeder {
 
    public function run()
    {
        // Uncomment the below to wipe the table clean before populating
        DB::table('users')->delete();
 
        $users = array(
            ['id' => 27, 'name' => 'Affiliate C.M', 'chathandle' => 'Affilemanager', 'email' => 'adgodmedia@gmail.com', 'password' => Hash::make('uradminpass'), 'google2fa_secret' => 'PGMKYCLEVCZMGRUYJPOUH3WPGRHGWC3T','role' => 'super_admin', 'read_msgs' => 'a:0:{}'],
            ['id' => 10, 'name' => 'Public', 'chathandle' => 'Public', 'email' => 'publicbot@adgodmedia.com', 'password' => Hash::make('cXBo4VSFbKZ7'), 'google2fa_secret' => 'PGMKYCLEVCZMGRUYJPOUH3WPGRHGWC3T','role' => 'bot', 'read_msgs' => 'a:0:{}'],
            ['id' => 20, 'name' => 'System', 'chathandle' => 'System', 'email' => 'systembot@gmail.com', 'password' => Hash::make('ZcrzsTBF9AuL'), 'google2fa_secret' => 'PGMKYCLEVCZMGRUYJPOUH3WPGRHGWC3T','role' => 'bot', 'read_msgs' => 'a:0:{}'],
        );
 
        // Uncomment the below to run the seeder
        DB::table('users')->insert($users);
    }
 
}