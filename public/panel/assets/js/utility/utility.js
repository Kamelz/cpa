/*
 * FitText.js 1.2
 * Copyright 2011, Dave Rupert http://daverupert.com
 * Released under the WTFPL license
 * http://sam.zoy.org/wtfpl/
 */
;
(function(e) {
   e.fn.fitText = function(t, n) {
      var r = t || 1,
         i = e.extend({
            minFontSize: Number.NEGATIVE_INFINITY,
            maxFontSize: Number.POSITIVE_INFINITY
         }, n);
      return this.each(function() {
         var t = e(this);
         var n = function() {
            t.css("font-size", Math.max(Math.min(t.width() / (r * 10), parseFloat(i.maxFontSize)), parseFloat(i.minFontSize)))
         };
         n();
         e(window).on("resize.fittext orientationchange.fittext", n)
      })
   }
})(jQuery)

/*
 * jQuery.fullscreen library v0.4.2
 * Copyright (c) 2013 Vladimir Zhuravlev
 * @license https://github.com/private-face/jquery.fullscreen/blob/master/LICENSE
 */
;
(function(e) {
   function t(e) {
      return typeof e !== "undefined"
   }

   function n(t, n, r) {
      var i = function() {};
      i.prototype = n.prototype;
      t.prototype = new i;
      t.prototype.constructor = t;
      n.prototype.constructor = n;
      t._super = n.prototype;
      if (r) {
         e.extend(t.prototype, r)
      }
   }

   function s(e, n) {
      var s;
      if (typeof e === "string") {
         n = e;
         e = document
      }
      for (var o = 0; o < r.length; ++o) {
         n = n.replace(r[o][0], r[o][1]);
         for (var u = 0; u < i.length; ++u) {
            s = i[u];
            s += u === 0 ? n : n.charAt(0).toUpperCase() + n.substr(1);
            if (t(e[s])) {
               return e[s]
            }
         }
      }
      return void 0
   }
   var r = [
      ["", ""],
      ["exit", "cancel"],
      ["screen", "Screen"]
   ];
   var i = ["", "o", "ms", "moz", "webkit", "webkitCurrent"];
   var o = navigator.userAgent;
   var u = s("fullscreenEnabled");
   var a = o.indexOf("Android") !== -1 && o.indexOf("Chrome") !== -1;
   var f = !a && t(s("fullscreenElement")) && (!t(u) || u === true);
   var l = e.fn.jquery.split(".");
   var c = parseInt(l[0]) < 2 && parseInt(l[1]) < 7;
   var h = function() {
      this.__options = null;
      this._fullScreenElement = null;
      this.__savedStyles = {}
   };
   h.prototype = {
      _DEFAULT_OPTIONS: {
         styles: {
            boxSizing: "border-box",
            MozBoxSizing: "border-box",
            WebkitBoxSizing: "border-box"
         },
         toggleClass: null
      },
      __documentOverflow: "visible",
      __htmlOverflow: "visible",
      _preventDocumentScroll: function() {
         this.__documentOverflow = e("body")[0].style.overflow;
         this.__htmlOverflow = e("html")[0].style.overflow
      },
      _allowDocumentScroll: function() {
         e("body")[0].style.overflow = this.__documentOverflow;
         e("html")[0].style.overflow = this.__htmlOverflow
      },
      _fullScreenChange: function() {
         if (!this.__options) return;
         if (!this.isFullScreen()) {
            this._allowDocumentScroll();
            this._revertStyles();
            this._triggerEvents();
            this._fullScreenElement = null
         } else {
            this._preventDocumentScroll();
            this._triggerEvents()
         }
      },
      _fullScreenError: function(t) {
         if (!this.__options) return;
         this._revertStyles();
         this._fullScreenElement = null;
         if (t) {
            e(document).trigger("fscreenerror", [t])
         }
      },
      _triggerEvents: function() {
         e(this._fullScreenElement).trigger(this.isFullScreen() ? "fscreenopen" : "fscreenclose");
         e(document).trigger("fscreenchange", [this.isFullScreen(), this._fullScreenElement])
      },
      _saveAndApplyStyles: function() {
         var t = e(this._fullScreenElement);
         this.__savedStyles = {};
         for (var n in this.__options.styles) {
            this.__savedStyles[n] = this._fullScreenElement.style[n];
            this._fullScreenElement.style[n] = this.__options.styles[n]
         }
         if (this.__options.toggleClass) {
            t.addClass(this.__options.toggleClass)
         }
      },
      _revertStyles: function() {
         var t = e(this._fullScreenElement);
         for (var n in this.__options.styles) {
            this._fullScreenElement.style[n] = this.__savedStyles[n]
         }
         if (this.__options.toggleClass) {
            t.removeClass(this.__options.toggleClass)
         }
      },
      open: function(t, n) {
         if (t === this._fullScreenElement) {
            return
         }
         if (this.isFullScreen()) {
            this.exit()
         }
         this._fullScreenElement = t;
         this.__options = e.extend(true, {}, this._DEFAULT_OPTIONS, n);
         this._saveAndApplyStyles()
      },
      exit: null,
      isFullScreen: null,
      isNativelySupported: function() {
         return f
      }
   };
   var p = function() {
      p._super.constructor.apply(this, arguments);
      this.exit = e.proxy(s("exitFullscreen"), document);
      this._DEFAULT_OPTIONS = e.extend(true, {}, this._DEFAULT_OPTIONS, {
         styles: {
            width: "100%",
            height: "100%"
         }
      });
      e(document).bind(this._prefixedString("fullscreenchange") + " MSFullscreenChange", e.proxy(this._fullScreenChange, this)).bind(this._prefixedString("fullscreenerror") + " MSFullscreenError", e.proxy(this._fullScreenError, this))
   };
   n(p, h, {
      VENDOR_PREFIXES: ["", "o", "moz", "webkit"],
      _prefixedString: function(t) {
         return e.map(this.VENDOR_PREFIXES, function(e) {
            return e + t
         }).join(" ")
      },
      open: function(e, t) {
         p._super.open.apply(this, arguments);
         var n = s(e, "requestFullscreen");
         n.call(e)
      },
      exit: e.noop,
      isFullScreen: function() {
         return s("fullscreenElement") !== null
      },
      element: function() {
         return s("fullscreenElement")
      }
   });
   var d = function() {
      d._super.constructor.apply(this, arguments);
      this._DEFAULT_OPTIONS = e.extend({}, this._DEFAULT_OPTIONS, {
         styles: {
            position: "fixed",
            zIndex: "2147483647",
            left: 0,
            top: 0,
            bottom: 0,
            right: 0
         }
      });
      this.__delegateKeydownHandler()
   };
   n(d, h, {
      __isFullScreen: false,
      __delegateKeydownHandler: function() {
         var t = e(document);
         t.delegate("*", "keydown.fullscreen", e.proxy(this.__keydownHandler, this));
         var n = c ? t.data("events") : e._data(document).events;
         var r = n["keydown"];
         if (!c) {
            r.splice(0, 0, r.splice(r.delegateCount - 1, 1)[0])
         } else {
            n.live.unshift(n.live.pop())
         }
      },
      __keydownHandler: function(e) {
         if (this.isFullScreen() && e.which === 27) {
            this.exit();
            return false
         }
         return true
      },
      _revertStyles: function() {
         d._super._revertStyles.apply(this, arguments);
         this._fullScreenElement.offsetHeight
      },
      open: function(e) {
         d._super.open.apply(this, arguments);
         this.__isFullScreen = true;
         this._fullScreenChange()
      },
      exit: function() {
         this.__isFullScreen = false;
         this._fullScreenChange()
      },
      isFullScreen: function() {
         return this.__isFullScreen
      },
      element: function() {
         return this.__isFullScreen ? this._fullScreenElement : null
      }
   });
   e.fullscreen = f ? new p : new d;
   e.fn.fullscreen = function(t) {
      var n = this[0];
      t = e.extend({
         toggleClass: null,
         overflow: "hidden"
      }, t);
      t.styles = {
         overflow: t.overflow
      };
      delete t.overflow;
      if (n) {
         e.fullscreen.open(n, t)
      }
      return this
   }
})(jQuery)

/*
 * Bootstrap Multiselect v0.9.8 (https://github.com/davidstutz/bootstrap-multiselect)
 * Dual licensed under the BSD-3-Clause and the Apache License, Version 2.0.
 * Copyright 2012 - 2014 David Stutz
 */
;
! function(e) {
   "use strict";

   function t(e) {
      return ko.isObservable(e) && !(e.destroyAll === undefined)
   }

   function n(e, t) {
      for (var n = 0; n < e.length; ++n) {
         t(e[n])
      }
   }

   function r(t, n) {
      this.$select = e(t);
      this.options = this.mergeOptions(e.extend({}, n, this.$select.data()));
      this.originalOptions = this.$select.clone()[0].options;
      this.query = "";
      this.searchTimeout = null;
      this.options.multiple = this.$select.attr("multiple") === "multiple";
      this.options.onChange = e.proxy(this.options.onChange, this);
      this.options.onDropdownShow = e.proxy(this.options.onDropdownShow, this);
      this.options.onDropdownHide = e.proxy(this.options.onDropdownHide, this);
      this.options.onDropdownShown = e.proxy(this.options.onDropdownShown, this);
      this.options.onDropdownHidden = e.proxy(this.options.onDropdownHidden, this);
      this.buildContainer();
      this.buildButton();
      this.buildDropdown();
      this.buildSelectAll();
      this.buildDropdownOptions();
      this.buildFilter();
      this.updateButtonText();
      this.updateSelectAll();
      if (this.options.disableIfEmpty && e("option", this.$select).length <= 0) {
         this.disable()
      }
      this.$select.hide().after(this.$container)
   }
   if (typeof ko !== "undefined" && ko.bindingHandlers && !ko.bindingHandlers.multiselect) {
      ko.bindingHandlers.multiselect = {
         init: function(r, i, s, o, u) {
            var a = s().selectedOptions;
            var f = ko.utils.unwrapObservable(i());
            e(r).multiselect(f);
            if (t(a)) {
               e(r).multiselect("select", ko.utils.unwrapObservable(a));
               a.subscribe(function(t) {
                  var i = [],
                     s = [];
                  n(t, function(e) {
                     switch (e.status) {
                        case "added":
                           i.push(e.value);
                           break;
                        case "deleted":
                           s.push(e.value);
                           break
                     }
                  });
                  if (i.length > 0) {
                     e(r).multiselect("select", i)
                  }
                  if (s.length > 0) {
                     e(r).multiselect("deselect", s)
                  }
               }, null, "arrayChange")
            }
         },
         update: function(n, r, i, s, o) {
            var u = i().options,
               a = e(n).data("multiselect"),
               f = ko.utils.unwrapObservable(r());
            if (t(u)) {
               u.subscribe(function(t) {
                  e(n).multiselect("rebuild")
               })
            }
            if (!a) {
               e(n).multiselect(f)
            } else {
               a.updateOriginalOptions()
            }
         }
      }
   }
   r.prototype = {
      defaults: {
         buttonText: function(t, n) {
            if (t.length === 0) {
               return this.nonSelectedText + ' <b class="caret"></b>'
            } else if (t.length == e("option", e(n)).length) {
               return this.allSelectedText + ' <b class="caret"></b>'
            } else if (t.length > this.numberDisplayed) {
               return t.length + " " + this.nSelectedText + ' <b class="caret"></b>'
            } else {
               var r = "";
               t.each(function() {
                  var t = e(this).attr("label") !== undefined ? e(this).attr("label") : e(this).html();
                  r += t + ", "
               });
               return r.substr(0, r.length - 2) + ' <b class="caret"></b>'
            }
         },
         buttonTitle: function(t, n) {
            if (t.length === 0) {
               return this.nonSelectedText
            } else {
               var r = "";
               t.each(function() {
                  r += e(this).text() + ", "
               });
               return r.substr(0, r.length - 2)
            }
         },
         label: function(t) {
            return e(t).attr("label") || e(t).html()
         },
         onChange: function(e, t) {},
         onDropdownShow: function(e) {},
         onDropdownHide: function(e) {},
         onDropdownShown: function(e) {},
         onDropdownHidden: function(e) {},
         buttonClass: "btn btn-default",
         buttonWidth: "auto",
         buttonContainer: '<div class="btn-group" />',
         dropRight: false,
         selectedClass: "active",
         maxHeight: false,
         checkboxName: false,
         includeSelectAllOption: false,
         includeSelectAllIfMoreThan: 0,
         selectAllText: " Select all",
         selectAllValue: "multiselect-all",
         selectAllName: false,
         enableFiltering: false,
         enableCaseInsensitiveFiltering: false,
         enableClickableOptGroups: false,
         filterPlaceholder: "Search",
         filterBehavior: "text",
         includeFilterClearBtn: true,
         preventInputChangeEvent: false,
         nonSelectedText: "None selected",
         nSelectedText: "selected",
         allSelectedText: "All selected",
         numberDisplayed: 3,
         disableIfEmpty: false,
         templates: {
            button: '<button type="button" class="multiselect dropdown-toggle" data-toggle="dropdown"></button>',
            ul: '<ul class="multiselect-container dropdown-menu"></ul>',
            filter: '<li class="multiselect-item filter"><div class="input-group"><span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span><input class="form-control multiselect-search" type="text"></div></li>',
            filterClearBtn: '<span class="input-group-btn"><button class="btn btn-default multiselect-clear-filter" type="button"><i class="glyphicon glyphicon-remove"></i></button></span>',
            li: '<li><a href="javascript:void(0);"><label></label></a></li>',
            divider: '<li class="multiselect-item divider"></li>',
            liGroup: '<li class="multiselect-item multiselect-group"><label></label></li>'
         }
      },
      constructor: r,
      buildContainer: function() {
         this.$container = e(this.options.buttonContainer);
         this.$container.on("show.bs.dropdown", this.options.onDropdownShow);
         this.$container.on("hide.bs.dropdown", this.options.onDropdownHide);
         this.$container.on("shown.bs.dropdown", this.options.onDropdownShown);
         this.$container.on("hidden.bs.dropdown", this.options.onDropdownHidden)
      },
      buildButton: function() {
         this.$button = e(this.options.templates.button).addClass(this.options.buttonClass);
         if (this.$select.prop("disabled")) {
            this.disable()
         } else {
            this.enable()
         }
         if (this.options.buttonWidth && this.options.buttonWidth !== "auto") {
            this.$button.css({
               width: this.options.buttonWidth
            });
            this.$container.css({
               width: this.options.buttonWidth
            })
         }
         var t = this.$select.attr("tabindex");
         if (t) {
            this.$button.attr("tabindex", t)
         }
         this.$container.prepend(this.$button)
      },
      buildDropdown: function() {
         this.$ul = e(this.options.templates.ul);
         if (this.options.dropRight) {
            this.$ul.addClass("pull-right")
         }
         if (this.options.maxHeight) {
            this.$ul.css({
               "max-height": this.options.maxHeight + "px",
               "overflow-y": "auto",
               "overflow-x": "hidden"
            })
         }
         this.$container.append(this.$ul)
      },
      buildDropdownOptions: function() {
         this.$select.children().each(e.proxy(function(t, n) {
            var r = e(n);
            var i = r.prop("tagName").toLowerCase();
            if (r.prop("value") === this.options.selectAllValue) {
               return
            }
            if (i === "optgroup") {
               this.createOptgroup(n)
            } else if (i === "option") {
               if (r.data("role") === "divider") {
                  this.createDivider()
               } else {
                  this.createOptionValue(n)
               }
            }
         }, this));
         e("li input", this.$ul).on("change", e.proxy(function(t) {
            var n = e(t.target);
            var r = n.prop("checked") || false;
            var i = n.val() === this.options.selectAllValue;
            if (this.options.selectedClass) {
               if (r) {
                  n.closest("li").addClass(this.options.selectedClass)
               } else {
                  n.closest("li").removeClass(this.options.selectedClass)
               }
            }
            var s = n.val();
            var o = this.getOptionByValue(s);
            var u = e("option", this.$select).not(o);
            var a = e("input", this.$container).not(n);
            if (i) {
               if (r) {
                  this.selectAll()
               } else {
                  this.deselectAll()
               }
            }
            if (!i) {
               if (r) {
                  o.prop("selected", true);
                  if (this.options.multiple) {
                     o.prop("selected", true)
                  } else {
                     if (this.options.selectedClass) {
                        e(a).closest("li").removeClass(this.options.selectedClass)
                     }
                     e(a).prop("checked", false);
                     u.prop("selected", false);
                     this.$button.click()
                  }
                  if (this.options.selectedClass === "active") {
                     u.closest("a").css("outline", "")
                  }
               } else {
                  o.prop("selected", false)
               }
            }
            this.$select.change();
            this.updateButtonText();
            this.updateSelectAll();
            this.options.onChange(o, r);
            if (this.options.preventInputChangeEvent) {
               return false
            }
         }, this));
         e("li a", this.$ul).on("touchstart click", function(t) {
            t.stopPropagation();
            var n = e(t.target);
            if (document.getSelection().type === "Range") {
               var r = e(this).find("input:first");
               r.prop("checked", !r.prop("checked")).trigger("change")
            }
            if (t.shiftKey) {
               var i = n.prop("checked") || false;
               if (i) {
                  var s = n.closest("li").siblings('li[class="active"]:first');
                  var o = n.closest("li").index();
                  var u = s.index();
                  if (o > u) {
                     n.closest("li").prevUntil(s).each(function() {
                        e(this).find("input:first").prop("checked", true).trigger("change")
                     })
                  } else {
                     n.closest("li").nextUntil(s).each(function() {
                        e(this).find("input:first").prop("checked", true).trigger("change")
                     })
                  }
               }
            }
            n.blur()
         });
         this.$container.off("keydown.multiselect").on("keydown.multiselect", e.proxy(function(t) {
            if (e('input[type="text"]', this.$container).is(":focus")) {
               return
            }
            if (t.keyCode === 9 && this.$container.hasClass("open")) {
               this.$button.click()
            } else {
               var n = e(this.$container).find("li:not(.divider):not(.disabled) a").filter(":visible");
               if (!n.length) {
                  return
               }
               var r = n.index(n.filter(":focus"));
               if (t.keyCode === 38 && r > 0) {
                  r--
               } else if (t.keyCode === 40 && r < n.length - 1) {
                  r++
               } else if (!~r) {
                  r = 0
               }
               var i = n.eq(r);
               i.focus();
               if (t.keyCode === 32 || t.keyCode === 13) {
                  var s = i.find("input");
                  s.prop("checked", !s.prop("checked"));
                  s.change()
               }
               t.stopPropagation();
               t.preventDefault()
            }
         }, this));
         if (this.options.enableClickableOptGroups && this.options.multiple) {
            e("li.multiselect-group", this.$ul).on("click", e.proxy(function(t) {
               t.stopPropagation();
               var n = e(t.target).parent();
               var r = n.nextUntil("li.multiselect-group");
               var i = true;
               var s = r.find("input");
               s.each(function() {
                  i = i && e(this).prop("checked")
               });
               s.prop("checked", !i).trigger("change")
            }, this))
         }
      },
      createOptionValue: function(t) {
         var n = e(t);
         if (n.is(":selected")) {
            n.prop("selected", true)
         }
         var r = this.options.label(t);
         var i = n.val();
         var s = this.options.multiple ? "checkbox" : "radio";
         var o = e(this.options.templates.li);
         var u = e("label", o);
         u.addClass(s);
         var a = e("<input/>").attr("type", s);
         if (this.options.checkboxName) {
            a.attr("name", this.options.checkboxName)
         }
         u.append(a);
         var f = n.prop("selected") || false;
         a.val(i);
         if (i === this.options.selectAllValue) {
            o.addClass("multiselect-item multiselect-all");
            a.parent().parent().addClass("multiselect-all")
         }
         u.append(" " + r);
         u.attr("title", n.attr("title"));
         this.$ul.append(o);
         if (n.is(":disabled")) {
            a.attr("disabled", "disabled").prop("disabled", true).closest("a").attr("tabindex", "-1").closest("li").addClass("disabled")
         }
         a.prop("checked", f);
         if (f && this.options.selectedClass) {
            a.closest("li").addClass(this.options.selectedClass)
         }
      },
      createDivider: function(t) {
         var n = e(this.options.templates.divider);
         this.$ul.append(n)
      },
      createOptgroup: function(t) {
         var n = e(t).prop("label");
         var r = e(this.options.templates.liGroup);
         e("label", r).text(n);
         if (this.options.enableClickableOptGroups) {
            r.addClass("multiselect-group-clickable")
         }
         this.$ul.append(r);
         if (e(t).is(":disabled")) {
            r.addClass("disabled")
         }
         e("option", t).each(e.proxy(function(e, t) {
            this.createOptionValue(t)
         }, this))
      },
      buildSelectAll: function() {
         if (typeof this.options.selectAllValue === "number") {
            this.options.selectAllValue = this.options.selectAllValue.toString()
         }
         var t = this.hasSelectAll();
         if (!t && this.options.includeSelectAllOption && this.options.multiple && e("option", this.$select).length > this.options.includeSelectAllIfMoreThan) {
            if (this.options.includeSelectAllDivider) {
               this.$ul.prepend(e(this.options.templates.divider))
            }
            var n = e(this.options.templates.li);
            e("label", n).addClass("checkbox");
            if (this.options.selectAllName) {
               e("label", n).append('<input type="checkbox" name="' + this.options.selectAllName + '" />')
            } else {
               e("label", n).append('<input type="checkbox" />')
            }
            var r = e("input", n);
            r.val(this.options.selectAllValue);
            n.addClass("multiselect-item multiselect-all");
            r.parent().parent().addClass("multiselect-all");
            e("label", n).append(" " + this.options.selectAllText);
            this.$ul.prepend(n);
            r.prop("checked", false)
         }
      },
      buildFilter: function() {
         if (this.options.enableFiltering || this.options.enableCaseInsensitiveFiltering) {
            var t = Math.max(this.options.enableFiltering, this.options.enableCaseInsensitiveFiltering);
            if (this.$select.find("option").length >= t) {
               this.$filter = e(this.options.templates.filter);
               e("input", this.$filter).attr("placeholder", this.options.filterPlaceholder);
               if (this.options.includeFilterClearBtn) {
                  var n = e(this.options.templates.filterClearBtn);
                  n.on("click", e.proxy(function(t) {
                     clearTimeout(this.searchTimeout);
                     this.$filter.find(".multiselect-search").val("");
                     e("li", this.$ul).show().removeClass("filter-hidden");
                     this.updateSelectAll()
                  }, this));
                  this.$filter.find(".input-group").append(n)
               }
               this.$ul.prepend(this.$filter);
               this.$filter.val(this.query).on("click", function(e) {
                  e.stopPropagation()
               }).on("input keydown", e.proxy(function(t) {
                  if (t.which === 13) {
                     t.preventDefault()
                  }
                  clearTimeout(this.searchTimeout);
                  this.searchTimeout = this.asyncFunction(e.proxy(function() {
                     if (this.query !== t.target.value) {
                        this.query = t.target.value;
                        var n, r;
                        e.each(e("li", this.$ul), e.proxy(function(t, i) {
                           var s = e("input", i).val();
                           var o = e("label", i).text();
                           var u = "";
                           if (this.options.filterBehavior === "text") {
                              u = o
                           } else if (this.options.filterBehavior === "value") {
                              u = s
                           } else if (this.options.filterBehavior === "both") {
                              u = o + "\n" + s
                           }
                           if (s !== this.options.selectAllValue && o) {
                              var a = false;
                              if (this.options.enableCaseInsensitiveFiltering && u.toLowerCase().indexOf(this.query.toLowerCase()) > -1) {
                                 a = true
                              } else if (u.indexOf(this.query) > -1) {
                                 a = true
                              }
                              e(i).toggle(a).toggleClass("filter-hidden", !a);
                              if (e(i).hasClass("multiselect-group")) {
                                 n = i;
                                 r = a
                              } else {
                                 if (a) {
                                    e(n).show().removeClass("filter-hidden")
                                 }
                                 if (!a && r) {
                                    e(i).show().removeClass("filter-hidden")
                                 }
                              }
                           }
                        }, this))
                     }
                     this.updateSelectAll()
                  }, this), 300, this)
               }, this))
            }
         }
      },
      destroy: function() {
         this.$container.remove();
         this.$select.show();
         this.$select.data("multiselect", null)
      },
      refresh: function() {
         e("option", this.$select).each(e.proxy(function(t, n) {
            var r = e("li input", this.$ul).filter(function() {
               return e(this).val() === e(n).val()
            });
            if (e(n).is(":selected")) {
               r.prop("checked", true);
               if (this.options.selectedClass) {
                  r.closest("li").addClass(this.options.selectedClass)
               }
            } else {
               r.prop("checked", false);
               if (this.options.selectedClass) {
                  r.closest("li").removeClass(this.options.selectedClass)
               }
            }
            if (e(n).is(":disabled")) {
               r.attr("disabled", "disabled").prop("disabled", true).closest("li").addClass("disabled")
            } else {
               r.prop("disabled", false).closest("li").removeClass("disabled")
            }
         }, this));
         this.updateButtonText();
         this.updateSelectAll()
      },
      select: function(t, n) {
         if (!e.isArray(t)) {
            t = [t]
         }
         for (var r = 0; r < t.length; r++) {
            var i = t[r];
            if (i === null || i === undefined) {
               continue
            }
            var s = this.getOptionByValue(i);
            var o = this.getInputByValue(i);
            if (s === undefined || o === undefined) {
               continue
            }
            if (!this.options.multiple) {
               this.deselectAll(false)
            }
            if (this.options.selectedClass) {
               o.closest("li").addClass(this.options.selectedClass)
            }
            o.prop("checked", true);
            s.prop("selected", true)
         }
         this.updateButtonText();
         this.updateSelectAll();
         if (n && t.length === 1) {
            this.options.onChange(s, true)
         }
      },
      clearSelection: function() {
         this.deselectAll(false);
         this.updateButtonText();
         this.updateSelectAll()
      },
      deselect: function(t, n) {
         if (!e.isArray(t)) {
            t = [t]
         }
         for (var r = 0; r < t.length; r++) {
            var i = t[r];
            if (i === null || i === undefined) {
               continue
            }
            var s = this.getOptionByValue(i);
            var o = this.getInputByValue(i);
            if (s === undefined || o === undefined) {
               continue
            }
            if (this.options.selectedClass) {
               o.closest("li").removeClass(this.options.selectedClass)
            }
            o.prop("checked", false);
            s.prop("selected", false)
         }
         this.updateButtonText();
         this.updateSelectAll();
         if (n && t.length === 1) {
            this.options.onChange(s, false)
         }
      },
      selectAll: function(t) {
         var t = typeof t === "undefined" ? true : t;
         var n = e("li input[type='checkbox']:enabled", this.$ul);
         var r = n.filter(":visible");
         var i = n.length;
         var s = r.length;
         if (t) {
            r.prop("checked", true);
            e("li:not(.divider):not(.disabled)", this.$ul).filter(":visible").addClass(this.options.selectedClass)
         } else {
            n.prop("checked", true);
            e("li:not(.divider):not(.disabled)", this.$ul).addClass(this.options.selectedClass)
         }
         if (i === s || t === false) {
            e("option:enabled", this.$select).prop("selected", true)
         } else {
            var o = r.map(function() {
               return e(this).val()
            }).get();
            e("option:enabled", this.$select).filter(function(t) {
               return e.inArray(e(this).val(), o) !== -1
            }).prop("selected", true)
         }
      },
      deselectAll: function(t) {
         var t = typeof t === "undefined" ? true : t;
         if (t) {
            var n = e("li input[type='checkbox']:enabled", this.$ul).filter(":visible");
            n.prop("checked", false);
            var r = n.map(function() {
               return e(this).val()
            }).get();
            e("option:enabled", this.$select).filter(function(t) {
               return e.inArray(e(this).val(), r) !== -1
            }).prop("selected", false);
            if (this.options.selectedClass) {
               e("li:not(.divider):not(.disabled)", this.$ul).filter(":visible").removeClass(this.options.selectedClass)
            }
         } else {
            e("li input[type='checkbox']:enabled", this.$ul).prop("checked", false);
            e("option:enabled", this.$select).prop("selected", false);
            if (this.options.selectedClass) {
               e("li:not(.divider):not(.disabled)", this.$ul).removeClass(this.options.selectedClass)
            }
         }
      },
      rebuild: function() {
         this.$ul.html("");
         this.options.multiple = this.$select.attr("multiple") === "multiple";
         this.buildSelectAll();
         this.buildDropdownOptions();
         this.buildFilter();
         this.updateButtonText();
         this.updateSelectAll();
         if (this.options.disableIfEmpty && e("option", this.$select).length <= 0) {
            this.disable()
         }
         if (this.options.dropRight) {
            this.$ul.addClass("pull-right")
         }
      },
      dataprovider: function(t) {
         var r = "";
         var i = 0;
         var s = e("");
         e.each(t, function(t, o) {
            var u;
            if (e.isArray(o.children)) {
               i++;
               u = e("<optgroup/>").attr({
                  label: o.label || "Group " + i
               });
               n(o.children, function(t) {
                  u.append(e("<option/>").attr({
                     value: t.value,
                     label: t.label || t.value,
                     title: t.title,
                     selected: !!t.selected
                  }))
               });
               r += "</optgroup>"
            } else {
               u = e("<option/>").attr({
                  value: o.value,
                  label: o.label || o.value,
                  title: o.title,
                  selected: !!o.selected
               })
            }
            s = s.add(u)
         });
         this.$select.empty().append(s);
         this.rebuild()
      },
      enable: function() {
         this.$select.prop("disabled", false);
         this.$button.prop("disabled", false).removeClass("disabled")
      },
      disable: function() {
         this.$select.prop("disabled", true);
         this.$button.prop("disabled", true).addClass("disabled")
      },
      setOptions: function(e) {
         this.options = this.mergeOptions(e)
      },
      mergeOptions: function(t) {
         return e.extend(true, {}, this.defaults, t)
      },
      hasSelectAll: function() {
         return e("li." + this.options.selectAllValue, this.$ul).length > 0
      },
      updateSelectAll: function() {
         if (this.hasSelectAll()) {
            var t = e("li:not(.multiselect-item):not(.filter-hidden) input:enabled", this.$ul);
            var n = t.length;
            var r = t.filter(":checked").length;
            var i = e("li." + this.options.selectAllValue, this.$ul);
            var s = i.find("input");
            if (r > 0 && r === n) {
               s.prop("checked", true);
               i.addClass(this.options.selectedClass)
            } else {
               s.prop("checked", false);
               i.removeClass(this.options.selectedClass)
            }
         }
      },
      updateButtonText: function() {
         var t = this.getSelected();
         e(".multiselect", this.$container).html(this.options.buttonText(t, this.$select));
         e(".multiselect", this.$container).attr("title", this.options.buttonTitle(t, this.$select))
      },
      getSelected: function() {
         return e("option", this.$select).filter(":selected")
      },
      getOptionByValue: function(t) {
         var n = e("option", this.$select);
         var r = t.toString();
         for (var i = 0; i < n.length; i = i + 1) {
            var s = n[i];
            if (s.value === r) {
               return e(s)
            }
         }
      },
      getInputByValue: function(t) {
         var n = e("li input", this.$ul);
         var r = t.toString();
         for (var i = 0; i < n.length; i = i + 1) {
            var s = n[i];
            if (s.value === r) {
               return e(s)
            }
         }
      },
      updateOriginalOptions: function() {
         this.originalOptions = this.$select.clone()[0].options
      },
      asyncFunction: function(e, t, n) {
         var r = Array.prototype.slice.call(arguments, 3);
         return setTimeout(function() {
            e.apply(n || window, r)
         }, t)
      }
   };
   e.fn.multiselect = function(t, n, i) {
      return this.each(function() {
         var s = e(this).data("multiselect");
         var o = typeof t === "object" && t;
         if (!s) {
            s = new r(this, o);
            e(this).data("multiselect", s)
         }
         if (typeof t === "string") {
            s[t](n, i);
            if (t === "destroy") {
               e(this).data("multiselect", false)
            }
         }
      })
   };
   e.fn.multiselect.Constructor = r;
   e(function() {
      e("select[data-role=multiselect]").multiselect()
   })
}(window.jQuery)

/*
 * jVectorMap version 1.2.2
 * Copyright 2011-2013, Kirill Lebedev
 * Licensed under the MIT license.
 */
;
(function(e) {
   var t = {
      set: {
         colors: 1,
         values: 1,
         backgroundColor: 1,
         scaleColors: 1,
         normalizeFunction: 1,
         focus: 1
      },
      get: {
         selectedRegions: 1,
         selectedMarkers: 1,
         mapObject: 1,
         regionName: 1
      }
   };
   e.fn.vectorMap = function(e) {
      var n, r, i, n = this.children(".jvectormap-container").data("mapObject");
      if (e === "addMap") jvm.WorldMap.maps[arguments[1]] = arguments[2];
      else {
         if (!(e !== "set" && e !== "get" || !t[e][arguments[1]])) return r = arguments[1].charAt(0).toUpperCase() + arguments[1].substr(1), n[e + r].apply(n, Array.prototype.slice.call(arguments, 2));
         e = e || {}, e.container = this, n = new jvm.WorldMap(e)
      }
      return this
   }
})(jQuery),
function(e) {
   function r(t) {
      var n = t || window.event,
         r = [].slice.call(arguments, 1),
         i = 0,
         s = !0,
         o = 0,
         u = 0;
      return t = e.event.fix(n), t.type = "mousewheel", n.wheelDelta && (i = n.wheelDelta / 120), n.detail && (i = -n.detail / 3), u = i, n.axis !== undefined && n.axis === n.HORIZONTAL_AXIS && (u = 0, o = -1 * i), n.wheelDeltaY !== undefined && (u = n.wheelDeltaY / 120), n.wheelDeltaX !== undefined && (o = -1 * n.wheelDeltaX / 120), r.unshift(t, i, o, u), (e.event.dispatch || e.event.handle).apply(this, r)
   }
   var t = ["DOMMouseScroll", "mousewheel"];
   if (e.event.fixHooks)
      for (var n = t.length; n;) e.event.fixHooks[t[--n]] = e.event.mouseHooks;
   e.event.special.mousewheel = {
      setup: function() {
         if (this.addEventListener)
            for (var e = t.length; e;) this.addEventListener(t[--e], r, !1);
         else this.onmousewheel = r
      },
      teardown: function() {
         if (this.removeEventListener)
            for (var e = t.length; e;) this.removeEventListener(t[--e], r, !1);
         else this.onmousewheel = null
      }
   }, e.fn.extend({
      mousewheel: function(e) {
         return e ? this.bind("mousewheel", e) : this.trigger("mousewheel")
      },
      unmousewheel: function(e) {
         return this.unbind("mousewheel", e)
      }
   })
}(jQuery);
var jvm = {
   inherits: function(e, t) {
      function n() {}
      n.prototype = t.prototype, e.prototype = new n, e.prototype.constructor = e, e.parentClass = t
   },
   mixin: function(e, t) {
      var n;
      for (n in t.prototype) t.prototype.hasOwnProperty(n) && (e.prototype[n] = t.prototype[n])
   },
   min: function(e) {
      var t = Number.MAX_VALUE,
         n;
      if (e instanceof Array)
         for (n = 0; n < e.length; n++) e[n] < t && (t = e[n]);
      else
         for (n in e) e[n] < t && (t = e[n]);
      return t
   },
   max: function(e) {
      var t = Number.MIN_VALUE,
         n;
      if (e instanceof Array)
         for (n = 0; n < e.length; n++) e[n] > t && (t = e[n]);
      else
         for (n in e) e[n] > t && (t = e[n]);
      return t
   },
   keys: function(e) {
      var t = [],
         n;
      for (n in e) t.push(n);
      return t
   },
   values: function(e) {
      var t = [],
         n, r;
      for (r = 0; r < arguments.length; r++) {
         e = arguments[r];
         for (n in e) t.push(e[n])
      }
      return t
   }
};
jvm.$ = jQuery, jvm.AbstractElement = function(e, t) {
   this.node = this.createElement(e), this.name = e, this.properties = {}, t && this.set(t)
}, jvm.AbstractElement.prototype.set = function(e, t) {
   var n;
   if (typeof e == "object")
      for (n in e) this.properties[n] = e[n], this.applyAttr(n, e[n]);
   else this.properties[e] = t, this.applyAttr(e, t)
}, jvm.AbstractElement.prototype.get = function(e) {
   return this.properties[e]
}, jvm.AbstractElement.prototype.applyAttr = function(e, t) {
   this.node.setAttribute(e, t)
}, jvm.AbstractElement.prototype.remove = function() {
   jvm.$(this.node).remove()
}, jvm.AbstractCanvasElement = function(e, t, n) {
   this.container = e, this.setSize(t, n), this.rootElement = new jvm[this.classPrefix + "GroupElement"], this.node.appendChild(this.rootElement.node), this.container.appendChild(this.node)
}, jvm.AbstractCanvasElement.prototype.add = function(e, t) {
   t = t || this.rootElement, t.add(e), e.canvas = this
}, jvm.AbstractCanvasElement.prototype.addPath = function(e, t, n) {
   var r = new jvm[this.classPrefix + "PathElement"](e, t);
   return this.add(r, n), r
}, jvm.AbstractCanvasElement.prototype.addCircle = function(e, t, n) {
   var r = new jvm[this.classPrefix + "CircleElement"](e, t);
   return this.add(r, n), r
}, jvm.AbstractCanvasElement.prototype.addGroup = function(e) {
   var t = new jvm[this.classPrefix + "GroupElement"];
   return e ? e.node.appendChild(t.node) : this.node.appendChild(t.node), t.canvas = this, t
}, jvm.AbstractShapeElement = function(e, t, n) {
   this.style = n || {}, this.style.current = {}, this.isHovered = !1, this.isSelected = !1, this.updateStyle()
}, jvm.AbstractShapeElement.prototype.setHovered = function(e) {
   this.isHovered !== e && (this.isHovered = e, this.updateStyle())
}, jvm.AbstractShapeElement.prototype.setSelected = function(e) {
   this.isSelected !== e && (this.isSelected = e, this.updateStyle(), jvm.$(this.node).trigger("selected", [e]))
}, jvm.AbstractShapeElement.prototype.setStyle = function(e, t) {
   var n = {};
   typeof e == "object" ? n = e : n[e] = t, jvm.$.extend(this.style.current, n), this.updateStyle()
}, jvm.AbstractShapeElement.prototype.updateStyle = function() {
   var e = {};
   jvm.AbstractShapeElement.mergeStyles(e, this.style.initial), jvm.AbstractShapeElement.mergeStyles(e, this.style.current), this.isHovered && jvm.AbstractShapeElement.mergeStyles(e, this.style.hover), this.isSelected && (jvm.AbstractShapeElement.mergeStyles(e, this.style.selected), this.isHovered && jvm.AbstractShapeElement.mergeStyles(e, this.style.selectedHover)), this.set(e)
}, jvm.AbstractShapeElement.mergeStyles = function(e, t) {
   var n;
   t = t || {};
   for (n in t) t[n] === null ? delete e[n] : e[n] = t[n]
}, jvm.SVGElement = function(e, t) {
   jvm.SVGElement.parentClass.apply(this, arguments)
}, jvm.inherits(jvm.SVGElement, jvm.AbstractElement), jvm.SVGElement.svgns = "http://www.w3.org/2000/svg", jvm.SVGElement.prototype.createElement = function(e) {
   return document.createElementNS(jvm.SVGElement.svgns, e)
}, jvm.SVGElement.prototype.addClass = function(e) {
   this.node.setAttribute("class", e)
}, jvm.SVGElement.prototype.getElementCtr = function(e) {
   return jvm["SVG" + e]
}, jvm.SVGElement.prototype.getBBox = function() {
   return this.node.getBBox()
}, jvm.SVGGroupElement = function() {
   jvm.SVGGroupElement.parentClass.call(this, "g")
}, jvm.inherits(jvm.SVGGroupElement, jvm.SVGElement), jvm.SVGGroupElement.prototype.add = function(e) {
   this.node.appendChild(e.node)
}, jvm.SVGCanvasElement = function(e, t, n) {
   this.classPrefix = "SVG", jvm.SVGCanvasElement.parentClass.call(this, "svg"), jvm.AbstractCanvasElement.apply(this, arguments)
}, jvm.inherits(jvm.SVGCanvasElement, jvm.SVGElement), jvm.mixin(jvm.SVGCanvasElement, jvm.AbstractCanvasElement), jvm.SVGCanvasElement.prototype.setSize = function(e, t) {
   this.width = e, this.height = t, this.node.setAttribute("width", e), this.node.setAttribute("height", t)
}, jvm.SVGCanvasElement.prototype.applyTransformParams = function(e, t, n) {
   this.scale = e, this.transX = t, this.transY = n, this.rootElement.node.setAttribute("transform", "scale(" + e + ") translate(" + t + ", " + n + ")")
}, jvm.SVGShapeElement = function(e, t, n) {
   jvm.SVGShapeElement.parentClass.call(this, e, t), jvm.AbstractShapeElement.apply(this, arguments)
}, jvm.inherits(jvm.SVGShapeElement, jvm.SVGElement), jvm.mixin(jvm.SVGShapeElement, jvm.AbstractShapeElement), jvm.SVGPathElement = function(e, t) {
   jvm.SVGPathElement.parentClass.call(this, "path", e, t), this.node.setAttribute("fill-rule", "evenodd")
}, jvm.inherits(jvm.SVGPathElement, jvm.SVGShapeElement), jvm.SVGCircleElement = function(e, t) {
   jvm.SVGCircleElement.parentClass.call(this, "circle", e, t)
}, jvm.inherits(jvm.SVGCircleElement, jvm.SVGShapeElement), jvm.VMLElement = function(e, t) {
   jvm.VMLElement.VMLInitialized || jvm.VMLElement.initializeVML(), jvm.VMLElement.parentClass.apply(this, arguments)
}, jvm.inherits(jvm.VMLElement, jvm.AbstractElement), jvm.VMLElement.VMLInitialized = !1, jvm.VMLElement.initializeVML = function() {
   try {
      document.namespaces.rvml || document.namespaces.add("rvml", "urn:schemas-microsoft-com:vml"), jvm.VMLElement.prototype.createElement = function(e) {
         return document.createElement("<rvml:" + e + ' class="rvml">')
      }
   } catch (e) {
      jvm.VMLElement.prototype.createElement = function(e) {
         return document.createElement("<" + e + ' xmlns="urn:schemas-microsoft.com:vml" class="rvml">')
      }
   }
   document.createStyleSheet().addRule(".rvml", "behavior:url(#default#VML)"), jvm.VMLElement.VMLInitialized = !0
}, jvm.VMLElement.prototype.getElementCtr = function(e) {
   return jvm["VML" + e]
}, jvm.VMLElement.prototype.addClass = function(e) {
   jvm.$(this.node).addClass(e)
}, jvm.VMLElement.prototype.applyAttr = function(e, t) {
   this.node[e] = t
}, jvm.VMLElement.prototype.getBBox = function() {
   var e = jvm.$(this.node);
   return {
      x: e.position().left / this.canvas.scale,
      y: e.position().top / this.canvas.scale,
      width: e.width() / this.canvas.scale,
      height: e.height() / this.canvas.scale
   }
}, jvm.VMLGroupElement = function() {
   jvm.VMLGroupElement.parentClass.call(this, "group"), this.node.style.left = "0px", this.node.style.top = "0px", this.node.coordorigin = "0 0"
}, jvm.inherits(jvm.VMLGroupElement, jvm.VMLElement), jvm.VMLGroupElement.prototype.add = function(e) {
   this.node.appendChild(e.node)
}, jvm.VMLCanvasElement = function(e, t, n) {
   this.classPrefix = "VML", jvm.VMLCanvasElement.parentClass.call(this, "group"), jvm.AbstractCanvasElement.apply(this, arguments), this.node.style.position = "absolute"
}, jvm.inherits(jvm.VMLCanvasElement, jvm.VMLElement), jvm.mixin(jvm.VMLCanvasElement, jvm.AbstractCanvasElement), jvm.VMLCanvasElement.prototype.setSize = function(e, t) {
   var n, r, i, s;
   this.width = e, this.height = t, this.node.style.width = e + "px", this.node.style.height = t + "px", this.node.coordsize = e + " " + t, this.node.coordorigin = "0 0";
   if (this.rootElement) {
      n = this.rootElement.node.getElementsByTagName("shape");
      for (i = 0, s = n.length; i < s; i++) n[i].coordsize = e + " " + t, n[i].style.width = e + "px", n[i].style.height = t + "px";
      r = this.node.getElementsByTagName("group");
      for (i = 0, s = r.length; i < s; i++) r[i].coordsize = e + " " + t, r[i].style.width = e + "px", r[i].style.height = t + "px"
   }
}, jvm.VMLCanvasElement.prototype.applyTransformParams = function(e, t, n) {
   this.scale = e, this.transX = t, this.transY = n, this.rootElement.node.coordorigin = this.width - t - this.width / 100 + "," + (this.height - n - this.height / 100), this.rootElement.node.coordsize = this.width / e + "," + this.height / e
}, jvm.VMLShapeElement = function(e, t) {
   jvm.VMLShapeElement.parentClass.call(this, e, t), this.fillElement = new jvm.VMLElement("fill"), this.strokeElement = new jvm.VMLElement("stroke"), this.node.appendChild(this.fillElement.node), this.node.appendChild(this.strokeElement.node), this.node.stroked = !1, jvm.AbstractShapeElement.apply(this, arguments)
}, jvm.inherits(jvm.VMLShapeElement, jvm.VMLElement), jvm.mixin(jvm.VMLShapeElement, jvm.AbstractShapeElement), jvm.VMLShapeElement.prototype.applyAttr = function(e, t) {
   switch (e) {
      case "fill":
         this.node.fillcolor = t;
         break;
      case "fill-opacity":
         this.fillElement.node.opacity = Math.round(t * 100) + "%";
         break;
      case "stroke":
         t === "none" ? this.node.stroked = !1 : this.node.stroked = !0, this.node.strokecolor = t;
         break;
      case "stroke-opacity":
         this.strokeElement.node.opacity = Math.round(t * 100) + "%";
         break;
      case "stroke-width":
         parseInt(t, 10) === 0 ? this.node.stroked = !1 : this.node.stroked = !0, this.node.strokeweight = t;
         break;
      case "d":
         this.node.path = jvm.VMLPathElement.pathSvgToVml(t);
         break;
      default:
         jvm.VMLShapeElement.parentClass.prototype.applyAttr.apply(this, arguments)
   }
}, jvm.VMLPathElement = function(e, t) {
   var n = new jvm.VMLElement("skew");
   jvm.VMLPathElement.parentClass.call(this, "shape", e, t), this.node.coordorigin = "0 0", n.node.on = !0, n.node.matrix = "0.01,0,0,0.01,0,0", n.node.offset = "0,0", this.node.appendChild(n.node)
}, jvm.inherits(jvm.VMLPathElement, jvm.VMLShapeElement), jvm.VMLPathElement.prototype.applyAttr = function(e, t) {
   e === "d" ? this.node.path = jvm.VMLPathElement.pathSvgToVml(t) : jvm.VMLShapeElement.prototype.applyAttr.call(this, e, t)
}, jvm.VMLPathElement.pathSvgToVml = function(e) {
   var t = "",
      n = 0,
      r = 0,
      i, s;
   return e = e.replace(/(-?\d+)e(-?\d+)/g, "0"), e.replace(/([MmLlHhVvCcSs])\s*((?:-?\d*(?:\.\d+)?\s*,?\s*)+)/g, function(e, t, o, u) {
      o = o.replace(/(\d)-/g, "$1,-").replace(/^\s+/g, "").replace(/\s+$/g, "").replace(/\s+/g, ",").split(","), o[0] || o.shift();
      for (var a = 0, f = o.length; a < f; a++) o[a] = Math.round(100 * o[a]);
      switch (t) {
         case "m":
            return n += o[0], r += o[1], "t" + o.join(",");
         case "M":
            return n = o[0], r = o[1], "m" + o.join(",");
         case "l":
            return n += o[0], r += o[1], "r" + o.join(",");
         case "L":
            return n = o[0], r = o[1], "l" + o.join(",");
         case "h":
            return n += o[0], "r" + o[0] + ",0";
         case "H":
            return n = o[0], "l" + n + "," + r;
         case "v":
            return r += o[0], "r0," + o[0];
         case "V":
            return r = o[0], "l" + n + "," + r;
         case "c":
            return i = n + o[o.length - 4], s = r + o[o.length - 3], n += o[o.length - 2], r += o[o.length - 1], "v" + o.join(",");
         case "C":
            return i = o[o.length - 4], s = o[o.length - 3], n = o[o.length - 2], r = o[o.length - 1], "c" + o.join(",");
         case "s":
            return o.unshift(r - s), o.unshift(n - i), i = n + o[o.length - 4], s = r + o[o.length - 3], n += o[o.length - 2], r += o[o.length - 1], "v" + o.join(",");
         case "S":
            return o.unshift(r + r - s), o.unshift(n + n - i), i = o[o.length - 4], s = o[o.length - 3], n = o[o.length - 2], r = o[o.length - 1], "c" + o.join(",")
      }
      return ""
   }).replace(/z/g, "e")
}, jvm.VMLCircleElement = function(e, t) {
   jvm.VMLCircleElement.parentClass.call(this, "oval", e, t)
}, jvm.inherits(jvm.VMLCircleElement, jvm.VMLShapeElement), jvm.VMLCircleElement.prototype.applyAttr = function(e, t) {
   switch (e) {
      case "r":
         this.node.style.width = t * 2 + "px", this.node.style.height = t * 2 + "px", this.applyAttr("cx", this.get("cx") || 0), this.applyAttr("cy", this.get("cy") || 0);
         break;
      case "cx":
         if (!t) return;
         this.node.style.left = t - (this.get("r") || 0) + "px";
         break;
      case "cy":
         if (!t) return;
         this.node.style.top = t - (this.get("r") || 0) + "px";
         break;
      default:
         jvm.VMLCircleElement.parentClass.prototype.applyAttr.call(this, e, t)
   }
}, jvm.VectorCanvas = function(e, t, n) {
   return this.mode = window.SVGAngle ? "svg" : "vml", this.mode == "svg" ? this.impl = new jvm.SVGCanvasElement(e, t, n) : this.impl = new jvm.VMLCanvasElement(e, t, n), this.impl
}, jvm.SimpleScale = function(e) {
   this.scale = e
}, jvm.SimpleScale.prototype.getValue = function(e) {
   return e
}, jvm.OrdinalScale = function(e) {
   this.scale = e
}, jvm.OrdinalScale.prototype.getValue = function(e) {
   return this.scale[e]
}, jvm.NumericScale = function(e, t, n, r) {
   this.scale = [], t = t || "linear", e && this.setScale(e), t && this.setNormalizeFunction(t), n && this.setMin(n), r && this.setMax(r)
}, jvm.NumericScale.prototype = {
   setMin: function(e) {
      this.clearMinValue = e, typeof this.normalize == "function" ? this.minValue = this.normalize(e) : this.minValue = e
   },
   setMax: function(e) {
      this.clearMaxValue = e, typeof this.normalize == "function" ? this.maxValue = this.normalize(e) : this.maxValue = e
   },
   setScale: function(e) {
      var t;
      for (t = 0; t < e.length; t++) this.scale[t] = [e[t]]
   },
   setNormalizeFunction: function(e) {
      e === "polynomial" ? this.normalize = function(e) {
         return Math.pow(e, .2)
      } : e === "linear" ? delete this.normalize : this.normalize = e, this.setMin(this.clearMinValue), this.setMax(this.clearMaxValue)
   },
   getValue: function(e) {
      var t = [],
         n = 0,
         r, i = 0,
         s;
      typeof this.normalize == "function" && (e = this.normalize(e));
      for (i = 0; i < this.scale.length - 1; i++) r = this.vectorLength(this.vectorSubtract(this.scale[i + 1], this.scale[i])), t.push(r), n += r;
      s = (this.maxValue - this.minValue) / n;
      for (i = 0; i < t.length; i++) t[i] *= s;
      i = 0, e -= this.minValue;
      while (e - t[i] >= 0) e -= t[i], i++;
      return i == this.scale.length - 1 ? e = this.vectorToNum(this.scale[i]) : e = this.vectorToNum(this.vectorAdd(this.scale[i], this.vectorMult(this.vectorSubtract(this.scale[i + 1], this.scale[i]), e / t[i]))), e
   },
   vectorToNum: function(e) {
      var t = 0,
         n;
      for (n = 0; n < e.length; n++) t += Math.round(e[n]) * Math.pow(256, e.length - n - 1);
      return t
   },
   vectorSubtract: function(e, t) {
      var n = [],
         r;
      for (r = 0; r < e.length; r++) n[r] = e[r] - t[r];
      return n
   },
   vectorAdd: function(e, t) {
      var n = [],
         r;
      for (r = 0; r < e.length; r++) n[r] = e[r] + t[r];
      return n
   },
   vectorMult: function(e, t) {
      var n = [],
         r;
      for (r = 0; r < e.length; r++) n[r] = e[r] * t;
      return n
   },
   vectorLength: function(e) {
      var t = 0,
         n;
      for (n = 0; n < e.length; n++) t += e[n] * e[n];
      return Math.sqrt(t)
   }
}, jvm.ColorScale = function(e, t, n, r) {
   jvm.ColorScale.parentClass.apply(this, arguments)
}, jvm.inherits(jvm.ColorScale, jvm.NumericScale), jvm.ColorScale.prototype.setScale = function(e) {
   var t;
   for (t = 0; t < e.length; t++) this.scale[t] = jvm.ColorScale.rgbToArray(e[t])
}, jvm.ColorScale.prototype.getValue = function(e) {
   return jvm.ColorScale.numToRgb(jvm.ColorScale.parentClass.prototype.getValue.call(this, e))
}, jvm.ColorScale.arrayToRgb = function(e) {
   var t = "#",
      n, r;
   for (r = 0; r < e.length; r++) n = e[r].toString(16), t += n.length == 1 ? "0" + n : n;
   return t
}, jvm.ColorScale.numToRgb = function(e) {
   e = e.toString(16);
   while (e.length < 6) e = "0" + e;
   return "#" + e
}, jvm.ColorScale.rgbToArray = function(e) {
   return e = e.substr(1), [parseInt(e.substr(0, 2), 16), parseInt(e.substr(2, 2), 16), parseInt(e.substr(4, 2), 16)]
}, jvm.DataSeries = function(e, t) {
   var n;
   e = e || {}, e.attribute = e.attribute || "fill", this.elements = t, this.params = e, e.attributes && this.setAttributes(e.attributes), jvm.$.isArray(e.scale) ? (n = e.attribute === "fill" || e.attribute === "stroke" ? jvm.ColorScale : jvm.NumericScale, this.scale = new n(e.scale, e.normalizeFunction, e.min, e.max)) : e.scale ? this.scale = new jvm.OrdinalScale(e.scale) : this.scale = new jvm.SimpleScale(e.scale), this.values = e.values || {}, this.setValues(this.values)
}, jvm.DataSeries.prototype = {
   setAttributes: function(e, t) {
      var n = e,
         r;
      if (typeof e == "string") this.elements[e] && this.elements[e].setStyle(this.params.attribute, t);
      else
         for (r in n) this.elements[r] && this.elements[r].element.setStyle(this.params.attribute, n[r])
   },
   setValues: function(e) {
      var t = Number.MIN_VALUE,
         n = Number.MAX_VALUE,
         r, i, s = {};
      if (this.scale instanceof jvm.OrdinalScale || this.scale instanceof jvm.SimpleScale)
         for (i in e) e[i] ? s[i] = this.scale.getValue(e[i]) : s[i] = this.elements[i].element.style.initial[this.params.attribute];
      else {
         if (!this.params.min || !this.params.max) {
            for (i in e) r = parseFloat(e[i]), r > t && (t = e[i]), r < n && (n = r);
            this.params.min || this.scale.setMin(n), this.params.max || this.scale.setMax(t), this.params.min = n, this.params.max = t
         }
         for (i in e) r = parseFloat(e[i]), isNaN(r) ? s[i] = this.elements[i].element.style.initial[this.params.attribute] : s[i] = this.scale.getValue(r)
      }
      this.setAttributes(s), jvm.$.extend(this.values, e)
   },
   clear: function() {
      var e, t = {};
      for (e in this.values) this.elements[e] && (t[e] = this.elements[e].element.style.initial[this.params.attribute]);
      this.setAttributes(t), this.values = {}
   },
   setScale: function(e) {
      this.scale.setScale(e), this.values && this.setValues(this.values)
   },
   setNormalizeFunction: function(e) {
      this.scale.setNormalizeFunction(e), this.values && this.setValues(this.values)
   }
}, jvm.Proj = {
   degRad: 180 / Math.PI,
   radDeg: Math.PI / 180,
   radius: 6381372,
   sgn: function(e) {
      return e > 0 ? 1 : e < 0 ? -1 : e
   },
   mill: function(e, t, n) {
      return {
         x: this.radius * (t - n) * this.radDeg,
         y: -this.radius * Math.log(Math.tan((45 + .4 * e) * this.radDeg)) / .8
      }
   },
   mill_inv: function(e, t, n) {
      return {
         lat: (2.5 * Math.atan(Math.exp(.8 * t / this.radius)) - 5 * Math.PI / 8) * this.degRad,
         lng: (n * this.radDeg + e / this.radius) * this.degRad
      }
   },
   merc: function(e, t, n) {
      return {
         x: this.radius * (t - n) * this.radDeg,
         y: -this.radius * Math.log(Math.tan(Math.PI / 4 + e * Math.PI / 360))
      }
   },
   merc_inv: function(e, t, n) {
      return {
         lat: (2 * Math.atan(Math.exp(t / this.radius)) - Math.PI / 2) * this.degRad,
         lng: (n * this.radDeg + e / this.radius) * this.degRad
      }
   },
   aea: function(e, t, n) {
      var r = 0,
         i = n * this.radDeg,
         s = 29.5 * this.radDeg,
         o = 45.5 * this.radDeg,
         u = e * this.radDeg,
         a = t * this.radDeg,
         f = (Math.sin(s) + Math.sin(o)) / 2,
         l = Math.cos(s) * Math.cos(s) + 2 * f * Math.sin(s),
         c = f * (a - i),
         h = Math.sqrt(l - 2 * f * Math.sin(u)) / f,
         p = Math.sqrt(l - 2 * f * Math.sin(r)) / f;
      return {
         x: h * Math.sin(c) * this.radius,
         y: -(p - h * Math.cos(c)) * this.radius
      }
   },
   aea_inv: function(e, t, n) {
      var r = e / this.radius,
         i = t / this.radius,
         s = 0,
         o = n * this.radDeg,
         u = 29.5 * this.radDeg,
         a = 45.5 * this.radDeg,
         f = (Math.sin(u) + Math.sin(a)) / 2,
         l = Math.cos(u) * Math.cos(u) + 2 * f * Math.sin(u),
         c = Math.sqrt(l - 2 * f * Math.sin(s)) / f,
         h = Math.sqrt(r * r + (c - i) * (c - i)),
         p = Math.atan(r / (c - i));
      return {
         lat: Math.asin((l - h * h * f * f) / (2 * f)) * this.degRad,
         lng: (o + p / f) * this.degRad
      }
   },
   lcc: function(e, t, n) {
      var r = 0,
         i = n * this.radDeg,
         s = t * this.radDeg,
         o = 33 * this.radDeg,
         u = 45 * this.radDeg,
         a = e * this.radDeg,
         f = Math.log(Math.cos(o) * (1 / Math.cos(u))) / Math.log(Math.tan(Math.PI / 4 + u / 2) * (1 / Math.tan(Math.PI / 4 + o / 2))),
         l = Math.cos(o) * Math.pow(Math.tan(Math.PI / 4 + o / 2), f) / f,
         c = l * Math.pow(1 / Math.tan(Math.PI / 4 + a / 2), f),
         h = l * Math.pow(1 / Math.tan(Math.PI / 4 + r / 2), f);
      return {
         x: c * Math.sin(f * (s - i)) * this.radius,
         y: -(h - c * Math.cos(f * (s - i))) * this.radius
      }
   },
   lcc_inv: function(e, t, n) {
      var r = e / this.radius,
         i = t / this.radius,
         s = 0,
         o = n * this.radDeg,
         u = 33 * this.radDeg,
         a = 45 * this.radDeg,
         f = Math.log(Math.cos(u) * (1 / Math.cos(a))) / Math.log(Math.tan(Math.PI / 4 + a / 2) * (1 / Math.tan(Math.PI / 4 + u / 2))),
         l = Math.cos(u) * Math.pow(Math.tan(Math.PI / 4 + u / 2), f) / f,
         c = l * Math.pow(1 / Math.tan(Math.PI / 4 + s / 2), f),
         h = this.sgn(f) * Math.sqrt(r * r + (c - i) * (c - i)),
         p = Math.atan(r / (c - i));
      return {
         lat: (2 * Math.atan(Math.pow(l / h, 1 / f)) - Math.PI / 2) * this.degRad,
         lng: (o + p / f) * this.degRad
      }
   }
}, jvm.WorldMap = function(e) {
   var t = this,
      n;
   this.params = jvm.$.extend(!0, {}, jvm.WorldMap.defaultParams, e);
   if (!jvm.WorldMap.maps[this.params.map]) throw new Error("Attempt to use map which was not loaded: " + this.params.map);
   this.mapData = jvm.WorldMap.maps[this.params.map], this.markers = {}, this.regions = {}, this.regionsColors = {}, this.regionsData = {}, this.container = jvm.$("<div>").css({
      width: "100%",
      height: "100%"
   }).addClass("jvectormap-container"), this.params.container.append(this.container), this.container.data("mapObject", this), this.container.css({
      position: "relative",
      overflow: "hidden"
   }), this.defaultWidth = this.mapData.width, this.defaultHeight = this.mapData.height, this.setBackgroundColor(this.params.backgroundColor), this.onResize = function() {
      t.setSize()
   }, jvm.$(window).resize(this.onResize);
   for (n in jvm.WorldMap.apiEvents) this.params[n] && this.container.bind(jvm.WorldMap.apiEvents[n] + ".jvectormap", this.params[n]);
   this.canvas = new jvm.VectorCanvas(this.container[0], this.width, this.height), "ontouchstart" in window || window.DocumentTouch && document instanceof DocumentTouch ? this.params.bindTouchEvents && this.bindContainerTouchEvents() : this.bindContainerEvents(), this.bindElementEvents(), this.createLabel(), this.params.zoomButtons && this.bindZoomButtons(), this.createRegions(), this.createMarkers(this.params.markers || {}), this.setSize(), this.params.focusOn && (typeof this.params.focusOn == "object" ? this.setFocus.call(this, this.params.focusOn.scale, this.params.focusOn.x, this.params.focusOn.y) : this.setFocus.call(this, this.params.focusOn)), this.params.selectedRegions && this.setSelectedRegions(this.params.selectedRegions), this.params.selectedMarkers && this.setSelectedMarkers(this.params.selectedMarkers), this.params.series && this.createSeries()
}, jvm.WorldMap.prototype = {
   transX: 0,
   transY: 0,
   scale: 1,
   baseTransX: 0,
   baseTransY: 0,
   baseScale: 1,
   width: 0,
   height: 0,
   setBackgroundColor: function(e) {
      this.container.css("background-color", e)
   },
   resize: function() {
      var e = this.baseScale;
      this.width / this.height > this.defaultWidth / this.defaultHeight ? (this.baseScale = this.height / this.defaultHeight, this.baseTransX = Math.abs(this.width - this.defaultWidth * this.baseScale) / (2 * this.baseScale)) : (this.baseScale = this.width / this.defaultWidth, this.baseTransY = Math.abs(this.height - this.defaultHeight * this.baseScale) / (2 * this.baseScale)), this.scale *= this.baseScale / e, this.transX *= this.baseScale / e, this.transY *= this.baseScale / e
   },
   setSize: function() {
      this.width = this.container.width(), this.height = this.container.height(), this.resize(), this.canvas.setSize(this.width, this.height), this.applyTransform()
   },
   reset: function() {
      var e, t;
      for (e in this.series)
         for (t = 0; t < this.series[e].length; t++) this.series[e][t].clear();
      this.scale = this.baseScale, this.transX = this.baseTransX, this.transY = this.baseTransY, this.applyTransform()
   },
   applyTransform: function() {
      var e, t, n, r;
      this.defaultWidth * this.scale <= this.width ? (e = (this.width - this.defaultWidth * this.scale) / (2 * this.scale), n = (this.width - this.defaultWidth * this.scale) / (2 * this.scale)) : (e = 0, n = (this.width - this.defaultWidth * this.scale) / this.scale), this.defaultHeight * this.scale <= this.height ? (t = (this.height - this.defaultHeight * this.scale) / (2 * this.scale), r = (this.height - this.defaultHeight * this.scale) / (2 * this.scale)) : (t = 0, r = (this.height - this.defaultHeight * this.scale) / this.scale), this.transY > t ? this.transY = t : this.transY < r && (this.transY = r), this.transX > e ? this.transX = e : this.transX < n && (this.transX = n), this.canvas.applyTransformParams(this.scale, this.transX, this.transY), this.markers && this.repositionMarkers(), this.container.trigger("viewportChange", [this.scale / this.baseScale, this.transX, this.transY])
   },
   bindContainerEvents: function() {
      var e = !1,
         t, n, r = this;
      this.container.mousemove(function(i) {
         return e && (r.transX -= (t - i.pageX) / r.scale, r.transY -= (n - i.pageY) / r.scale, r.applyTransform(), t = i.pageX, n = i.pageY), !1
      }).mousedown(function(r) {
         return e = !0, t = r.pageX, n = r.pageY, !1
      }), jvm.$("body").mouseup(function() {
         e = !1
      }), this.params.zoomOnScroll && this.container.mousewheel(function(e, t, n, i) {
         var s = jvm.$(r.container).offset(),
            o = e.pageX - s.left,
            u = e.pageY - s.top,
            a = Math.pow(1.3, i);
         r.label.hide(), r.setScale(r.scale * a, o, u), e.preventDefault()
      })
   },
   bindContainerTouchEvents: function() {
      var e, t, n = this,
         r, i, s, o, u, a = function(a) {
            var f = a.originalEvent.touches,
               l, c, h, p;
            a.type == "touchstart" && (u = 0), f.length == 1 ? (u == 1 && (h = n.transX, p = n.transY, n.transX -= (r - f[0].pageX) / n.scale, n.transY -= (i - f[0].pageY) / n.scale, n.applyTransform(), n.label.hide(), (h != n.transX || p != n.transY) && a.preventDefault()), r = f[0].pageX, i = f[0].pageY) : f.length == 2 && (u == 2 ? (c = Math.sqrt(Math.pow(f[0].pageX - f[1].pageX, 2) + Math.pow(f[0].pageY - f[1].pageY, 2)) / t, n.setScale(e * c, s, o), n.label.hide(), a.preventDefault()) : (l = jvm.$(n.container).offset(), f[0].pageX > f[1].pageX ? s = f[1].pageX + (f[0].pageX - f[1].pageX) / 2 : s = f[0].pageX + (f[1].pageX - f[0].pageX) / 2, f[0].pageY > f[1].pageY ? o = f[1].pageY + (f[0].pageY - f[1].pageY) / 2 : o = f[0].pageY + (f[1].pageY - f[0].pageY) / 2, s -= l.left, o -= l.top, e = n.scale, t = Math.sqrt(Math.pow(f[0].pageX - f[1].pageX, 2) + Math.pow(f[0].pageY - f[1].pageY, 2)))), u = f.length
         };
      jvm.$(this.container).bind("touchstart", a), jvm.$(this.container).bind("touchmove", a)
   },
   bindElementEvents: function() {
      var e = this,
         t;
      this.container.mousemove(function() {
         t = !0
      }), this.container.delegate("[class~='jvectormap-element']", "mouseover mouseout", function(t) {
         var n = this,
            r = jvm.$(this).attr("class").baseVal ? jvm.$(this).attr("class").baseVal : jvm.$(this).attr("class"),
            i = r.indexOf("jvectormap-region") === -1 ? "marker" : "region",
            s = i == "region" ? jvm.$(this).attr("data-code") : jvm.$(this).attr("data-index"),
            o = i == "region" ? e.regions[s].element : e.markers[s].element,
            u = i == "region" ? e.mapData.paths[s].name : e.markers[s].config.name || "",
            a = jvm.$.Event(i + "LabelShow.jvectormap"),
            f = jvm.$.Event(i + "Over.jvectormap");
         t.type == "mouseover" ? (e.container.trigger(f, [s]), f.isDefaultPrevented() || o.setHovered(!0), e.label.text(u), e.container.trigger(a, [e.label, s]), a.isDefaultPrevented() || (e.label.show(), e.labelWidth = e.label.width(), e.labelHeight = e.label.height())) : (o.setHovered(!1), e.label.hide(), e.container.trigger(i + "Out.jvectormap", [s]))
      }), this.container.delegate("[class~='jvectormap-element']", "mousedown", function(e) {
         t = !1
      }), this.container.delegate("[class~='jvectormap-element']", "mouseup", function(n) {
         var r = this,
            i = jvm.$(this).attr("class").baseVal ? jvm.$(this).attr("class").baseVal : jvm.$(this).attr("class"),
            s = i.indexOf("jvectormap-region") === -1 ? "marker" : "region",
            o = s == "region" ? jvm.$(this).attr("data-code") : jvm.$(this).attr("data-index"),
            u = jvm.$.Event(s + "Click.jvectormap"),
            a = s == "region" ? e.regions[o].element : e.markers[o].element;
         if (!t) {
            e.container.trigger(u, [o]);
            if (s === "region" && e.params.regionsSelectable || s === "marker" && e.params.markersSelectable) u.isDefaultPrevented() || (e.params[s + "sSelectableOne"] && e.clearSelected(s + "s"), a.setSelected(!a.isSelected))
         }
      })
   },
   bindZoomButtons: function() {
      var e = this;
      jvm.$("<div/>").addClass("jvectormap-zoomin").text("+").appendTo(this.container), jvm.$("<div/>").addClass("jvectormap-zoomout").html("&#x2212;").appendTo(this.container), this.container.find(".jvectormap-zoomin").click(function() {
         e.setScale(e.scale * e.params.zoomStep, e.width / 2, e.height / 2)
      }), this.container.find(".jvectormap-zoomout").click(function() {
         e.setScale(e.scale / e.params.zoomStep, e.width / 2, e.height / 2)
      })
   },
   createLabel: function() {
      var e = this;
      this.label = jvm.$("<div/>").addClass("jvectormap-label").appendTo(jvm.$("body")), this.container.mousemove(function(t) {
         var n = t.pageX - 15 - e.labelWidth,
            r = t.pageY - 15 - e.labelHeight;
         n < 5 && (n = t.pageX + 15), r < 5 && (r = t.pageY + 15), e.label.is(":visible") && e.label.css({
            left: n,
            top: r
         })
      })
   },
   setScale: function(e, t, n, r) {
      var i, s = jvm.$.Event("zoom.jvectormap");
      e > this.params.zoomMax * this.baseScale ? e = this.params.zoomMax * this.baseScale : e < this.params.zoomMin * this.baseScale && (e = this.params.zoomMin * this.baseScale), typeof t != "undefined" && typeof n != "undefined" && (i = e / this.scale, r ? (this.transX = t + this.defaultWidth * (this.width / (this.defaultWidth * e)) / 2, this.transY = n + this.defaultHeight * (this.height / (this.defaultHeight * e)) / 2) : (this.transX -= (i - 1) / e * t, this.transY -= (i - 1) / e * n)), this.scale = e, this.applyTransform(), this.container.trigger(s, [e / this.baseScale])
   },
   setFocus: function(e, t, n) {
      var r, i, s, o, u;
      if (jvm.$.isArray(e) || this.regions[e]) {
         jvm.$.isArray(e) ? o = e : o = [e];
         for (u = 0; u < o.length; u++) this.regions[o[u]] && (i = this.regions[o[u]].element.getBBox(), i && (typeof r == "undefined" ? r = i : (s = {
            x: Math.min(r.x, i.x),
            y: Math.min(r.y, i.y),
            width: Math.max(r.x + r.width, i.x + i.width) - Math.min(r.x, i.x),
            height: Math.max(r.y + r.height, i.y + i.height) - Math.min(r.y, i.y)
         }, r = s)));
         this.setScale(Math.min(this.width / r.width, this.height / r.height), -(r.x + r.width / 2), -(r.y + r.height / 2), !0)
      } else e *= this.baseScale, this.setScale(e, -t * this.defaultWidth, -n * this.defaultHeight, !0)
   },
   getSelected: function(e) {
      var t, n = [];
      for (t in this[e]) this[e][t].element.isSelected && n.push(t);
      return n
   },
   getSelectedRegions: function() {
      return this.getSelected("regions")
   },
   getSelectedMarkers: function() {
      return this.getSelected("markers")
   },
   setSelected: function(e, t) {
      var n;
      typeof t != "object" && (t = [t]);
      if (jvm.$.isArray(t))
         for (n = 0; n < t.length; n++) this[e][t[n]].element.setSelected(!0);
      else
         for (n in t) this[e][n].element.setSelected(!!t[n])
   },
   setSelectedRegions: function(e) {
      this.setSelected("regions", e)
   },
   setSelectedMarkers: function(e) {
      this.setSelected("markers", e)
   },
   clearSelected: function(e) {
      var t = {},
         n = this.getSelected(e),
         r;
      for (r = 0; r < n.length; r++) t[n[r]] = !1;
      this.setSelected(e, t)
   },
   clearSelectedRegions: function() {
      this.clearSelected("regions")
   },
   clearSelectedMarkers: function() {
      this.clearSelected("markers")
   },
   getMapObject: function() {
      return this
   },
   getRegionName: function(e) {
      return this.mapData.paths[e].name
   },
   createRegions: function() {
      var e, t, n = this;
      for (e in this.mapData.paths) t = this.canvas.addPath({
         d: this.mapData.paths[e].path,
         "data-code": e
      }, jvm.$.extend(!0, {}, this.params.regionStyle)), jvm.$(t.node).bind("selected", function(e, t) {
         n.container.trigger("regionSelected.jvectormap", [jvm.$(this).attr("data-code"), t, n.getSelectedRegions()])
      }), t.addClass("jvectormap-region jvectormap-element"), this.regions[e] = {
         element: t,
         config: this.mapData.paths[e]
      }
   },
   createMarkers: function(e) {
      var t, n, r, i, s, o = this;
      this.markersGroup = this.markersGroup || this.canvas.addGroup();
      if (jvm.$.isArray(e)) {
         s = e.slice(), e = {};
         for (t = 0; t < s.length; t++) e[t] = s[t]
      }
      for (t in e) i = e[t] instanceof Array ? {
         latLng: e[t]
      } : e[t], r = this.getMarkerPosition(i), r !== !1 && (n = this.canvas.addCircle({
         "data-index": t,
         cx: r.x,
         cy: r.y
      }, jvm.$.extend(!0, {}, this.params.markerStyle, {
         initial: i.style || {}
      }), this.markersGroup), n.addClass("jvectormap-marker jvectormap-element"), jvm.$(n.node).bind("selected", function(e, t) {
         o.container.trigger("markerSelected.jvectormap", [jvm.$(this).attr("data-index"), t, o.getSelectedMarkers()])
      }), this.markers[t] && this.removeMarkers([t]), this.markers[t] = {
         element: n,
         config: i
      })
   },
   repositionMarkers: function() {
      var e, t;
      for (e in this.markers) t = this.getMarkerPosition(this.markers[e].config), t !== !1 && this.markers[e].element.setStyle({
         cx: t.x,
         cy: t.y
      })
   },
   getMarkerPosition: function(e) {
      return jvm.WorldMap.maps[this.params.map].projection ? this.latLngToPoint.apply(this, e.latLng || [0, 0]) : {
         x: e.coords[0] * this.scale + this.transX * this.scale,
         y: e.coords[1] * this.scale + this.transY * this.scale
      }
   },
   addMarker: function(e, t, n) {
      var r = {},
         i = [],
         s, o, n = n || [];
      r[e] = t;
      for (o = 0; o < n.length; o++) s = {}, s[e] = n[o], i.push(s);
      this.addMarkers(r, i)
   },
   addMarkers: function(e, t) {
      var n;
      t = t || [], this.createMarkers(e);
      for (n = 0; n < t.length; n++) this.series.markers[n].setValues(t[n] || {})
   },
   removeMarkers: function(e) {
      var t;
      for (t = 0; t < e.length; t++) this.markers[e[t]].element.remove(), delete this.markers[e[t]]
   },
   removeAllMarkers: function() {
      var e, t = [];
      for (e in this.markers) t.push(e);
      this.removeMarkers(t)
   },
   latLngToPoint: function(e, t) {
      var n, r = jvm.WorldMap.maps[this.params.map].projection,
         i = r.centralMeridian,
         s = this.width - this.baseTransX * 2 * this.baseScale,
         o = this.height - this.baseTransY * 2 * this.baseScale,
         u, a, f = this.scale / this.baseScale;
      return t < -180 + i && (t += 360), n = jvm.Proj[r.type](e, t, i), u = this.getInsetForPoint(n.x, n.y), u ? (a = u.bbox, n.x = (n.x - a[0].x) / (a[1].x - a[0].x) * u.width * this.scale, n.y = (n.y - a[0].y) / (a[1].y - a[0].y) * u.height * this.scale, {
         x: n.x + this.transX * this.scale + u.left * this.scale,
         y: n.y + this.transY * this.scale + u.top * this.scale
      }) : !1
   },
   pointToLatLng: function(e, t) {
      var n = jvm.WorldMap.maps[this.params.map].projection,
         r = n.centralMeridian,
         i = jvm.WorldMap.maps[this.params.map].insets,
         s, o, u, a, f;
      for (s = 0; s < i.length; s++) {
         o = i[s], u = o.bbox, a = e - (this.transX * this.scale + o.left * this.scale), f = t - (this.transY * this.scale + o.top * this.scale), a = a / (o.width * this.scale) * (u[1].x - u[0].x) + u[0].x, f = f / (o.height * this.scale) * (u[1].y - u[0].y) + u[0].y;
         if (a > u[0].x && a < u[1].x && f > u[0].y && f < u[1].y) return jvm.Proj[n.type + "_inv"](a, -f, r)
      }
      return !1
   },
   getInsetForPoint: function(e, t) {
      var n = jvm.WorldMap.maps[this.params.map].insets,
         r, i;
      for (r = 0; r < n.length; r++) {
         i = n[r].bbox;
         if (e > i[0].x && e < i[1].x && t > i[0].y && t < i[1].y) return n[r]
      }
   },
   createSeries: function() {
      var e, t;
      this.series = {
         markers: [],
         regions: []
      };
      for (t in this.params.series)
         for (e = 0; e < this.params.series[t].length; e++) this.series[t][e] = new jvm.DataSeries(this.params.series[t][e], this[t])
   },
   remove: function() {
      this.label.remove(), this.container.remove(), jvm.$(window).unbind("resize", this.onResize)
   }
}, jvm.WorldMap.maps = {}, jvm.WorldMap.defaultParams = {
   map: "world_mill_en",
   backgroundColor: "#505050",
   zoomButtons: !0,
   zoomOnScroll: !0,
   zoomMax: 8,
   zoomMin: 1,
   zoomStep: 1.6,
   regionsSelectable: !1,
   markersSelectable: !1,
   bindTouchEvents: !0,
   regionStyle: {
      initial: {
         fill: "white",
         "fill-opacity": 1,
         stroke: "none",
         "stroke-width": 0,
         "stroke-opacity": 1
      },
      hover: {
         "fill-opacity": .8
      },
      selected: {
         fill: "yellow"
      },
      selectedHover: {}
   },
   markerStyle: {
      initial: {
         fill: "grey",
         stroke: "#505050",
         "fill-opacity": 1,
         "stroke-width": 1,
         "stroke-opacity": 1,
         r: 5
      },
      hover: {
         stroke: "black",
         "stroke-width": 2
      },
      selected: {
         fill: "blue"
      },
      selectedHover: {}
   }
}, jvm.WorldMap.apiEvents = {
   onRegionLabelShow: "regionLabelShow",
   onRegionOver: "regionOver",
   onRegionOut: "regionOut",
   onRegionClick: "regionClick",
   onRegionSelected: "regionSelected",
   onMarkerLabelShow: "markerLabelShow",
   onMarkerOver: "markerOver",
   onMarkerOut: "markerOut",
   onMarkerClick: "markerClick",
   onMarkerSelected: "markerSelected",
   onViewportChange: "viewportChange"
};

/*
 * fgnass.github.com/spin.js#v2.0.1
 */
;
! function(a, b) {
   "object" == typeof exports ? module.exports = b() : "function" == typeof define && define.amd ? define(b) : a.Spinner = b()
}(this, function() {
   "use strict";

   function a(a, b) {
      var c, d = document.createElement(a || "div");
      for (c in b) d[c] = b[c];
      return d
   }

   function b(a) {
      for (var b = 1, c = arguments.length; c > b; b++) a.appendChild(arguments[b]);
      return a
   }

   function c(a, b, c, d) {
      var e = ["opacity", b, ~~(100 * a), c, d].join("-"),
         f = .01 + c / d * 100,
         g = Math.max(1 - (1 - a) / b * (100 - f), a),
         h = j.substring(0, j.indexOf("Animation")).toLowerCase(),
         i = h && "-" + h + "-" || "";
      return l[e] || (m.insertRule("@" + i + "keyframes " + e + "{0%{opacity:" + g + "}" + f + "%{opacity:" + a + "}" + (f + .01) + "%{opacity:1}" + (f + b) % 100 + "%{opacity:" + a + "}100%{opacity:" + g + "}}", m.cssRules.length), l[e] = 1), e
   }

   function d(a, b) {
      var c, d, e = a.style;
      for (b = b.charAt(0).toUpperCase() + b.slice(1), d = 0; d < k.length; d++)
         if (c = k[d] + b, void 0 !== e[c]) return c;
      return void 0 !== e[b] ? b : void 0
   }

   function e(a, b) {
      for (var c in b) a.style[d(a, c) || c] = b[c];
      return a
   }

   function f(a) {
      for (var b = 1; b < arguments.length; b++) {
         var c = arguments[b];
         for (var d in c) void 0 === a[d] && (a[d] = c[d])
      }
      return a
   }

   function g(a, b) {
      return "string" == typeof a ? a : a[b % a.length]
   }

   function h(a) {
      this.opts = f(a || {}, h.defaults, n)
   }

   function i() {
      function c(b, c) {
         return a("<" + b + ' xmlns="urn:schemas-microsoft.com:vml" class="spin-vml">', c)
      }
      m.addRule(".spin-vml", "behavior:url(#default#VML)"), h.prototype.lines = function(a, d) {
         function f() {
            return e(c("group", {
               coordsize: k + " " + k,
               coordorigin: -j + " " + -j
            }), {
               width: k,
               height: k
            })
         }

         function h(a, h, i) {
            b(m, b(e(f(), {
               rotation: 360 / d.lines * a + "deg",
               left: ~~h
            }), b(e(c("roundrect", {
               arcsize: d.corners
            }), {
               width: j,
               height: d.width,
               left: d.radius,
               top: -d.width >> 1,
               filter: i
            }), c("fill", {
               color: g(d.color, a),
               opacity: d.opacity
            }), c("stroke", {
               opacity: 0
            }))))
         }
         var i, j = d.length + d.width,
            k = 2 * j,
            l = 2 * -(d.width + d.length) + "px",
            m = e(f(), {
               position: "absolute",
               top: l,
               left: l
            });
         if (d.shadow)
            for (i = 1; i <= d.lines; i++) h(i, -2, "progid:DXImageTransform.Microsoft.Blur(pixelradius=2,makeshadow=1,shadowopacity=.3)");
         for (i = 1; i <= d.lines; i++) h(i);
         return b(a, m)
      }, h.prototype.opacity = function(a, b, c, d) {
         var e = a.firstChild;
         d = d.shadow && d.lines || 0, e && b + d < e.childNodes.length && (e = e.childNodes[b + d], e = e && e.firstChild, e = e && e.firstChild, e && (e.opacity = c))
      }
   }
   var j, k = ["webkit", "Moz", "ms", "O"],
      l = {},
      m = function() {
         var c = a("style", {
            type: "text/css"
         });
         return b(document.getElementsByTagName("head")[0], c), c.sheet || c.styleSheet
      }(),
      n = {
         lines: 12,
         length: 7,
         width: 5,
         radius: 10,
         rotate: 0,
         corners: 1,
         color: "#000",
         direction: 1,
         speed: 1,
         trail: 100,
         opacity: .25,
         fps: 20,
         zIndex: 2e9,
         className: "spinner",
         top: "50%",
         left: "50%",
         position: "absolute"
      };
   h.defaults = {}, f(h.prototype, {
      spin: function(b) {
         this.stop(); {
            var c = this,
               d = c.opts,
               f = c.el = e(a(0, {
                  className: d.className
               }), {
                  position: d.position,
                  width: 0,
                  zIndex: d.zIndex
               });
            d.radius + d.length + d.width
         }
         if (e(f, {
               left: d.left,
               top: d.top
            }), b && b.insertBefore(f, b.firstChild || null), f.setAttribute("role", "progressbar"), c.lines(f, c.opts), !j) {
            var g, h = 0,
               i = (d.lines - 1) * (1 - d.direction) / 2,
               k = d.fps,
               l = k / d.speed,
               m = (1 - d.opacity) / (l * d.trail / 100),
               n = l / d.lines;
            ! function o() {
               h++;
               for (var a = 0; a < d.lines; a++) g = Math.max(1 - (h + (d.lines - a) * n) % l * m, d.opacity), c.opacity(f, a * d.direction + i, g, d);
               c.timeout = c.el && setTimeout(o, ~~(1e3 / k))
            }()
         }
         return c
      },
      stop: function() {
         var a = this.el;
         return a && (clearTimeout(this.timeout), a.parentNode && a.parentNode.removeChild(a), this.el = void 0), this
      },
      lines: function(d, f) {
         function h(b, c) {
            return e(a(), {
               position: "absolute",
               width: f.length + f.width + "px",
               height: f.width + "px",
               background: b,
               boxShadow: c,
               transformOrigin: "left",
               transform: "rotate(" + ~~(360 / f.lines * k + f.rotate) + "deg) translate(" + f.radius + "px,0)",
               borderRadius: (f.corners * f.width >> 1) + "px"
            })
         }
         for (var i, k = 0, l = (f.lines - 1) * (1 - f.direction) / 2; k < f.lines; k++) i = e(a(), {
            position: "absolute",
            top: 1 + ~(f.width / 2) + "px",
            transform: f.hwaccel ? "translate3d(0,0,0)" : "",
            opacity: f.opacity,
            animation: j && c(f.opacity, f.trail, l + k * f.direction, f.lines) + " " + 1 / f.speed + "s linear infinite"
         }), f.shadow && b(i, e(h("#000", "0 0 4px #000"), {
            top: "2px"
         })), b(d, b(i, h(g(f.color, k), "0 0 1px rgba(0,0,0,.1)")));
         return d
      },
      opacity: function(a, b, c) {
         b < a.childNodes.length && (a.childNodes[b].style.opacity = c)
      }
   });
   var o = e(a("group"), {
      behavior: "url(#default#VML)"
   });
   return !d(o, "transform") && o.adj ? i() : j = d(o, "animation"), h
});

/*
 * nanoScrollerJS - v0.8.4 - (c) 2014 James Florentino; Licensed MIT
 */
;
! function(a) {
   return "function" == typeof define && define.amd ? define(["jquery"], function(b) {
      return a(b, window, document)
   }) : a(jQuery, window, document)
}(function(a, b, c) {
   "use strict";
   var d, e, f, g, h, i, j, k, l, m, n, o, p, q, r, s, t, u, v, w, x, y, z, A, B, C, D, E, F, G, H;
   z = {
      paneClass: "nano-pane",
      sliderClass: "nano-slider",
      contentClass: "nano-content",
      iOSNativeScrolling: !1,
      preventPageScrolling: !1,
      disableResize: !1,
      alwaysVisible: !1,
      flashDelay: 1500,
      sliderMinHeight: 20,
      sliderMaxHeight: null,
      documentContext: null,
      windowContext: null
   }, u = "scrollbar", t = "scroll", l = "mousedown", m = "mouseenter", n = "mousemove", p = "mousewheel", o = "mouseup", s = "resize", h = "drag", i = "enter", w = "up", r = "panedown", f = "DOMMouseScroll", g = "down", x = "wheel", j = "keydown", k = "keyup", v = "touchmove", d = "Microsoft Internet Explorer" === b.navigator.appName && /msie 7./i.test(b.navigator.appVersion) && b.ActiveXObject, e = null, D = b.requestAnimationFrame, y = b.cancelAnimationFrame, F = c.createElement("div").style, H = function() {
      var a, b, c, d, e, f;
      for (d = ["t", "webkitT", "MozT", "msT", "OT"], a = e = 0, f = d.length; f > e; a = ++e)
         if (c = d[a], b = d[a] + "ransform", b in F) return d[a].substr(0, d[a].length - 1);
      return !1
   }(), G = function(a) {
      return H === !1 ? !1 : "" === H ? a : H + a.charAt(0).toUpperCase() + a.substr(1)
   }, E = G("transform"), B = E !== !1, A = function() {
      var a, b, d;
      return a = c.createElement("div"), b = a.style, b.position = "absolute", b.width = "100px", b.height = "100px", b.overflow = t, b.top = "-9999px", c.body.appendChild(a), d = a.offsetWidth - a.clientWidth, c.body.removeChild(a), d
   }, C = function() {
      var a, c, d;
      return c = b.navigator.userAgent, (a = /(?=.+Mac OS X)(?=.+Firefox)/.test(c)) ? (d = /Firefox\/\d{2}\./.exec(c), d && (d = d[0].replace(/\D+/g, "")), a && +d > 23) : !1
   }, q = function() {
      function j(d, f) {
         this.el = d, this.options = f, e || (e = A()), this.$el = a(this.el), this.doc = a(this.options.documentContext || c), this.win = a(this.options.windowContext || b), this.body = this.doc.find("body"), this.$content = this.$el.children("." + f.contentClass), this.$content.attr("tabindex", this.options.tabIndex || 0), this.content = this.$content[0], this.previousPosition = 0, this.options.iOSNativeScrolling && null != this.el.style.WebkitOverflowScrolling ? this.nativeScrolling() : this.generate(), this.createEvents(), this.addEvents(), this.reset()
      }
      return j.prototype.preventScrolling = function(a, b) {
         if (this.isActive)
            if (a.type === f)(b === g && a.originalEvent.detail > 0 || b === w && a.originalEvent.detail < 0) && a.preventDefault();
            else if (a.type === p) {
            if (!a.originalEvent || !a.originalEvent.wheelDelta) return;
            (b === g && a.originalEvent.wheelDelta < 0 || b === w && a.originalEvent.wheelDelta > 0) && a.preventDefault()
         }
      }, j.prototype.nativeScrolling = function() {
         this.$content.css({
            WebkitOverflowScrolling: "touch"
         }), this.iOSNativeScrolling = !0, this.isActive = !0
      }, j.prototype.updateScrollValues = function() {
         var a, b;
         a = this.content, this.maxScrollTop = a.scrollHeight - a.clientHeight, this.prevScrollTop = this.contentScrollTop || 0, this.contentScrollTop = a.scrollTop, b = this.contentScrollTop > this.previousPosition ? "down" : this.contentScrollTop < this.previousPosition ? "up" : "same", this.previousPosition = this.contentScrollTop, "same" !== b && this.$el.trigger("update", {
            position: this.contentScrollTop,
            maximum: this.maxScrollTop,
            direction: b
         }), this.iOSNativeScrolling || (this.maxSliderTop = this.paneHeight - this.sliderHeight, this.sliderTop = 0 === this.maxScrollTop ? 0 : this.contentScrollTop * this.maxSliderTop / this.maxScrollTop)
      }, j.prototype.setOnScrollStyles = function() {
         var a;
         B ? (a = {}, a[E] = "translate(0, " + this.sliderTop + "px)") : a = {
            top: this.sliderTop
         }, D ? (y && this.scrollRAF && y(this.scrollRAF), this.scrollRAF = D(function(b) {
            return function() {
               return b.scrollRAF = null, b.slider.css(a)
            }
         }(this))) : this.slider.css(a)
      }, j.prototype.createEvents = function() {
         this.events = {
            down: function(a) {
               return function(b) {
                  return a.isBeingDragged = !0, a.offsetY = b.pageY - a.slider.offset().top, a.slider.is(b.target) || (a.offsetY = 0), a.pane.addClass("active"), a.doc.bind(n, a.events[h]).bind(o, a.events[w]), a.body.bind(m, a.events[i]), !1
               }
            }(this),
            drag: function(a) {
               return function(b) {
                  return a.sliderY = b.pageY - a.$el.offset().top - a.paneTop - (a.offsetY || .5 * a.sliderHeight), a.scroll(), a.contentScrollTop >= a.maxScrollTop && a.prevScrollTop !== a.maxScrollTop ? a.$el.trigger("scrollend") : 0 === a.contentScrollTop && 0 !== a.prevScrollTop && a.$el.trigger("scrolltop"), !1
               }
            }(this),
            up: function(a) {
               return function() {
                  return a.isBeingDragged = !1, a.pane.removeClass("active"), a.doc.unbind(n, a.events[h]).unbind(o, a.events[w]), a.body.unbind(m, a.events[i]), !1
               }
            }(this),
            resize: function(a) {
               return function() {
                  a.reset()
               }
            }(this),
            panedown: function(a) {
               return function(b) {
                  return a.sliderY = (b.offsetY || b.originalEvent.layerY) - .5 * a.sliderHeight, a.scroll(), a.events.down(b), !1
               }
            }(this),
            scroll: function(a) {
               return function(b) {
                  a.updateScrollValues(), a.isBeingDragged || (a.iOSNativeScrolling || (a.sliderY = a.sliderTop, a.setOnScrollStyles()), null != b && (a.contentScrollTop >= a.maxScrollTop ? (a.options.preventPageScrolling && a.preventScrolling(b, g), a.prevScrollTop !== a.maxScrollTop && a.$el.trigger("scrollend")) : 0 === a.contentScrollTop && (a.options.preventPageScrolling && a.preventScrolling(b, w), 0 !== a.prevScrollTop && a.$el.trigger("scrolltop"))))
               }
            }(this),
            wheel: function(a) {
               return function(b) {
                  var c;
                  if (null != b) return c = b.delta || b.wheelDelta || b.originalEvent && b.originalEvent.wheelDelta || -b.detail || b.originalEvent && -b.originalEvent.detail, c && (a.sliderY += -c / 3), a.scroll(), !1
               }
            }(this),
            enter: function(a) {
               return function(b) {
                  var c;
                  if (a.isBeingDragged) return 1 !== (b.buttons || b.which) ? (c = a.events)[w].apply(c, arguments) : void 0
               }
            }(this)
         }
      }, j.prototype.addEvents = function() {
         var a;
         this.removeEvents(), a = this.events, this.options.disableResize || this.win.bind(s, a[s]), this.iOSNativeScrolling || (this.slider.bind(l, a[g]), this.pane.bind(l, a[r]).bind("" + p + " " + f, a[x])), this.$content.bind("" + t + " " + p + " " + f + " " + v, a[t])
      }, j.prototype.removeEvents = function() {
         var a;
         a = this.events, this.win.unbind(s, a[s]), this.iOSNativeScrolling || (this.slider.unbind(), this.pane.unbind()), this.$content.unbind("" + t + " " + p + " " + f + " " + v, a[t])
      }, j.prototype.generate = function() {
         var a, c, d, f, g, h, i;
         return f = this.options, h = f.paneClass, i = f.sliderClass, a = f.contentClass, (g = this.$el.children("." + h)).length || g.children("." + i).length || this.$el.append('<div class="' + h + '"><div class="' + i + '" /></div>'), this.pane = this.$el.children("." + h), this.slider = this.pane.find("." + i), 0 === e && C() ? (d = b.getComputedStyle(this.content, null).getPropertyValue("padding-right").replace(/[^0-9.]+/g, ""), c = {
            right: -14,
            paddingRight: +d + 14
         }) : e && (c = {
            right: -e
         }, this.$el.addClass("has-scrollbar")), null != c && this.$content.css(c), this
      }, j.prototype.restore = function() {
         this.stopped = !1, this.iOSNativeScrolling || this.pane.show(), this.addEvents()
      }, j.prototype.reset = function() {
         var a, b, c, f, g, h, i, j, k, l, m, n;
         return this.iOSNativeScrolling ? void(this.contentHeight = this.content.scrollHeight) : (this.$el.find("." + this.options.paneClass).length || this.generate().stop(), this.stopped && this.restore(), a = this.content, f = a.style, g = f.overflowY, d && this.$content.css({
            height: this.$content.height()
         }), b = a.scrollHeight + e, l = parseInt(this.$el.css("max-height"), 10), l > 0 && (this.$el.height(""), this.$el.height(a.scrollHeight > l ? l : a.scrollHeight)), i = this.pane.outerHeight(!1), k = parseInt(this.pane.css("top"), 10), h = parseInt(this.pane.css("bottom"), 10), j = i + k + h, n = Math.round(j / b * j), n < this.options.sliderMinHeight ? n = this.options.sliderMinHeight : null != this.options.sliderMaxHeight && n > this.options.sliderMaxHeight && (n = this.options.sliderMaxHeight), g === t && f.overflowX !== t && (n += e), this.maxSliderTop = j - n, this.contentHeight = b, this.paneHeight = i, this.paneOuterHeight = j, this.sliderHeight = n, this.paneTop = k, this.slider.height(n), this.events.scroll(), this.pane.show(), this.isActive = !0, a.scrollHeight === a.clientHeight || this.pane.outerHeight(!0) >= a.scrollHeight && g !== t ? (this.pane.hide(), this.isActive = !1) : this.el.clientHeight === a.scrollHeight && g === t ? this.slider.hide() : this.slider.show(), this.pane.css({
            opacity: this.options.alwaysVisible ? 1 : "",
            visibility: this.options.alwaysVisible ? "visible" : ""
         }), c = this.$content.css("position"), ("static" === c || "relative" === c) && (m = parseInt(this.$content.css("right"), 10), m && this.$content.css({
            right: "",
            marginRight: m
         })), this)
      }, j.prototype.scroll = function() {
         return this.isActive ? (this.sliderY = Math.max(0, this.sliderY), this.sliderY = Math.min(this.maxSliderTop, this.sliderY), this.$content.scrollTop(this.maxScrollTop * this.sliderY / this.maxSliderTop), this.iOSNativeScrolling || (this.updateScrollValues(), this.setOnScrollStyles()), this) : void 0
      }, j.prototype.scrollBottom = function(a) {
         return this.isActive ? (this.$content.scrollTop(this.contentHeight - this.$content.height() - a).trigger(p), this.stop().restore(), this) : void 0
      }, j.prototype.scrollTop = function(a) {
         return this.isActive ? (this.$content.scrollTop(+a).trigger(p), this.stop().restore(), this) : void 0
      }, j.prototype.scrollTo = function(a) {
         return this.isActive ? (this.scrollTop(this.$el.find(a).get(0).offsetTop), this) : void 0
      }, j.prototype.stop = function() {
         return y && this.scrollRAF && (y(this.scrollRAF), this.scrollRAF = null), this.stopped = !0, this.removeEvents(), this.iOSNativeScrolling || this.pane.hide(), this
      }, j.prototype.destroy = function() {
         return this.stopped || this.stop(), !this.iOSNativeScrolling && this.pane.length && this.pane.remove(), d && this.$content.height(""), this.$content.removeAttr("tabindex"), this.$el.hasClass("has-scrollbar") && (this.$el.removeClass("has-scrollbar"), this.$content.css({
            right: ""
         })), this
      }, j.prototype.flash = function() {
         return !this.iOSNativeScrolling && this.isActive ? (this.reset(), this.pane.addClass("flashed"), setTimeout(function(a) {
            return function() {
               a.pane.removeClass("flashed")
            }
         }(this), this.options.flashDelay), this) : void 0
      }, j
   }(), a.fn.nanoScroller = function(b) {
      return this.each(function() {
         var c, d;
         if ((d = this.nanoscroller) || (c = a.extend({}, z, b), this.nanoscroller = d = new q(this, c)), b && "object" == typeof b) {
            if (a.extend(d.options, b), null != b.scrollBottom) return d.scrollBottom(b.scrollBottom);
            if (null != b.scrollTop) return d.scrollTop(b.scrollTop);
            if (b.scrollTo) return d.scrollTo(b.scrollTo);
            if ("bottom" === b.scroll) return d.scrollBottom(0);
            if ("top" === b.scroll) return d.scrollTop(0);
            if (b.scroll && b.scroll instanceof a) return d.scrollTo(b.scroll);
            if (b.stop) return d.stop();
            if (b.destroy) return d.destroy();
            if (b.flash) return d.flash()
         }
         return d.reset()
      })
   }, a.fn.nanoScroller.Constructor = q
});

/*!
Underscore.js 1.7.0
http://underscorejs.org
(c) 2009-2014 Jeremy Ashkenas, DocumentCloud and Investigative Reporters & Editors
Underscore may be freely distributed under the MIT license.
*/
;
(function(){var n=this,t=n._,r=Array.prototype,e=Object.prototype,u=Function.prototype,i=r.push,a=r.slice,o=r.concat,l=e.toString,c=e.hasOwnProperty,f=Array.isArray,s=Object.keys,p=u.bind,h=function(n){return n instanceof h?n:this instanceof h?void(this._wrapped=n):new h(n)};"undefined"!=typeof exports?("undefined"!=typeof module&&module.exports&&(exports=module.exports=h),exports._=h):n._=h,h.VERSION="1.7.0";var g=function(n,t,r){if(t===void 0)return n;switch(null==r?3:r){case 1:return function(r){return n.call(t,r)};case 2:return function(r,e){return n.call(t,r,e)};case 3:return function(r,e,u){return n.call(t,r,e,u)};case 4:return function(r,e,u,i){return n.call(t,r,e,u,i)}}return function(){return n.apply(t,arguments)}};h.iteratee=function(n,t,r){return null==n?h.identity:h.isFunction(n)?g(n,t,r):h.isObject(n)?h.matches(n):h.property(n)},h.each=h.forEach=function(n,t,r){if(null==n)return n;t=g(t,r);var e,u=n.length;if(u===+u)for(e=0;u>e;e++)t(n[e],e,n);else{var i=h.keys(n);for(e=0,u=i.length;u>e;e++)t(n[i[e]],i[e],n)}return n},h.map=h.collect=function(n,t,r){if(null==n)return[];t=h.iteratee(t,r);for(var e,u=n.length!==+n.length&&h.keys(n),i=(u||n).length,a=Array(i),o=0;i>o;o++)e=u?u[o]:o,a[o]=t(n[e],e,n);return a};var v="Reduce of empty array with no initial value";h.reduce=h.foldl=h.inject=function(n,t,r,e){null==n&&(n=[]),t=g(t,e,4);var u,i=n.length!==+n.length&&h.keys(n),a=(i||n).length,o=0;if(arguments.length<3){if(!a)throw new TypeError(v);r=n[i?i[o++]:o++]}for(;a>o;o++)u=i?i[o]:o,r=t(r,n[u],u,n);return r},h.reduceRight=h.foldr=function(n,t,r,e){null==n&&(n=[]),t=g(t,e,4);var u,i=n.length!==+n.length&&h.keys(n),a=(i||n).length;if(arguments.length<3){if(!a)throw new TypeError(v);r=n[i?i[--a]:--a]}for(;a--;)u=i?i[a]:a,r=t(r,n[u],u,n);return r},h.find=h.detect=function(n,t,r){var e;return t=h.iteratee(t,r),h.some(n,function(n,r,u){return t(n,r,u)?(e=n,!0):void 0}),e},h.filter=h.select=function(n,t,r){var e=[];return null==n?e:(t=h.iteratee(t,r),h.each(n,function(n,r,u){t(n,r,u)&&e.push(n)}),e)},h.reject=function(n,t,r){return h.filter(n,h.negate(h.iteratee(t)),r)},h.every=h.all=function(n,t,r){if(null==n)return!0;t=h.iteratee(t,r);var e,u,i=n.length!==+n.length&&h.keys(n),a=(i||n).length;for(e=0;a>e;e++)if(u=i?i[e]:e,!t(n[u],u,n))return!1;return!0},h.some=h.any=function(n,t,r){if(null==n)return!1;t=h.iteratee(t,r);var e,u,i=n.length!==+n.length&&h.keys(n),a=(i||n).length;for(e=0;a>e;e++)if(u=i?i[e]:e,t(n[u],u,n))return!0;return!1},h.contains=h.include=function(n,t){return null==n?!1:(n.length!==+n.length&&(n=h.values(n)),h.indexOf(n,t)>=0)},h.invoke=function(n,t){var r=a.call(arguments,2),e=h.isFunction(t);return h.map(n,function(n){return(e?t:n[t]).apply(n,r)})},h.pluck=function(n,t){return h.map(n,h.property(t))},h.where=function(n,t){return h.filter(n,h.matches(t))},h.findWhere=function(n,t){return h.find(n,h.matches(t))},h.max=function(n,t,r){var e,u,i=-1/0,a=-1/0;if(null==t&&null!=n){n=n.length===+n.length?n:h.values(n);for(var o=0,l=n.length;l>o;o++)e=n[o],e>i&&(i=e)}else t=h.iteratee(t,r),h.each(n,function(n,r,e){u=t(n,r,e),(u>a||u===-1/0&&i===-1/0)&&(i=n,a=u)});return i},h.min=function(n,t,r){var e,u,i=1/0,a=1/0;if(null==t&&null!=n){n=n.length===+n.length?n:h.values(n);for(var o=0,l=n.length;l>o;o++)e=n[o],i>e&&(i=e)}else t=h.iteratee(t,r),h.each(n,function(n,r,e){u=t(n,r,e),(a>u||1/0===u&&1/0===i)&&(i=n,a=u)});return i},h.shuffle=function(n){for(var t,r=n&&n.length===+n.length?n:h.values(n),e=r.length,u=Array(e),i=0;e>i;i++)t=h.random(0,i),t!==i&&(u[i]=u[t]),u[t]=r[i];return u},h.sample=function(n,t,r){return null==t||r?(n.length!==+n.length&&(n=h.values(n)),n[h.random(n.length-1)]):h.shuffle(n).slice(0,Math.max(0,t))},h.sortBy=function(n,t,r){return t=h.iteratee(t,r),h.pluck(h.map(n,function(n,r,e){return{value:n,index:r,criteria:t(n,r,e)}}).sort(function(n,t){var r=n.criteria,e=t.criteria;if(r!==e){if(r>e||r===void 0)return 1;if(e>r||e===void 0)return-1}return n.index-t.index}),"value")};var m=function(n){return function(t,r,e){var u={};return r=h.iteratee(r,e),h.each(t,function(e,i){var a=r(e,i,t);n(u,e,a)}),u}};h.groupBy=m(function(n,t,r){h.has(n,r)?n[r].push(t):n[r]=[t]}),h.indexBy=m(function(n,t,r){n[r]=t}),h.countBy=m(function(n,t,r){h.has(n,r)?n[r]++:n[r]=1}),h.sortedIndex=function(n,t,r,e){r=h.iteratee(r,e,1);for(var u=r(t),i=0,a=n.length;a>i;){var o=i+a>>>1;r(n[o])<u?i=o+1:a=o}return i},h.toArray=function(n){return n?h.isArray(n)?a.call(n):n.length===+n.length?h.map(n,h.identity):h.values(n):[]},h.size=function(n){return null==n?0:n.length===+n.length?n.length:h.keys(n).length},h.partition=function(n,t,r){t=h.iteratee(t,r);var e=[],u=[];return h.each(n,function(n,r,i){(t(n,r,i)?e:u).push(n)}),[e,u]},h.first=h.head=h.take=function(n,t,r){return null==n?void 0:null==t||r?n[0]:0>t?[]:a.call(n,0,t)},h.initial=function(n,t,r){return a.call(n,0,Math.max(0,n.length-(null==t||r?1:t)))},h.last=function(n,t,r){return null==n?void 0:null==t||r?n[n.length-1]:a.call(n,Math.max(n.length-t,0))},h.rest=h.tail=h.drop=function(n,t,r){return a.call(n,null==t||r?1:t)},h.compact=function(n){return h.filter(n,h.identity)};var y=function(n,t,r,e){if(t&&h.every(n,h.isArray))return o.apply(e,n);for(var u=0,a=n.length;a>u;u++){var l=n[u];h.isArray(l)||h.isArguments(l)?t?i.apply(e,l):y(l,t,r,e):r||e.push(l)}return e};h.flatten=function(n,t){return y(n,t,!1,[])},h.without=function(n){return h.difference(n,a.call(arguments,1))},h.uniq=h.unique=function(n,t,r,e){if(null==n)return[];h.isBoolean(t)||(e=r,r=t,t=!1),null!=r&&(r=h.iteratee(r,e));for(var u=[],i=[],a=0,o=n.length;o>a;a++){var l=n[a];if(t)a&&i===l||u.push(l),i=l;else if(r){var c=r(l,a,n);h.indexOf(i,c)<0&&(i.push(c),u.push(l))}else h.indexOf(u,l)<0&&u.push(l)}return u},h.union=function(){return h.uniq(y(arguments,!0,!0,[]))},h.intersection=function(n){if(null==n)return[];for(var t=[],r=arguments.length,e=0,u=n.length;u>e;e++){var i=n[e];if(!h.contains(t,i)){for(var a=1;r>a&&h.contains(arguments[a],i);a++);a===r&&t.push(i)}}return t},h.difference=function(n){var t=y(a.call(arguments,1),!0,!0,[]);return h.filter(n,function(n){return!h.contains(t,n)})},h.zip=function(n){if(null==n)return[];for(var t=h.max(arguments,"length").length,r=Array(t),e=0;t>e;e++)r[e]=h.pluck(arguments,e);return r},h.object=function(n,t){if(null==n)return{};for(var r={},e=0,u=n.length;u>e;e++)t?r[n[e]]=t[e]:r[n[e][0]]=n[e][1];return r},h.indexOf=function(n,t,r){if(null==n)return-1;var e=0,u=n.length;if(r){if("number"!=typeof r)return e=h.sortedIndex(n,t),n[e]===t?e:-1;e=0>r?Math.max(0,u+r):r}for(;u>e;e++)if(n[e]===t)return e;return-1},h.lastIndexOf=function(n,t,r){if(null==n)return-1;var e=n.length;for("number"==typeof r&&(e=0>r?e+r+1:Math.min(e,r+1));--e>=0;)if(n[e]===t)return e;return-1},h.range=function(n,t,r){arguments.length<=1&&(t=n||0,n=0),r=r||1;for(var e=Math.max(Math.ceil((t-n)/r),0),u=Array(e),i=0;e>i;i++,n+=r)u[i]=n;return u};var d=function(){};h.bind=function(n,t){var r,e;if(p&&n.bind===p)return p.apply(n,a.call(arguments,1));if(!h.isFunction(n))throw new TypeError("Bind must be called on a function");return r=a.call(arguments,2),e=function(){if(!(this instanceof e))return n.apply(t,r.concat(a.call(arguments)));d.prototype=n.prototype;var u=new d;d.prototype=null;var i=n.apply(u,r.concat(a.call(arguments)));return h.isObject(i)?i:u}},h.partial=function(n){var t=a.call(arguments,1);return function(){for(var r=0,e=t.slice(),u=0,i=e.length;i>u;u++)e[u]===h&&(e[u]=arguments[r++]);for(;r<arguments.length;)e.push(arguments[r++]);return n.apply(this,e)}},h.bindAll=function(n){var t,r,e=arguments.length;if(1>=e)throw new Error("bindAll must be passed function names");for(t=1;e>t;t++)r=arguments[t],n[r]=h.bind(n[r],n);return n},h.memoize=function(n,t){var r=function(e){var u=r.cache,i=t?t.apply(this,arguments):e;return h.has(u,i)||(u[i]=n.apply(this,arguments)),u[i]};return r.cache={},r},h.delay=function(n,t){var r=a.call(arguments,2);return setTimeout(function(){return n.apply(null,r)},t)},h.defer=function(n){return h.delay.apply(h,[n,1].concat(a.call(arguments,1)))},h.throttle=function(n,t,r){var e,u,i,a=null,o=0;r||(r={});var l=function(){o=r.leading===!1?0:h.now(),a=null,i=n.apply(e,u),a||(e=u=null)};return function(){var c=h.now();o||r.leading!==!1||(o=c);var f=t-(c-o);return e=this,u=arguments,0>=f||f>t?(clearTimeout(a),a=null,o=c,i=n.apply(e,u),a||(e=u=null)):a||r.trailing===!1||(a=setTimeout(l,f)),i}},h.debounce=function(n,t,r){var e,u,i,a,o,l=function(){var c=h.now()-a;t>c&&c>0?e=setTimeout(l,t-c):(e=null,r||(o=n.apply(i,u),e||(i=u=null)))};return function(){i=this,u=arguments,a=h.now();var c=r&&!e;return e||(e=setTimeout(l,t)),c&&(o=n.apply(i,u),i=u=null),o}},h.wrap=function(n,t){return h.partial(t,n)},h.negate=function(n){return function(){return!n.apply(this,arguments)}},h.compose=function(){var n=arguments,t=n.length-1;return function(){for(var r=t,e=n[t].apply(this,arguments);r--;)e=n[r].call(this,e);return e}},h.after=function(n,t){return function(){return--n<1?t.apply(this,arguments):void 0}},h.before=function(n,t){var r;return function(){return--n>0?r=t.apply(this,arguments):t=null,r}},h.once=h.partial(h.before,2),h.keys=function(n){if(!h.isObject(n))return[];if(s)return s(n);var t=[];for(var r in n)h.has(n,r)&&t.push(r);return t},h.values=function(n){for(var t=h.keys(n),r=t.length,e=Array(r),u=0;r>u;u++)e[u]=n[t[u]];return e},h.pairs=function(n){for(var t=h.keys(n),r=t.length,e=Array(r),u=0;r>u;u++)e[u]=[t[u],n[t[u]]];return e},h.invert=function(n){for(var t={},r=h.keys(n),e=0,u=r.length;u>e;e++)t[n[r[e]]]=r[e];return t},h.functions=h.methods=function(n){var t=[];for(var r in n)h.isFunction(n[r])&&t.push(r);return t.sort()},h.extend=function(n){if(!h.isObject(n))return n;for(var t,r,e=1,u=arguments.length;u>e;e++){t=arguments[e];for(r in t)c.call(t,r)&&(n[r]=t[r])}return n},h.pick=function(n,t,r){var e,u={};if(null==n)return u;if(h.isFunction(t)){t=g(t,r);for(e in n){var i=n[e];t(i,e,n)&&(u[e]=i)}}else{var l=o.apply([],a.call(arguments,1));n=new Object(n);for(var c=0,f=l.length;f>c;c++)e=l[c],e in n&&(u[e]=n[e])}return u},h.omit=function(n,t,r){if(h.isFunction(t))t=h.negate(t);else{var e=h.map(o.apply([],a.call(arguments,1)),String);t=function(n,t){return!h.contains(e,t)}}return h.pick(n,t,r)},h.defaults=function(n){if(!h.isObject(n))return n;for(var t=1,r=arguments.length;r>t;t++){var e=arguments[t];for(var u in e)n[u]===void 0&&(n[u]=e[u])}return n},h.clone=function(n){return h.isObject(n)?h.isArray(n)?n.slice():h.extend({},n):n},h.tap=function(n,t){return t(n),n};var b=function(n,t,r,e){if(n===t)return 0!==n||1/n===1/t;if(null==n||null==t)return n===t;n instanceof h&&(n=n._wrapped),t instanceof h&&(t=t._wrapped);var u=l.call(n);if(u!==l.call(t))return!1;switch(u){case"[object RegExp]":case"[object String]":return""+n==""+t;case"[object Number]":return+n!==+n?+t!==+t:0===+n?1/+n===1/t:+n===+t;case"[object Date]":case"[object Boolean]":return+n===+t}if("object"!=typeof n||"object"!=typeof t)return!1;for(var i=r.length;i--;)if(r[i]===n)return e[i]===t;var a=n.constructor,o=t.constructor;if(a!==o&&"constructor"in n&&"constructor"in t&&!(h.isFunction(a)&&a instanceof a&&h.isFunction(o)&&o instanceof o))return!1;r.push(n),e.push(t);var c,f;if("[object Array]"===u){if(c=n.length,f=c===t.length)for(;c--&&(f=b(n[c],t[c],r,e)););}else{var s,p=h.keys(n);if(c=p.length,f=h.keys(t).length===c)for(;c--&&(s=p[c],f=h.has(t,s)&&b(n[s],t[s],r,e)););}return r.pop(),e.pop(),f};h.isEqual=function(n,t){return b(n,t,[],[])},h.isEmpty=function(n){if(null==n)return!0;if(h.isArray(n)||h.isString(n)||h.isArguments(n))return 0===n.length;for(var t in n)if(h.has(n,t))return!1;return!0},h.isElement=function(n){return!(!n||1!==n.nodeType)},h.isArray=f||function(n){return"[object Array]"===l.call(n)},h.isObject=function(n){var t=typeof n;return"function"===t||"object"===t&&!!n},h.each(["Arguments","Function","String","Number","Date","RegExp"],function(n){h["is"+n]=function(t){return l.call(t)==="[object "+n+"]"}}),h.isArguments(arguments)||(h.isArguments=function(n){return h.has(n,"callee")}),"function"!=typeof/./&&(h.isFunction=function(n){return"function"==typeof n||!1}),h.isFinite=function(n){return isFinite(n)&&!isNaN(parseFloat(n))},h.isNaN=function(n){return h.isNumber(n)&&n!==+n},h.isBoolean=function(n){return n===!0||n===!1||"[object Boolean]"===l.call(n)},h.isNull=function(n){return null===n},h.isUndefined=function(n){return n===void 0},h.has=function(n,t){return null!=n&&c.call(n,t)},h.noConflict=function(){return n._=t,this},h.identity=function(n){return n},h.constant=function(n){return function(){return n}},h.noop=function(){},h.property=function(n){return function(t){return t[n]}},h.matches=function(n){var t=h.pairs(n),r=t.length;return function(n){if(null==n)return!r;n=new Object(n);for(var e=0;r>e;e++){var u=t[e],i=u[0];if(u[1]!==n[i]||!(i in n))return!1}return!0}},h.times=function(n,t,r){var e=Array(Math.max(0,n));t=g(t,r,1);for(var u=0;n>u;u++)e[u]=t(u);return e},h.random=function(n,t){return null==t&&(t=n,n=0),n+Math.floor(Math.random()*(t-n+1))},h.now=Date.now||function(){return(new Date).getTime()};var _={"&":"&amp;","<":"&lt;",">":"&gt;",'"':"&quot;","'":"&#x27;","`":"&#x60;"},w=h.invert(_),j=function(n){var t=function(t){return n[t]},r="(?:"+h.keys(n).join("|")+")",e=RegExp(r),u=RegExp(r,"g");return function(n){return n=null==n?"":""+n,e.test(n)?n.replace(u,t):n}};h.escape=j(_),h.unescape=j(w),h.result=function(n,t){if(null==n)return void 0;var r=n[t];return h.isFunction(r)?n[t]():r};var x=0;h.uniqueId=function(n){var t=++x+"";return n?n+t:t},h.templateSettings={evaluate:/<%([\s\S]+?)%>/g,interpolate:/<%=([\s\S]+?)%>/g,escape:/<%-([\s\S]+?)%>/g};var A=/(.)^/,k={"'":"'","\\":"\\","\r":"r","\n":"n","\u2028":"u2028","\u2029":"u2029"},O=/\\|'|\r|\n|\u2028|\u2029/g,F=function(n){return"\\"+k[n]};h.template=function(n,t,r){!t&&r&&(t=r),t=h.defaults({},t,h.templateSettings);var e=RegExp([(t.escape||A).source,(t.interpolate||A).source,(t.evaluate||A).source].join("|")+"|$","g"),u=0,i="__p+='";n.replace(e,function(t,r,e,a,o){return i+=n.slice(u,o).replace(O,F),u=o+t.length,r?i+="'+\n((__t=("+r+"))==null?'':_.escape(__t))+\n'":e?i+="'+\n((__t=("+e+"))==null?'':__t)+\n'":a&&(i+="';\n"+a+"\n__p+='"),t}),i+="';\n",t.variable||(i="with(obj||{}){\n"+i+"}\n"),i="var __t,__p='',__j=Array.prototype.join,"+"print=function(){__p+=__j.call(arguments,'');};\n"+i+"return __p;\n";try{var a=new Function(t.variable||"obj","_",i)}catch(o){throw o.source=i,o}var l=function(n){return a.call(this,n,h)},c=t.variable||"obj";return l.source="function("+c+"){\n"+i+"}",l},h.chain=function(n){var t=h(n);return t._chain=!0,t};var E=function(n){return this._chain?h(n).chain():n};h.mixin=function(n){h.each(h.functions(n),function(t){var r=h[t]=n[t];h.prototype[t]=function(){var n=[this._wrapped];return i.apply(n,arguments),E.call(this,r.apply(h,n))}})},h.mixin(h),h.each(["pop","push","reverse","shift","sort","splice","unshift"],function(n){var t=r[n];h.prototype[n]=function(){var r=this._wrapped;return t.apply(r,arguments),"shift"!==n&&"splice"!==n||0!==r.length||delete r[0],E.call(this,r)}}),h.each(["concat","join","slice"],function(n){var t=r[n];h.prototype[n]=function(){return E.call(this,t.apply(this._wrapped,arguments))}}),h.prototype.value=function(){return this._wrapped},"function"==typeof define&&define.amd&&define("underscore",[],function(){return h})}).call(this);

/*! Magnific Popup - v0.9.9 - 2014-09-06
* http://dimsemenov.com/plugins/magnific-popup/
* Copyright (c) 2014 Dmitry Semenov; */
(function(e){var t,n,i,o,r,a,s,l="Close",c="BeforeClose",d="AfterClose",u="BeforeAppend",p="MarkupParse",f="Open",m="Change",g="mfp",h="."+g,v="mfp-ready",C="mfp-removing",y="mfp-prevent-close",w=function(){},b=!!window.jQuery,I=e(window),x=function(e,n){t.ev.on(g+e+h,n)},k=function(t,n,i,o){var r=document.createElement("div");return r.className="mfp-"+t,i&&(r.innerHTML=i),o?n&&n.appendChild(r):(r=e(r),n&&r.appendTo(n)),r},T=function(n,i){t.ev.triggerHandler(g+n,i),t.st.callbacks&&(n=n.charAt(0).toLowerCase()+n.slice(1),t.st.callbacks[n]&&t.st.callbacks[n].apply(t,e.isArray(i)?i:[i]))},E=function(n){return n===s&&t.currTemplate.closeBtn||(t.currTemplate.closeBtn=e(t.st.closeMarkup.replace("%title%",t.st.tClose)),s=n),t.currTemplate.closeBtn},_=function(){e.magnificPopup.instance||(t=new w,t.init(),e.magnificPopup.instance=t)},S=function(){var e=document.createElement("p").style,t=["ms","O","Moz","Webkit"];if(void 0!==e.transition)return!0;for(;t.length;)if(t.pop()+"Transition"in e)return!0;return!1};w.prototype={constructor:w,init:function(){var n=navigator.appVersion;t.isIE7=-1!==n.indexOf("MSIE 7."),t.isIE8=-1!==n.indexOf("MSIE 8."),t.isLowIE=t.isIE7||t.isIE8,t.isAndroid=/android/gi.test(n),t.isIOS=/iphone|ipad|ipod/gi.test(n),t.supportsTransition=S(),t.probablyMobile=t.isAndroid||t.isIOS||/(Opera Mini)|Kindle|webOS|BlackBerry|(Opera Mobi)|(Windows Phone)|IEMobile/i.test(navigator.userAgent),o=e(document),t.popupsCache={}},open:function(n){i||(i=e(document.body));var r;if(n.isObj===!1){t.items=n.items.toArray(),t.index=0;var s,l=n.items;for(r=0;l.length>r;r++)if(s=l[r],s.parsed&&(s=s.el[0]),s===n.el[0]){t.index=r;break}}else t.items=e.isArray(n.items)?n.items:[n.items],t.index=n.index||0;if(t.isOpen)return t.updateItemHTML(),void 0;t.types=[],a="",t.ev=n.mainEl&&n.mainEl.length?n.mainEl.eq(0):o,n.key?(t.popupsCache[n.key]||(t.popupsCache[n.key]={}),t.currTemplate=t.popupsCache[n.key]):t.currTemplate={},t.st=e.extend(!0,{},e.magnificPopup.defaults,n),t.fixedContentPos="auto"===t.st.fixedContentPos?!t.probablyMobile:t.st.fixedContentPos,t.st.modal&&(t.st.closeOnContentClick=!1,t.st.closeOnBgClick=!1,t.st.showCloseBtn=!1,t.st.enableEscapeKey=!1),t.bgOverlay||(t.bgOverlay=k("bg").on("click"+h,function(){t.close()}),t.wrap=k("wrap").attr("tabindex",-1).on("click"+h,function(e){t._checkIfClose(e.target)&&t.close()}),t.container=k("container",t.wrap)),t.contentContainer=k("content"),t.st.preloader&&(t.preloader=k("preloader",t.container,t.st.tLoading));var c=e.magnificPopup.modules;for(r=0;c.length>r;r++){var d=c[r];d=d.charAt(0).toUpperCase()+d.slice(1),t["init"+d].call(t)}T("BeforeOpen"),t.st.showCloseBtn&&(t.st.closeBtnInside?(x(p,function(e,t,n,i){n.close_replaceWith=E(i.type)}),a+=" mfp-close-btn-in"):t.wrap.append(E())),t.st.alignTop&&(a+=" mfp-align-top"),t.fixedContentPos?t.wrap.css({overflow:t.st.overflowY,overflowX:"hidden",overflowY:t.st.overflowY}):t.wrap.css({top:I.scrollTop(),position:"absolute"}),(t.st.fixedBgPos===!1||"auto"===t.st.fixedBgPos&&!t.fixedContentPos)&&t.bgOverlay.css({height:o.height(),position:"absolute"}),t.st.enableEscapeKey&&o.on("keyup"+h,function(e){27===e.keyCode&&t.close()}),I.on("resize"+h,function(){t.updateSize()}),t.st.closeOnContentClick||(a+=" mfp-auto-cursor"),a&&t.wrap.addClass(a);var u=t.wH=I.height(),m={};if(t.fixedContentPos&&t._hasScrollBar(u)){var g=t._getScrollbarSize();g&&(m.marginRight=g)}t.fixedContentPos&&(t.isIE7?e("body, html").css("overflow","hidden"):m.overflow="hidden");var C=t.st.mainClass;return t.isIE7&&(C+=" mfp-ie7"),C&&t._addClassToMFP(C),t.updateItemHTML(),T("BuildControls"),e("html").css(m),t.bgOverlay.add(t.wrap).prependTo(t.st.prependTo||i),t._lastFocusedEl=document.activeElement,setTimeout(function(){t.content?(t._addClassToMFP(v),t._setFocus()):t.bgOverlay.addClass(v),o.on("focusin"+h,t._onFocusIn)},16),t.isOpen=!0,t.updateSize(u),T(f),n},close:function(){t.isOpen&&(T(c),t.isOpen=!1,t.st.removalDelay&&!t.isLowIE&&t.supportsTransition?(t._addClassToMFP(C),setTimeout(function(){t._close()},t.st.removalDelay)):t._close())},_close:function(){T(l);var n=C+" "+v+" ";if(t.bgOverlay.detach(),t.wrap.detach(),t.container.empty(),t.st.mainClass&&(n+=t.st.mainClass+" "),t._removeClassFromMFP(n),t.fixedContentPos){var i={marginRight:""};t.isIE7?e("body, html").css("overflow",""):i.overflow="",e("html").css(i)}o.off("keyup"+h+" focusin"+h),t.ev.off(h),t.wrap.attr("class","mfp-wrap").removeAttr("style"),t.bgOverlay.attr("class","mfp-bg"),t.container.attr("class","mfp-container"),!t.st.showCloseBtn||t.st.closeBtnInside&&t.currTemplate[t.currItem.type]!==!0||t.currTemplate.closeBtn&&t.currTemplate.closeBtn.detach(),t._lastFocusedEl&&e(t._lastFocusedEl).focus(),t.currItem=null,t.content=null,t.currTemplate=null,t.prevHeight=0,T(d)},updateSize:function(e){if(t.isIOS){var n=document.documentElement.clientWidth/window.innerWidth,i=window.innerHeight*n;t.wrap.css("height",i),t.wH=i}else t.wH=e||I.height();t.fixedContentPos||t.wrap.css("height",t.wH),T("Resize")},updateItemHTML:function(){var n=t.items[t.index];t.contentContainer.detach(),t.content&&t.content.detach(),n.parsed||(n=t.parseEl(t.index));var i=n.type;if(T("BeforeChange",[t.currItem?t.currItem.type:"",i]),t.currItem=n,!t.currTemplate[i]){var o=t.st[i]?t.st[i].markup:!1;T("FirstMarkupParse",o),t.currTemplate[i]=o?e(o):!0}r&&r!==n.type&&t.container.removeClass("mfp-"+r+"-holder");var a=t["get"+i.charAt(0).toUpperCase()+i.slice(1)](n,t.currTemplate[i]);t.appendContent(a,i),n.preloaded=!0,T(m,n),r=n.type,t.container.prepend(t.contentContainer),T("AfterChange")},appendContent:function(e,n){t.content=e,e?t.st.showCloseBtn&&t.st.closeBtnInside&&t.currTemplate[n]===!0?t.content.find(".mfp-close").length||t.content.append(E()):t.content=e:t.content="",T(u),t.container.addClass("mfp-"+n+"-holder"),t.contentContainer.append(t.content)},parseEl:function(n){var i,o=t.items[n];if(o.tagName?o={el:e(o)}:(i=o.type,o={data:o,src:o.src}),o.el){for(var r=t.types,a=0;r.length>a;a++)if(o.el.hasClass("mfp-"+r[a])){i=r[a];break}o.src=o.el.attr("data-mfp-src"),o.src||(o.src=o.el.attr("href"))}return o.type=i||t.st.type||"inline",o.index=n,o.parsed=!0,t.items[n]=o,T("ElementParse",o),t.items[n]},addGroup:function(e,n){var i=function(i){i.mfpEl=this,t._openClick(i,e,n)};n||(n={});var o="click.magnificPopup";n.mainEl=e,n.items?(n.isObj=!0,e.off(o).on(o,i)):(n.isObj=!1,n.delegate?e.off(o).on(o,n.delegate,i):(n.items=e,e.off(o).on(o,i)))},_openClick:function(n,i,o){var r=void 0!==o.midClick?o.midClick:e.magnificPopup.defaults.midClick;if(r||2!==n.which&&!n.ctrlKey&&!n.metaKey){var a=void 0!==o.disableOn?o.disableOn:e.magnificPopup.defaults.disableOn;if(a)if(e.isFunction(a)){if(!a.call(t))return!0}else if(a>I.width())return!0;n.type&&(n.preventDefault(),t.isOpen&&n.stopPropagation()),o.el=e(n.mfpEl),o.delegate&&(o.items=i.find(o.delegate)),t.open(o)}},updateStatus:function(e,i){if(t.preloader){n!==e&&t.container.removeClass("mfp-s-"+n),i||"loading"!==e||(i=t.st.tLoading);var o={status:e,text:i};T("UpdateStatus",o),e=o.status,i=o.text,t.preloader.html(i),t.preloader.find("a").on("click",function(e){e.stopImmediatePropagation()}),t.container.addClass("mfp-s-"+e),n=e}},_checkIfClose:function(n){if(!e(n).hasClass(y)){var i=t.st.closeOnContentClick,o=t.st.closeOnBgClick;if(i&&o)return!0;if(!t.content||e(n).hasClass("mfp-close")||t.preloader&&n===t.preloader[0])return!0;if(n===t.content[0]||e.contains(t.content[0],n)){if(i)return!0}else if(o&&e.contains(document,n))return!0;return!1}},_addClassToMFP:function(e){t.bgOverlay.addClass(e),t.wrap.addClass(e)},_removeClassFromMFP:function(e){this.bgOverlay.removeClass(e),t.wrap.removeClass(e)},_hasScrollBar:function(e){return(t.isIE7?o.height():document.body.scrollHeight)>(e||I.height())},_setFocus:function(){(t.st.focus?t.content.find(t.st.focus).eq(0):t.wrap).focus()},_onFocusIn:function(n){return n.target===t.wrap[0]||e.contains(t.wrap[0],n.target)?void 0:(t._setFocus(),!1)},_parseMarkup:function(t,n,i){var o;i.data&&(n=e.extend(i.data,n)),T(p,[t,n,i]),e.each(n,function(e,n){if(void 0===n||n===!1)return!0;if(o=e.split("_"),o.length>1){var i=t.find(h+"-"+o[0]);if(i.length>0){var r=o[1];"replaceWith"===r?i[0]!==n[0]&&i.replaceWith(n):"img"===r?i.is("img")?i.attr("src",n):i.replaceWith('<img src="'+n+'" class="'+i.attr("class")+'" />'):i.attr(o[1],n)}}else t.find(h+"-"+e).html(n)})},_getScrollbarSize:function(){if(void 0===t.scrollbarSize){var e=document.createElement("div");e.style.cssText="width: 99px; height: 99px; overflow: scroll; position: absolute; top: -9999px;",document.body.appendChild(e),t.scrollbarSize=e.offsetWidth-e.clientWidth,document.body.removeChild(e)}return t.scrollbarSize}},e.magnificPopup={instance:null,proto:w.prototype,modules:[],open:function(t,n){return _(),t=t?e.extend(!0,{},t):{},t.isObj=!0,t.index=n||0,this.instance.open(t)},close:function(){return e.magnificPopup.instance&&e.magnificPopup.instance.close()},registerModule:function(t,n){n.options&&(e.magnificPopup.defaults[t]=n.options),e.extend(this.proto,n.proto),this.modules.push(t)},defaults:{disableOn:0,key:null,midClick:!1,mainClass:"",preloader:!0,focus:"",closeOnContentClick:!1,closeOnBgClick:!0,closeBtnInside:!0,showCloseBtn:!0,enableEscapeKey:!0,modal:!1,alignTop:!1,removalDelay:0,prependTo:null,fixedContentPos:"auto",fixedBgPos:"auto",overflowY:"auto",closeMarkup:'<button title="%title%" type="button" class="mfp-close">&times;</button>',tClose:"Close (Esc)",tLoading:"Loading..."}},e.fn.magnificPopup=function(n){_();var i=e(this);if("string"==typeof n)if("open"===n){var o,r=b?i.data("magnificPopup"):i[0].magnificPopup,a=parseInt(arguments[1],10)||0;r.items?o=r.items[a]:(o=i,r.delegate&&(o=o.find(r.delegate)),o=o.eq(a)),t._openClick({mfpEl:o},i,r)}else t.isOpen&&t[n].apply(t,Array.prototype.slice.call(arguments,1));else n=e.extend(!0,{},n),b?i.data("magnificPopup",n):i[0].magnificPopup=n,t.addGroup(i,n);return i};var P,O,z,M="inline",B=function(){z&&(O.after(z.addClass(P)).detach(),z=null)};e.magnificPopup.registerModule(M,{options:{hiddenClass:"hide",markup:"",tNotFound:"Content not found"},proto:{initInline:function(){t.types.push(M),x(l+"."+M,function(){B()})},getInline:function(n,i){if(B(),n.src){var o=t.st.inline,r=e(n.src);if(r.length){var a=r[0].parentNode;a&&a.tagName&&(O||(P=o.hiddenClass,O=k(P),P="mfp-"+P),z=r.after(O).detach().removeClass(P)),t.updateStatus("ready")}else t.updateStatus("error",o.tNotFound),r=e("<div>");return n.inlineElement=r,r}return t.updateStatus("ready"),t._parseMarkup(i,{},n),i}}});var F,H="ajax",L=function(){F&&i.removeClass(F)},A=function(){L(),t.req&&t.req.abort()};e.magnificPopup.registerModule(H,{options:{settings:null,cursor:"mfp-ajax-cur",tError:'<a href="%url%">The content</a> could not be loaded.'},proto:{initAjax:function(){t.types.push(H),F=t.st.ajax.cursor,x(l+"."+H,A),x("BeforeChange."+H,A)},getAjax:function(n){F&&i.addClass(F),t.updateStatus("loading");var o=e.extend({url:n.src,success:function(i,o,r){var a={data:i,xhr:r};T("ParseAjax",a),t.appendContent(e(a.data),H),n.finished=!0,L(),t._setFocus(),setTimeout(function(){t.wrap.addClass(v)},16),t.updateStatus("ready"),T("AjaxContentAdded")},error:function(){L(),n.finished=n.loadError=!0,t.updateStatus("error",t.st.ajax.tError.replace("%url%",n.src))}},t.st.ajax.settings);return t.req=e.ajax(o),""}}});var j,N=function(n){if(n.data&&void 0!==n.data.title)return n.data.title;var i=t.st.image.titleSrc;if(i){if(e.isFunction(i))return i.call(t,n);if(n.el)return n.el.attr(i)||""}return""};e.magnificPopup.registerModule("image",{options:{markup:'<div class="mfp-figure"><div class="mfp-close"></div><figure><div class="mfp-img"></div><figcaption><div class="mfp-bottom-bar"><div class="mfp-title"></div><div class="mfp-counter"></div></div></figcaption></figure></div>',cursor:"mfp-zoom-out-cur",titleSrc:"title",verticalFit:!0,tError:'<a href="%url%">The image</a> could not be loaded.'},proto:{initImage:function(){var e=t.st.image,n=".image";t.types.push("image"),x(f+n,function(){"image"===t.currItem.type&&e.cursor&&i.addClass(e.cursor)}),x(l+n,function(){e.cursor&&i.removeClass(e.cursor),I.off("resize"+h)}),x("Resize"+n,t.resizeImage),t.isLowIE&&x("AfterChange",t.resizeImage)},resizeImage:function(){var e=t.currItem;if(e&&e.img&&t.st.image.verticalFit){var n=0;t.isLowIE&&(n=parseInt(e.img.css("padding-top"),10)+parseInt(e.img.css("padding-bottom"),10)),e.img.css("max-height",t.wH-n)}},_onImageHasSize:function(e){e.img&&(e.hasSize=!0,j&&clearInterval(j),e.isCheckingImgSize=!1,T("ImageHasSize",e),e.imgHidden&&(t.content&&t.content.removeClass("mfp-loading"),e.imgHidden=!1))},findImageSize:function(e){var n=0,i=e.img[0],o=function(r){j&&clearInterval(j),j=setInterval(function(){return i.naturalWidth>0?(t._onImageHasSize(e),void 0):(n>200&&clearInterval(j),n++,3===n?o(10):40===n?o(50):100===n&&o(500),void 0)},r)};o(1)},getImage:function(n,i){var o=0,r=function(){n&&(n.img[0].complete?(n.img.off(".mfploader"),n===t.currItem&&(t._onImageHasSize(n),t.updateStatus("ready")),n.hasSize=!0,n.loaded=!0,T("ImageLoadComplete")):(o++,200>o?setTimeout(r,100):a()))},a=function(){n&&(n.img.off(".mfploader"),n===t.currItem&&(t._onImageHasSize(n),t.updateStatus("error",s.tError.replace("%url%",n.src))),n.hasSize=!0,n.loaded=!0,n.loadError=!0)},s=t.st.image,l=i.find(".mfp-img");if(l.length){var c=document.createElement("img");c.className="mfp-img",n.img=e(c).on("load.mfploader",r).on("error.mfploader",a),c.src=n.src,l.is("img")&&(n.img=n.img.clone()),c=n.img[0],c.naturalWidth>0?n.hasSize=!0:c.width||(n.hasSize=!1)}return t._parseMarkup(i,{title:N(n),img_replaceWith:n.img},n),t.resizeImage(),n.hasSize?(j&&clearInterval(j),n.loadError?(i.addClass("mfp-loading"),t.updateStatus("error",s.tError.replace("%url%",n.src))):(i.removeClass("mfp-loading"),t.updateStatus("ready")),i):(t.updateStatus("loading"),n.loading=!0,n.hasSize||(n.imgHidden=!0,i.addClass("mfp-loading"),t.findImageSize(n)),i)}}});var W,R=function(){return void 0===W&&(W=void 0!==document.createElement("p").style.MozTransform),W};e.magnificPopup.registerModule("zoom",{options:{enabled:!1,easing:"ease-in-out",duration:300,opener:function(e){return e.is("img")?e:e.find("img")}},proto:{initZoom:function(){var e,n=t.st.zoom,i=".zoom";if(n.enabled&&t.supportsTransition){var o,r,a=n.duration,s=function(e){var t=e.clone().removeAttr("style").removeAttr("class").addClass("mfp-animated-image"),i="all "+n.duration/1e3+"s "+n.easing,o={position:"fixed",zIndex:9999,left:0,top:0,"-webkit-backface-visibility":"hidden"},r="transition";return o["-webkit-"+r]=o["-moz-"+r]=o["-o-"+r]=o[r]=i,t.css(o),t},d=function(){t.content.css("visibility","visible")};x("BuildControls"+i,function(){if(t._allowZoom()){if(clearTimeout(o),t.content.css("visibility","hidden"),e=t._getItemToZoom(),!e)return d(),void 0;r=s(e),r.css(t._getOffset()),t.wrap.append(r),o=setTimeout(function(){r.css(t._getOffset(!0)),o=setTimeout(function(){d(),setTimeout(function(){r.remove(),e=r=null,T("ZoomAnimationEnded")},16)},a)},16)}}),x(c+i,function(){if(t._allowZoom()){if(clearTimeout(o),t.st.removalDelay=a,!e){if(e=t._getItemToZoom(),!e)return;r=s(e)}r.css(t._getOffset(!0)),t.wrap.append(r),t.content.css("visibility","hidden"),setTimeout(function(){r.css(t._getOffset())},16)}}),x(l+i,function(){t._allowZoom()&&(d(),r&&r.remove(),e=null)})}},_allowZoom:function(){return"image"===t.currItem.type},_getItemToZoom:function(){return t.currItem.hasSize?t.currItem.img:!1},_getOffset:function(n){var i;i=n?t.currItem.img:t.st.zoom.opener(t.currItem.el||t.currItem);var o=i.offset(),r=parseInt(i.css("padding-top"),10),a=parseInt(i.css("padding-bottom"),10);o.top-=e(window).scrollTop()-r;var s={width:i.width(),height:(b?i.innerHeight():i[0].offsetHeight)-a-r};return R()?s["-moz-transform"]=s.transform="translate("+o.left+"px,"+o.top+"px)":(s.left=o.left,s.top=o.top),s}}});var Z="iframe",q="//about:blank",D=function(e){if(t.currTemplate[Z]){var n=t.currTemplate[Z].find("iframe");n.length&&(e||(n[0].src=q),t.isIE8&&n.css("display",e?"block":"none"))}};e.magnificPopup.registerModule(Z,{options:{markup:'<div class="mfp-iframe-scaler"><div class="mfp-close"></div><iframe class="mfp-iframe" src="//about:blank" frameborder="0" allowfullscreen></iframe></div>',srcAction:"iframe_src",patterns:{youtube:{index:"youtube.com",id:"v=",src:"//www.youtube.com/embed/%id%?autoplay=1"},vimeo:{index:"vimeo.com/",id:"/",src:"//player.vimeo.com/video/%id%?autoplay=1"},gmaps:{index:"//maps.google.",src:"%id%&output=embed"}}},proto:{initIframe:function(){t.types.push(Z),x("BeforeChange",function(e,t,n){t!==n&&(t===Z?D():n===Z&&D(!0))}),x(l+"."+Z,function(){D()})},getIframe:function(n,i){var o=n.src,r=t.st.iframe;e.each(r.patterns,function(){return o.indexOf(this.index)>-1?(this.id&&(o="string"==typeof this.id?o.substr(o.lastIndexOf(this.id)+this.id.length,o.length):this.id.call(this,o)),o=this.src.replace("%id%",o),!1):void 0});var a={};return r.srcAction&&(a[r.srcAction]=o),t._parseMarkup(i,a,n),t.updateStatus("ready"),i}}});var K=function(e){var n=t.items.length;return e>n-1?e-n:0>e?n+e:e},Y=function(e,t,n){return e.replace(/%curr%/gi,t+1).replace(/%total%/gi,n)};e.magnificPopup.registerModule("gallery",{options:{enabled:!1,arrowMarkup:'<button title="%title%" type="button" class="mfp-arrow mfp-arrow-%dir%"></button>',preload:[0,2],navigateByImgClick:!0,arrows:!0,tPrev:"Previous (Left arrow key)",tNext:"Next (Right arrow key)",tCounter:"%curr% of %total%"},proto:{initGallery:function(){var n=t.st.gallery,i=".mfp-gallery",r=Boolean(e.fn.mfpFastClick);return t.direction=!0,n&&n.enabled?(a+=" mfp-gallery",x(f+i,function(){n.navigateByImgClick&&t.wrap.on("click"+i,".mfp-img",function(){return t.items.length>1?(t.next(),!1):void 0}),o.on("keydown"+i,function(e){37===e.keyCode?t.prev():39===e.keyCode&&t.next()})}),x("UpdateStatus"+i,function(e,n){n.text&&(n.text=Y(n.text,t.currItem.index,t.items.length))}),x(p+i,function(e,i,o,r){var a=t.items.length;o.counter=a>1?Y(n.tCounter,r.index,a):""}),x("BuildControls"+i,function(){if(t.items.length>1&&n.arrows&&!t.arrowLeft){var i=n.arrowMarkup,o=t.arrowLeft=e(i.replace(/%title%/gi,n.tPrev).replace(/%dir%/gi,"left")).addClass(y),a=t.arrowRight=e(i.replace(/%title%/gi,n.tNext).replace(/%dir%/gi,"right")).addClass(y),s=r?"mfpFastClick":"click";o[s](function(){t.prev()}),a[s](function(){t.next()}),t.isIE7&&(k("b",o[0],!1,!0),k("a",o[0],!1,!0),k("b",a[0],!1,!0),k("a",a[0],!1,!0)),t.container.append(o.add(a))}}),x(m+i,function(){t._preloadTimeout&&clearTimeout(t._preloadTimeout),t._preloadTimeout=setTimeout(function(){t.preloadNearbyImages(),t._preloadTimeout=null},16)}),x(l+i,function(){o.off(i),t.wrap.off("click"+i),t.arrowLeft&&r&&t.arrowLeft.add(t.arrowRight).destroyMfpFastClick(),t.arrowRight=t.arrowLeft=null}),void 0):!1},next:function(){t.direction=!0,t.index=K(t.index+1),t.updateItemHTML()},prev:function(){t.direction=!1,t.index=K(t.index-1),t.updateItemHTML()},goTo:function(e){t.direction=e>=t.index,t.index=e,t.updateItemHTML()},preloadNearbyImages:function(){var e,n=t.st.gallery.preload,i=Math.min(n[0],t.items.length),o=Math.min(n[1],t.items.length);for(e=1;(t.direction?o:i)>=e;e++)t._preloadItem(t.index+e);for(e=1;(t.direction?i:o)>=e;e++)t._preloadItem(t.index-e)},_preloadItem:function(n){if(n=K(n),!t.items[n].preloaded){var i=t.items[n];i.parsed||(i=t.parseEl(n)),T("LazyLoad",i),"image"===i.type&&(i.img=e('<img class="mfp-img" />').on("load.mfploader",function(){i.hasSize=!0}).on("error.mfploader",function(){i.hasSize=!0,i.loadError=!0,T("LazyLoadError",i)}).attr("src",i.src)),i.preloaded=!0}}}});var U="retina";e.magnificPopup.registerModule(U,{options:{replaceSrc:function(e){return e.src.replace(/\.\w+$/,function(e){return"@2x"+e})},ratio:1},proto:{initRetina:function(){if(window.devicePixelRatio>1){var e=t.st.retina,n=e.ratio;n=isNaN(n)?n():n,n>1&&(x("ImageHasSize."+U,function(e,t){t.img.css({"max-width":t.img[0].naturalWidth/n,width:"100%"})}),x("ElementParse."+U,function(t,i){i.src=e.replaceSrc(i,n)}))}}}}),function(){var t=1e3,n="ontouchstart"in window,i=function(){I.off("touchmove"+r+" touchend"+r)},o="mfpFastClick",r="."+o;e.fn.mfpFastClick=function(o){return e(this).each(function(){var a,s=e(this);if(n){var l,c,d,u,p,f;s.on("touchstart"+r,function(e){u=!1,f=1,p=e.originalEvent?e.originalEvent.touches[0]:e.touches[0],c=p.clientX,d=p.clientY,I.on("touchmove"+r,function(e){p=e.originalEvent?e.originalEvent.touches:e.touches,f=p.length,p=p[0],(Math.abs(p.clientX-c)>10||Math.abs(p.clientY-d)>10)&&(u=!0,i())}).on("touchend"+r,function(e){i(),u||f>1||(a=!0,e.preventDefault(),clearTimeout(l),l=setTimeout(function(){a=!1},t),o())})})}s.on("click"+r,function(){a||o()})})},e.fn.destroyMfpFastClick=function(){e(this).off("touchstart"+r+" click"+r),n&&I.off("touchmove"+r+" touchend"+r)}}(),_()})(window.jQuery||window.Zepto);