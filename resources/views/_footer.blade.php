<!-- Footer start -->
		    <footer id="footWrapper">
		    	
		    	<!-- footer top bar start -->
		    	<div class="footer-top">
		    		<div class="container">
						<p class="lg-txt f-left">
							We Always care of you! Earn Money As A proud Publisher of <span class="main-color heavy">ADGODMEDIA</span> CPA Network
						</p>
						<a class="f-right btn uppercase main-gradient skew-btn" href="http://www.adgodmedia.com/auth/register"><span>Sign Up Now</span></a>
		    		</div>
		    	</div>
		    	<!-- footer top bar start -->


			    <!-- footer bottom bar start -->
			    <div class="footer-bottom">
				    <div class="container">
			    		<div class="row">
				    		<!-- footer copyrights left cell -->
				    		<div class="copyrights col-md-5">© Copyrights <b class="main-color">adgodmedia.com</b> 2017. All rights reserved.</div>
				    						    		
				    		<!-- footer bottom menu start -->
						    <div class="col-md-7 last">
						    	<ul class="footer-menu f-right">
								    <li><a href="http://www.adgodmedia.com">Home</a></li>
								    <li><a href="http://www.adgodmedia.com/terms-and-conditions">Terms & Conditions</a></li>
								    <li><a href="http://www.adgodmedia.com/blog">Blog</a></li>
								    <li><a href="http://www.adgodmedia.com/auth/register">Signup</a></li>
								    <li><a href="http://www.adgodmedia.com/auth/login">Login</a></li>
								    <li><a href="http://www.adgodmedia.com/contactus">Contact</a></li>
							    </ul>
						    </div>
						    <!-- footer bottom menu end -->
						    
			    		</div>
				    </div>
			    </div>
			    <!-- footer bottom bar end -->

			</footer>
			<!-- Footer end -->