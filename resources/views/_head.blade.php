
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
		<meta name="description" content="Adgodmedia">
		
		
		<!-- Put favicon.ico and apple-touch-icon(s).png in the images folder -->
	    <link rel="shortcut icon" href="{{ asset('site/images/favicon1.ico') }}" >
		    	
		<link href='https://fonts.googleapis.com/css?family=Oswald:400,100,300,500,700%7CLato:400,300,700,900&subset=latin,latin-ext' rel='stylesheet' type='text/css'>
		
		<!-- Plugins CSS files -->
		<link rel="stylesheet" href="{{ asset('site/css/assets.css')}}">

		<!-- REVOLUTION SLIDER STYLES -->
		<link rel="stylesheet" href="{{ asset('site/css/pe-icon-7-stroke.css') }}" type="text/css">
		<link rel="stylesheet" href="{{ asset('site/css/settings.css') }}" type="text/css">
		<link rel="stylesheet" href="{{ asset('site/css/navigation.css') }}" type="text/css">
		
		<!-- Template CSS files -->
		<link rel="stylesheet" href="{{ asset('site/css/style.css') }}">
		<link rel="stylesheet" href="{{ asset('site/css/shortcodes.css') }}">
		<link id="theme_css" rel="stylesheet" href="{{ asset('site/css/light.css') }}">
		<link id="skin_css" rel="stylesheet" href="{{ asset('site/css/skins/default.css') }}">