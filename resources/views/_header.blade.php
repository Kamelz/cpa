<!-- site preloader start -->
		<div class="page-loader"></div>
		<!-- site preloader end -->
		
		<div class="pageWrapper">
			
			<!-- Header start -->
			<div class="top-bar main-bg no-border">
				<div class="container">
					
					<ul class="top-info f-left">
					    <li><i class="fa fa-envelope white"></i><b class="white">Email:</b> <a href="#">support@adgodmedia.com</a></li>
					    <li><i class="fa fa-phone white"></i><b class="white">Phone:</b> +1 (888) 000-0000</li>
					    <li class="dropdown language-selector">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown" data-close-others="true"><i class="fa fa-flag white"></i>EN</a>
							<ul class="dropdown-menu">
								<li>
									<a href="#">AR</a>
								</li>
								<li class="active">
									<a href="#">EN</a>
								</li>
								<li>
									<a href="#">FR</a>
								</li>
								<li>
									<a href="#">RU</a>
								</li>
							</ul>
						</li>
				    </ul>
					
				    <div class="f-right social-list">
					    <span class="lbl-txt">Follow Us On:</span>
					    <a href="#" data-toggle="tooltip" data-placement="bottom" data-original-title="Facebook"><i class="fa fa-facebook ic-facebook no-border"></i></a>
						<a href="#" data-toggle="tooltip" data-placement="bottom" data-original-title="Twitter"><i class="fa fa-twitter ic-twitter no-border"></i></a>
						<a href="#" data-toggle="tooltip" data-placement="bottom" data-original-title="Linkedin"><i class="fa fa-linkedin ic-linkedin no-border"></i></a>
						<a href="#" data-toggle="tooltip" data-placement="bottom" data-original-title="Dribbble"><i class="fa fa-dribbble ic-dribbble no-border"></i></a>
						<a href="#" data-toggle="tooltip" data-placement="bottom" data-original-title="Google Plus"><i class="fa fa-google-plus ic-gplus no-border"></i></a>
				    </div>

				</div>
			</div>
			<header class="top-head header-1 skew">
		    	<div class="container">
			    	<!-- Logo start -->
			    	<div class="logo">
				    	<a href="index.html"><img alt="" src="{{ asset('site/images/logo1.png') }}" /></a>
				    </div>
				    <!-- Logo end -->
				    
					<div class="responsive-nav">
						<!-- top navigation menu start -->
						<nav class="top-nav">
							<ul>
								<li class="selected"><a href="http://adgodmedia.com/"><span>Home</span></a>
										
								</li>
								<li><a href="http://adgodmedia.com/publishers"><span>Publisher</span></a>
								</li>
																<li><a href="http://adgodmedia.com/advertisers"><span>Advertiser</span></a>
								</li>
																<li><a href="http://adgodmedia.com/about"><span>About Us</span></a>
								</li>
																<li><a href="http://www.adgodmedia.com/contactus"><span>Contact Us</span></a>
								</li>
																<li><a href="http://www.adgodmedia.com/auth/register"><span>Sign Up</span></a>
								</li>								<li><a href="http://www.adgodmedia.com/auth/login"><span>Sign In</span></a>
								</li>
								
							</ul>
						</nav>
					
					  
					</div>
		    	</div>
		    </header>
		    <!-- Header start -->