<!DOCTYPE html>
<html lang="en">
<head>
 <title>AdGodMedia - About</title>
  @include('_head')
  @include('ganalytics')

</head>
<body>

@include('_header')

<div class="md-padding p-b-0 gry-bg">
<div class="container">
<div class="heading main centered">
<h3 class="uppercase lg-title"> <span class="main-color">About Us</span></h3>

<br>
</div>

<p>AdGodMedia is an innovative CPA network providing advanced high performance publisher tools to enable users earn most cash online with their contents like files, links, web urls etc. We brought a wide array of advanced sophisticated publisher tools including a File / Link Locker, Notes Locker, Content Locker as well as an advanced reporting system
</p>
</div>
</div>

<div class="md-padding">
										
					<div class="container">
						
						<div class="heading main centered">
							<h3 class="uppercase lg-title">Our <span class="main-color"> Team</span></h3>
							
						</div>

						<div class="row">
							<div class="col-md-3">
								<div class="team-box box-3 main-bg">
									<div class="team-over">
										<h4 class="team-name">Devid Dee</h4>
										<h5 class="uppercase">Director</h5>
										<div class="top-team">
											<div class="front face"></div>										
											<div class="back face main-bg">
												<p>All About Director</p>
												<div class="social-list tbl">
													<a href="#" data-toggle="tooltip" data-placement="top" data-original-title="Facebook"><i class="fa fa-facebook ic-facebook sm-icon"></i></a>
													<a href="#" data-toggle="tooltip" data-placement="top" data-original-title="Twitter"><i class="fa fa-twitter ic-twitter sm-icon"></i></a>
													<a href="#" data-toggle="tooltip" data-placement="top" data-original-title="Linkedin"><i class="fa fa-linkedin ic-linkedin sm-icon"></i></a>
													<a href="#" data-toggle="tooltip" data-placement="top" data-original-title="Dribbble"><i class="fa fa-dribbble ic-dribbble sm-icon"></i></a>
												</div>
											</div>
										</div>
										<div class="bottom-team"></div>
									</div>
									<img alt="" src="{{ asset('site/images/1.jpg') }}" class="tm-img" />
								</div>
							</div>
							<div class="col-md-3">
								<div class="team-box box-3 main-bg">
									<div class="team-over">
										<h4 class="team-name">Tom Hook</h4>
										<h5 class="uppercase">Affiliate Manager</h5>
										<div class="top-team">
											<div class="front face"></div>										
											<div class="back face main-bg">
												<p>All About Affiliate Manager.</p>
												<div class="social-list tbl">
													<a href="#" data-toggle="tooltip" data-placement="top" data-original-title="Facebook"><i class="fa fa-facebook ic-facebook sm-icon"></i></a>
													<a href="#" data-toggle="tooltip" data-placement="top" data-original-title="Twitter"><i class="fa fa-twitter ic-twitter sm-icon"></i></a>
													<a href="#" data-toggle="tooltip" data-placement="top" data-original-title="Linkedin"><i class="fa fa-linkedin ic-linkedin sm-icon"></i></a>
													<a href="#" data-toggle="tooltip" data-placement="top" data-original-title="Dribbble"><i class="fa fa-dribbble ic-dribbble sm-icon"></i></a>
												</div>
											</div>
										</div>
										<div class="bottom-team"></div>
									</div>
									<img alt="" src="{{ asset('site/images/3.jpg') }}" class="tm-img" />
								</div>
							</div>
							<div class="col-md-3">
								<div class="team-box box-3 main-bg">
									<div class="team-over">
										<h4 class="team-name">Elina Vee</h4>
										<h5 class="uppercase">Affiliate Manager</h5>
										<div class="top-team">
											<div class="front face"></div>										
											<div class="back face main-bg">
												<p>All About Affiliate manager.</p>
												<div class="social-list tbl">
													<a href="#" data-toggle="tooltip" data-placement="top" data-original-title="Facebook"><i class="fa fa-facebook ic-facebook sm-icon"></i></a>
													<a href="#" data-toggle="tooltip" data-placement="top" data-original-title="Twitter"><i class="fa fa-twitter ic-twitter sm-icon"></i></a>
													<a href="#" data-toggle="tooltip" data-placement="top" data-original-title="Linkedin"><i class="fa fa-linkedin ic-linkedin sm-icon"></i></a>
													<a href="#" data-toggle="tooltip" data-placement="top" data-original-title="Dribbble"><i class="fa fa-dribbble ic-dribbble sm-icon"></i></a>
												</div>
											</div>
										</div>
										<div class="bottom-team"></div>
									</div>
									<img alt="" src="{{ asset('site/images/2.jpg') }}" class="tm-img" />
								</div>
							</div>
							<div class="col-md-3">
								<div class="team-box box-3 main-bg">
									<div class="team-over">
										<h4 class="team-name">Clark Dow</h4>
										<h5 class="uppercase">Designer</h5>
										<div class="top-team">
											<div class="front face"></div>										
											<div class="back face main-bg">
												<p>All About Designer.</p>
												<div class="social-list tbl">
													<a href="#" data-toggle="tooltip" data-placement="top" data-original-title="Facebook"><i class="fa fa-facebook ic-facebook sm-icon"></i></a>
													<a href="#" data-toggle="tooltip" data-placement="top" data-original-title="Twitter"><i class="fa fa-twitter ic-twitter sm-icon"></i></a>
													<a href="#" data-toggle="tooltip" data-placement="top" data-original-title="Linkedin"><i class="fa fa-linkedin ic-linkedin sm-icon"></i></a>
													<a href="#" data-toggle="tooltip" data-placement="top" data-original-title="Dribbble"><i class="fa fa-dribbble ic-dribbble sm-icon"></i></a>
												</div>
											</div>
										</div>
										<div class="bottom-team"></div>
									</div>
									<img alt="" src="{{ asset('site/images/4.jpg') }}" class="tm-img" />
								</div>
							</div>
						</div>

					</div>
				</div>
<br><br><br>


 @include('_footer')

 @include('_footer_js')
	
</body>
</html>