<!DOCTYPE html>
<html ng-app="ngDashboard" ng-controller="PageController">

<head>
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
     <title>AdGodMedia - Network Manager</title>
    <meta name="keywords" content="{{ $gset->keywords }}" />
    <meta name="description" content="{{ $gset->description }}">

    <meta name="csrf-token" content="{{ csrf_token() }}" />

    <meta name="author" content="AdminDesigns">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- Font CSS (Via CDN) -->
    <link rel='stylesheet' type='text/css' href='//fonts.googleapis.com/css?family=Open+Sans:400,600,700'>
    <link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Raleway:300,400,500,700">

    <!-- Theme CSS -->
    <link rel="stylesheet" type="text/css" href="{{ asset('panel/assets/skin/default_skin/css/theme.css') }}">

    <!-- Custom CSS -->
    <link rel="stylesheet" type="text/css" href="{{ asset('panel/assets/skin/default_skin/css/custom.css') }}">

    <!-- Admin Panels CSS -->
    <link rel="stylesheet" type="text/css" href="{{ asset('panel/assets/admin-tools/admin-plugins/admin-panels/adminpanels.css') }}">

    <link rel="stylesheet" href="{{ asset('panel/css/igrowl.min.css') }}">

    <link href="{{ asset('panel/vendor/editors/xeditable/css/bootstrap-editable.css') }}" rel="stylesheet" type="text/css">

    <!-- Favicon -->
    <link rel="shortcut icon" href="{{ asset('favicon.ico') }}">

    <script type="text/javascript" src="{{ asset('panel/vendor/jquery/jquery-1.11.1.min.js') }}"></script>
    <script src="//ajax.googleapis.com/ajax/libs/angularjs/1.2.26/angular.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.3.14/angular-sanitize.min.js"></script>


    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->

@include('ganalytics') </head>

<body class="dashboard-page sb-l-o sb-r-c">

    <!-- Start: Main -->
    <div id="main">

    @include('admin.aheader')

    @include('admin.asidebar')



        <!-- Start: Content-Wrapper -->
        <section id="content_wrapper">

            <!-- Start: Topbar -->
            <header id="topbar">
                <div class="topbar-left">
                    <ol class="breadcrumb">
                        <li class="crumb-active">
                            <a href="{{ url('admin/fetchoffers') }}" id="status">Network Manager</a>
                        </li>
                    </ol>
                </div>
            </header>
            <!-- End: Topbar -->

            <!-- Begin: Content -->
            <section id="content" class="animated fadeIn">

            <div class="alert alert-primary alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                Only offers from active networks are shown in lockers. Please activate atleast one network which have offers. <br>You can also <a href="{{ url('admin/networkenable') }}" style="color:#fafafa;text-decoration:underline !important;">enable additional networks for individual publishers.</a>
              </div>

		<div class="panel">
      <div class="panel-heading"><div class="panel-title">Networks</div></div>
      <div class="panel-body pn">
      <input type="hidden" name="_token" value="{{ csrf_token() }}">
          <table class="table table-bordered">
              <thead>
              <tr>
                  <th>Network</th>
                  <th>Active</th>
                  <th>No. of Offers</th>
                  <th>Last Fetched</th>
                  <th>Min. EPC</th>
                  <th>Actions</th>
                  </tr>
              </thead>
              <tbody>
                  @forelse ($networks as $network)
                  <tr>
                  <td><b>{{ $network->name }}</b></td>
                  <td>
                        <div class="checkbox-custom fill checkbox-success">
                        <input type="checkbox" @if($network->active==1) checked="" @endif id="ntoggle{{ $network->id }}" data-pk="{{ $network->id }}">
                        <label for="ntoggle{{ $network->id }}">&nbsp;</label>
                        </div>
                  </td>
                  <td>{{$network->numOffers}}</td>
                  <td>{{ date('j F Y',strtotime($network->updated_at)) }}</td>
                  <td>$<a href="#" id="minepc" data-type="text" data-pk="{{ $network->id }}" data-title="Enter amount">{{ $network->minepc }}</a></td>
                  <td><a href="" class="btn btn-xs btn-success mr5" data-network="{{ $network->name }}" id="fetchOff">Fetch Offers</a> <a href="{{ url('admin/confirmdelete/offers/'.$network->name) }}" data-toggle="modal" class="btn btn-xs btn-danger">Delete Offers</a></td>
                  </tr>
                  @empty
                        <td colspan="4">No networks found!</td>
                  @endforelse
              </tbody>
          </table>
      </div>      
        </div>


            </section>
            <!-- End: Content -->

        </section>

    </div>
    <!-- End: Main -->

    <!-- BEGIN: PAGE SCRIPTS -->

    <!-- jQuery -->
    <script type="text/javascript" src="{{ asset('panel/vendor/jquery/jquery_ui/jquery-ui.min.js') }}"></script>

    <!-- Bootstrap -->
    <script type="text/javascript" src="{{ asset('panel/assets/js/bootstrap/bootstrap.min.js') }}"></script>


    <!-- Theme Javascript -->
    <script type="text/javascript" src="{{ asset('panel/assets/js/utility/utility.js') }}"></script>
    <script type="text/javascript" src="{{ asset('panel/assets/js/main.js') }}"></script>

    <!-- Admin Panels  -->
    <script type="text/javascript" src="{{ asset('panel/assets/admin-tools/admin-plugins/admin-panels/json2.js') }}"></script>
    <script type="text/javascript" src="{{ asset('panel/assets/admin-tools/admin-plugins/admin-panels/jquery.ui.touch-punch.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('panel/assets/admin-tools/admin-plugins/admin-panels/adminpanels.js') }}"></script>

    <script type="text/javascript" src="{{ asset('panel/assets/js/igrowl.min.js') }}"></script>

    <script src="{{ asset('panel/vendor/editors/xeditable/js/bootstrap-editable.min.js') }}"></script>

    <script>

soundManager.setup({
  url: '{{ asset('panel/assets/swf') }}/',
  onready: function() {
    lead_snd = soundManager.createSound({
        url: '{{ asset('panel/assets/audio/chaching.mp3') }}'
      });
  }
});

var app = angular.module('ngDashboard', ['ngSanitize']);
app.controller('PageController', function($scope,$http,$interval) {
  
  $scope.earnings = '${{ formatnum($today->payout,2) }}';
  $scope.clicks = '{{ formatnum($today->clicks) }}';
  $scope.leads = '{{ formatnum($today->leads) }}';
  $scope.epc = '${{ formatnum(division($today->payout,$today->clicks),2) }}';

var serverdate = new Date({{ date("Y, m, d, H, i, s") }});
function displaytime(){
    serverdate.setSeconds(serverdate.getSeconds() + 1);
    var hh=serverdate.getHours();
    var m=serverdate.getMinutes();
    var s=serverdate.getSeconds();
    
    var dd = "AM";
    var h = hh;
    if (h >= 12) {
        h = hh-12;
        dd = "PM";
    }
    if (h == 0) {
        h = 12;
    }
    m=checkTime(m);
    s=checkTime(s);
    var output = h+":"+m+" "+dd;
    $scope.serverTime = output;
}

// Add leading zero in time
function checkTime(i){if (i<10){i="0" + i;}return i;}

    var getEarn = function() {
        $http.get( "{{ url('admin/earnings') }}").success(function(data, status, headers, config) {
            if ($scope.leads < data.leads) { lead_snd.play(); }
            if (data.clicks == undefined) { $interval.cancel(makeitrain); return; }
            $scope.clicks = data.clicks;
            $scope.leads = data.leads;
            $scope.earnings = data.earnings;
            $scope.epc = data.epc;
        });
    };

    $interval(displaytime, 1000);
    makeitrain = $interval(getEarn, 60000);
});

</script>


    <script type="text/javascript">
        jQuery(document).ready(function() {

            "use strict";

            // Init Theme Core      
            Core.init({
                sbm: "sb-l-c",
            });

            $("[id^=ntoggle]").change(function() {
                if ($(this).is(':checked')) { var status = 1; } else { var status = 0; }
                $.post('{{ url('admin/network/toggle') }}', { _token: $('[name="_token"]').val(), id: $(this).data('pk'), status: status });
            });

            $('a#minepc').editable({
                url: '{{ url('admin/offers/minepc') }}',
                params: { _token: $('[name="_token"]').val() }
            });

            $("a#fetchOff").click(function(){
                var network = $(this).data('network');

                $('#status').html('Starting Up..');

                $.ajax({url: "{{ url('admin/fetchoffers') }}/"+network, success: function(result){
                        console.log(result);
                }});

                var statCheck = setInterval(function() {
                     $.ajax({url: "{{ url('admin/getfetch') }}/"+network, success: function(result){
                                $('#status').html(result);
                                 if(result == 'Processing Complete') { clearInterval(statCheck); $.iGrowl({title: network,message: result,type: 'success'}); $('#status').html('Network Manager'); location.reload(); }
                         }});
                },3000);
            });



            @foreach ($errors->all() as $error)
            $.iGrowl({title:'Notice',message: "{{{ $error }}}",type: 'notice'});
            @endforeach

            $('.admin-panels').adminpanel({
                grid: '.admin-grid',
                draggable: false,
                preserveGrid: true,
                mobile: true,
                callback: function() {
                    bootbox.confirm('<h3>A Custom Callback!</h3>', function() {});
                },
                onFinish: function() {
                    $('.admin-panels').addClass('animated fadeIn').removeClass('fade-onload');
                },
                onSave: function() {
                    $(window).trigger('resize');
                }
            });

            $('.ann-tab').click(function() {
                $('.ann-tab').removeClass('active');
                $(this).addClass('active');

                $('.ann-body').addClass('hidden');
                $($(this).attr('href')).removeClass('hidden')
            });


        });
    </script>

    <!-- END: PAGE SCRIPTS -->

</body>

</html>
