<!DOCTYPE html>
<html ng-app="ngDashboard" ng-controller="PageController">

<head>
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <title>AdGodMedia - General Settings</title>
    <meta name="keywords" content="{{ $gset->keywords }}" />
    <meta name="description" content="{{ $gset->description }}">

    <meta name="csrf-token" content="{{ csrf_token() }}" />

    <meta name="author" content="AdminDesigns">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- Font CSS (Via CDN) -->
    <link rel='stylesheet' type='text/css' href='//fonts.googleapis.com/css?family=Open+Sans:400,600,700'>
    <link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Raleway:300,400,500,700">

    <!-- Theme CSS -->
    <link rel="stylesheet" type="text/css" href="{{ asset('panel/assets/skin/default_skin/css/theme.css') }}">

    <!-- Custom CSS -->
    <link rel="stylesheet" type="text/css" href="{{ asset('panel/assets/skin/default_skin/css/custom.css') }}">

    <!-- Admin Panels CSS -->
    <link rel="stylesheet" type="text/css" href="{{ asset('panel/assets/admin-tools/admin-plugins/admin-panels/adminpanels.css') }}">

    <link rel="stylesheet" href="{{ asset('panel/css/igrowl.min.css') }}">

    <!-- Favicon -->
    <link rel="shortcut icon" href="{{ asset('favicon.ico') }}">

    <script type="text/javascript" src="{{ asset('panel/vendor/jquery/jquery-1.11.1.min.js') }}"></script>
    <script src="//ajax.googleapis.com/ajax/libs/angularjs/1.2.26/angular.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.3.14/angular-sanitize.min.js"></script>


    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->

@include('ganalytics') </head>

<body class="dashboard-page sb-l-o sb-r-c">

    <!-- Start: Main -->
    <div id="main">

    @include('admin.aheader')

    @include('admin.asidebar')



        <!-- Start: Content-Wrapper -->
        <section id="content_wrapper">

            <!-- Start: Topbar -->
            <header id="topbar">
                <div class="topbar-left">
                    <ol class="breadcrumb">
                        <li class="crumb-active">
                            <a href="{{ url('admin/settings/general') }}">Settings</a>
                        </li>
                    </ol>
                </div>
            </header>
            <!-- End: Topbar -->

            <!-- Begin: Content -->
            <section id="content" class="animated fadeIn">

	<div class="panel">
      <div class="panel-heading"><div class="panel-title">General</div></div>
      <div class="panel-body p30">

      <form action="{{ url('admin/settings/general/edit') }}" method="post">
      <input type="hidden" name="_token" value="{{ csrf_token() }}">

        <div class="row p10">
          <div class="col-md-2">
            <label for="" class="control-label">Site Name</label>
          </div>
          <div class="col-md-10">
            <input type="text" class="form-control" value="Adgodmedia|Content Locking & Incentive CPA Network" readonly="">
          </div>
        </div>

        <div class="row p10">
          <div class="col-md-2">
            <label for="" class="control-label">Description</label>
          </div>
          <div class="col-md-10">
                <input type="text" class="form-control" name="description" value="{{ $gset->description }}">
          </div>
        </div>

        <div class="row p10">
          <div class="col-md-2">
            <label for="" class="control-label">Keywords</label>
          </div>
          <div class="col-md-10">
            <input type="text" class="form-control" name="keywords" value="{{ $gset->keywords }}">
          </div>
        </div>

        <hr class="short alt">

        <div class="row p10">
          <div class="col-md-2">
            <label for="" class="control-label">Admin Email</label>
          </div>
          <div class="col-md-10">
            <input type="text" class="form-control" value="{{ $gset->adminemail }}" name="admin_email">
          </div>
        </div>

        <div class="row p10">
          <div class="col-md-2">
            <label for="" class="control-label">PayPal Email</label>
          </div>
          <div class="col-md-10">
            <input type="text" class="form-control" name="paypal_email" value="{{ $gset->paypalemail }}">
          </div>
        </div>

        <hr class="short alt">

        <div class="row p10">
          <div class="col-md-2">
            <label for="" class="control-label">Minimum Cashout</label>
          </div>
          <div class="col-md-10">
                <div class="input-group">
                  <span class="input-group-addon">$</span>
                  <input class="form-control" type="text" name="min_cashout" value="{{ $gset->mincashout }}">
                </div>
          </div>
        </div>

        <div class="row p10">
          <div class="col-md-2">
            <label for="" class="control-label">Quick Payouts</label>
          </div>
          <div class="col-md-10">
             <select name="quick_payout" id="" class="form-control">
                 <option value="0" @if($gset->quickpayout == 0) selected @endif >Disabled</option>
                 <option value="1" @if($gset->quickpayout == 1) selected @endif >Enabled</option>
             </select>
          </div>
        </div>

        <div class="row p10">
          <div class="col-md-2">
            <label for="" class="control-label">Quick Payout Fee <span class="text-muted">%</span></label>
          </div>
          <div class="col-md-10">
            <input type="text" class="form-control" name="quick_payout_fee"  value="{{ $gset->quickpayoutfee }}">
          </div>
        </div>

        <div class="row p10">
          <div class="col-md-2">
            <label for="" class="control-label">Offer Payout Rate</label>
          </div>
          <div class="col-md-10">
            <input type="text" class="form-control" name="offer_rate" value="{{ $gset->offerrate }}">
          </div>
        </div>

        <div class="row p10">
          <div class="col-md-2">
            <label for="" class="control-label">Referral Rate</label>
          </div>
          <div class="col-md-10">
            <input type="text" class="form-control" name="referral_rate" value="{{ $gset->referralrate }}">
          </div>
        </div>

        <hr class="short alt">

        <div class="row p10">
          <div class="col-md-2">
            <label for="" class="control-label">Account Auto Approve</label>
          </div>
          <div class="col-md-10">
            <select name="auto_approve" id="" class="form-control">
                 <option value="0" @if($gset->autoapprove == 0) selected @endif >Disabled</option>
                 <option value="1" @if($gset->autoapprove == 1) selected @endif >Enabled</option>
             </select>
          </div>
        </div>

        <div class="row p10">
          <div class="col-md-2">
            <label for="" class="control-label">Total Payments</label>
          </div>
          <div class="col-md-10">
            <input type="text" class="form-control" name="total_payments" value="{{ $gset->totalpayments }}">
          </div>
        </div>

         <div class="row p10">
          <div class="col-md-2">
            <label for="" class="control-label">Secret Token</label>
          </div>
          <div class="col-md-10">
            <input type="text" class="form-control" name="secret_token">
          </div>
        </div>

        <div class="row p10">
          <div class="col-md-2"></div>
          <div class="col-md-10"><button type="submit" class="btn btn-success">Save Settings</button></div>
        </div>

        </form>
      </div>      
        </div>


            </section>
            <!-- End: Content -->

        </section>

    </div>
    <!-- End: Main -->

    <!-- BEGIN: PAGE SCRIPTS -->

    <!-- jQuery -->
    <script type="text/javascript" src="{{ asset('panel/vendor/jquery/jquery_ui/jquery-ui.min.js') }}"></script>

    <!-- Bootstrap -->
    <script type="text/javascript" src="{{ asset('panel/assets/js/bootstrap/bootstrap.min.js') }}"></script>

    <!-- Theme Javascript -->
    <script type="text/javascript" src="{{ asset('panel/assets/js/utility/utility.js') }}"></script>
    <script type="text/javascript" src="{{ asset('panel/assets/js/main.js') }}"></script>
  
    <!-- Admin Panels  -->
    <script type="text/javascript" src="{{ asset('panel/assets/admin-tools/admin-plugins/admin-panels/json2.js') }}"></script>
    <script type="text/javascript" src="{{ asset('panel/assets/admin-tools/admin-plugins/admin-panels/jquery.ui.touch-punch.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('panel/assets/admin-tools/admin-plugins/admin-panels/adminpanels.js') }}"></script>

    <script type="text/javascript" src="{{ asset('panel/assets/js/igrowl.min.js') }}"></script>

    <script>

soundManager.setup({
  url: '{{ asset('panel/assets/swf') }}/',
  onready: function() {
    lead_snd = soundManager.createSound({
        url: '{{ asset('panel/assets/audio/chaching.mp3') }}'
      });
  }
});

var app = angular.module('ngDashboard', ['ngSanitize']);
app.controller('PageController', function($scope,$http,$interval) {
  
  $scope.earnings = '${{ formatnum($today->payout,2) }}';
  $scope.clicks = '{{ formatnum($today->clicks) }}';
  $scope.leads = '{{ formatnum($today->leads) }}';
  $scope.epc = '${{ formatnum(division($today->payout,$today->clicks),2) }}';

var serverdate = new Date({{ date("Y, m, d, H, i, s") }});
function displaytime(){
    serverdate.setSeconds(serverdate.getSeconds() + 1);
    var hh=serverdate.getHours();
    var m=serverdate.getMinutes();
    var s=serverdate.getSeconds();
    
    var dd = "AM";
    var h = hh;
    if (h >= 12) {
        h = hh-12;
        dd = "PM";
    }
    if (h == 0) {
        h = 12;
    }
    m=checkTime(m);
    s=checkTime(s);
    var output = h+":"+m+" "+dd;
    $scope.serverTime = output;
}

// Add leading zero in time
function checkTime(i){if (i<10){i="0" + i;}return i;}

    var getEarn = function() {
        $http.get( "{{ url('admin/earnings') }}").success(function(data, status, headers, config) {
            if ($scope.leads < data.leads) { lead_snd.play(); }
            if (data.clicks == undefined) { $interval.cancel(makeitrain); return; }
            $scope.clicks = data.clicks;
            $scope.leads = data.leads;
            $scope.earnings = data.earnings;
            $scope.epc = data.epc;
        });
    };

    $interval(displaytime, 1000);
    makeitrain = $interval(getEarn, 60000);
});

</script>

    <script type="text/javascript">
        jQuery(document).ready(function() {

            "use strict";

            // Init Theme Core      
            Core.init({
                sbm: "sb-l-c",
            });

            @foreach ($errors->all() as $error)
                $.iGrowl({title:'Whoops!',message: "{{{ $error }}}",type: 'notice'});
            @endforeach

            @if ($successmsg)
                $.iGrowl({title:'Success',message: "{{ $successmsg }}",type: 'success'});
            @endif

            $('.admin-panels').adminpanel({
                grid: '.admin-grid',
                draggable: false,
                preserveGrid: true,
                mobile: true,
                callback: function() {
                    bootbox.confirm('<h3>A Custom Callback!</h3>', function() {});
                },
                onFinish: function() {
                    $('.admin-panels').addClass('animated fadeIn').removeClass('fade-onload');
                },
                onSave: function() {
                    $(window).trigger('resize');
                }
            });

            $('.ann-tab').click(function() {
                $('.ann-tab').removeClass('active');
                $(this).addClass('active');

                $('.ann-body').addClass('hidden');
                $($(this).attr('href')).removeClass('hidden')
            });
        });
    </script>

    <!-- END: PAGE SCRIPTS -->

</body>

</html>
