        <!-- Start: Header -->
        <header class="navbar navbar-fixed-top bg-danger">
            <div class="navbar-branding dark bg-danger">
                <a class="navbar-brand" href="{{ url('dashboard') }}"> <b>Admin</b>Panel
                </a>
                <span id="toggle_sidemenu_l" class="octicon octicon-three-bars"></span>
            </div>
            <ul class="nav navbar-nav navbar-left">
                <li class="hidden-xs">
                    <a class="request-fullscreen toggle-active" href="#">
                        <span class="octicon octicon-screen-full fs18"></span>
                    </a>
                </li>
                <li><a href=""><span class="status" style="font-weight:bold;"></span></a></li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <span class="octicon octicon-clock"></span>
                        <span class="fw600" ng-bind="serverTime"></span>
                    </a>
                </li>
                <li class="dropdown dropdown-item-slide">
                    <a class="dropdown-toggle pl20" data-toggle="dropdown" href="#">
                        <span class="icon-mail fs18"></span>
                        @if (count($unreads) > 0)<span class="badge badge-hero bg-warning">{{ count($unreads) }}</span>@endif
                    </a>
                    <ul class="dropdown-menu dropdown-hover dropdown-persist pn w350 bg-white animated animated-shorter fadeIn" role="menu">
                        <li class="bg-light p8">
                            <span class="fw600 pl5 lh30"> Messages</span>
                        </li>
                        @forelse ($unreads as $unread)
                        <li class="p10 br-t item-1" @if($unread->type=='warning')style="border-left:3px solid #e9573f;"@endif data-mfp-src="{{ url('admin/message/'.$unread->id) }}" data-toggle="modal">
                            <div class="media">
                                <div class="media-body va-m">
                                    <h5 class="media-heading mv5">{{ $unread->title }}<br><small class="">{{ date('d M Y',strtotime($unread->timestamp))}}</small> </h5> {{str_limit($unread->message,40)}}
                                </div>
                            </div>
                        </li>
                        @empty
                         <li class="p10 br-t item-3">
                            <div class="media">
                                <div class="media-body va-m">
                                    <span>No unread messages</span>
                                </div>
                            </div>
                        </li>
                        @endforelse
                        <li class="p10 br-t item-3" onclick="location.href='{{ url('admin/messages'.'#messages') }}'">
                            <div class="media">
                                <div class="media-body va-m">
                                    <span><a href="">See All Messages</a></span>
                                </div>
                            </div>
                        </li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle fw600 pt15" data-toggle="dropdown"> <img src="{{ asset('panel/assets/img/avatars/1.png') }}" alt="avatar" class="mw30 br64 mr15">
                        <span>{{Auth::user()->name}}</span>
                        <span class="caret caret-tp hidden-xs"></span>
                    </a>
                    <ul class="dropdown-menu dropdown-persist pn w250 bg-white" role="menu">
                        <li class="br-t of-h">
                            <a href="{{ url('dashboard/account') }}" class="fw600 p12 animated animated-short fadeInDown">
                                <span class="octicon octicon-gear pr5"></span> Account Settings </a>
                        </li>
                        <li class="br-t of-h">
                            <a href="{{ url('dashboard') }}" target="_blank" class="fw600 p12 animated animated-short fadeInDown">
                                <span class="octicon octicon-split pr5"></span> Publisher Dashboard </a>
                        </li>
                        <li class="br-t of-h">
                            <a href="{{ url('auth/logout') }}" class="fw600 p12 animated animated-short fadeInDown">
                                <span class="octicon octicon-plug pr5"></span> Logout </a>
                        </li>
                    </ul>
                </li>
            </ul>

        </header>
        <!-- End: Header --?

