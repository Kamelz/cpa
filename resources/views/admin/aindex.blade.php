<!DOCTYPE html>
<html ng-app="ngDashboard" ng-Controller="PageController">

<head>

    <meta charset="utf-8">
    <title>AdGodMedia - Admin Dashboard</title>
    <meta name="keywords" content="{{ $gset->keywords }}" />
    <meta name="description" content="{{ $gset->description }}">

    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- Font CSS (Via CDN) -->
    <link rel='stylesheet' type='text/css' href='//fonts.googleapis.com/css?family=Open+Sans:400,600,700'>
    <link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Roboto:300,400,500,700">

    <!-- Theme CSS -->
    <link rel="stylesheet" type="text/css" href="{{ asset('panel/assets/skin/default_skin/css/theme.css') }}">
    <!-- Custom CSS -->
    <link rel="stylesheet" type="text/css" href="{{ asset('panel/assets/skin/default_skin/css/custom.css') }}">

    <!-- Admin Panels CSS -->
    <link rel="stylesheet" type="text/css" href="{{ asset('panel/assets/admin-tools/admin-plugins/admin-panels/adminpanels.css') }}">

    <link rel="stylesheet" type="text/css" href="{{ asset('panel/css/emoticons.css') }}">

    <link rel="stylesheet" type="text/css" href="//cloud.github.com/downloads/lafeber/world-flags-sprite/flags16.css" />

    <!-- Favicon -->
    <link rel="shortcut icon" href="{{ asset('favicon.ico') }}">

    <script type="text/javascript" src="{{ asset('panel/vendor/jquery/jquery-1.11.1.min.js') }}"></script>
    <script src="//ajax.googleapis.com/ajax/libs/angularjs/1.2.26/angular.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.3.14/angular-sanitize.min.js"></script>


    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->

<style>
.msg-list .delmessage {
    display:none;
}
.msg-list:hover .delmessage {
    display:inline-block;
}
</style>

@include('ganalytics') </head>

<body class="dashboard-page sb-l-o sb-r-c">

    <!-- Start: Main -->
    <div id="main">

    @include('admin.aheader')

    @include('admin.asidebar')



        <!-- Start: Content-Wrapper -->
        <section id="content_wrapper">

            <!-- Start: Topbar -->
            <header id="topbar">
                <div class="topbar-left">
                    <ol class="breadcrumb">
                        <li class="crumb-active">
                            <a href="#">Dashboard</a>
                        </li>
                    </ol>
                </div>
            </header>
            <!-- End: Topbar -->

            <!-- Begin: Content -->
            <section id="content" class="animated fadeIn">

                <!-- Dashboard Tiles -->
                <div class="row mb10">
                    <div class="col-md-3">
                        <div class="panel bg-alert of-h mb10">
                            <div class="pn pl20 p5">
                                <div class="icon-bg"> <i class="octicon octicon-history"></i> </div>
                                <h2 class="mt15 lh15"> <b>{{ $pendingcashouts }}</b> </h2>
                                <h5 class="text-muted">Pending Cashouts</h5>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="panel bg-info of-h mb10">
                            <div class="pn pl20 p5">
                                <div class="icon-bg"> <i class="octicon octicon-mail-read"></i> </div>
                                <h2 class="mt15 lh15"> <b>{{ $pendingtickets }}</b> </h2>
                                <h5 class="text-muted">Tickets</h5>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="panel bg-success of-h mb10">
                            <div class="pn pl20 p5">
                                <div class="icon-bg"> <i class="icon-money"></i> </div>
                                <h2 class="mt15 lh15"> <b ng-bind="earnings"></b> </h2>
                                <h5 class="text-muted">Earnings</h5>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="panel bg-warning of-h mb10">
                            <div class="pn pl20 p5">
                                <div class="icon-bg"> <i class="octicon octicon-organization"></i> </div>
                                <h2 class="mt15 lh15"> <b>{{ $newusers }}</b> </h2>
                                <h5 class="text-muted">New Users</h5>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- Admin-panels -->
                <div class="admin-panels fade-onload sb-l-o-full">

                    <!-- full width widgets -->

                    <div class="row">
                      
                      @include('dashboard.chatbox')

                        <div class="col-md-4">
                            <!-- set a unique ID for every panel -->
        <div class="panel">
            <div class="panel-heading">
                 <span class="panel-icon"><i class="fa fa-newspaper-o"></i></span>
                 <span class="panel-title"> Announcements</span>
                 <div class="widget-menu pull-right mr10">
                    <div class="btn-group">
    @foreach ($announcements as $index => $amts)
        <button type="button" class="ann-tab btn btn-xs btn-default @if($index == 0) active @endif" href="#ann-{{ $index+1 }}">{{ $index+1 }}</button>
    @endforeach
                    </div>
                    </div>
            </div>        
            <div class="panel-body">
    @foreach ($announcements as $index => $amts)
        <div  style="height:150px;overflow-y:auto;" id="ann-{{ $index + 1}}" class="ann-body @if ($index > 0) hidden @endif"><h5>{{ $amts->title }}</h5><small>{{ date(('d M Y'), strtotime($amts->timestamp)) }}</small><br><br>{!! nl2br($amts->message) !!}
        </div>
    @endforeach
            </div>
        </div>

        <div class="panel panel-tile bg-danger" style="padding:10px;">
            <div class="row">
                <div class="col-md-12">
                    <h3 style="margin-top:2px;color:#fafafa;">Ripon C.M</h3>
                    <b>Managing Director</b><br>
                    <small>Feel free to get in touch if you have any questions , comments or requests. You can reach me from 9am - 5pm (GMT)</small><br>
                    <i class="icon-tasks fs20 mt10 pointer" data-toggle="tooltip" data-original-title="Submit a Ticket" onclick="javascript:location.href='{{ url('dashboard/tickets') }}'"></i>

                    <i class="icon-mail fs20 mt10 pointer" data-toggle="tooltip" data-original-title="Mail: admin@adgodmedia.com" onclick="javascript.location.href='mailto:admin@adgodmedia.com'"></i>

                    <i class="icon-skype fs20 mt10 pointer" data-toggle="tooltip" data-original-title="Skype: adgodmedia.com"></i>
                </div>
            </div>
        </div>



                </div>

                </div>
                <!-- 1st Row -->

                <div class="row">
 
<div class="col-md-12">

<div class="panel sort-disable">
<div class="panel-heading">
<span class="panel-title">Performance</span>
<!--<span class="panel-controls"><a href="#" class="panel-control-loader"></a><a href="#" class="panel-control-remove"></a><a href="#" class="panel-control-title"></a><a href="#" class="panel-control-color"></a><a href="#" class="panel-control-collapse"></a><a href="#" class="panel-control-fullscreen"></a></span>--></div>
<div class="panel-body mnw700 of-a">
<div class="row">
 
<div class="col-md-4 pln br-r mvn15">
<div class="monthchart"  style="width:100%;height:280px"></div>
</div>
 
<div class="col-md-3 br-r">
<h5 class="mt5 mbn ph10 pb5 br-b fw700">Latest Downloads</h5>
<table class="table mbn tc-med-1 tc-bold-last tc-fs13-last f16">
<tbody>
@forelse ($latest as $lt)
<tr>
    <td><span class="flag {{ strtolower($lt->country) }} mr5 va-b"></span> <span data-toggle="tooltip" data-title="{{ $lt->offname }}">{{ str_limit($lt->offname,15) }}</span></td>
    <td>${{ $lt->payout }}</td>
</tr>
@empty
<tr>
    <td>No downloads yet</td>
</tr>
@endforelse
</tbody>
</table>
</div>
 
<div class="col-md-5">
<h5 class="mt5 ph10 pb5 br-b fw700">Account Summary <small class="pull-right fw700 text-primary">View Stats </small> </h5>
<table class="table mbn">
<thead>
<tr>
<th>Period</th>
<th>Clicks</th>
<th>Leads</th>
<th>Earnings</th>
<th>EPC</th>
</tr>
</thead>
<tbody>
<tr>
<td class="va-m fw600 text-info">
Today</td>
<td>{{ formatnum($today->clicks) }}</td>
<td>{{ formatnum($today->leads) }}</td>
<td>${{ formatnum($today->payout,2) }}</td>
<td>${{ formatnum(division($today->payout,$today->clicks),2) }}</td>
</tr>
<tr>
<td class="va-m fw600 text-info">
Yesterday</td>
<td>{{ formatnum($yesterday->clicks) }}</td>
<td>{{ formatnum($yesterday->leads) }}</td>
<td>${{ formatnum($yesterday->payout,2) }}</td>
<td>${{ formatnum(division($yesterday->payout,$yesterday->clicks),2) }}</td>
</tr>
<tr>
<td class="va-m fw600 text-info">
MTD</td>
<td>{{ formatnum($mtd->clicks) }}</td>
<td>{{ formatnum($mtd->leads) }}</td>
<td>${{ formatnum($mtd->payout,2) }}</td>
<td>${{ formatnum(division($mtd->payout,$mtd->clicks),2) }}</td>
</tr>
<tr>
<td class="va-m fw600 text-info">
Last Month</td>
<td>{{ formatnum($lastmonth->clicks) }}</td>
<td>{{ formatnum($lastmonth->leads) }}</td>
<td>${{ formatnum($lastmonth->payout,2) }}</td>
<td>${{ formatnum(division($lastmonth->payout,$lastmonth->clicks),2) }}</td>
</tr>
<tr>
<td class="va-m fw600 text-info">
All Time</td>
<?php $allclicks = $alltime->clicks + $lastmonth->clicks + $mtd->clicks;
$allpayout = $alltime->payout + $lastmonth->payout + $mtd->payout; ?>
<td>{{ formatnum($allclicks) }}</td>
<td>{{ formatnum($alltime->leads + $lastmonth->leads + $mtd->leads) }}</td>
<td>${{ formatnum($allpayout,2) }}</td>
<td>${{ formatnum(division($allpayout,$allclicks),2) }}</td>
</tr>
</tbody>
</table>
</div>
</div>

</div>

</div>

<div class="row">

    <div class="col-md-7 admin-grid">
        <div class="panel">
                        <div class="panel-heading" style="padding-left:0px;">
                            <ul class="nav panel-tabs panel-tabs-left">
                                <li class="active">
                                    <a href="#tab1_1" data-toggle="tab"> Top Performing Offers</a>
                                </li>
                                <li>
                                    <a href="#tab1_2" data-toggle="tab"> Recently Added Offers</a>
                                </li>
                                <li>
                                    <a href="#tab1_3" data-toggle="tab"> Recently Expired Offers</a>
                                </li>
                            </ul>
                        </div>
                        <div class="panel-body pn" style="border-top:3px solid #70ca63;">
                        <div class="tab-content pn br-n">
                    <div id="tab1_1" class="tab-pane active">
                       <table class="table admin-form theme-warning tc-checkbox-1 fs13">
                                <thead>
                                    <tr class="bg-light">
                                        <th class="">No.</th>
                                        <th class="">Offer</th>
                                        <th class="text-right">Payout</th>

                                    </tr>
                                </thead>
                                <tbody>
                                        @foreach ($topoffers as $topoffer)
                                        <tr>
                                        <td class="text-left">{{ $topoffer->campid }}</td>
                                        <td>{{ $topoffer->title }}</td>
                                        <td class="text-right">${{ $topoffer->payout }}</td>
                                        </tr>
                                        @endforeach
                                </tbody>
                            </table>
                    </div>
                    <div id="tab1_2" class="tab-pane">
                      <table class="table admin-form theme-warning tc-checkbox-1 fs13">
                                <thead>
                                    <tr class="bg-light">
                                        <th class="">No.</th>
                                        <th class="">Offer</th>
                                        <th class="text-right">Payout</th>

                                    </tr>
                                </thead>
                                <tbody>
                                @foreach ($recentoffers as $recentoffer)
                                <tr>
                                  <td class="text-left">{{ $recentoffer->campid }}</td>
                                  <td>{{ $recentoffer->title }}</td>
                                  <td class="text-right">${{ $recentoffer->payout }}</td>
                                </tr>
                                @endforeach
                                </tbody>
                            </table>
                    </div>
                    <div id="tab1_3" class="tab-pane">
                            <table class="table admin-form theme-warning tc-checkbox-1 fs13">
                                <thead>
                                    <tr class="bg-light">
                                        <th class="">No.</th>
                                        <th class="">Offer</th>
                                        <th class="text-right">Payout</th>

                                    </tr>
                                </thead>
                                <tbody>
                                @foreach ($expiredoffers as $expiredoffer)
                                <tr>
                                  <td class="text-left">{{ $expiredoffer->campid }}</td>
                                  <td>{{ $expiredoffer->title }}</td>
                                  <td class="text-right">${{ $expiredoffer->payout }}</td>
                                </tr>
                                @endforeach
                                </tbody>
                            </table>
                    </div>
                  </div>

                            
                        </div>
                    </div>
    </div>


    <div class="col-md-5">
                                <div class="panel" id="p9">
                                <div class="panel-heading">
                                    <span class="panel-title"><i class="fa fa-pencil hidden"></i> Leads Heatmap</span>
                                </div>
                                <div class="panel-body">
                                    <div id="WidgetMap" class="jvector-colors hide-jzoom" style="width: 100%; height: 220px;"></div>
                                </div>
                            </div>
</div>

</div>

<div class="panel preserve-grid"></div></div>
 
</div>
<!-- 2nd Row -->

                </div>
                <!-- Admin Panels -->

            </section>
            <!-- End: Content -->

        </section>
    </div>

    <!-- End: Main -->

    <!-- BEGIN: PAGE SCRIPTS -->

    <!-- jQuery -->
    <script type="text/javascript" src="{{ asset('panel/vendor/jquery/jquery_ui/jquery-ui.min.js') }}"></script>

    <!-- Bootstrap -->
    <script type="text/javascript" src="{{ asset('panel/assets/js/bootstrap/bootstrap.min.js') }}"></script>

        <!-- Page Plugins -->
    <script type="text/javascript" src="{{ asset('panel/vendor/plugins/jqueryflot/jquery.flot.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('panel/vendor/plugins/jqueryflot/jquery.flot.resize.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('panel/vendor/plugins/jqueryflot/jquery.flot.pie.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('panel/vendor/plugins/jqueryflot/jquery.flot.tooltip.min.js') }}"></script>

    <!-- Page Javascript -->
    <script type="text/javascript" src="{{ asset('panel/assets/js/pages/charts/flot.js') }}"></script>

    <!-- Theme Javascript -->
    <script type="text/javascript" src="{{ asset('panel/assets/js/utility/utility.js') }}"></script>
    <script type="text/javascript" src="{{ asset('panel/assets/js/main.js') }}"></script>

        <!-- Load Additional Jvectormaps -->
    <script type="text/javascript" src="{{ asset('panel/assets/js/utility/jvectormap/assets/jquery-jvectormap-world-mill-en.js') }}"></script>

    <!-- Admin Panels  -->
    <script type="text/javascript" src="{{ asset('panel/assets/admin-tools/admin-plugins/admin-panels/json2.js') }}"></script>
    <script type="text/javascript" src="{{ asset('panel/assets/admin-tools/admin-plugins/admin-panels/jquery.ui.touch-punch.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('panel/assets/admin-tools/admin-plugins/admin-panels/adminpanels.js') }}"></script>

    <!-- Page Javascript -->
    <script type="text/javascript" src="{{ asset('panel/assets/js/pages/widgets.js') }}"></script>

<script>
var plc = 10; 

soundManager.setup({
  url: '{{ asset('panel/assets/swf') }}/',
  onready: function() {
    lead_snd = soundManager.createSound({
        url: '{{ asset('panel/assets/audio/chaching.mp3') }}'
      });
  }
});

var app = angular.module('ngDashboard', ['ngSanitize']);
app.controller('PageController', function($scope,$http,$interval) {
  
  $scope.myID = "{{ Auth::user()->id }}";

  $scope.curtime = {{ strtotime("-7 days", time()) }};

  $scope.msg = "";

  $scope.btntext = "Send";
  
  $scope.messages = [];
  
  $scope.users = [];
  
  $scope.earnings = '${{ formatnum($today->payout,2) }}';
  $scope.clicks = '{{ formatnum($today->clicks) }}';
  $scope.leads = '{{ formatnum($today->leads) }}';
  $scope.epc = '${{ formatnum(division($today->payout,$today->clicks),2) }}';
  
  $scope.context = plc;
  
  $scope.changecontext = function(e) {
    $scope.context = e;
    console.log($scope.context);
  };

  $scope.formatTime = function(e) {
      var d = new Date(e * 1000),
        yyyy = d.getFullYear(),
        mm = ('0' + (d.getMonth() + 1)).slice(-2),
        dd = ('0' + d.getDate()).slice(-2),
        hh = d.getHours(),
        h = hh,
        min = ('0' + d.getMinutes()).slice(-2),
        ampm = 'AM',
        time;
            
    if (hh > 12) {
        h = hh - 12;
        ampm = 'PM';
    } else if (hh === 12) {
        h = 12;
        ampm = 'PM';
    } else if (hh == 0) {
        h = 12;
    }

    time = h + ':' + min + ' ' + ampm;
    return time;
  };

var serverdate = new Date({{ date("Y, m, d, H, i, s") }});
function displaytime(){
    serverdate.setSeconds(serverdate.getSeconds() + 1);
    var hh=serverdate.getHours();
    var m=serverdate.getMinutes();
    var s=serverdate.getSeconds();
    
    var dd = "AM";
    var h = hh;
    if (h >= 12) {
        h = hh-12;
        dd = "PM";
    }
    if (h == 0) {
        h = 12;
    }
    m=checkTime(m);
    s=checkTime(s);
    var output = h+":"+m+" "+dd;

    $scope.serverTime = output;
    if ($('#msg-body').is(":hover") == false) {
        $("#msg-body").scrollTop($("#msg-body")[0].scrollHeight);
    }
}

// Add leading zero in time
function checkTime(i){if (i<10){i="0" + i;}return i;}

  
  $scope.addChat = function(e,n) {
    var tab = 0;
    angular.forEach($scope.messages, function (msg) {
        if (msg.id == e) {
          tab = 1;
        }
    });
     if (tab == 1) {
       $scope.context = e;
     } else { $scope.messages.push({id:e,name:'System',scope:n,msg:'Start a Private Chat!',role:'system'});
            }
            $scope.context = e;
  };
  
  $scope.closecontext = function(e) {
    if (e !== plc) {
            $.getJSON( "{{ url('json/chat/close') }}/"+e, function( data ) {
            });
            var newMsgs = [];
            angular.forEach($scope.messages, function (message) {
                if (message.id !== e) {
                    newMsgs.push(message);
                }
            });
            $scope.messages = newMsgs;
            console.log($scope.messages);
            $scope.context = plc;
    }
  };

    var loadChat = function() {
        $http.get( "{{ url('json/chat') }}/"+$scope.curtime).success(function(data, status, headers, config) {
        angular.forEach(data.messages, function(value,key) {    
                  $scope.messages.push({
                    id:value.id,
                    msgid:value.msgid,
                    name:value.name,
                    scope:value.scope,
                    msg:emoticons(value.msg),
                    role:value.role,
                    timestamp:value.timestamp
                  });
        });
        $scope.users = [];
        angular.forEach(data.users, function(value,key) {    
                  $scope.users.push({
                    id:value.id,
                    name:value.name,
                    role:value.role
                  });
        });
        $scope.curtime = data.curtime;
    });
        $scope.btntext = "Send";
    };

    loadChat();

    $scope.sendmsg = function() {
        if ($scope.msg != "") {
        $scope.btntext = "Sending..";
        $.post( "{{ url('json/chat') }}", { msg: $scope.msg, to: $scope.context, _token: $('meta[name="csrf-token"]').attr('content'), curtime: $scope.curtime })
        .done(function( data ) {
            loadChat();
            $scope.msg = "";
        });
    }
    };

    var getEarn = function() {
        $http.get( "{{ url('admin/earnings') }}").success(function(data, status, headers, config) {
            if ($scope.leads < data.leads) { lead_snd.play(); }
            if (data.clicks == undefined) { $interval.cancel(makeitrain); $interval.cancel(chatmsgs); return; }
            $scope.clicks = data.clicks;
            $scope.leads = data.leads;
            $scope.earnings = data.earnings;
            $scope.epc = data.epc;
        });
    };

    chatmsgs = $interval(loadChat, 20000);
    $interval(displaytime, 1000);
    makeitrain = $interval(getEarn, 60000);
});

///////////////////////////////////
app.filter('unique', function() {
   return function(collection, keyname) {
      var output = [], 
          keys = [];

      angular.forEach(collection, function(item) {
          var key = item[keyname];
          if(keys.indexOf(key) === -1) {
              keys.push(key);
              output.push(item);
          }
      });

      return output;
   };
});

app.directive('ngEnter', function () {
    return function (scope, element, attrs) {
        element.bind("keydown keypress", function (event) {
            if(event.which === 13) {
                scope.$apply(function (){
                    scope.$eval(attrs.ngEnter);
                });

                event.preventDefault();
            }
        });
    };
});

</script>

    <script type="text/javascript">
        jQuery(document).ready(function() {

            "use strict";

            // Init Theme Core      
            Core.init({
                sbm: "sb-l-c",
            });

            FlotCharts.init();

            var Grid = {
                grid: {
                    show: true,
                    aboveData: true,
                    color: "#bbbbbb",
                    labelMargin: 15,
                    axisMargin: 0,
                    borderWidth: 0,
                    borderColor: null,
                    minBorderMargin: 5,
                    clickable: true,
                    hoverable: true,
                    autoHighlight: true,
                    mouseActiveRadius: 20,
                },
                xaxis: {
                  mode: 'time',
                  timeformat: '%d %b',
                },
                yaxis: [{
                  position: "left",
                  tickDecimals: 2,
                }],
                legend: {
                  show: false
                }
            };

            var Colors = [bgPrimary, bgSuccess, bgInfo, bgWarning, bgDanger, bgAlert, bgSystem];

            var earn = [@foreach ($graph as $day) [{{ strtotime($day->timestamp)*1000 }} ,{{ $day->payout}}], @endforeach];

            var clicks = [@foreach ($graph as $day) [{{ strtotime($day->timestamp)*1000 }} ,{{ $day->clicks }}], @endforeach];

            var epc = [@foreach ($graph as $day) [{{ strtotime($day->timestamp)*1000 }} ,{{ $day->payout/$day->clicks }}], @endforeach];

            $.plot($(".monthchart"), [{
                data: earn,
                label: 'Earnings',
                points: {
                    show: true,
                    radius: 4
                },
                lines: {
                    show: true
                },
                color: '#70ca63'
            }, {
                data: clicks,
                label: 'Clicks',
                points: {
                    show: true,
                    radius: 4
                },
                lines: {
                    show: true
                },
                color: '#ccc',
            }, {
                data: epc,
                label: 'EPC',
                points: {
                    show: true,
                    radius: 4
                },
                lines: {
                    show: true
                },
                color: '#888',
            }], Grid);

            function showTooltip(x, y, contents) {
            $('<div id="tooltip">' + contents + '</div>').css( {
                position: 'absolute',
                display: 'none',
                //float: 'left',
                top:  y - 40,
                left: x - 30,
                border: '1px solid #fdd',
                padding: '2px',
                'background-color': '#eee',
                opacity: 0.80
            }).appendTo("body").fadeIn(200);
         }

        var previousPoint = null;

        $(".monthchart").bind("plothover", function (event, pos, item){
            $("#x").text(pos.x.toFixed(2));
            $("#y").text(pos.y.toFixed(2));
            if (item) {
                if (previousPoint != item.dataIndex){
                    previousPoint = item.dataIndex;

                    $("#tooltip").remove();
                    var x = item.datapoint[0].toFixed(2),
                        y = item.datapoint[1].toFixed(2);

                        var append = '';
                        if (item.series.label == 'Earnings' || item.series.label == 'EPC') { append = '$' }

                        showTooltip(item.pageX, item.pageY,
                                item.series.label +": "+append+ y);
                                                    }
                    }
                else {
                    $("#tooltip").remove();
                    previousPoint = null;
                    }

            });

            $('.admin-panels').adminpanel({
                grid: '.admin-grid',
                draggable: false,
                preserveGrid: true,
                mobile: true,
                callback: function() {
                    bootbox.confirm('<h3>A Custom Callback!</h3>', function() {});
                },
                onFinish: function() {
                    $('.admin-panels').addClass('animated fadeIn').removeClass('fade-onload');

                },
                onSave: function() {
                    $(window).trigger('resize');
                }
            });

            $('.ann-tab').click(function() {
                $('.ann-tab').removeClass('active');
                $(this).addClass('active');

                $('.ann-body').addClass('hidden');
                $($(this).attr('href')).removeClass('hidden')
            });

            var earn = {@foreach ($countries as $country)
                            "{{$country->country}}": {{ $country->payout }},
                          @endforeach "undefined": 0};

            $('#WidgetMap').vectorMap({
                map: 'world_mill_en',
                backgroundColor: 'transparent',
                regionStyle: {
                    initial: {
                        fill: '#f7c65f'
                    },
                    hover: {
                        fill: '#6e7072'
                    }
                },
                onRegionLabelShow: function(e, el, code) {
                    if (earn[code] == undefined) earn[code] = 0;
                    el.html(el.html()+' (Earnings: $'+earn[code]+')');
                },
                series: {
                    regions: [{
                        scale: ['#f7c65f', '#ec6f5a'],
                        normalizeFunction: 'polynomial',
                        values: earn
                    }]
                }
            });


        });
    </script>

    <!-- END: PAGE SCRIPTS -->

</body>

</html>
