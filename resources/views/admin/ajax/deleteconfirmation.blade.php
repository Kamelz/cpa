<div class="modal-panel">

<div class="panel">
    <div class="panel-heading"><div class="panel-title"><span class="pr5 panel-icon octicon octicon-alert"></span> Warning</div></div>
    <div class="panel-body">
       	Are you really sure? This action cannot be undone.
    </div>
     <div class="panel-footer text-right">
            <a href="javascript:$.magnificPopup.close();" class="btn btn-default mr10">Close</a> <a href="{{ url('/') }}/admin/{{ $scope }}/delete/{{ $id }}" class="btn btn-primary">Yes</a>
    </div>
</div>

</div>