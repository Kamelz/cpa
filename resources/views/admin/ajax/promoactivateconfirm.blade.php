<div class="modal-panel">

<div class="panel">
    <div class="panel-heading"><div class="panel-title"><span class="pr5 panel-icon octicon octicon-alert"></span> Alert</div></div>
    <div class="panel-body">
       	Do you want to activate this global promotion for all users now? It will deactivate after {{ $promo->hours }} hours
    </div>
     <div class="panel-footer text-right">
            <a href="javascript:$.magnificPopup.close();" class="btn btn-default mr10">Close</a> <a href="{{ url('/') }}/admin/promo/activate/{{ $id }}" class="btn btn-primary">Yes</a>
    </div>
</div>

</div>