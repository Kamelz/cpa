<div class="modal-panel">
<div class="panel">
    <div class="panel-heading"><div class="panel-title"><span class="pr5 panel-icon octicon octicon-megaphone"></span>Announcement</div></div>
    <div class="panel-body">
        <div class="row p10">
        	<div class="col-md-2">
        		<b>Subject</b>
        	</div>
        	<div class="col-md-10">{{ $anmt->title }}</div>
        </div>
        <div class="row p10">
        	<div class="col-md-2"><b>Message</b></div>
        	<div class="col-md-10">{{ $anmt->message }}</div>
        </div>
        <div class="row p10">
        	<div class="col-md-2"><b>Date</b></div>
        	<div class="col-md-10">{{ date('d M Y',strtotime($anmt->timestamp)) }}</div>
        </div>
    </div>
     <div class="panel-footer text-right">
           <a href="javascript:$.magnificPopup.close();" class="btn btn-default mr10">Close</a>
    </div>
</div>
</div>