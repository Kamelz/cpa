<div class="modal-panel">
<div class="panel">
    <div class="panel-heading"><div class="panel-title"><span class="panel-icon octicon octicon-file-text pr5"></span> View Content Report</div></div>
    <div class="panel-body">
       <div class="row p10">
       	<div class="col-md-2">
       		<b>Message</b>
       	</div>
       	<div class="col-md-10">
       		{{ $report->message }}
       	</div>
       </div>
    </div>
     <div class="panel-footer text-right">
            <a href="javascript:$.magnificPopup.close();" class="btn btn-default mr10">Close</a>
    </div>
</div>
</div>