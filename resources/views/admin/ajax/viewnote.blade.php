<div class="modal-panel">
<div class="panel">
    <div class="panel-heading"><div class="panel-title"><span class="panel-icon octicon octicon-file-text pr5"></span> View Note</div></div>
    <div class="panel-body">
       <div class="row p10">
       	<div class="col-md-2">
       		<b>Summary</b>
       	</div>
       	<div class="col-md-10">
       		{{ $note->summary }}
       	</div>
       </div>
       <div class="row p10">
        <div class="col-md-2">
          <b>Note</b>
        </div>
        <div class="col-md-10">
          {{ $note->note }}
        </div>
       </div>
    </div>
     <div class="panel-footer text-right">
            <a href="javascript:$.magnificPopup.close();" class="btn btn-default mr10">Close</a>
    </div>
</div>
</div>