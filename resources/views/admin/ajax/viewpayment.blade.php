<div class="modal-panel">
<div class="panel">
    <div class="panel-heading"><div class="panel-title"><span class="panel-icon octicon octicon-file-text pr5"></span> View Cashout Request</div></div>
    <div class="panel-body">
       <div class="row p10">
       	<div class="col-md-3">
       		<b>Payment Details</b>
       	</div>
       	<div class="col-md-9">
        @if ($payment->payment_method != 'Wire Transfer')
        {{ $payment->payment_details }}
        @else
          Name on Account: 
       		<b>{{ $detail['Name on Account'] }}</b> 
          Bank Name: 
          <b>{{ $detail['Bank Name'] }}</b> 
          Bank Address: 
          <b>{{ $detail['Bank Address'] }}</b> 
          Bank Country: 
          <b>{{ $detail['Bank Country'] }}</b> 
          Account Type: 
          <b>{{ $detail['Account Type'] }}</b> 
          Account / IBAN #: 
          <b>{{ $detail['Account / IBAN #'] }}</b> 
          Routing / ABA #: 
          <b>{{ $detail['Routing / ABA #'] }}</b> 
          SWIFT Code: 
          <b>{{ $detail['SWIFT Code'] }}</b>
        @endif
       	</div>
    </div>
     <div class="panel-footer text-right">
            <a href="javascript:$.magnificPopup.close();" class="btn btn-default mr10">Close</a>
    </div>
</div>
</div>