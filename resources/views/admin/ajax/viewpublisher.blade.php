<div class="modal-panel">
<div class="panel">
<input type="hidden" name="_token" value="{{ csrf_token() }}">
    <div class="panel-heading"><div class="panel-title"><span class="panel-icon octicon octicon-file-text pr5"></span> View Publisher</div></div>
    <div class="panel-body">
       <div class="row p10">
       	<div class="col-md-3">
       		<b>Name</b>
       	</div>
       	<div class="col-md-9">
       		<a href="#" id="name" data-type="text" data-pk="{{ $user->id }}" data-title="Enter name">{{ $user->name }}</a>
       	</div>
       </div>
       <div class="row p10">
       	<div class="col-md-3">
       		<b>Display Name</b>
       	</div>
       	<div class="col-md-9"><a href="#" id="chathandle" data-type="text" data-pk="{{ $user->id }}" data-title="Enter displayname">{{ $user->chathandle }}</a><span class="text-muted"> (Changing frequently is not recommended)</span></div>
       </div>
       <div class="row p10">
         <div class="col-md-3">
           <b>Balance</b>
         </div>
         <div class="col-md-9">$<a href="#" id="balance" data-type="text" data-pk="{{ $user->id }}" data-title="Enter balance">{{ $user->balance }}</a></div>
       </div>
      <div class="row p10">
         <div class="col-md-3">
           <b>Email Address</b>
         </div>
         <div class="col-md-9">{{ $user->email }}</div>
       </div>
       <div class="row p10">
         <div class="col-md-3">
           <b>Street Address</b>
         </div>
         <div class="col-md-9"><a href="#" id="address" data-type="text" data-pk="{{ $user->id }}" data-title="Enter address">{{ $user->user_profile->address }}</a></div>
       </div>
       <div class="row p10">
         <div class="col-md-3">
           <b>City</b>
         </div>
         <div class="col-md-9"><a href="#" id="city" data-type="text" data-pk="{{ $user->id }}" data-title="Enter city">{{ $user->user_profile->city }}</a></div>
       </div>
       <div class="row p10">
         <div class="col-md-3">
           <b>State</b>
         </div>
         <div class="col-md-9"><a href="#" id="state" data-type="text" data-pk="{{ $user->id }}" data-title="Enter state">{{ $user->user_profile->state }}</a></div>
       </div>
       <div class="row p10">
         <div class="col-md-3">
           <b>ZIP / Postal Code</b>
         </div>
         <div class="col-md-9"><a href="#" id="zip" data-type="text" data-pk="{{ $user->id }}" data-title="Enter zip">{{ $user->user_profile->zip }}</a></div>
       </div>
       <div class="row p10">
         <div class="col-md-3">
           <b>Country</b>
         </div>
         <div class="col-md-9"><a href="#" id="country" data-type="select" data-pk="{{ $user->id }}" data-value="{{ $user->user_profile->country }}">{{ $user->user_profile->country }}</a></div>
       </div>
       <div class="row p10">
         <div class="col-md-3">
           <b>Referred By</b>
         </div>
         <div class="col-md-9">refferrer</div>
       </div>
        <div class="row p10">
         <div class="col-md-3">
           <b>Register Date</b>
         </div>
         <div class="col-md-9">{{ date('d M Y', strtotime($user->register_date)) }}</div>
       </div>
      <div class="row p10">
         <div class="col-md-3">
           <b>Register IP</b>
         </div>
         <div class="col-md-9">{{ long2ip($user->user_profile->register_ip) }}</div>
       </div>
       <div class="row p10">
         <div class="col-md-3">
           <b>Reason to accept</b>
         </div>
         <div class="col-md-9">{{ $user->user_profile->reason_toaccept }}</div>
       </div>
        <div class="row p10">
         <div class="col-md-3">
           <b>Websites</b>
         </div>
         <div class="col-md-9">{{ $user->user_profile->websites }}</div>
       </div>
        <div class="row p10">
         <div class="col-md-3">
           <b>Promotional Methods</b>
         </div>
         <div class="col-md-9">{{ $user->user_profile->promo_methods }}</div>
       </div>
      <div class="row p10">
         <div class="col-md-3">
           <b>Monthly Income</b>
         </div>
         <div class="col-md-9">${{ $user->user_profile->monthly_income }}</div>
       </div>
      <div class="row p10">
         <div class="col-md-3">
           <b>Skype</b>
         </div>
         <div class="col-md-9"><a href="#" id="skype" data-type="text" data-pk="{{ $user->id }}" data-title="Enter skype">{{ $user->user_profile->skype }}</a></div>
       </div>
      <div class="row p10">
         <div class="col-md-3">
           <b>Payment Method</b>
         </div>
         <div class="col-md-9">{{ $user->user_profile->payment_method }}</div>
       </div>
      <div class="row p10">
         <div class="col-md-3">
           <b>Payment Detail</b>
         </div>
         <div class="col-md-9">
               @if ($user->user_profile->payment_method != 'Wire Transfer')
            <?php echo $user->user_profile->payment_detail; ?>
            @else <?php $detail = unserialize($user->user_profile->payment_detail) ?>
              Name on Account: 
              <b><?php echo $detail['Name on Account'] ?></b> 
              Bank Name: 
              <b><?php echo $detail['Bank Name'] ?></b> 
              Bank Address: 
              <b><?php echo $detail['Bank Address'] ?></b> 
              Bank Country: 
              <b><?php echo $detail['Bank Country'] ?></b> 
              Account Type: 
              <b><?php echo $detail['Account Type'] ?></b> 
              Account / IBAN #: 
              <b><?php echo $detail['Account / IBAN #'] ?></b> 
              Routing / ABA #: 
              <b><?php echo $detail['Routing / ABA #'] ?></b> 
              SWIFT Code: 
              <b><?php echo $detail['SWIFT Code'] ?></b>
            @endif
         </div>
       </div>
       <div class="row p10">
         <div class="col-md-3">
           <b>Payment Cycle</b>
         </div>
         <div class="col-md-9"><a href="#" id="payment_cycle" data-type="select" data-pk="{{ $user->id }}" data-value="{{ $user->user_profile->payment_cycle }}">{{ $user->user_profile->payment_cycle }}</a></div>
       </div>
       <div class="row p10">
         <div class="col-md-3">
           <b>Disable Chat</b>
         </div>
         <div class="col-md-9"><a href="#" id="disablechat" data-type="select" data-pk="{{ $user->id }}" data-value="{{ $disabled }}"> @if($disabled == 1) Yes @else No @endif </a></div>
       </div>
       <div class="row p10">
         <div class="col-md-3">
           <b>Custom Offer Rate</b>
         </div>
         <div class="col-md-9"><a href="#" id="customrate" data-type="text" data-pk="{{ $user->id }}" data-title="Enter custom offer rate">{{ $customrate }}</a>%</div>
       </div>
    </div>
     <div class="panel-footer text-right">
            <a href="javascript:$.magnificPopup.close();" class="btn btn-default mr10">Close</a>
    </div>
</div>
</div>

<script>
  $.fn.editable.defaults.placement = 'bottom';
  $.fn.editable.defaults.ajaxOptions = {type: "POST"};

  $.fn.editable.defaults.url = '{{ url('admin/publisher/edit') }}',
  $.fn.editable.defaults.params = { _token: $('[name="_token"]').val() }

  $('a#name').editable();
  $('a#chathandle').editable();
  $('a#address').editable();
  $('a#city').editable();
  $('a#state').editable();
  $('a#zip').editable();

  $('a#country').editable({
                source: [
                    @foreach ($countries as $country)
                    {value: '{{ $country->value }}', text: '{{ $country->value }}'},
                    @endforeach
                ]
            });

  $('a#skype').editable();
  $('a#customrate').editable();
  $('a#balance').editable();

  $('a#payment_cycle').editable({
                source: [
                    {value: 'Net30', text: 'Net30'},
                    {value: 'Net15', text: 'Net15'},
                    {value: 'Net7', text: 'Net7'},
                    {value: 'Net0', text: 'Net0'},
                    {value: 'Bi-Weekly', text: 'Bi-Weekly'},
                    {value: 'Weekly', text: 'Weekly'},
                ]
            });

  $('a#disablechat').editable({
      source: [{value: 1, text: "Yes"}, {value: 0, text: "No"}],
      display: function(value, sourceData) {
         var html = [],
          checked = $.fn.editableutils.itemsByValue(value, sourceData);
          var color,text = '';
         $.each(checked, function(i, v) { if(v.text == 'Yes') { color = 'red'; text = 'Yes'; } else { color = 'green'; text = 'No' } });
         $(this).css('color',color);
         $(this).html(text);
      }
  });
</script>