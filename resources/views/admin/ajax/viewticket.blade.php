<div class="modal-panel">
<div class="panel">
    <div class="panel-heading"><div class="panel-title"><span class="panel-icon octicon octicon-file-text pr5"></span> View Ticket #{{ $id }}</div></div>
    <div class="panel-body pn">
      <table class="table table-striped table-bordered">
        <thead>
          <tr>
            <th>User</th>
            <th>Message</th>
          </tr>
        </thead>
        <tbody>
          @foreach ($tmsgs as $tmsg)
          <tr>
            <td><b @if($tmsg->user == 'AdGodMedia') class="text-success" @endif >{{ $tmsg->user }}</b><br>{{ date('F j, Y, g:i a', strtotime($tmsg->timestamp)) }}</td>
            <td width="70%"><div class="well">{{ $tmsg->message }}</div></td>
          </tr>
          @endforeach
          <tr>
            <td></td>
            <td width="70%">
              <input type="hidden" name="_token" value="{{ csrf_token() }}">
              <textarea name="message" id="" rows="5" class="form-control mb10" placeholder="Add your message here.."></textarea>
            </td>
          </tr>
        </tbody>
      </table>
    </div>
     <div class="panel-footer text-right">
            <button id="submit" class="btn btn-primary mr10">Add Message</button> <a href="javascript:$.magnificPopup.close();" class="btn btn-default mr10">Close</a>
    </div>
</div>
</div>

<script>
  $('#submit').click(function() {
    $.post('{{ url('admin/ticket/add/'.$id) }}', { _token: $('[name="_token"]').val(), message: $('[name="message"]').val() })
    .done(function(data) {
                    $.each( data, function( key, value ) {
                                    if (value.title == 'Success') {
                                      $.iGrowl({title:'Success',message: value.message});
                                      window.location.href = "{{ url('admin/tickets') }}";
                                    } else {
                                      $.iGrowl({title:'Whoops!',message: value.message, type: 'notice'});
                                    }
                    });
    });
  });
</script>