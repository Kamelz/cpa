<!DOCTYPE html>
<html ng-app="ngDashboard" ng-controller="PageController">

<head>
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <title>AdGodMedia - Leads Log</title>
    <meta name="keywords" content="{{ $gset->keywords }}" />
    <meta name="description" content="{{ $gset->description }}">

    <meta name="csrf-token" content="{{ csrf_token() }}" />

    <meta name="author" content="AdminDesigns">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- Font CSS (Via CDN) -->
    <link rel='stylesheet' type='text/css' href='//fonts.googleapis.com/css?family=Open+Sans:400,600,700'>
    <link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Raleway:300,400,500,700">

    <!-- Theme CSS -->
    <link rel="stylesheet" type="text/css" href="{{ asset('panel/assets/skin/default_skin/css/theme.css') }}">

    <!-- Custom CSS -->
    <link rel="stylesheet" type="text/css" href="{{ asset('panel/assets/skin/default_skin/css/custom.css') }}">

    <!-- Admin Panels CSS -->
    <link rel="stylesheet" type="text/css" href="{{ asset('panel/assets/admin-tools/admin-plugins/admin-panels/adminpanels.css') }}">

    <link rel="stylesheet" href="{{ asset('panel/css/igrowl.min.css') }}">

    <link href="{{ asset('panel/vendor/editors/xeditable/css/bootstrap-editable.css') }}" rel="stylesheet" type="text/css">

    <!-- Favicon -->
    <link rel="shortcut icon" href="{{ asset('favicon.ico') }}">

    <script type="text/javascript" src="{{ asset('panel/vendor/jquery/jquery-1.11.1.min.js') }}"></script>
    <script src="//ajax.googleapis.com/ajax/libs/angularjs/1.2.26/angular.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.3.14/angular-sanitize.min.js"></script>


    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->

@include('ganalytics') </head>

<body class="dashboard-page sb-l-o sb-r-c">

    <!-- Start: Main -->
    <div id="main">

    @include('admin.aheader')

    @include('admin.asidebar')



        <!-- Start: Content-Wrapper -->
        <section id="content_wrapper">

            <!-- Start: Topbar -->
            <header id="topbar">
                <div class="topbar-left">
                    <ol class="breadcrumb">
                        <li class="crumb-active">
                            <a href="{{ url('admin/leadslog') }}">Leads Log</a>
                        </li>
                    </ol>
                </div>
                <div class="topbar-right">
                    <a href="" id="togglebulkr" class="btn btn-default btn-sm light fw600 m10">Bulk Reverse Leads</a>
                </div>
            </header>
            <!-- End: Topbar -->

            <!-- Begin: Content -->
            <section id="content" class="animated fadeIn">

        <div class="tray-bin p10 bg-light dark admin-filter-panels">
        <form action="{{ url('admin/leadslog') }}" method="get">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">

        <input id="date1" class="form-control" name="fromDate" type="hidden" value="{{ date('Y-m-d',strtotime($from)) }}">
        <input id="date2" class="form-control" name="toDate" type="hidden" value="{{ date('Y-m-d',strtotime($to)) }}">

            <div class="row p10">
                <div class="col-md-3">
                    Search User ID: <input type="text" class="form-control" name="user">
                </div>
                <div class="col-md-3">
                    Filter: <select name="filter" id="" class="form-control">
                    <option value="">All</option>
                    <option value="2">Reversed Leads</option>
                    <option value="1">Completed Leads</option>
                    </select>
                </div>
                <div class="col-md-3">
                    Duration: <select name="quickselect" id="quickselect" class="form-control">
                    <option value="today">Today</option>
                    <option value="yesterday">Yesterday</option>
                    <option value="-7 days" selected>Last 7 Days</option>
                    <option value="this month">This Month</option>
                    <option value="last month">Last Month</option>
                    <option value="ytd">YTD</option>
                    <option value="alltime">All Time</option>
                    </select>
                </div>
                <div class="col-md-3">
                    &nbsp; <button type="submit" class="btn btn-block btn-primary">Submit</button>
                </div>
            </div> 
            </form>     
        </div>

        <div class="panel bulkreverse" style="display:none;">
            <div class="panel-heading"><div class="panel-title">Bulk Reverse Leads</div></div>
            <div class="panel-body">
                <div class="row pb10">
                <div class="col-md-2">
                    <input type="text" class="form-control" placeholder="User ID" name="buserid">
                </div>
                <div class="col-md-2">
                    <input type="text" class="form-control" placeholder="Camp ID" name="bcampid">
                </div>
                <div class="col-md-3">
                    <select class="form-control" name="bnetwork">
                        @foreach ($networks as $network)
                        <option value="{{ $network }}">{{ $network }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="col-md-3">
                    <input type="text" class="form-control" placeholder="Number of Leads" name="bnleads">
                </div>
                <div class="col-md-2">
                    <button type="submit" id="brsubmit" class="btn btn-block btn-primary">Submit</button>
                </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <span class="octicon octicon-info"></span> Please make sure above details are correct. Reversed leads cannot be credited back.
                    </div>
                </div>
            </div>
        </div>

        <div class="panel">
            <div class="panel-heading"><div class="panel-title">Leads</div><span class="panel-controls">{{ date('Y-m-d',strtotime($from)) }} to {{ date('Y-m-d',strtotime($to)) }}</span></div>
            <div class="panel-body pn">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">

                <table class="table-bordered table">
                    <thead>
                        <tr>
                            <th>OID</th>
                            <th>Offer Name</th>
                            <th>Network</th>
                            <th>UID</th>
                            <th>User</th>
                            <th>CID</th>
                            <th>Type</th>
                            <th>IP</th>
                            <th>Country</th>
                            <th>UA</th>
                            <th>Status</th>
                            <th>Payout</th>
                            <th>Date</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($leads as $lead)
                            <tr>
                                <td>{{ $lead->campid }}</td>
                                <td>{{ $lead->offname }}</td>
                                <td>{{ $lead->network }}</td>
                                <td>{{ $lead->user_id }}</td>
                                <td>{{ $lead->user->chathandle }}</td>
                                <td>{{ $lead->locker_id }}</td>
                                <td>{{ ucfirst($lead->locker_type) }}</td>
                                <td>{{ long2ip($lead->ip) }}</td>
                                <td>{{ $lead->country }}</td>
                                <td>{{ $lead->ua }}</td>
                                <td> @if($lead->status == 1) <a href="#" id="status" data-type="select" data-value="{{ $lead->status }}" data-pk="{{ $lead->id }}">Complete</a> @elseif($lead->status == 0) Pending @else <a href="#" style="color:red;">Reversed</a> @endif </td>
                                <td>${{ $lead->payout }}</td>
                                <td>{{ date('d M Y', strtotime($lead->timestamp)) }}</td>
                            </tr>
                        @endforeach
                    </tbody>
                    <tfoot>
                        <tr>
                            <td colspan="3" class="p30" style="border-right:none;"><b>{{ $leads->total() }}</b> Lead(s)</td>
                            <td colspan="10" class="text-right p20" style="border-left:none;"><?php echo $leads->render(); ?></td>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>

            </section>
            <!-- End: Content -->

        </section>

    </div>
    <!-- End: Main -->

    <!-- BEGIN: PAGE SCRIPTS -->

    <!-- jQuery -->
    <script type="text/javascript" src="{{ asset('panel/vendor/jquery/jquery_ui/jquery-ui.min.js') }}"></script>

    <!-- Bootstrap -->
    <script type="text/javascript" src="{{ asset('panel/assets/js/bootstrap/bootstrap.min.js') }}"></script>


    <!-- Theme Javascript -->
    <script type="text/javascript" src="{{ asset('panel/assets/js/utility/utility.js') }}"></script>
    <script type="text/javascript" src="{{ asset('panel/assets/js/main.js') }}"></script>

    <!-- Admin Panels  -->
    <script type="text/javascript" src="{{ asset('panel/assets/admin-tools/admin-plugins/admin-panels/json2.js') }}"></script>
    <script type="text/javascript" src="{{ asset('panel/assets/admin-tools/admin-plugins/admin-panels/jquery.ui.touch-punch.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('panel/assets/admin-tools/admin-plugins/admin-panels/adminpanels.js') }}"></script>

    <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.8.3/moment.js"></script>

    <script type="text/javascript" src="{{ asset('panel/assets/js/igrowl.min.js') }}"></script>

    <script src="{{ asset('panel/vendor/editors/xeditable/js/bootstrap-editable.min.js') }}"></script>

     <script>

soundManager.setup({
  url: '{{ asset('panel/assets/swf') }}/',
  onready: function() {
    lead_snd = soundManager.createSound({
        url: '{{ asset('panel/assets/audio/chaching.mp3') }}'
      });
  }
});

var app = angular.module('ngDashboard', ['ngSanitize']);
app.controller('PageController', function($scope,$http,$interval) {
  
  $scope.earnings = '${{ formatnum($today->payout,2) }}';
  $scope.clicks = '{{ formatnum($today->clicks) }}';
  $scope.leads = '{{ formatnum($today->leads) }}';
  $scope.epc = '${{ formatnum(division($today->payout,$today->clicks),2) }}';

var serverdate = new Date({{ date("Y, m, d, H, i, s") }});
function displaytime(){
    serverdate.setSeconds(serverdate.getSeconds() + 1);
    var hh=serverdate.getHours();
    var m=serverdate.getMinutes();
    var s=serverdate.getSeconds();
    
    var dd = "AM";
    var h = hh;
    if (h >= 12) {
        h = hh-12;
        dd = "PM";
    }
    if (h == 0) {
        h = 12;
    }
    m=checkTime(m);
    s=checkTime(s);
    var output = h+":"+m+" "+dd;
    $scope.serverTime = output;
}

// Add leading zero in time
function checkTime(i){if (i<10){i="0" + i;}return i;}

    var getEarn = function() {
        $http.get( "{{ url('admin/earnings') }}").success(function(data, status, headers, config) {
            if ($scope.leads < data.leads) { lead_snd.play(); }
            if (data.clicks == undefined) { $interval.cancel(makeitrain); return; }
            $scope.clicks = data.clicks;
            $scope.leads = data.leads;
            $scope.earnings = data.earnings;
            $scope.epc = data.epc;
        });
    };

    $interval(displaytime, 1000);
    makeitrain = $interval(getEarn, 60000);
});

</script>

    <script type="text/javascript">
        jQuery(document).ready(function() {

            "use strict";

            // Init Theme Core      
            Core.init({
                sbm: "sb-l-c",
            });

            @foreach ($errors->all() as $error)
                $.iGrowl({title:'Whoops!',message: "{{{ $error }}}",type: 'notice'});
            @endforeach

            $('#togglebulkr').click(function() {
                $('.bulkreverse').toggle();
            });

            $('#brsubmit').click(function() {

                $(this).text('Please wait');

                $.post("{{ url('admin/bulkreverse/confirm') }}", { _token: $('[name="_token"]').val(), userid: $('[name="buserid"]').val(), campid: $('[name="bcampid"]').val(), network: $('[name="bnetwork"]').val(), no: $('[name="bnleads"]').val() } )
                .done(function(data) {
                    $.each( data, function( key, value ) {
                                    if (value.title == 'Success') {
                                      $.iGrowl({title:'Success',message: value.message});
                                    } else {
                                      $.iGrowl({title:'Whoops!',message: value.message, type: 'notice'});
                                    }
                    });
                    $('#brsubmit').text('Submit');
                });
            });

            $('a#status').editable({
                url: '{{ url('admin/toggleleadstatus') }}',
                params: { _token: $('[name="_token"]').val() },
                source: [
                {value: 1, text: "Complete"},
                {value: 2, text: "Reversed"},
                ],
                display: function(value, sourceData) {
                   var html = [],
                    checked = $.fn.editableutils.itemsByValue(value, sourceData);
                    var color,text = '';
                   $.each(checked, function(i, v) { if(v.text == 'Reversed') { color = 'red'; text = 'Reversed'; } else { color = 'green'; text = 'Complete' } });
                   $(this).css('color',color);
                   $(this).html(text);
                }
            });

            $('#quickselect').change( function() {

                var d = new Date();
                if ($(this).val() == 'today') { 
                    var from = moment().format('YYYY')+'-'+moment().format('MM')+'-'+moment().format('DD'); 
                    var to = moment().format('YYYY')+'-'+moment().format('MM')+'-'+moment().format('DD'); 

                } else if ($(this).val() == '-7 days') {
                  var from = moment().subtract(7,'days').format('YYYY')+'-'+moment().subtract(7,'days').format('MM')+'-'+moment().subtract(7,'days').format('DD');
                  var to = moment().format('YYYY')+'-'+moment().format('MM')+'-'+moment().format('DD');
                } else if ($(this).val() == 'this month') {
                    var from = moment().startOf('month').format('YYYY')+'-'+moment().startOf('month').format('MM')+'-'+moment().startOf('month').format('DD');
                    var to = moment().format('YYYY')+'-'+moment().format('MM')+'-'+moment().format('DD');
                } else if ($(this).val() == 'yesterday') {
                    var from = moment().subtract(1,'days').format('YYYY')+'-'+moment().subtract(1,'days').format('MM')+'-'+moment().subtract(1,'days').format('DD');
                    var to = moment().subtract(1,'days').format('YYYY')+'-'+moment().subtract(1,'days').format('MM')+'-'+moment().subtract(1,'days').format('DD');
                } else if ($(this).val() == 'last month') {
                    var from = moment().subtract(1,'months').startOf('month').format('YYYY')+'-'+moment().subtract(1,'months').startOf('month').format('MM')+'-'+moment().subtract(1,'months').startOf('month').format('DD');
                    var to = moment().subtract(1,'months').endOf('month').format('YYYY')+'-'+moment().subtract(1,'months').endOf('month').format('MM')+'-'+moment().subtract(1,'months').endOf('month').format('DD');
                } else if ($(this).val() == 'ytd') {
                    var from = moment().startOf('year').format('YYYY')+'-'+moment().startOf('year').format('MM')+'-'+moment().startOf('year').format('DD');
                    var to = moment().format('YYYY')+'-'+moment().format('MM')+'-'+moment().format('DD'); 
                } else if ($(this).val() == 'alltime') {
                    var from = '2015-01-01';
                    var to = moment().format('YYYY')+'-'+moment().format('MM')+'-'+moment().format('DD'); 
                }
                    $('#date1').val(from); 
                    $('#date2').val(to); 
            });

            $('.admin-panels').adminpanel({
                grid: '.admin-grid',
                draggable: false,
                preserveGrid: true,
                mobile: true,
                callback: function() {
                    bootbox.confirm('<h3>A Custom Callback!</h3>', function() {});
                },
                onFinish: function() {
                    $('.admin-panels').addClass('animated fadeIn').removeClass('fade-onload');

                },
                onSave: function() {
                    $(window).trigger('resize');
                }
            });

            $('.ann-tab').click(function() {
                $('.ann-tab').removeClass('active');
                $(this).addClass('active');

                $('.ann-body').addClass('hidden');
                $($(this).attr('href')).removeClass('hidden')
            });


        });
    </script>

    <!-- END: PAGE SCRIPTS -->

</body>

</html>
