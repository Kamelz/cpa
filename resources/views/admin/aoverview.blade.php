<!DOCTYPE html>
<html ng-app="ngDashboard" ng-Controller="PageController">

<head>
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <title>AdGodMedia - Overview</title>
    <meta name="keywords" content="{{ $gset->keywords }}" />
    <meta name="description" content="{{ $gset->description }}">

    <meta name="csrf-token" content="{{ csrf_token() }}" />

    <meta name="author" content="AdminDesigns">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- Font CSS (Via CDN) -->
    <link rel='stylesheet' type='text/css' href='//fonts.googleapis.com/css?family=Open+Sans:400,600,700'>
    <link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Roboto:300,400,500,700">

    <!-- Theme CSS -->
    <link rel="stylesheet" type="text/css" href="{{ asset('panel/assets/skin/default_skin/css/theme.css') }}">

    <!-- Custom CSS -->
    <link rel="stylesheet" type="text/css" href="{{ asset('panel/assets/skin/default_skin/css/custom.css') }}">

    <!-- Admin Panels CSS -->
    <link rel="stylesheet" type="text/css" href="{{ asset('panel/assets/admin-tools/admin-plugins/admin-panels/adminpanels.css') }}">

    <link rel="stylesheet" href="{{ asset('panel/css/igrowl.min.css') }}">

    <link rel="stylesheet" type="text/css" href="{{ asset('panel/vendor/plugins/datepicker/css/bootstrap-datetimepicker.min.css') }}">

    <link href="{{ asset('panel/vendor/plugins/datatables/media/css/dataTables.bootstrap.css') }}" rel="stylesheet" type="text/css">

    <!-- Favicon -->
    <link rel="shortcut icon" href="{{ asset('favicon.ico') }}">

    <script type="text/javascript" src="{{ asset('panel/vendor/jquery/jquery-1.11.1.min.js') }}"></script>
    <script src="//ajax.googleapis.com/ajax/libs/angularjs/1.2.26/angular.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.3.14/angular-sanitize.min.js"></script>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->

@include('ganalytics') </head>

<body class="dashboard-page sb-l-o sb-r-c">

    <!-- Start: Main -->
    <div id="main">

    @include('admin.aheader')

    @include('admin.asidebar')



        <!-- Start: Content-Wrapper -->
        <section id="content_wrapper">

            <!-- Start: Topbar -->
            <header id="topbar">
                <div class="topbar-left">
                    <ol class="breadcrumb">
                        <li class="crumb-active">
                            <a href="{{ url('admin/overview') }}">Stats Overview</a>
                        </li>
                    </ol>
                </div>
                <div class="topbar-right">
                    <a href="" id="togglefilter" class="btn btn-default btn-sm light fw600 m10" >Show/Hide Filter</a>
                </div>
            </header>
            <!-- End: Topbar -->

            <!-- Begin: Content -->
            <section id="content" class="animated fadeIn table-layout">

            <div class="tray tray-center p25 va-t posr">
              
<div class="panel" id="spy3">
<div class="panel-heading">
<span class="panel-icon"><i class="fa fa-pencil"></i>
</span>
<span class="panel-title"> Daily Overview</span><span class="panel-controls">{{ date('Y-m-d',strtotime($from)) }} to {{ date('Y-m-d',strtotime($to)) }}</span>
</div>
<div class="panel-body dark">
<div class="monthchart" style="height: 220px; width: 100%;"></div>
</div>
</div>

<div class="panel panel-visible" id="spy1">
<div class="panel-heading">
<div class="panel-title hidden-xs">
<span class="glyphicon glyphicon-tasks"></span>Overall Network Stats</div>
</div>
<div class="panel-body pn">
<table class="table table-striped table-hover" cellspacing="0" width="100%">
<thead>
<tr>
<th>Clicks</th>
<th>Leads</th>
<th>Earnings</th>
<th>CR</th>
<th>EPC</th>
<th>Avg. CPA</th>
</tr>
</thead>
<tbody>
<tr>
  <td>{{ formatnum($users->clicks) }}</td>
  <td>{{ formatnum($users->leads) }}</td>
  <td>${{ formatnum($users->payout,2) }}</td>
  <td>{{ formatnum(division($users->clicks, $users->leads),2) }}%</td>
  <td>${{ formatnum(division($users->payout, $users->clicks),2) }}</td>
  <td>${{ formatnum(division($users->payout, $users->leads),2) }}</td>
</tr>
</tbody>
</table>
</div>
</div>

<div class="panel">
  <div class="panel-heading"><div class="panel-title">Daily User Earnings</div></div>
  <div class="panel-body pn">
    <table class="table table-striped table-hover" cellspacing="0" width="100%" id="datatable">
    <thead>
    <tr>
    <th>Date</th>
    <th>Clicks</th>
    <th>Leads</th>
    <th>Earnings</th>
    <th>CR</th>
    <th>EPC</th>
    <th>Avg. CPA</th>
    </tr>
    </thead>
    <tbody>
    @foreach ($days as $day)
    <tr>
      <td>{{ date('d M Y', strtotime($day->timestamp)) }}</td>
      <td>{{ formatnum($day->clicks) }}</td>
      <td>{{ formatnum($day->leads) }}</td>
      <td>${{ formatnum($day->payout,2) }}</td>
      <td>{{ formatnum(division($day->clicks, $day->leads),2) }}%</td>
      <td>${{ formatnum(division($day->payout, $day->clicks),2) }}</td>
      <td>${{ formatnum(division($day->payout, $users->leads),2) }}</td>
    </tr>
    @endforeach
    </tbody>
    </table>
  </div>
</div>

<div class="panel">
  <div class="panel-heading"><div class="panel-title">Admin Earnings</div></div>
  <div class="panel-body pn">
    <table class="table table-striped table-hover" cellspacing="0" width="100%" id="datatable2">
    <thead>
    <tr>
    <th>Date</th>
    <th>Earnings</th>
    <th>UID</th>
    <th>OID</th>
    <th>Offer Name</th>
    <th>Network</th>
    <th>CID</th>
    <th>Type</th>
    <th>Country</th>
    </tr>
    </thead>
    <tbody>
    @foreach ($adminearns as $adminearn)
    <tr>
      <td>{{ date('d M Y', strtotime($adminearn->timestamp)) }}</td>
      <td>${{ $adminearn->payout }}</td>
      <td>{{ $adminearn->user_id }}</td>
      <td>{{ $adminearn->campid }}</td>
      <td>{{ $adminearn->offname }}</td>
      <td>{{ $adminearn->network }}</td>
      <td>{{ $adminearn->locker_id }}</td>
      <td>{{ ucfirst($adminearn->locker_type) }}</td>
      <td>{{ $adminearn->country }}</td>
    </tr>
    @endforeach
    </tbody>
    </table>
  </div>
</div>

</div>

<aside class="tray tray-right tray290 va-t pn" data-tray-height="match" style="height: 1100px;display:none;">

<div class="p20 admin-form">
<h4 class="mt5 text-muted fw500"> Filter</h4>
<hr class="short">
<h6 class="fw400">Quick Select</h6>
<div class="section mb15">
<label class="field select">
<select class="quickfilter form-control">
<option value="" selected="">Select..</option>
<option value="today">Today</option>
<option value="yesterday">Yesterday</option>
<option value="-7 days">Last 7 Days</option>
<option value="this month">This Month</option>
<option value="last month">Last Month</option>
<option value="ytd">YTD</option>
<option value="alltime">All Time</option>
</select>
<i class="arrow double"></i>
</label>
</div>

<form action="{{ url('admin/overview') }}" method="post">
<input type="hidden" name="_token" value="{{ csrf_token() }}">

<h6 class="fw400">Date Range</h6>
<div class="section row">
<div class="col-md-12">

<div class="input-group mb20">
<input id="date1" class="form-control" placeholder="2015-03-21" name="fromDate" type="text">
<span class="input-group-addon octicon octicon-calendar"></span>
</div>

<div class="input-group mb20">
<input id="date2" class="form-control" placeholder="2015-03-21" name="toDate" type="text">
<span class="input-group-addon octicon octicon-calendar"></span>
</div>

</div>
</div>

<div class="section row">
<div class="col-sm-12">
<button class="btn btn-default btn-sm ph25" type="submit">Update</button>
</div>
</div>
</div>

</form>
</aside>

            </section>
            <!-- End: Content -->

        </section>

    </div>
    <!-- End: Main -->

    <!-- BEGIN: PAGE SCRIPTS -->

    <!-- jQuery -->
    <script type="text/javascript" src="{{ asset('panel/vendor/jquery/jquery_ui/jquery-ui.min.js') }}"></script>

    <!-- Bootstrap -->
    <script type="text/javascript" src="{{ asset('panel/assets/js/bootstrap/bootstrap.min.js') }}"></script>

        <!-- Page Plugins -->
    <script type="text/javascript" src="{{ asset('panel/vendor/plugins/jqueryflot/jquery.flot.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('panel/vendor/plugins/jqueryflot/jquery.flot.resize.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('panel/vendor/plugins/jqueryflot/jquery.flot.pie.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('panel/vendor/plugins/jqueryflot/jquery.flot.tooltip.min.js') }}"></script>

    <!-- Page Javascript -->
    <script type="text/javascript" src="{{ asset('panel/assets/js/pages/charts/flot.js') }}"></script>

    <!-- Theme Javascript -->
    <script type="text/javascript" src="{{ asset('panel/assets/js/utility/utility.js') }}"></script>
    <script type="text/javascript" src="{{ asset('panel/assets/js/main.js') }}"></script>

    <!-- Admin Panels  -->
    <script type="text/javascript" src="{{ asset('panel/assets/admin-tools/admin-plugins/admin-panels/json2.js') }}"></script>
    <script type="text/javascript" src="{{ asset('panel/assets/admin-tools/admin-plugins/admin-panels/jquery.ui.touch-punch.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('panel/assets/admin-tools/admin-plugins/admin-panels/adminpanels.js') }}"></script>

    <script type="text/javascript" src="{{ asset('panel/assets/js/igrowl.min.js') }}"></script>

    <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.8.3/moment.js"></script>

    <script type="text/javascript" src="{{ asset('panel/vendor/plugins/datepicker/js/bootstrap-datetimepicker.min.js') }}"></script>

    <script src="{{ asset('panel/vendor/plugins/datatables/media/js/jquery.dataTables.js') }}"></script>
<script src="{{ asset('panel/vendor/plugins/datatables/media/js/dataTables.bootstrap.js') }}"></script>

<script>

soundManager.setup({
  url: '{{ asset('panel/assets/swf') }}/',
  onready: function() {
    lead_snd = soundManager.createSound({
        url: '{{ asset('panel/assets/audio/chaching.mp3') }}'
      });
  }
});

var app = angular.module('ngDashboard', ['ngSanitize']);
app.controller('PageController', function($scope,$http,$interval) {
  
  $scope.earnings = '${{ formatnum($today->payout,2) }}';
  $scope.clicks = '{{ formatnum($today->clicks) }}';
  $scope.leads = '{{ formatnum($today->leads) }}';
  $scope.epc = '${{ formatnum(division($today->payout,$today->clicks),2) }}';

var serverdate = new Date({{ date("Y, m, d, H, i, s") }});
function displaytime(){
    serverdate.setSeconds(serverdate.getSeconds() + 1);
    var hh=serverdate.getHours();
    var m=serverdate.getMinutes();
    var s=serverdate.getSeconds();
    
    var dd = "AM";
    var h = hh;
    if (h >= 12) {
        h = hh-12;
        dd = "PM";
    }
    if (h == 0) {
        h = 12;
    }
    m=checkTime(m);
    s=checkTime(s);
    var output = h+":"+m+" "+dd;
    $scope.serverTime = output;
}

// Add leading zero in time
function checkTime(i){if (i<10){i="0" + i;}return i;}

    var getEarn = function() {
        $http.get( "{{ url('admin/earnings') }}").success(function(data, status, headers, config) {
            if ($scope.leads < data.leads) { lead_snd.play(); }
            if (data.clicks == undefined) { $interval.cancel(makeitrain); return; }
            $scope.clicks = data.clicks;
            $scope.leads = data.leads;
            $scope.earnings = data.earnings;
            $scope.epc = data.epc;
        });
    };

    $interval(displaytime, 1000);
    makeitrain = $interval(getEarn, 60000);
});

</script>

    <script type="text/javascript">
        jQuery(document).ready(function() {

            "use strict";

            // Init Theme Core      
            Core.init({
                sbm: "sb-l-c",
            });

            FlotCharts.init();

            $('#togglefilter').click(function() {
                $('.tray-right').toggle();
            });

            $('#datatable').dataTable({"sDom": 't<"dt-panelfooter clearfix"ip>',"aaSorting": []});
            $('#datatable2').dataTable({"sDom": 't<"dt-panelfooter clearfix"ip>',"aaSorting": []});


            $('#date1').datetimepicker({format: "YYYY-MM-DD"});
            $('#date2').datetimepicker({format: "YYYY-MM-DD"});

            $('.quickfilter').change( function() {
                var d = new Date();
                if ($(this).val() == 'today') { 
                    var from = moment().format('YYYY')+'-'+moment().format('MM')+'-'+moment().format('DD'); 
                    var to = moment().format('YYYY')+'-'+moment().format('MM')+'-'+moment().format('DD'); 

                } else if ($(this).val() == '-7 days') {
                  var from = moment().subtract(7,'days').format('YYYY')+'-'+moment().subtract(7,'days').format('MM')+'-'+moment().subtract(7,'days').format('DD');
                  var to = moment().format('YYYY')+'-'+moment().format('MM')+'-'+moment().format('DD');
                } else if ($(this).val() == 'this month') {
                    var from = moment().startOf('month').format('YYYY')+'-'+moment().startOf('month').format('MM')+'-'+moment().startOf('month').format('DD');
                    var to = moment().format('YYYY')+'-'+moment().format('MM')+'-'+moment().format('DD');
                } else if ($(this).val() == 'yesterday') {
                    var from = moment().subtract(1,'days').format('YYYY')+'-'+moment().subtract(1,'days').format('MM')+'-'+moment().subtract(1,'days').format('DD');
                    var to = moment().subtract(1,'days').format('YYYY')+'-'+moment().subtract(1,'days').format('MM')+'-'+moment().subtract(1,'days').format('DD');
                } else if ($(this).val() == 'last month') {
                    var from = moment().subtract(1,'months').startOf('month').format('YYYY')+'-'+moment().subtract(1,'months').startOf('month').format('MM')+'-'+moment().subtract(1,'months').startOf('month').format('DD');
                    var to = moment().subtract(1,'months').endOf('month').format('YYYY')+'-'+moment().subtract(1,'months').endOf('month').format('MM')+'-'+moment().subtract(1,'months').endOf('month').format('DD');
                } else if ($(this).val() == 'ytd') {
                    var from = moment().startOf('year').format('YYYY')+'-'+moment().startOf('year').format('MM')+'-'+moment().startOf('year').format('DD');
                    var to = moment().format('YYYY')+'-'+moment().format('MM')+'-'+moment().format('DD'); 
                } else if ($(this).val() == 'alltime') {
                    var from = '2015-01-01';
                    var to = moment().format('YYYY')+'-'+moment().format('MM')+'-'+moment().format('DD'); 
                }
                    $('#date1').val(from); 
                    $('#date2').val(to); 
            });

            var Grid = {
            grid: {
                show: true,
                aboveData: true,
                color: "#bbbbbb",
                labelMargin: 15,
                axisMargin: 0,
                borderWidth: 0,
                borderColor: null,
                minBorderMargin: 5,
                clickable: true,
                hoverable: true,
                autoHighlight: true,
                mouseActiveRadius: 20,
            },
            xaxis: {
              mode: 'time',
              timeformat: '%d %b %Y',
            },
            yaxis: [{
              position: "left",
              tickDecimals: 2,
            }]
        };


        var Colors = [bgPrimary, bgSuccess, bgInfo, bgWarning, bgDanger, bgAlert, bgSystem];

        var earn = [@foreach ($adminchart as $adminearn) [{{ strtotime($adminearn->timestamp)*1000 }} ,{{ $adminearn->payout}}], @endforeach];

            $.plot($(".monthchart"), [{
                data: earn,
                label: 'Earnings',
                points: {
                    show: true,
                    radius: 4
                },
                lines: {
                    show: true
                },
                color: '#70ca63'
            }], Grid);

            function showTooltip(x, y, contents) {
            $('<div id="tooltip">' + contents + '</div>').css( {
                position: 'absolute',
                display: 'none',
                //float: 'left',
                top:  y - 40,
                left: x - 30,
                border: '1px solid #fdd',
                padding: '2px',
                'background-color': '#eee',
                opacity: 0.80
            }).appendTo("body").fadeIn(200);
         }



        var previousPoint = null;
        $(".monthchart").bind("plothover", function (event, pos, item){
            $("#x").text(pos.x.toFixed(2));
            $("#y").text(pos.y.toFixed(2));
            if (item) {
                if (previousPoint != item.dataIndex){
                    previousPoint = item.dataIndex;

                    $("#tooltip").remove();
                    var x = item.datapoint[0].toFixed(2),
                        y = item.datapoint[1].toFixed(2);

                        var append = '';
                        if (item.series.label == 'Earnings' || item.series.label == 'EPC') { append = '$' }

                        showTooltip(item.pageX, item.pageY,
                                item.series.label +": "+append+ y);
                                                    }
                    }
                else {
                    $("#tooltip").remove();
                    previousPoint = null;
                    }

            });

            $('.admin-panels').adminpanel({
                grid: '.admin-grid',
                draggable: false,
                preserveGrid: true,
                mobile: true,
                callback: function() {
                    bootbox.confirm('<h3>A Custom Callback!</h3>', function() {});
                },
                onFinish: function() {
                    $('.admin-panels').addClass('animated fadeIn').removeClass('fade-onload');

                },
                onSave: function() {
                    $(window).trigger('resize');
                }
            });

            $('.ann-tab').click(function() {
                $('.ann-tab').removeClass('active');
                $(this).addClass('active');

                $('.ann-body').addClass('hidden');
                $($(this).attr('href')).removeClass('hidden')
            });


        });
    </script>

    <!-- END: PAGE SCRIPTS -->

</body>

</html>
