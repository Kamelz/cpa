<!DOCTYPE html>
<html ng-app="ngDashboard" ng-controller="PageController">

<head>
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <title>AdGodMedia - Payments</title>
    <meta name="keywords" content="{{ $gset->keywords }}" />
    <meta name="description" content="{{ $gset->description }}">

    <meta name="csrf-token" content="{{ csrf_token() }}" />

    <meta name="author" content="AdminDesigns">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- Font CSS (Via CDN) -->
    <link rel='stylesheet' type='text/css' href='//fonts.googleapis.com/css?family=Open+Sans:400,600,700'>
    <link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Raleway:300,400,500,700">

    <!-- Theme CSS -->
    <link rel="stylesheet" type="text/css" href="{{ asset('panel/assets/skin/default_skin/css/theme.css') }}">

    <!-- Custom CSS -->
    <link rel="stylesheet" type="text/css" href="{{ asset('panel/assets/skin/default_skin/css/custom.css') }}">

    <!-- Admin Panels CSS -->
    <link rel="stylesheet" type="text/css" href="{{ asset('panel/assets/admin-tools/admin-plugins/admin-panels/adminpanels.css') }}">

    <link rel="stylesheet" href="{{ asset('panel/css/igrowl.min.css') }}">

    <link href="{{ asset('panel/vendor/editors/xeditable/css/bootstrap-editable.css') }}" rel="stylesheet" type="text/css">

    <!-- Favicon -->
    <link rel="shortcut icon" href="{{ asset('favicon.ico') }}">

    <script type="text/javascript" src="{{ asset('panel/vendor/jquery/jquery-1.11.1.min.js') }}"></script>
    <script src="//ajax.googleapis.com/ajax/libs/angularjs/1.2.26/angular.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.3.14/angular-sanitize.min.js"></script>


    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->

@include('ganalytics') </head>

<body class="dashboard-page sb-l-o sb-r-c">

    <!-- Start: Main -->
    <div id="main">

    @include('admin.aheader')

    @include('admin.asidebar')



        <!-- Start: Content-Wrapper -->
        <section id="content_wrapper">

            <!-- Start: Topbar -->
            <header id="topbar">
                <div class="topbar-left">
                    <ol class="breadcrumb">
                        <li class="crumb-active">
                            <a href="{{ url('admin/payments') }}">Payments</a>
                        </li>
                    </ol>
                </div>
            </header>
            <!-- End: Topbar -->

            <!-- Begin: Content -->
            <section id="content" class="animated fadeIn">

            

        <div class="panel" id="messages">
            <div class="panel-heading"><div class="panel-title">Payments</div></div>
            <div class="panel-body pn">
                <table class="table-bordered table">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>UID</th>
                            <th>User</th>
                            <th>Request Date</th>
                            <th>Payment Date</th>
                            <th>Amount</th>
                            <th>Method</th>
                            <th>Schedule</th>
                            <th>Status</th>
                            <th>Priority</th>
                            <th>Balance</th>
                            <th>Fee</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($payments as $payment)
                            <tr>
                               <td>{{ $payment->id }}</td>
                               <td>{{ $payment->user_id }}</td>
                               <td>{{ $payment->user->chathandle }}</td>
                               <td>{{ date('j F Y',strtotime($payment->request_date)) }}</td>
                               <td>@if (is_null($payment->payment_date)) - @else {{ date('j F Y',strtotime($payment->payment_date)) }} @endif</td>
                               <td>$<a href="#" id="amount" data-type="text" data-pk="{{ $payment->id }}" data-title="Enter amount">{{ $payment->amount }}</a></td>
                               <td>{{ $payment->payment_method }}</td>
                               <td>{{ $payment->schedule }}</td>
                               <td><a href="#" id="status" data-type="select" data-pk="{{ $payment->id }}" data-value="{{ $payment->status }}">{{ $payment->status }}</a>
                               </td>
                               <td><a href="#" id="priority" data-type="select" data-pk="{{ $payment->id }}" data-value="{{ $payment->priority }}">{{ $payment->priority }}</a></td>
                               <td>${{ $payment->user->balance }}</td>
                               <td>$<a href="#" id="fee" data-type="text" data-pk="{{ $payment->id }}" data-title="Enter amount">{{ $payment->fee }}</a></td>
                               <td><a href="{{ url('admin/payments/view/'.$payment->id) }}" class="btn btn-xs btn-success" data-toggle="modal">View</a> <a href="{{ url('admin/confirmdelete/paymentrequest/'.$payment->id) }}" data-toggle="modal" class="btn btn-xs btn-danger">Delete</a></td>
                            </tr>
                        @endforeach
                    </tbody>
                    <tfoot>
                        <tr>
                            <td colspan="3" class="p30" style="border-right:none;"><b>{{ $payments->total() }}</b> Payment Requests</td>
                            <td colspan="9" class="text-right p20" style="border-left:none;"><?php echo $payments->render(); ?></td>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
        </div>

        <input type="hidden" name="_token" value="{{ csrf_token() }}">


            </section>
            <!-- End: Content -->

        </section>

    </div>
    <!-- End: Main -->

    <!-- BEGIN: PAGE SCRIPTS -->

    <!-- jQuery -->
    <script type="text/javascript" src="{{ asset('panel/vendor/jquery/jquery_ui/jquery-ui.min.js') }}"></script>

    <!-- Bootstrap -->
    <script type="text/javascript" src="{{ asset('panel/assets/js/bootstrap/bootstrap.min.js') }}"></script>

    <!-- Theme Javascript -->
    <script type="text/javascript" src="{{ asset('panel/assets/js/utility/utility.js') }}"></script>
    <script type="text/javascript" src="{{ asset('panel/assets/js/main.js') }}"></script>

    <!-- Admin Panels  -->
    <script type="text/javascript" src="{{ asset('panel/assets/admin-tools/admin-plugins/admin-panels/json2.js') }}"></script>
    <script type="text/javascript" src="{{ asset('panel/assets/admin-tools/admin-plugins/admin-panels/jquery.ui.touch-punch.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('panel/assets/admin-tools/admin-plugins/admin-panels/adminpanels.js') }}"></script>

    <script type="text/javascript" src="{{ asset('panel/assets/js/igrowl.min.js') }}"></script>

    <script src="{{ asset('panel/vendor/editors/xeditable/js/bootstrap-editable.min.js') }}"></script>

     <script>

soundManager.setup({
  url: '{{ asset('panel/assets/swf') }}/',
  onready: function() {
    lead_snd = soundManager.createSound({
        url: '{{ asset('panel/assets/audio/chaching.mp3') }}'
      });
  }
});

var app = angular.module('ngDashboard', ['ngSanitize']);
app.controller('PageController', function($scope,$http,$interval) {
  
  $scope.earnings = '${{ formatnum($today->payout,2) }}';
  $scope.clicks = '{{ formatnum($today->clicks) }}';
  $scope.leads = '{{ formatnum($today->leads) }}';
  $scope.epc = '${{ formatnum(division($today->payout,$today->clicks),2) }}';

var serverdate = new Date({{ date("Y, m, d, H, i, s") }});
function displaytime(){
    serverdate.setSeconds(serverdate.getSeconds() + 1);
    var hh=serverdate.getHours();
    var m=serverdate.getMinutes();
    var s=serverdate.getSeconds();
    
    var dd = "AM";
    var h = hh;
    if (h >= 12) {
        h = hh-12;
        dd = "PM";
    }
    if (h == 0) {
        h = 12;
    }
    m=checkTime(m);
    s=checkTime(s);
    var output = h+":"+m+" "+dd;
    $scope.serverTime = output;
}

// Add leading zero in time
function checkTime(i){if (i<10){i="0" + i;}return i;}

    var getEarn = function() {
        $http.get( "{{ url('admin/earnings') }}").success(function(data, status, headers, config) {
            if ($scope.leads < data.leads) { lead_snd.play(); }
            if (data.clicks == undefined) { $interval.cancel(makeitrain); return; }
            $scope.clicks = data.clicks;
            $scope.leads = data.leads;
            $scope.earnings = data.earnings;
            $scope.epc = data.epc;
        });
    };

    $interval(displaytime, 1000);
    makeitrain = $interval(getEarn, 60000);
});

</script>

    <script type="text/javascript">

        function deletePayment(id) {
            if (confirm("Are you sure?") == true) {
                location.href = '{{ url('admin/payments/delete') }}/'+id;
            }
        }

        jQuery(document).ready(function() {

            "use strict";

            // Init Theme Core      
            Core.init({
                sbm: "sb-l-c",
            });


            @foreach ($errors->all() as $error)
                $.iGrowl({title:'Whoops!',message: "{{{ $error }}}",type: 'notice'});
            @endforeach


            $('a#amount').editable({
                url: '{{ url('admin/payments') }}',
                params: { _token: $('[name="_token"]').val() }
            });
            $('a#status').editable({
                url: '{{ url('admin/payments') }}',
                params: { _token: $('[name="_token"]').val() },
                source: [
                    {value: 'Pending', text: 'Pending'},
                    {value: 'Complete', text: 'Complete'},
                    {value: 'Denied', text: 'Denied'}
                ],
                display: function(value, sourceData) {
                   var html = [],
                    checked = $.fn.editableutils.itemsByValue(value, sourceData);
                    var color,text = '';
                   $.each(checked, function(i, v) { if(v.text == 'Pending') { color = 'orange'; text = 'Pending'; } else if (v.text == 'Denied') { color = 'red'; text = 'Denied'; } else { color = 'green'; text = 'Complete' } });
                   $(this).css('color',color);
                   $(this).html(text);
                }
            });
            $('a#priority').editable({
                url: '{{ url('admin/payments') }}',
                params: { _token: $('[name="_token"]').val() },
                source: [
                    {value: 'Normal', text: 'Normal'},
                    {value: 'Quick', text: 'Quick'}
                ]
            });
            $('a#fee').editable({
                url: '{{ url('admin/payments') }}',
                params: { _token: $('[name="_token"]').val() }
            });

            $('.admin-panels').adminpanel({
                grid: '.admin-grid',
                draggable: false,
                preserveGrid: true,
                mobile: true,
                callback: function() {
                    bootbox.confirm('<h3>A Custom Callback!</h3>', function() {});
                },
                onFinish: function() {
                    $('.admin-panels').addClass('animated fadeIn').removeClass('fade-onload');

                },
                onSave: function() {
                    $(window).trigger('resize');
                }
            });

            $('.ann-tab').click(function() {
                $('.ann-tab').removeClass('active');
                $(this).addClass('active');

                $('.ann-body').addClass('hidden');
                $($(this).attr('href')).removeClass('hidden')
            });


        });
    </script>

    <!-- END: PAGE SCRIPTS -->

</body>

</html>
