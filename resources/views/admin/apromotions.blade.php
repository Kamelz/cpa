<!DOCTYPE html>
<html ng-app="ngDashboard" ng-controller="PageController">

<head>
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <title>AdGodMedia - Promotions</title>
    <meta name="keywords" content="{{ $gset->keywords }}" />
    <meta name="description" content="{{ $gset->description }}">

    <meta name="csrf-token" content="{{ csrf_token() }}" />

    <meta name="author" content="AdminDesigns">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- Font CSS (Via CDN) -->
    <link rel='stylesheet' type='text/css' href='//fonts.googleapis.com/css?family=Open+Sans:400,600,700'>
    <link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Raleway:300,400,500,700">

    <!-- Theme CSS -->
    <link rel="stylesheet" type="text/css" href="{{ asset('panel/assets/skin/default_skin/css/theme.css') }}">

    <!-- Custom CSS -->
    <link rel="stylesheet" type="text/css" href="{{ asset('panel/assets/skin/default_skin/css/custom.css') }}">

    <!-- Admin Panels CSS -->
    <link rel="stylesheet" type="text/css" href="{{ asset('panel/assets/admin-tools/admin-plugins/admin-panels/adminpanels.css') }}">

    <link rel="stylesheet" href="{{ asset('panel/css/igrowl.min.css') }}">

    <link href="{{ asset('panel/vendor/editors/xeditable/css/bootstrap-editable.css') }}" rel="stylesheet" type="text/css">

    <!-- Favicon -->
    <link rel="shortcut icon" href="{{ asset('favicon.ico') }}">

    <script type="text/javascript" src="{{ asset('panel/vendor/jquery/jquery-1.11.1.min.js') }}"></script>
    <script src="//ajax.googleapis.com/ajax/libs/angularjs/1.2.26/angular.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.3.14/angular-sanitize.min.js"></script>


    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->

@include('ganalytics') </head>

<body class="dashboard-page sb-l-o sb-r-c">

    <!-- Start: Main -->
    <div id="main">

    @include('admin.aheader')

    @include('admin.asidebar')



        <!-- Start: Content-Wrapper -->
        <section id="content_wrapper">

            <!-- Start: Topbar -->
            <header id="topbar">
                <div class="topbar-left">
                    <ol class="breadcrumb">
                        <li class="crumb-active">
                            <a href="{{ url('admin/promotions') }}">Promotions</a>
                        </li>
                    </ol>
                </div>
            </header>
            <!-- End: Topbar -->

            <!-- Begin: Content -->
            <section id="content" class="animated fadeIn">

            <div class="panel" id="messages">
            <div class="panel-heading"><div class="panel-title">Promotions</div></div>
            <div class="panel-body pn">
                <table class="table-bordered table">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Name</th>
                            <th>Bonus Percentage</th>
                            <th>Leads</th>
                            <th>Hours</th>
                            <th>Date Added</th>
                            <th>Status</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        @forelse ($promotions as $promo)
                            <tr>
                                <td>{{ $promo->id }}</td>
                                <td>{{ $promo->name }} @if ($globalpromo['promo_id'] == $promo->id)
                                <span class="text-success">(Activated {{ $timeago }})</span> @endif </td>
                                <td>{{ $promo->bonus_percentage }}%</td>
                                <td>{{ $promo->leads }}</td>
                                <td>{{ $promo->hours }}</td>
                                <td>{{ date('d M Y',strtotime($promo->timestamp)) }}</td>
                                <td><a href="#" id="status" data-type="select" data-value="{{ $promo->active }}" data-pk="{{ $promo->id }}"> @if($promo->active == 1) Active @else Disabled @endif </a></td>
                                <td><a href="{{ url('admin/confirmdelete/promo/'.$promo->id) }}" data-toggle="modal" class="btn btn-xs btn-danger">Delete</a>
                                @if ($promo->leads == 0 && $globalpromo['promo_id'] != $promo->id)
                                <a href="{{ url('admin/activateconfirm/promo/'.$promo->id) }}" data-toggle="modal" class="btn btn-xs btn-success">Activate Now</a>
                                @endif</td>
                            </tr>
                        @empty
                            <tr><td colspan="6">No promotions</td></tr>
                        @endforelse
                    </tbody>
                </table>
            </div>
        </div>

	<div class="panel">
      <div class="panel-heading"><div class="panel-title">Add New Promotion</div></div>
      <div class="panel-body p30">

      <input type="hidden" name="_token" value="{{ csrf_token() }}">

      <div class="row p20">
          <span class="octicon octicon-info"></span> Enter number of leads as 0 to specify global promotion
      </div>

        <div class="row p10">
          <div class="col-md-2">
            <label for="" class="control-label">Name</label>
          </div>
          <div class="col-md-10">
            <input type="text" class="form-control" name="name">
          </div>
        </div>

        <div class="row p10">
          <div class="col-md-2">
            <label for="" class="control-label">Bonus</label>
          </div>
          <div class="col-md-10">
                <div class="input-group">
                  <input class="form-control" type="text" name="bonus">
                  <span class="input-group-addon">%</span>
                </div>
          </div>
        </div>

        <div class="row p10">
          <div class="col-md-2">
            <label for="" class="control-label">Leads</label>
          </div>
          <div class="col-md-10">
            <input type="text" class="form-control" name="leads">
          </div>
        </div>

        <div class="row p10">
          <div class="col-md-2">
            <label for="" class="control-label">Hours</label>
          </div>
          <div class="col-md-10">
            <input type="text" class="form-control" name="hours">
          </div>
        </div>

        <div class="row p10">
          <div class="col-md-2"></div>
          <div class="col-md-10"><a href="" class="submit btn btn-primary">Add Promotion</a></div>
        </div>

      </div>      
        </div>


            </section>
            <!-- End: Content -->

        </section>

    </div>
    <!-- End: Main -->

    <!-- BEGIN: PAGE SCRIPTS -->

    <!-- jQuery -->
    <script type="text/javascript" src="{{ asset('panel/vendor/jquery/jquery_ui/jquery-ui.min.js') }}"></script>

    <!-- Bootstrap -->
    <script type="text/javascript" src="{{ asset('panel/assets/js/bootstrap/bootstrap.min.js') }}"></script>

    <!-- Theme Javascript -->
    <script type="text/javascript" src="{{ asset('panel/assets/js/utility/utility.js') }}"></script>
    <script type="text/javascript" src="{{ asset('panel/assets/js/main.js') }}"></script>
   
    <!-- Admin Panels  -->
    <script type="text/javascript" src="{{ asset('panel/assets/admin-tools/admin-plugins/admin-panels/json2.js') }}"></script>
    <script type="text/javascript" src="{{ asset('panel/assets/admin-tools/admin-plugins/admin-panels/jquery.ui.touch-punch.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('panel/assets/admin-tools/admin-plugins/admin-panels/adminpanels.js') }}"></script>

    <script type="text/javascript" src="{{ asset('panel/assets/js/igrowl.min.js') }}"></script>

    <script src="{{ asset('panel/vendor/editors/xeditable/js/bootstrap-editable.min.js') }}"></script>

    <script>

soundManager.setup({
  url: '{{ asset('panel/assets/swf') }}/',
  onready: function() {
    lead_snd = soundManager.createSound({
        url: '{{ asset('panel/assets/audio/chaching.mp3') }}'
      });
  }
});

var app = angular.module('ngDashboard', ['ngSanitize']);
app.controller('PageController', function($scope,$http,$interval) {
  
  $scope.earnings = '${{ formatnum($today->payout,2) }}';
  $scope.clicks = '{{ formatnum($today->clicks) }}';
  $scope.leads = '{{ formatnum($today->leads) }}';
  $scope.epc = '${{ formatnum(division($today->payout,$today->clicks),2) }}';

var serverdate = new Date({{ date("Y, m, d, H, i, s") }});
function displaytime(){
    serverdate.setSeconds(serverdate.getSeconds() + 1);
    var hh=serverdate.getHours();
    var m=serverdate.getMinutes();
    var s=serverdate.getSeconds();
    
    var dd = "AM";
    var h = hh;
    if (h >= 12) {
        h = hh-12;
        dd = "PM";
    }
    if (h == 0) {
        h = 12;
    }
    m=checkTime(m);
    s=checkTime(s);
    var output = h+":"+m+" "+dd;
    $scope.serverTime = output;
}

// Add leading zero in time
function checkTime(i){if (i<10){i="0" + i;}return i;}

    var getEarn = function() {
        $http.get( "{{ url('admin/earnings') }}").success(function(data, status, headers, config) {
            if ($scope.leads < data.leads) { lead_snd.play(); }
            if (data.clicks == undefined) { $interval.cancel(makeitrain); return; }
            $scope.clicks = data.clicks;
            $scope.leads = data.leads;
            $scope.earnings = data.earnings;
            $scope.epc = data.epc;
        });
    };

    $interval(displaytime, 1000);
    makeitrain = $interval(getEarn, 60000);
});

</script>

    <script type="text/javascript">
        jQuery(document).ready(function() {

            "use strict";

            // Init Theme Core      
            Core.init({
                sbm: "sb-l-c",
            });

            @foreach ($errors->all() as $error)
                $.iGrowl({title:'Whoops!',message: "{{{ $error }}}",type: 'notice'});
            @endforeach

            @if ($successmsg)
                $.iGrowl({title:'Success',message: "{{ $successmsg }}",type: 'success'});
            @endif

            $('a#deletepromo').click(function() {
                    if (confirm("Are you really sure?") == true) {
                        location.href = '{{ url('admin/promo/delete') }}/'+$(this).data('pk');
                    }
            });

            $('.submit').click(function() {
                $.post("{{ url('admin/promotion/add') }}", { _token: $('[name="_token"]').val(), name: $('[name="name"]').val(), bonus: $('[name="bonus"]').val(), leads: $('[name="leads"]').val(), hours: $('[name="hours"]').val()  } )
                .done(function(data) {
                    $.each( data, function( key, value ) {
                                    if (value.title == 'Success') {
                                      $.iGrowl({title:'Success',message: value.message});
                                      window.location.href = "{{ url('admin/promotions') }}";
                                    } else {
                                      $.iGrowl({title:'Whoops!',message: value.message, type: 'notice'});
                                    }
                    });
                });
            });


            $('a#status').editable({
                url: '{{ url('admin/promo/toggle') }}',
                params: { _token: $('[name="_token"]').val() },
                source: [{value: 1, text: "Active"}, {value: 0, text: "Disabled"}],
                display: function(value, sourceData) {
                   var html = [],
                    checked = $.fn.editableutils.itemsByValue(value, sourceData);
                    var color,text = '';
                   $.each(checked, function(i, v) { if(v.text == 'Disabled') { color = 'red'; text = 'Disabled'; } else { color = 'green'; text = 'Active' } });
                   $(this).css('color',color);
                   $(this).html(text);
                }
            });

            $('.admin-panels').adminpanel({
                grid: '.admin-grid',
                draggable: false,
                preserveGrid: true,
                mobile: true,
                callback: function() {
                    bootbox.confirm('<h3>A Custom Callback!</h3>', function() {});
                },
                onFinish: function() {
                    $('.admin-panels').addClass('animated fadeIn').removeClass('fade-onload');
                },
                onSave: function() {
                    $(window).trigger('resize');
                }
            });

            $('.ann-tab').click(function() {
                $('.ann-tab').removeClass('active');
                $(this).addClass('active');

                $('.ann-body').addClass('hidden');
                $($(this).attr('href')).removeClass('hidden')
            });
        });
    </script>

    <!-- END: PAGE SCRIPTS -->

</body>

</html>
