         <!-- Start: Sidebar -->
        <aside id="sidebar_left" class="nano nano-primary">
            <div class="nano-content">               

                <!-- sidebar menu -->
                <ul class="nav sidebar-menu">
                    <li class="sidebar-label pt20">Earnings Today</li>
                    <li>
                        <a href="{{ url('dashboard') }}" data-toggle="tooltip" title="Updated every 60 seconds">
                            <span class="octicon octicon-radio-tower"></span>
                            <span class="sidebar-title text-success fs20" ng-bind="earnings"></span>
                        </a>
                    </li>
                    <li class="sidebar-label pt20">Dashboard</li>
                    <li>
                        <a href="{{ url('admin') }}">
                            <span class="octicon octicon-home"></span>
                            <span class="sidebar-title">Home</span>
                        </a>
                    </li>

                    <li class="sidebar-label pt20">Statistics</li>
                    <li>
                        <a href="{{ url('admin/overview') }}">
                            <span class="octicon octicon-eye"></span>
                            <span class="sidebar-title">Overview</span>
                        </a>
                    </li>
                     <li>
                        <a href="#" class="accordion-toggle">
                            <span class="octicon octicon-database"></span>
                            <span class="sidebar-title">Detailed Stats</span>
                            <span class="caret"></span>
                        </a>
                        <ul class="nav sub-nav">
                            <li>
                                <a href="{{ url('admin/downloadlog') }}">
                                    <span class="octicon octicon-cloud-download"></span> Downloads Log</a>
                            </li>
                            <li>
                                <a href="{{ url('admin/leadslog') }}">
                                    <span class="octicon octicon-checklist"></span> Leads Log</a>
                            </li>
                            <li>
                                <a href="{{ url('admin/sitereferrer') }}">
                                    <span class="octicon octicon-alignment-aligned-to"></span> Referrer Log</a>
                            </li>
                            @if($role == "super_admin")
                            <li class="sa">
                                <a href="{{ url('admin/loginlog') }}">
                                    <span class="octicon octicon-sign-in"></span> Login Log</a>
                            </li>
                            @endif
                        </ul>
                    </li>

                    <li class="sidebar-label pt20">Management</li>
                    <li>
                        <a href="{{ url('admin/tickets') }}">
                            <span class="octicon octicon-mail-read"></span>
                            <span class="sidebar-title">Tickets</span>
                        </a>
                    </li>
                    <li>
                        <a href="{{ url('admin/payments') }}">
                            <span class="octicon octicon-credit-card"></span>
                            <span class="sidebar-title">Payments</span>
                        </a>
                    </li>
                    <li>
                        <a href="#" class="accordion-toggle">
                            <span class="octicon octicon-organization"></span>
                            <span class="sidebar-title">Users</span>
                            <span class="caret"></span>
                        </a>
                        <ul class="nav sub-nav">
                            <li>
                                <a href="{{ url('admin/publishers') }}">
                                    <span class="octicon octicon-person"></span> Publishers</a>
                            </li>
                            @if($role == "super_admin")
                            <li class="sa">
                                <a href="{{ url('admin/administrators') }}">
                                    <span class="octicon octicon-gist-secret"></span> Administrators</a>
                            </li>
                            @endif
                        </ul>
                    </li>
                    <li>
                        <a href="#" class="accordion-toggle">
                            <span class="octicon octicon-server"></span>
                            <span class="sidebar-title">Offers</span>
                            <span class="caret"></span>
                        </a>
                        <ul class="nav sub-nav">
                            <li>
                                <a href="{{ url('admin/offers') }}">
                                    <span class="octicon octicon-server"></span> List / Optimize Offers</a>
                            </li>
                            @if($role == "super_admin")
                            <li class="sa">
                                <a href="{{ url('admin/fetchoffers') }}">
                                    <span class="octicon octicon-versions"></span> Network Manager</a>
                            </li>
                            @endif
                            <li>
                                <a href="{{ url('admin/expiredoffers') }}">
                                    <span class="octicon octicon-issue-reopened"></span> Expired Offers</a>
                            </li>
                        </ul>
                    </li>

                    <li>
                        <a href="#" class="accordion-toggle">
                            <span class="octicon octicon-file-submodule"></span>
                            <span class="sidebar-title">Content</span>
                            <span class="caret"></span>
                        </a>
                        <ul class="nav sub-nav">
                            <li>
                                <a href="{{ url('admin/files') }}">
                                    <span class="octicon octicon-file-directory"></span>Files</a>
                            </li>
                            <li>
                                <a href="{{ url('admin/gateways') }}">
                                    <span class="octicon octicon-browser"></span>Gateways</a>
                            </li>
                            <li>
                                <a href="{{ url('admin/links') }}">
                                    <span class="octicon octicon-link"></span>Links</a>
                            </li>
                            <li>
                                <a href="{{ url('admin/notes') }}">
                                    <span class="octicon octicon-file-text"></span>Notes</a>
                            </li>
                            <li>
                                <a href="{{ url('admin/contentreports') }}">
                                    <span class="octicon octicon-stop"></span>Content Reports</a>
                            </li>
                            <li>
                                <a href="{{ url('admin/mirrors') }}">
                                    <span class="octicon octicon-color-mode"></span>Mirrors</a>
                            </li>
                        </ul>
                    </li>

                    @if($role == "super_admin")
                    <li>
                        <a href="#" class="accordion-toggle">
                            <span class="octicon octicon-alert"></span>
                            <span class="sidebar-title">Blocks & Bans</span>
                            <span class="caret"></span>
                        </a>
                        <ul class="nav sub-nav">
                            <li class="sa">
                                <a href="{{ url('admin/banusers') }}">
                                    <span class="octicon octicon-alert"></span> Ban User</a>
                            </li>
                            <li class="sa">
                                <a href="{{ url('admin/subidblock') }}">
                                    <span class="octicon octicon-alert"></span> Block SubID</a>
                            </li>
                            <li class="sa">
                                <a href="{{ url('admin/banip') }}">
                                    <span class="octicon octicon-alert"></span> Ban IP Address</a>
                            </li>
                            <li class="sa">
                                <a href="{{ url('admin/bancountries') }}">
                                    <span class="octicon octicon-alert"></span> Ban Country</a>
                            </li>
                        </ul>
                    </li>
                    @endif


                    <li class="sidebar-label pt20">Network</li>
                    @if($role == "super_admin")
                    <li>
                        <a href="#" class="accordion-toggle">
                            <span class="octicon octicon-settings"></span>
                            <span class="sidebar-title">Settings</span>
                            <span class="caret"></span>
                        </a>
                        <ul class="nav sub-nav">
                            <li class="sa">
                                <a href="{{ url('admin/settings/general') }}">
                                    <span class="octicon octicon-settings"></span>General Settings</a>
                            </li>
                            <li class="sa">
                                <a href="{{ url('admin/settings/api') }}">
                                    <span class="octicon octicon-git-compare"></span>API Settings</a>
                            </li>
                        </ul>
                    </li>
                    @endif
                     <li>
                        <a href="{{ url('admin/messages') }}">
                            <span class="octicon octicon-inbox"></span>
                            <span class="sidebar-title">Messages</span>
                        </a>
                    </li>
                    <li>
                        <a href="{{ url('admin/announcements') }}">
                            <span class="octicon octicon-megaphone"></span>
                            <span class="sidebar-title">Announcements</span>
                        </a>
                    </li>
                     <li>
                        <a href="{{ url('admin/promotions') }}">
                            <span class="octicon octicon-gift"></span>
                            <span class="sidebar-title">Promotions</span>
                        </a>
                    </li>

                    @if($role == "super_admin")
                    <li class="sidebar-label pt20">Extras</li>
                    <li>
                        <a href="#" class="accordion-toggle">
                            <span class="octicon octicon-book"></span>
                            <span class="sidebar-title">Blog</span>
                            <span class="caret"></span>
                        </a>
                        <ul class="nav sub-nav">
                            <li class="sa">
                                <a href="{{ url('admin/blogposts') }}">
                                    <span class="octicon octicon-calendar"></span> Browse Posts</a>
                            </li>
                            <li class="sa">
                                <a href="{{ url('admin/blog/createpost') }}">
                                    <span class="octicon octicon-pencil"></span> Write New Post</a>
                            </li>
                        </ul>
                    </li>

                    <li>
                        <a href="{{ url('admin/healthcheck') }}">
                            <span class="octicon octicon-briefcase"></span>
                            <span class="sidebar-title">System Check</span>
                        </a>
                    </li>
                    @endif
                    <!--<li>
                        <a href="#">
                            <span class="octicon octicon-file-directory"></span>
                            <span class="sidebar-title">Maintenance</span>
                        </a>
                    </li>-->

                    <!-- sidebar progress bars -->
                    <!--
                    <li class="sidebar-label pt25 pb10">Last Login</li>
                    <li class="sidebar-stat mb10">
                        <a href="#projectOne" class="fs11">
                            <span class="icon-info-circled text-info"></span>
                            <span class="sidebar-title text-muted">117.193.110.195</span>
                            <span class="pull-right mr20 text-muted">Yesterday</span>
                        </a>
                    </li>-->
                </ul>
            </div>
        </aside>