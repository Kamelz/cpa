<!DOCTYPE html>
<html lang="en">
<head>
    <title>AdGodMedia - Advertisers</title>
	
	@include('_head')

    @include('ganalytics')

</head>
<body>

@include('_header')

<div class="md-padding p-b-0 gry-bg">
<div class="container">
<div class="heading main centered">
<h3 class="uppercase lg-title"> <span class="main-color">Advertisers</span></h3>
</div>
<h1 class="text-center">Why Advertise with AdGodMedia?</h1><br>
<p>Get amazing results for your campaign with our performance marketing network. We do not charge for any impressions or clicks, instead only for the results you receive.</p><br><br>
		
  <p>Contact us to become an adveriser on AdGodMedia! Please include your <b>your full name and company name, the offer(s) you have that you would like promoted, payout, payment terms, countries accepted and any other necessary information. </p></b>
		<br><br><br>
	<div class="row">
	<div class="large-12 columns">
	<div class="large-12 columns text-center">
		<a href="{{ url('contactus') }}"><button class="button success large">CONTACT US</button></a>
	</div>
	</div>
</div>	

<br>
	<div class="row">
		<div class="large-12 columns">
			<ul class="large-block-grid-2">
				
				<h4 class="green"><i class="fa fa-check-square-o"></i> Extremely High Quality</h4>
				With strict publisher approval and screening process, you can be sure our traffic is of hight quality. We work together with advertisers and optimize well to ensure we bring profit to you.
				<br>
			
				<h4 class="green"><i class="fa fa-check-square-o"></i> Real-Time Reporting</h4>
				Our analytic tools will help you understand where and how your offer is running - in realtime! Make changes to your campaign and see changes implemented in seconds.<br>
				
			</ul>
		</div>
	</div>
		<div class="row">
		<div class="large-12 columns">
			<ul class="large-block-grid-2">
				
				<br>
				<h4 class="green"><i class="fa fa-check-square-o"></i> Absolutely No Fraud</h4>
				We have dedicated staff and automated systems to detect bad quality traffic traffic sources and instantly pull them off from our system. Get real, high quality visitors to your campaign.<br>
				
				
				<br>
				<h4 class="green"><i class="fa fa-check-square-o"></i> Worldwide Reach</h4>
				It doesn't matter your audience are based on America or Japan, we have quality traffic from almost every geographical location you need for your brand.<br>
				
			</ul>
		</div>
	</div>
</div>
</div>



<br><br>

@include('_footer')
@include('_footer_js')
	
</body>
</html>