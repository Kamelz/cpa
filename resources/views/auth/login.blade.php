<!DOCTYPE html>
<html>

<head>
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <title>Login | AdGodMedia</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- Font CSS (Via CDN) -->
    <link rel='stylesheet' type='text/css' href='//fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800'>
    <link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Roboto:400,500,700,300">

    <!-- Theme CSS -->
    <link rel="stylesheet" type="text/css" href="{{ asset('panel/assets/skin/default_skin/css/theme.css') }}">

    <link rel="stylesheet" type="text/css" href="{{ asset('panel/assets/skin/default_skin/css/custom.css') }}">

    <!-- Admin Forms CSS -->
    <link rel="stylesheet" type="text/css" href="{{ asset('panel/assets/admin-tools/admin-forms/css/admin-forms.css') }}">

    <!-- Favicon -->
    <link rel="shortcut icon" href="{{ asset('favicon.ico') }}">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
   <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
   <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
   <![endif]-->
</head>

<body class="external-page sb-l-c sb-r-c">

    <!-- Start: Main -->
    <div id="main" class="animated fadeIn">

        <!-- Start: Content-Wrapper -->
        <section id="content_wrapper">

            <!-- Begin: Content -->
            <section id="content">

                <div class="admin-form theme-success" id="login1">

                    <div class="row mb15 table-layout">

                        <div class="col-md-12 va-m pln">
                                <img src="{{ asset('panel/assets/img/logos/adgodlogo.png') }}" title="AdGodMedia Logo" class="img-responsive w250 center">

                        </div>

                    </div>

                    <div class="panel panel-success mt10 br-n">

                        <div class="panel-heading heading-border bg-white">
                            <div class="section-divider" id="spy1">
                                            <span>Publisher Login</span>
                                        </div>
                        </div>

                        @if (count($errors) > 0)
                        <div class="alert alert-micro alert-border-left alert-primary pastel alert-dismissable mn">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        @foreach ($errors->all() as $error)
                            {{ $error }}<br>
                        @endforeach
                        </div>
                        @endif

                        <!-- end .form-header section -->
                        <form method="POST" id="contact" role="form" action="{{ url('/auth/login') }}">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">

                            <div class="panel-body bg-light p30">
                                <div class="row">
                                    <div class="col-md-12">

                                        <div class="section">
                                            <label for="email" class="field-label text-muted fs18 mb10">Email Address</label>
                                            <label for="email" class="field prepend-icon">
                                                <input type="text" name="email" id="email" class="gui-input" placeholder="Enter email" value="{{ old('email') }}">
                                                <label for="email" class="field-icon"><i class="octicon octicon-gist-secret"></i>
                                                </label>
                                            </label>
                                        </div>
                                        <!-- end section -->

                                        <div class="section mbn">
                                            <label for="username" class="field-label text-muted fs18 mb10">Password</label>
                                            <label for="password" class="field prepend-icon">
                                                <input type="password" name="password" id="password" class="gui-input" placeholder="Enter password">
                                                <label for="password" class="field-icon"><i class="octicon octicon-key"></i>
                                                </label>
                                            </label>
                                        </div>
                                        <!-- end section -->
                                        <label class="switch block switch-success pull-left input-align mt10">
                                    <input type="checkbox" name="remember" id="remember">
                                    <label for="remember" data-on="YES" data-off="NO"></label>
                                    <span>Remember me</span>
                                </label>

                                    </div>
                                </div>
                            </div>
                            <!-- end .form-body section -->
                            <div class="panel-footer clearfix p10 ph15">
                                <button type="submit" class="button btn-success btn-block -mr10 pull-right">Login</button>
                            </div>
                            <!-- end .form-footer section -->
                        </form>
                    </div>

                                    <div class="row">
                                    <div class="col-md-12">
                                        <div class="login-links text-center">
                                            <a href="{{ url('/password/email') }}">Forgot password</a> | <a href="{{ url('/auth/register') }}">Register</a></span>
                                        </div>
                                    </div>
                                    
                                </div>
                </div>

            </section>
            <!-- End: Content -->

        </section>
        <!-- End: Content-Wrapper -->

    </div>
    <!-- End: Main -->

    <!-- BEGIN: PAGE SCRIPTS -->



</body>

</html>