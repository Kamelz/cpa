<!DOCTYPE html>
<html>

<head>
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <title>Password Reset</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- Font CSS (Via CDN) -->
    <link rel='stylesheet' type='text/css' href='//fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800'>
    <link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Roboto:400,500,700,300">

    <!-- Theme CSS -->
    <link rel="stylesheet" type="text/css" href="{{ asset('panel/assets/skin/default_skin/css/theme.css') }}">

    <link rel="stylesheet" type="text/css" href="{{ asset('panel/assets/skin/default_skin/css/custom.css') }}">

    <!-- Admin Forms CSS -->
    <link rel="stylesheet" type="text/css" href="{{ asset('panel/assets/admin-tools/admin-forms/css/admin-forms.css') }}">

    <!-- Favicon -->
    <link rel="shortcut icon" href="{{ asset('favicon.ico') }}">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
   <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
   <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
   <![endif]-->
</head>

<body class="external-page sb-l-c sb-r-c">

    <!-- Start: Main -->
    <div id="main" class="animated fadeIn">

        <!-- Start: Content-Wrapper -->
        <section id="content_wrapper">

            <!-- Begin: Content -->
            <section id="content">

                <div class="admin-form theme-success" id="login1" style="max-width:500px;">

                    <div class="row mb15 table-layout">

                        <div class="col-md-12 va-m pln">
                                <img src="{{ asset('panel/assets/img/logos/adgodlogo.png') }}" title="AdGodMedia Logo" class="img-responsive w250 center">

                        </div>

                    </div>

                    <div class="panel panel-info mt10 br-n">

                        <div class="panel-heading heading-border bg-white">
                            <div class="section-divider" id="spy1">
                                            <span>Password Reset</span>
                                        </div>
                        </div>

                        @if (session('status'))
                        <div class="alert alert-micro alert-border-left alert-primary pastel alert-dismissable mn">
                            {{ session('status') }}
                        </div>
                        @elseif (count($errors) > 0)
                        <div class="alert alert-micro alert-border-left alert-primary pastel alert-dismissable mn">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        @foreach ($errors->all() as $error)
                            {{ $error }}<br>
                        @endforeach
                        </div>
                        @else
                        <div class="alert alert-micro alert-border-left alert-primary pastel alert-dismissable mn">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            Enter your <b>email</b> and instructions will be sent to you!
                        </div>
                        @endif

                        <!-- end .form-header section -->
                        <form method="POST" id="contact" role="form" action="{{ url('/password/email') }}">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">

                            <!-- end .form-body section -->
                            <div class="panel-footer p25 pv15">

                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="section mn">
                                            <label for="email" class="field-label text-muted fs18 mb10 hidden">Password Reset</label>
                                            <div class="smart-widget sm-right smr-80">
                                                <label for="email" class="field prepend-icon">
                                                    <input type="text" name="email" id="email" class="gui-input" placeholder="Your Email Address" type="email" value="{{ old('email') }}">
                                                    <label for="email" class="field-icon"><i class="octicon octicon-mail"></i>
                                                    </label>
                                                </label>
                                                <label for="email" class="button" onclick="form.submit();">Reset</label>
                                            </div>
                                            <!-- end .smart-widget section -->
                                        </div>
                                        <!-- end section -->
                                    </div>

                                </div>

                            </div>
                            <!-- end .form-footer section -->
                        </form>
                    </div>

                </div>

            </section>
            <!-- End: Content -->

        </section>
        <!-- End: Content-Wrapper -->

    </div>
    <!-- End: Main -->

    <!-- BEGIN: PAGE SCRIPTS -->



</body>

</html>