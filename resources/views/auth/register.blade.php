<!DOCTYPE html>
<html>

<head>
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <title>Register</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- Font CSS (Via CDN) -->
    <link rel='stylesheet' type='text/css' href='//fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800'>
    <link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Roboto:400,500,700,300">

    <!-- Theme CSS -->
    <link rel="stylesheet" type="text/css" href="{{ asset('panel/assets/skin/default_skin/css/theme.css') }}">

    <link rel="stylesheet" type="text/css" href="{{ asset('panel/assets/skin/default_skin/css/custom.css') }}">

    <link rel="stylesheet" type="text/css" href="{{ asset('panel/assets/fonts/font-awesome/font-awesome.css') }}">

    <!-- Admin Forms CSS -->
    <link rel="stylesheet" type="text/css" href="{{ asset('panel/assets/admin-tools/admin-forms/css/admin-forms.css') }}">

    <!-- Favicon -->
    <link rel="shortcut icon" href="{{ asset('favicon.ico') }}">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
   <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
   <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
   <![endif]-->
   <script src='https://www.google.com/recaptcha/api.js'></script>
</head>

<body class="external-page sb-l-c sb-r-c">

    <!-- Start: Main -->
    <div id="main" class="animated fadeIn">

        <!-- Start: Content-Wrapper -->
        <section id="content_wrapper">

            <!-- Begin: Content -->
            <section id="content" class="">

                <div class="admin-form theme-success mw700">

                    <div class="row mb15 table-layout">

                        <div class="col-md-12 va-m pln">
                                <img src="{{ asset('panel/assets/img/logos/adgodlogo.png') }}" title="AdGodMedia Logo" class="img-responsive w250 center">
                        
                        @if (count($errors) > 0)
                        <div class="alert alert-micro alert-border-left alert-primary pastel alert-dismissable mn mb20 mt20">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            @foreach ($errors->all() as $error)
                                <i class="octicon octicon-info"></i>  {{ $error }} <br>
                            @endforeach
                        </div>
                        @endif

                        </div>



                    </div>

                    <div class="panel panel-success mt10 br-n">

                        <div class="panel-heading heading-border bg-white">
                            <div class="section-divider" id="spy1">
                                            <span>Set Up Your Account</span>
                                        </div>
                        </div>

                        <form method="POST" action="{{ url('/auth/register') }}" id="account2" role="form">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <input type="hidden" name="referral" value="{{ $referral }}">
                        <input type="hidden" name="ip" value="{{ ip2long($location->ip) }}">

                            <div class="panel-body p25 bg-light">
                                <div class="section-divider">
                                    <span>Account Information</span>
                                </div>
                                <!-- .section-divider -->

                                <div class="section row">
                                    <div class="col-md-6">
                                        <label for="name" class="field prepend-icon">
                                            <input type="text" name="name" id="name" class="gui-input" placeholder="Your Name (Required)" value="{{ old('name') }}">
                                            <label for="name" class="field-icon"><i class="fa fa-user"></i>
                                            </label>
                                        </label>
                                    </div>
                                    <!-- end section -->

                                    <div class="col-md-6">
                                        <label for="email" class="field prepend-icon">
                                            <input type="email" name="email" id="email" class="gui-input" placeholder="Email address (Required)" value="{{ old('email') }}">
                                            <label for="email" class="field-icon"><i class="fa fa-envelope"></i>
                                            </label>
                                        </label>
                                    </div>
                                    <!-- end section -->
                                </div>

                                <!-- end .section row section -->

                                <div class="section row">
                                        <div class="col-md-6">
                                        <label for="password" class="field prepend-icon">
                                        <input type="password" name="password" id="password" class="gui-input" placeholder="Password (Required)">
                                        <label for="password" class="field-icon"><i class="fa fa-unlock-alt"></i>
                                        </label>
                                        </label>
                                        </div>
                                        <div class="col-md-6">
                                        <label for="password" class="field prepend-icon">
                                        <input type="password" name="password_confirmation" id="password" class="gui-input" placeholder="Confirm Password (Required)">
                                        <label for="password" class="field-icon"><i class="fa fa-unlock-alt"></i>
                                        </label>
                                    </label>
                                        </div>
                                </div>

                                <div class="section row">
                                    <div class="col-md-12">
                                        <label for="name" class="field prepend-icon">
                                            <input type="text" name="displayname" id="name" class="gui-input" placeholder="Display Name (Required)" value="{{ old('displayname') }}">
                                            <label for="name" class="field-icon"><i class="fa fa-user"></i>
                                            </label>
                                        </label>
                                    </div>
                                </div>

                                <div class="section-divider mt40">
                                    <span>Personal Information</span>
                                </div>
                                <!-- end section -->

                                <div class="section row">
                                    <div class="col-md-6">
                                        <label for="address" class="field prepend-icon">
                                            <input type="text" name="address" id="street" class="gui-input" placeholder="Street Address" value="{{ old('address') }}">
                                            <label for="address" class="field-icon"><i class="fa fa-user"></i>
                                            </label>
                                        </label>
                                    </div>
                                    <div class="col-md-6">
                                        <label for="city" class="field prepend-icon">
                                            <input type="text" name="city" id="city" class="gui-input" placeholder="City / Town" value="{{ old('city') }}">
                                            <label for="city" class="field-icon"><i class="fa fa-user"></i>
                                            </label>
                                        </label>
                                    </div>
                                </div>

                                <div class="section row">
                                    <div class="col-md-6">
                                        <label for="state" class="field prepend-icon">
                                            <input type="text" name="state" id="state" class="gui-input" placeholder="State" value="{{ old('state') }}">
                                            <label for="state" class="field-icon"><i class="fa fa-user"></i>
                                            </label>
                                        </label>
                                    </div>
                                    <div class="col-md-6">
                                        <label for="zip" class="field prepend-icon">
                                            <input type="text" name="zip" id="zipcode" class="gui-input" placeholder="Zip Code" value="{{ old('zip') }}">
                                            <label for="zip" class="field-icon"><i class="fa fa-user"></i>
                                            </label>
                                        </label>
                                    </div>
                                </div>

                                <div class="section row">
                                    <div class="col-md-6">
                                        <label class="field select">
                                        <select id="location" name="country"  value="{{ old('country') }}">
                                            <option value="">Select country (Required)</option>
                                        @foreach ($countries as $country)
                                            <option value="{{ $country->value }}">{{ $country->value }}</option>
                                        @endforeach
                                        </select>
                                        <i class="arrow double"></i>
                                    </label>
                                    </div>
                                    <div class="col-md-6">
                                        <label for="phone" class="field prepend-icon">
                                            <input type="text" name="phone" id="phone" class="gui-input" placeholder="Phone"  value="{{ old('phone') }}">
                                            <label for="phone" class="field-icon"><i class="fa fa-user"></i>
                                            </label>
                                        </label>
                                    </div>
                                </div>

                                <div class="section row">
                                    <div class="col-md-6">
                                        <label class="field select">
                                        <select id="location" name="gender"  value="{{ old('gender') }}">
                                            <option value="">Gender</option>
                                            <option value="Male" selected="">Male</option>
                                            <option value="Female">Female</option>
                                        </select>
                                        <i class="arrow double"></i>
                                    </label>
                                    </div>
                                    <div class="col-md-6">
                                        <label for="phone" class="field prepend-icon">
                                            <input type="text" name="websites" id="websites" class="gui-input" placeholder="Websites"  value="{{ old('websites') }}">
                                            <label for="websites" class="field-icon"><i class="fa fa-globe"></i>
                                            </label>
                                        </label>
                                    </div>
                                </div>

                                <div class="section-divider mt40">
                                    <span>Additional Information</span>
                                </div>

                                <div class="section row">
                                    <div class="col-md-6">
                                        <label for="skype" class="field prepend-icon">
                                            <input type="text" name="skype" id="skype" class="gui-input" placeholder="Skype" value="{{ old('skype') }}">
                                            <label for="skype" class="field-icon"><i class="fa fa-user"></i>
                                            </label>
                                        </label>
                                    </div>
                                    <div class="col-md-6">
                                        <label class="field select">
                                        <select id="location" name="monthly_income"  value="{{ old('monthly_income') }}">
                                            <option value="">Monthly Earnings (Required)</option>
                                            <option value="100-1000">$100 - $1,000</option>
                                            <option value="1000-5000">$1,000 - $5,000</option>
                                            <option value="5000-10000">$5,000 - $10,000</option>
                                            <option value="10000+">$10,000+</option>
                                            </select>
                                        <i class="arrow double"></i>
                                    </label>
                                    </div>
                                </div>

                                <div class="section row">
                                    <div class="col-md-6">
                                        <textarea name="promo_methods" class="form-control" id="textArea2" rows="3" placeholder="How do you plan to drive traffic? (Required)">{{ old('promo_methods') }}</textarea>
                                    </div>
                                    <div class="col-md-6">
                                        <textarea name="reason_toaccept" class="form-control" id="textArea2" rows="3" placeholder="Why should you be accepted into AdGodMedia.com? (Required)">{{ old('reason_toaccept') }}</textarea>
                                    </div>
                                </div>

                                <div class="section row">
                                    <div class="col-md-6">
                                        <div class="g-recaptcha" data-sitekey="6LfV9h8TAAAAAHazRAk41GFJERURA3ydP_PmQRsg"></div>
                                    </div>
                                </div>


                                <div class="section mb15">
                                    <label class="option block mt15">
                                        <input type="checkbox" name="terms">
                                        <span class="checkbox"></span>I agree to the
                                        <a href="#" class="smart-link"> terms of service </a>and <a href="#" class="smart-link">privacy policy</a>
                                    </label>
                                </div>
                                <!-- end section -->

                            </div>
                            <!-- end .form-body section -->
                            <div class="panel-footer clearfix">
                                <button type="submit" class="button btn-primary pull-right">Create Account</button>
                            </div>
                            <!-- end .form-footer section -->
                        </form>
                    </div>
                </div>

            </section>
            <!-- End: Content -->

        </section>
        <!-- End: Content-Wrapper -->

    </div>
    <!-- End: Main -->

    <!-- BEGIN: PAGE SCRIPTS -->

    <!-- jQuery -->
    <script type="text/javascript" src="vendor/jquery/jquery-1.11.1.min.js"></script>
    <script type="text/javascript" src="vendor/jquery/jquery_ui/jquery-ui.min.js"></script>

    <!-- Bootstrap -->
    <script type="text/javascript" src="assets/js/bootstrap/bootstrap.min.js"></script>

    <!-- Page Plugins -->
    <script type="text/javascript" src="assets/js/pages/login/EasePack.min.js"></script>
    <script type="text/javascript" src="assets/js/pages/login/rAF.js"></script>
    <script type="text/javascript" src="assets/js/pages/login/TweenLite.min.js"></script>
    <script type="text/javascript" src="assets/js/pages/login/login.js"></script>

    <!-- Theme Javascript -->
    <script type="text/javascript" src="assets/js/utility/utility.js"></script>
    <script type="text/javascript" src="assets/js/main.js"></script>
    <script type="text/javascript" src="assets/js/demo.js"></script>

    <!-- Page Javascript -->
    <script type="text/javascript">
    jQuery(document).ready(function() {
        "use strict";
        // Init Theme Core      
        Core.init();
        
        // Init Demo JS
        Demo.init();

        // Init CanvasBG and pass target starting location
        CanvasBG.init({
            Loc: {
                x: window.innerWidth / 2.1,
                y: window.innerHeight / 4.2
            },
        });

    });
    </script>

    <!-- END: PAGE SCRIPTS -->

</body>

</html>
