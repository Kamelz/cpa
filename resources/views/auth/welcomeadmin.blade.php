<!DOCTYPE html>
<html>

<head>
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <title>Welcome</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- Font CSS (Via CDN) -->
    <link rel='stylesheet' type='text/css' href='//fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800'>
    <link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Roboto:400,500,700,300">

    <!-- Theme CSS -->
    <link rel="stylesheet" type="text/css" href="{{ asset('panel/assets/skin/default_skin/css/theme.css') }}">

    <link rel="stylesheet" type="text/css" href="{{ asset('panel/assets/skin/default_skin/css/custom.css') }}">

    <!-- Admin Forms CSS -->
    <link rel="stylesheet" type="text/css" href="{{ asset('panel/assets/admin-tools/admin-forms/css/admin-forms.css') }}">

    <!-- Favicon -->
    <link rel="shortcut icon" href="{{ asset('favicon.ico') }}">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
   <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
   <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
   <![endif]-->
</head>

<body class="external-page sb-l-c sb-r-c">

    <!-- Start: Main -->
    <div id="main" class="animated fadeIn">

        <!-- Start: Content-Wrapper -->
        <section id="content_wrapper">

            <!-- Begin: Content -->
            <section id="content" class="mt20">

                <div class="admin-form theme-danger" id="login1" style="max-width:900px;">

                    <div class="row mb15 table-layout">

                        <div class="col-md-12 va-m pln">
                                <img src="{{ asset('panel/assets/img/logos/uploadcash-logo-white.png') }}" title="AdGodMedia Logo" class="img-responsive w250 center">

                        </div>

                    </div>

                    <div class="panel panel-danger mt10 br-n">

                        <div class="panel-heading heading-border bg-white">
                            <div class="section-divider" id="spy1">
                                            <span>Setup AdGodMedia Account</span>
                                        </div>
                        </div>

                        <div class="panel-body">
                        @if ($url)
                        <div class="row">
                            <div class="col-md-4 text-center">
                                <img src="{{ $url }}" alt="">
                            </div>
                            <div class="col-md-8 fs15">
                                <b>TWO FACTOR AUTHENTICATION</b><br>
                                As an administrator, additional security have been enabled for your account. Please follow these simple steps to configure your account. <br><br>

                                <ol>
                                <li>Install any one authenticator app for your device. We recommend Authy. <br>
                                 Android: <a target="_blank" href="https://play.google.com/store/apps/details?id=com.authy.authy&hl=en">Authy</a> | <a target="_blank" href="https://play.google.com/store/apps/details?id=com.google.android.apps.authenticator2">Google Authenticator</a> | <a target="_blank" href="https://play.google.com/store/apps/details?id=org.fedorahosted.freeotp&hl=en">FreeOTP</a> <br>
                                 iOS: <a target="_blank" href="https://itunes.apple.com/in/app/authy/id494168017?mt=8">Authy</a> | <a target="_blank" href="https://itunes.apple.com/us/app/google-authenticator/id388497605?mt=8">Google Authenticator</a></li>
                                <li>Open the app and scan the QR code using camera.</li>
                                <li>Thats it! Your app now generates 6-digit secret codes which you can use to login to AdGodMedia.</li>
                                </ol>
                            </div>
                        </div>
                        <div class="row mt20"><div class="col-md-12"><a href="{{ url('auth/login') }}" class="btn btn-block btn-primary">I completed this, proceed to login</a></div></div>
                        @else
                        <div class="row">
                            <div class="col-md-12 text-center">This resource has expired. Please contact administrator for support.</div>
                        </div>
                        @endif

                        </div>

                                    
                    </div>
                </div>

            </section>
            <!-- End: Content -->

        </section>
        <!-- End: Content-Wrapper -->

    </div>
    <!-- End: Main -->

    <!-- BEGIN: PAGE SCRIPTS -->



</body>

</html>