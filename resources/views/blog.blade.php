<!DOCTYPE html>
<html lang="en">
<head>
       <title>AdGodMedia Blog</title>
    <link rel="stylesheet" href="{{ asset('site/css/foundation.css') }}" />
    <!-- Favicon -->
   
    <!-- Custom CSS-->
    <link rel="stylesheet" href="{{ asset('site/css/app.css') }}" />
    <script src="{{ asset('site/js/modernizr.js') }}"></script>

    <link rel="stylesheet" href="{{ asset('site/css/animate.min.css') }}">

    <link rel="stylesheet" href="{{ asset('site/css/font-awesome.min.css') }}">
    @include('_head')
    @include('ganalytics')

</head>
<body>

@include('_header')


<div class="md-padding p-b-0 gry-bg">
<div class="container">
<div class="heading main centered">
<h3 class="uppercase lg-title"> <span class="main-color">Blog</span></h3>
</div>


	<div class="large-9 columns">
		@foreach ($posts as $post)
			<h2><a href="{{ url('blog'.'/'.$post->slug) }}">{{ $post->title }}</a></h2>
			<p><?php echo str_limit($post->content,600); ?></p><br>
			<a href="{{ url('blog'.'/'.$post->slug) }}"><button class="button small success">Read More</button></a>
		@endforeach
	</div>
	<div class="large-3 columns">
		<h4>Recent Posts</h4>
			@foreach ($recentposts as $recent)
			<a href="{{ url('blog'.'/'.$recent->slug) }}">{{ $recent->title }}</a><br><br>
			@endforeach
	
	</div>


</div>
</div>



<br><br>
@include('_footer')

@include('_footer_js')


</body>
</html>