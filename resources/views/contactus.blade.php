<!DOCTYPE html>
<html lang="en">
<head>
    <title>AdGodMedia - Contact Us</title>
    <link rel="stylesheet" href="{{ asset('site/css/foundation.css') }}" />
    <!-- Favicon -->
    <link rel="shortcut icon" href="{{ asset('favicon.ico') }}">
    <!-- Google Font -->
    <link href='//fonts.googleapis.com/css?family=Open+Sans:400,700|Raleway:700,400' rel='stylesheet' type='text/css'>
    <!-- Custom CSS-->
    <link rel="stylesheet" href="{{ asset('site/css/app.css') }}" />
    <script src="{{ asset('site/js/modernizr.js') }}"></script>

    <link rel="stylesheet" href="{{ asset('site/css/animate.min.css') }}">

    <link rel="stylesheet" href="{{ asset('site/css/font-awesome.min.css') }}">
	@include('_head')
	
</head>
<body>

@include('_header')


<div class="md-padding p-b-0 gry-bg">
<div class="container">
<div class="heading main centered">
<h3 class="uppercase lg-title"> <span class="main-color">Contact Us</span></h3>
<br>
<br>
<div class="row">
	<div class="large-8 columns">

	<form action="{{ url('contactus') }}" method="post">
	<input type="hidden" name="_token" value="{{ csrf_token() }}">

		<h4>General Queries</h4>
		Please submit your inquiries below. We will get back to you as soon as possible. <br><br>

		@foreach ($errors->all() as $error)
            <span style="color:red;">{{ $error }}</span> <br>
        @endforeach

        @if ($successmsg)
            <span style="color:#0AB600;">{{ $successmsg }}</span>
        @endif

        <br>

		<div class="row">
			<div class="large-6 columns">
				<label for="name">Your Name</label><br>
				<input type="text" name="name" class="form-control" value="{{ Input::old('name') }}">
			</div>
			<div class="large-6 columns">
				<label for="email">Email</label><br>
				<input type="text" name="email" class="form-control" value="{{ Input::old('email') }}">
			</div>
		</div>

		<br>

		<div class="row">
			<div class="large-12 columns">
				<label for="subject">Subject</label><br>
				<input type="text" name="subject" class="form-control" value="{{ Input::old('subject') }}">
			</div>
		</div>

		<br>

		<div class="row">
			<div class="large-12 columns">
				<label for="message">Message</label><br>
				<textarea name="message" id="" class="form-control">{{ Input::old('message') }}</textarea>
			</div>
		</div>

		<br>

		<div class="row">
			<div class="large-12 columns">
				<button type="submit" class="success medium">Send Message</button>
			</div>
		</div>

		</form>
	</div>
	
	<br><br>
	
	<div class="large-4 columns">
		<h4>Publishers</h4>
		If you're a publisher and have a valid login, please use our ticket system found in dashboard.

		<br><br>

		<h4>Head Office</h4>
		<br>
Email: </b><a href="mailto:support@adgodmedia.com">support@adgodmedia.com</a>

		<br>
Skype: </b>adgodmedia.com
	</div>
</div>
</div>

</div>
</div>

<br>

@include('_footer')

 @include('_footer_js')
	
</body>
</html>