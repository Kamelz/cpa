<!DOCTYPE html>
<html ng-app="ngDashboard" ng-controller="PageController">

<head>
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <title>AdGodMedia - Account Settings</title>
    <meta name="keywords" content="{{ $gset->keywords }}" />
    <meta name="description" content="{{ $gset->description }}">

    <meta name="csrf-token" content="{{ csrf_token() }}" />

    <meta name="author" content="AdminDesigns">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- Font CSS (Via CDN) -->
    <link rel='stylesheet' type='text/css' href='//fonts.googleapis.com/css?family=Open+Sans:400,600,700'>
    <link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Raleway:300,400,500,700">

    <!-- Theme CSS -->
    <link rel="stylesheet" type="text/css" href="{{ asset('panel/assets/skin/default_skin/css/theme.css') }}">

    <!-- Custom CSS -->
    <link rel="stylesheet" type="text/css" href="{{ asset('panel/assets/skin/default_skin/css/custom.css') }}">

    <!-- Admin Panels CSS -->
    <link rel="stylesheet" type="text/css" href="{{ asset('panel/assets/admin-tools/admin-plugins/admin-panels/adminpanels.css') }}">

     <!-- Admin Panels CSS -->
    <link rel="stylesheet" type="text/css" href="{{ asset('panel/assets/admin-tools/admin-forms/css/admin-forms.css') }}">

    <link rel="stylesheet" href="{{ asset('panel/css/igrowl.min.css') }}">

    <link rel="stylesheet" type="text/css" href="{{ asset('panel/vendor/plugins/datepicker/css/bootstrap-datetimepicker.min.css') }}">

    <link href="{{ asset('panel/vendor/plugins/datatables/media/css/dataTables.bootstrap.css') }}" rel="stylesheet" type="text/css">

    <!-- Favicon -->
    <link rel="shortcut icon" href="{{ asset('favicon.ico') }}">

    <script type="text/javascript" src="{{ asset('panel/vendor/jquery/jquery-1.11.1.min.js') }}"></script>
    <script src="//ajax.googleapis.com/ajax/libs/angularjs/1.2.26/angular.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.3.14/angular-sanitize.min.js"></script>


    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->

@include('ganalytics') </head>

<body class="dashboard-page sb-l-o sb-r-c">

    <!-- Start: Main -->
    <div id="main">

    @include('dashboard.header')

    @include('dashboard.sidebar')



        <!-- Start: Content-Wrapper -->
        <section id="content_wrapper">

            <!-- Start: Topbar -->
            <header id="topbar">
                <div class="topbar-left">
                    <ol class="breadcrumb">
                        <li class="crumb-active">
                            <a href="{{ url('dashboard/account') }}">Account</a>
                        </li>
                    </ol>
                </div>
            </header>
            <!-- End: Topbar -->

            <!-- Begin: Content -->
            <section id="content" class="animated fadeIn table-layout admin-form">

            <aside class="tray tray-left tray290 va-t pn" data-tray-height="match" style="height: 1100px;">
<div class="m40 text-center">
	<img src="@if (empty($user->profileimg)) {{ asset('panel/assets/img/avatars/2.png') }} @else {{ $user->profileimg }} @endif" alt="" width="200">
	<h2>{{ $user->name }}</h2>
	<h5>{{ ucfirst($role) }}</h5>
</div>
</aside>

<div class="tray tray-center p25 va-t posr">

<div class="panel panel-info heading-border">
<div class="panel-heading">
<span class="panel-title"><i class="fa fa-rocket"></i>Account Settings</span>
</div>
<div class="panel-body p25">

<div class="section-divider mt10 mb40">
<span>Personal Details</span>
</div>

<input type="hidden" name="_token" value="{{ csrf_token() }}">

<div class="section row">
<div class="col-md-6">

<div class="input-group">
<input type="text" class="form-control" disabled value="{{ $user->name }}">
<span class="input-group-addon octicon octicon-person"></span>
</div>

</div>
 
<div class="col-md-6">

<div class="input-group">
<input type="text" class="form-control" disabled value="{{ $user->email }}">
<span class="input-group-addon octicon octicon-mail"></span>
</div>

</div>
 
</div>

<div class="section-divider mt40 mb40">
<span>Change Password</span>
</div>

<div class="row">
  <div class="col-md-4" data-toggle="tooltip" data-original-title="Leave blank, if you do not wish to change your password">
      <div class="section">

      <div class="input-group mb20">
      <input type="password" name="oldpass" id="oldpassword" class="form-control" placeholder="Old Password">
      <span class="input-group-addon octicon octicon-key"></span>
      </div>

      </div>
  </div>
    <div class="col-md-4">
      <div class="section">

      <div class="input-group mb20">
      <input type="password" name="newpass" id="newpassword" class="form-control" placeholder="New Password">
      <span class="input-group-addon octicon octicon-key"></span>
      </div>

      </div>
  </div>
    <div class="col-md-4">
      <div class="section">

      <div class="input-group mb20">
      <input type="password" name="confirmnewpass" id="cnewpassword" class="form-control" placeholder="Confirm New Password">
      <span class="input-group-addon octicon octicon-key"></span>
      </div>

      </div>
  </div>
</div>

<div class="section-divider mt40 mb40">
<span>Payout Settings</span>
</div>

<h5>Payment Method: </h5>
<div class="section">
<div class="option-group field">
<label for="paypal" class="option">
<input type="radio" name="payment_method" id="paypal" value="PayPal" ng-model="method">
<span class="radio"></span>PayPal</label>
<label for="Payoneer" class="option">
<input type="radio" name="payment_method" id="Payoneer" value="Payoneer" ng-model="method">
<span class="radio"></span>Payoneer</label>
<label for="skrill" class="option">
<input type="radio" name="payment_method" id="skrill" value="Moneybookers / Skrill" ng-model="method">
<span class="radio"></span>Moneybookers / Skrill</label>
<label for="wire" class="option">
<input type="radio" name="payment_method" id="wire" value="Wire Transfer" ng-model="method">
<span class="radio"></span>Wire Transfer</label>
<label for="westernunion" class="option">
<input type="radio" name="payment_method" id="westernunion" value="Western Union" ng-model="method">
<span class="radio"></span>Western Union</label>
<label for="Neteller" class="option">
<input type="radio" name="payment_method" id="Neteller" value="Neteller" ng-model="method">
<span class="radio"></span>Neteller</label>
</div>
</div>

<div class="section" ng-if="method=='PayPal'">
<label for="paypal" class="field prepend-icon">
<input type="text" name="paypal" id="paypal" class="gui-input" placeholder="PayPal Email" value="@if ($profile->payment_method == 'PayPal'){{ $profile->payment_detail }}@endif">
<label for="paypal" class="field-icon"><i class="octicon octicon-mail"></i>
</label>
</label>
<div class="alert alert-primary mt20"><b>Minimum</b>: $10 <b>Fees</b>: None</div>
</div>

<div class="section" ng-if="method=='Payoneer'">
<label for="Payoneer" class="field prepend-icon">
<input type="text" name="Payoneer" id="Payoneer" class="gui-input" placeholder="Payoneer Email" value="@if ($profile->payment_method == 'Payoneer'){{ $profile->payment_detail }}@endif">
<label for="Payoneer" class="field-icon"><i class="octicon octicon-mail"></i>
</label>
</label>
<div class="alert alert-primary mt20"><b>Minimum</b>: $20 <b>Fees</b>: None</div>
</div>

<div class="section" ng-if="method=='Moneybookers / Skrill'">
<label for="moneybookers" class="field prepend-icon">
<input type="text" name="moneybookers" id="moneybookers" class="gui-input" placeholder="Moneybookers Email" value="@if ($profile->payment_method == 'Moneybookers / Skrill'){{ $profile->payment_detail }}@endif">
<label for="moneybookers" class="field-icon"><i class="octicon octicon-mail"></i>
</label>
</label>
<div class="alert alert-primary mt20"><b>Minimum</b>: $10 <b>Fees</b>: 2.5%</div>
</div>

<div class="section" ng-if="method=='Wire Transfer'">
<div class="row">
  <div class="col-md-6">
    <input type="text" class="gui-input" name="account_name" placeholder="Name on Account" value="{{ $wire['Name on Account'] }}">
  </div>
  <div class="col-md-6">
    <input type="text" class="gui-input" name="bank_name" placeholder="Bank Name" value="{{ $wire['Bank Name'] }}">
  </div>
</div>

<div class="row mt20">
  <div class="col-md-4">
    <input type="text" class="gui-input" name="bank_address" placeholder="Bank Address" value="{{ $wire['Bank Address'] }}">
  </div>
  <div class="col-md-4">
                          <div class="form-group">
                      <div class="section">
                      <label class="field select">
                      <select name="bank_country">
                       @foreach ($countries as $country)
                      <option value="{{ $country->value}}" @if ($wire['Bank Country'] == $country->value) selected @endif>{{ $country->value }}</option>
                      @endforeach
                      </select>
                      <i class="arrow"></i>
                      </label>
                      </div>
                      </div>
  </div>
  <div class="col-md-4">
                      <div class="form-group">
                      <div class="section">
                      <label class="field select">
                      <select name="account_type">
                      <option value="">Account Type</option>
                      <option value="Checking" @if ($wire['Account Type'] == 'Checking') selected @endif>Checking</option>
                      <option value="Savings" @if ($wire['Account Type'] == 'Savings') selected @endif>Savings</option>
                      </select>
                      <i class="arrow"></i>
                      </label>
                      </div>
                      </div>
  </div>
</div>

<div class="row">
  <div class="col-md-4">
    <input type="text" class="gui-input" placeholder="Account / IBAN #" name="account_iban" value="{{ $wire['Account / IBAN #'] }}">
  </div>
  <div class="col-md-4">
    <input type="text" class="gui-input" placeholder="Routing / ABA #" name="routing_aba" value="{{ $wire['Routing / ABA #'] }}">
  </div>
  <div class="col-md-4">
    <input type="text" class="gui-input" placeholder="SWIFT Code" name="swift_code" value="{{ $wire['SWIFT Code'] }}">
  </div>
</div>

<div class="row"><div class="col-md-12">
  <div class="alert alert-primary mt20"><b>Minimum</b>: $1000 <b>Fees</b>: $25</div>
</div></div>
</div>

<div class="section" ng-if="method=='Western Union'">
<label for="westernunion" class="field prepend-icon">
<input type="text" name="westernunion" id="westernunion" class="gui-input" placeholder="Address" value="@if ($profile->payment_method == 'Wetsern Union'){{ $profile->payment_detail }}@endif">
<label for="westernunion" class="field-icon"><i class="octicon octicon-location"></i>
</label>
</label>
<div class="alert alert-primary mt20"><b>Minimum</b>: $500 <b>Fees</b>: $20</div>
</div>

<div class="section" ng-if="method=='Neteller'">
<label for="Neteller" class="field prepend-icon">
<input type="text" name="Neteller" id="Neteller_address" class="gui-input" placeholder="Neteller Address" value="@if ($profile->payment_method == 'Neteller'){{ $profile->payment_detail }}@endif">
<label for="Neteller" class="field-icon"><i class="octicon octicon-mail"></i>
</label>
</label>
<div class="alert alert-primary mt20"><b>Minimum</b>: $10 <b>Fees</b>: 2%</div>
</div>

<div class="section-divider mt40 mb40">
<span>Interface Settings</span>
</div>

<div class="row">
  <div class="col-md-4" data-toggle="tooltip" data-original-title="To change your display name, contact your Affiliate Manager">
    <label for="firstname" class="field prepend-icon">
    <input type="text" class="form-control" disabled value="{{ $user->chathandle }}">
    <label for="firstname" class="field-icon"><i class="octicon octicon-eye"></i>
    </label>
    </label>
  </div>

    <div class="col-md-4">
      <div class="form-group">
      <div class="section">
      <label class="field select">
      <select name="chatsound">
      <option value="">Chat Sound</option>
      <option value="none" @if ($user->chatsound == 'none') selected @endif>None</option>
      <option value="all" @if ($user->chatsound == 'all') selected @endif>Public & Private Messages</option>
      <option value="privateonly" @if ($user->chatsound == 'privateonly') selected @endif>Private Messages Only</option>
      </select>
      <i class="arrow"></i>
      </label>
      </div>
      </div>
  </div>

  <div class="col-md-4">
    <div class="input-group">
    <span class="input-group-addon"><i class="octicon octicon-file-media"></i>
    </span>
    <input class="form-control" type="text" placeholder="Profile Image Url" value="{{ $user->profileimg }}" name="profileimg">
    </div>
  </div>
</div>

<div class="section-divider mt40 mb40">
<span>Contact Settings</span>
</div>

<div class="row mb20">
  <div class="col-md-4">
    <b>Street Address</b><p class="form-control-static text-muted">{{ $profile->address }}</p>
  </div>
  <div class="col-md-4">
    <b>City</b><p class="form-control-static text-muted">{{ $profile->city }}</p>
  </div>
  <div class="col-md-4">
    <b>State</b><p class="form-control-static text-muted">{{ $profile->state }}</p>
  </div>
</div>

<div class="row mb20">
  <div class="col-md-4">
    <b>Zip / Postal Code</b><p class="form-control-static text-muted">{{ $profile->zip }}</p>
  </div>
  <div class="col-md-4">
    <b>Country</b><p class="form-control-static text-muted">{{ $profile->country }}</p>
  </div>
</div>

<div class="row">
  <div class="col-md-6">
      <div class="input-group">
		<span class="input-group-addon"><i class="icon-skype"></i>
		</span>
		<input class="form-control" type="text" value="{{ $profile->skype }}" placeholder="Skype" name="skype">
		</div>
  </div>
  <div class="col-md-6">
      <div class="input-group">
		<span class="input-group-addon"><i class="octicon octicon-device-mobile"></i>
		</span>
		<input class="form-control" type="text" placeholder="Phone" value="{{ $profile->phone }}" name="phone">
		</div>
  </div>
</div>
 
</div>
 
</div>
 
<div class="panel-footer">
<button type="submit" class="submit button btn-primary">Save Changes</button>
</div>
 

</div>

</div>

            </section>
            <!-- End: Content -->

        </section>
    </div>
    <!-- End: Main -->

    <!-- BEGIN: PAGE SCRIPTS -->

    <!-- jQuery -->
    <script type="text/javascript" src="{{ asset('panel/vendor/jquery/jquery_ui/jquery-ui.min.js') }}"></script>

    <!-- Bootstrap -->
    <script type="text/javascript" src="{{ asset('panel/assets/js/bootstrap/bootstrap.min.js') }}"></script>

    <!-- Theme Javascript -->
    <script type="text/javascript" src="{{ asset('panel/assets/js/utility/utility.js') }}"></script>
    <script type="text/javascript" src="{{ asset('panel/assets/js/main.js') }}"></script>

    <!-- Admin Panels  -->
    <script type="text/javascript" src="{{ asset('panel/assets/admin-tools/admin-plugins/admin-panels/json2.js') }}"></script>
    <script type="text/javascript" src="{{ asset('panel/assets/admin-tools/admin-plugins/admin-panels/jquery.ui.touch-punch.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('panel/assets/admin-tools/admin-plugins/admin-panels/adminpanels.js') }}"></script>

    <script type="text/javascript" src="{{ asset('panel/assets/js/igrowl.min.js') }}"></script>

     <script>

soundManager.setup({
  url: '{{ asset('panel/assets/swf') }}/',
  onready: function() {
    lead_snd = soundManager.createSound({
        url: '{{ asset('panel/assets/audio/chaching.mp3') }}'
      });
  }
});

var app = angular.module('ngDashboard', ['ngSanitize']);
app.controller('PageController', function($scope,$http,$interval) {
  
  $scope.earnings = '${{ formatnum($today->payout,2) }}';
  $scope.clicks = '{{ formatnum($today->clicks) }}';
  $scope.leads = '{{ formatnum($today->leads) }}';
  $scope.epc = '${{ formatnum(division($today->payout,$today->clicks),2) }}';

  $scope.method = "{{ $profile->payment_method }}";

var serverdate = new Date({{ date("Y, m, d, H, i, s") }});
function displaytime(){
    serverdate.setSeconds(serverdate.getSeconds() + 1);
    var hh=serverdate.getHours();
    var m=serverdate.getMinutes();
    var s=serverdate.getSeconds();
    
    var dd = "AM";
    var h = hh;
    if (h >= 12) {
        h = hh-12;
        dd = "PM";
    }
    if (h == 0) {
        h = 12;
    }
    m=checkTime(m);
    s=checkTime(s);
    var output = h+":"+m+" "+dd;
    $scope.serverTime = output;
}

// Add leading zero in time
function checkTime(i){if (i<10){i="0" + i;}return i;}

var getEarn = function() {
    $http.get( "{{ url('dashboard/earnings') }}").success(function(data, status, headers, config) {
        if ($scope.leads < data.leads) { lead_snd.play(); }
        if (data.clicks == undefined) { $interval.cancel(makeitrain); return; }
        $scope.clicks = data.clicks;
        $scope.leads = data.leads;
        $scope.earnings = data.earnings;
        $scope.epc = data.epc;
    });
};

$interval(displaytime, 1000);
makeitrain = $interval(getEarn, 60000);
});

</script>

    <script type="text/javascript">
        jQuery(document).ready(function() {

            "use strict";

            // Init Theme Core      
            Core.init({
                sbm: "sb-l-c",
            });

            $('.submit').click(function() {

              $(this).text('Please wait');

                $.post( "{{ url('dashboard/account') }}", {
                      _token: $('[name="_token"]').val(),
                      old_password: $('[name="oldpass"]').val(),
                      new_password: $('[name="newpass"]').val(),
                      confirm_new_password: $('[name="confirmnewpass"]').val(),
                      payment_method: $('[name="payment_method"]:checked').val(),
                      paypal: $('[name="paypal"]').val(),
                      Payoneer: $('[name="Payoneer"]').val(),
                      moneybookers: $('[name="moneybookers"]').val(),
                      account_name: $('[name="account_name"]').val(),
                      bank_name: $('[name="bank_name"]').val(),
                      bank_address: $('[name="bank_address"]').val(),
                      bank_country: $('[name="bank_country"]').val(),
                      account_type: $('[name="account_type"]').val(),
                      account_iban: $('[name="account_iban"]').val(),
                      routing_aba: $('[name="routing_aba"]').val(),
                      swift_code: $('[name="swift_code"]').val(),
                      westernunion: $('[name="westernunion"]').val(),
                      Neteller_address: $('[name="Neteller_address"]').val(),
                      chatsound: $('[name="chatsound"]').val(),
                      profileimg: $('[name="profileimg"]').val(),
                      skype: $('[name="skype"]').val(),
                      phone: $('[name="phone"]').val(),
                    })
                .done(function( data ) {
                   $.each( data, function( key, value ) {
                                    if (value.title == 'Success') {
                                      $.iGrowl({title:'Success',message: value.message});
                                      //window.location.href = "{{ url('dashboard/notes-links') }}";
                                    } else {
                                      $.iGrowl({title:'Whoops!',message: value.message, type: 'notice'});
                                    }
                            });
                   $('.submit').text('Save Changes');
                });
            });


            
            $('.admin-panels').adminpanel({
                grid: '.admin-grid',
                draggable: false,
                preserveGrid: true,
                mobile: true,
                callback: function() {
                    bootbox.confirm('<h3>A Custom Callback!</h3>', function() {});
                },
                onFinish: function() {
                    $('.admin-panels').addClass('animated fadeIn').removeClass('fade-onload');

                },
                onSave: function() {
                    $(window).trigger('resize');
                }
            });

            $('.ann-tab').click(function() {
                $('.ann-tab').removeClass('active');
                $(this).addClass('active');

                $('.ann-body').addClass('hidden');
                $($(this).attr('href')).removeClass('hidden')
            });

        });
    </script>

    <!-- END: PAGE SCRIPTS -->

</body>

</html>
