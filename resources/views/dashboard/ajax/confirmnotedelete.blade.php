<div class="modal-panel">

<form action="{{ url('dashboard/note/delete/'.$note->id) }}" method="post">
<input type="hidden" name="_token" value="{{ csrf_token() }}">

<div class="panel">
    <div class="panel-heading"><div class="panel-title"><span class="pr5 panel-icon octicon octicon-alert"></span> Warning</div></div>
    <div class="panel-body">
        Are you sure you want to delete this note? This action cannot be undone.
    </div>
     <div class="panel-footer text-right">
            <a href="javascript:$.magnificPopup.close();" class="btn btn-default mr10">Close</a> <button type="submit" class="btn btn-primary">Yes, delete</button>
    </div>
</div>

</form>
</div>