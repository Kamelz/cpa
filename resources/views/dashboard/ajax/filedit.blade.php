<div class="modal-panel">

<form action="{{ url('dashboard/file/edit/'.$code) }}" method="post">
<input type="hidden" name="_token" value="{{ csrf_token() }}">

<div class="panel">
    <div class="panel-heading"><div class="panel-title"><span class="pr5 panel-icon octicon octicon-pencil"></span> Edit File</div></div>
    <div class="panel-body">
  
        <div class="row p10">
        	<div class="col-md-4 p10">
        		<b>Filename</b> (without extension)
        	</div>
        	<div class="col-md-8">
        		<input name="filename" type="text" class="form-control" value="{{ $file->filename }}">
        	</div>
        </div>
        <div class="row p10">
        	<div class="col-md-4 p10">
        		<b>Fake size</b>
        	</div>
        	<div class="col-md-8">
        		<input type="text" value="{{$file->fakesize}}" class="form-control" name="fakesize">
        	</div>
        </div>
        <div class="row p10">
        	<div class="col-md-4 p10">
        		<b>Fake downloads</b>
        	</div>
        	<div class="col-md-8">
        		<input type="text" class="form-control" value="{{$file->fake_leads}}" name="fakeleads">
        	</div>
        </div>
    	
    </div>
     <div class="panel-footer text-right">
            <button type="submit" class="btn btn-primary">Save Details</button>
    </div>
</div>
</form>
</div>