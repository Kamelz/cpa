<div class="modal-panel">

<form action="{{ url('dashboard/file/'.$code) }}" method="post">
<input type="hidden" name="_token" value="{{ csrf_token() }}">

<div class="panel">
    <div class="panel-heading"><div class="panel-title"><span class="pr5 panel-icon octicon octicon-link"></span> Get File Links</div></div>
    <div class="panel-body">
        <div class="row p10">
            <div class="col-md-4 p10">
                <b>Your Link</b>
            </div>
            <div class="col-md-8 p10">
                <b class="text-success"><span class="domain">http://adgodmedia.com</span>/file/{{$code}}</b>
            </div>
        </div>
        <div class="row p10">
        <div class="col-md-4 p10">
            <b>Domain</b>
        </div>
        <div class="col-md-8">
                <select id="" class="domainselect form-control">
                    <option value="https://adgodmedia.com" selected="">adgodmedia.com</option>
                    @foreach ($mirrors as $mirror)
                    <option value="http://{{ $mirror->domain }}">{{ $mirror->domain }}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <hr class="short alt">
        <div class="row p10">
            <div class="col-md-12 p10">
                <b>Template</b><br>
                <span class="text-muted">Select your design and save</span>
            </div>
            <div class="col-md-12">
                @foreach ($templates as $template)
                    <div class="template">
                    <img src="{{ asset('templates') }}/{{$template->slug}}/{{$template->thumbnail}}" alt="" class="tmp holder-style" slug="{{$template->slug}}"><br>
                    <a href="{{ url('file/'.$code.'?template='.$template->slug) }}" target="_blank" class="btn btn-xs btn-default">Preview</a>
                    </div>
                @endforeach
            </div>
            <input type="hidden" name="template" value="default">
        </div>
    </div>
     <div class="panel-footer text-right">
            <button type="submit" class="btn btn-primary">Save Details</button>
    </div>
</div>
    
</form>
</div>

<style>
.holder-style {
    width: 180px;
    display: inline !important;
    padding: 8px !important;
    margin: 20px;
}
.template {
    padding: 20px;
    display:inline-block;
    text-align: center;
}
</style>

<script type="text/javascript">
    $('.tmp').click(function() {
        $('.tmp').removeClass('holder-active');
        $(this).addClass('holder-active');
        $('[name="template"]').val($(this).attr('slug'));
    });

    $('.domainselect').change(function() {
        $('.domain').html($(this).val());
    });
</script>
