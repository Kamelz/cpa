<div class="modal-panel">
<div class="panel">
    <div class="panel-heading"><div class="panel-title"><span class="pr5 panel-icon octicon octicon-file-text"></span>Your Note Link</div></div>
    <div class="panel-body">
        <div class="row">
        	<div class="col-md-12">
        		<b class="text-success">http://adgodmedia.com/note/{{ $note->code }}</b>
        	</div>
        </div>
    </div>
     <div class="panel-footer text-right">
            <a href="javascript:$.magnificPopup.close();" class="btn btn-default mr10">Close</a>
    </div>
</div>
</div>