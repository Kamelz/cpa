<div class="modal-panel">

<div class="panel">
    <div class="panel-heading"><div class="panel-title"><span class="pr5 panel-icon octicon octicon-link"></span> Integrate Gateway</div></div>
    <div class="panel-body">
        <div class="row p10">
            <div class="col-md-2 p10">
                <b>Your Link</b>
            </div>
            <div class="col-md-10 p10">
                <code>{{ htmlspecialchars('<script type="text/javascript" src="') }}{{ url('gateway/'.$gateway->code) }}{{ htmlspecialchars('"></script>')}}</code><br><br>
                Add script tag between your {{ htmlspecialchars('<head>') }} and {{ htmlspecialchars('</head>') }} tags
            </div>
        </div>
        @if($gateway->load_method == 'onclick')
        <div class="row p10">
        <div class="col-md-2 p10">
            <b>OnClick Method</b>
        </div>
        <div class="col-md-10">
                <div class="well">Add <code>onclick="loadlocker()"</code> to your links to manually trigger gateway locker</div>
            </div>
        </div>
        @endif
    </div>
</div>
</div>
