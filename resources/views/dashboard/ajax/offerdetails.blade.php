<div class="modal-panel">
<?php 
//var_dump($offer_details);
$pub = Auth::user()->id;
$payout = $offer_details['payout'] * ($gset->offerrate / 100); 
?>
<div class="panel">
    <div class="panel-heading"><div class="panel-title"><span class="pr5 panel-icon octicon octicon-database"></span> Offer details</div></div>
    <div class="panel-body">
        <div class="row p10">
            <div class="col-md-4 p10">
                <b>Offer title</b>
            </div>
            <div class="col-md-8 p10">
                <b>{{$offer_details['title']}}</b>
            </div>
        </div>
        <div class="row p10">
            <div class="col-md-4 p10">
                <b>Offer payout</b>
            </div>
            <div class="col-md-8 p10">
                <b>${{ formatnum($payout,2) }}</b>
            </div>
        </div>
        <div class="row p10">
            <div class="col-md-4 p10">
                <b>Offer Link</b>
            </div>
            <div class="col-md-8 p10">
                <div class="input-group mb20">
				  <input type="text" class="form-control" value="http://adgodmedia.com/go/{{$offer_details['campid']}}/{{$pub}}">
				  <span class="input-group-addon octicon octicon-link"></span>
				</div>

            </div>
        </div>		
    </div>
</div>
</div>

<style>
.holder-style {
    width: 180px;
    display: inline !important;
    padding: 8px !important;
    margin: 20px;
}
.template {
    padding: 20px;
    display:inline-block;
    text-align: center;
}
</style>

<script type="text/javascript">
    $('.tmp').click(function() {
        $('.tmp').removeClass('holder-active');
        $(this).addClass('holder-active');
        $('[name="template"]').val($(this).attr('slug'));
    });

    $('.domainselect').change(function() {
        $('.domain').html($(this).val());
    });
</script>
