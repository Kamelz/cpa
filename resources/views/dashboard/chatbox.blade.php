                        <div class="col-md-8">
                            <!-- Panel Wrapper -->
                                <div class="chat panel">

       <!-- Panel Heading -->
       <div class="panel-heading" style="height:auto;padding:0px;border-bottom:3px solid #70ca63;">

       <span class="panel-controls">
       <a href="" onclick="$('.panel-help').toggle();" class="octicon octicon-three-bars" data-toggle="tooltip" data-placement="left" title="" data-original-title="List of Commands"></a></span>

          <ul class="nav panel-tabs-border panel-tabs panel-tabs-left">
        <li ng-repeat="msg in messages | unique: 'scope'" ng-click="changecontext(msg.id)" ng-class="{'active':msg.id==context}" ng-dblclick="closecontext(msg.id)">
            <a href="">@{{msg.scope}}</a>
        </li>
    </ul>

       </div>

      <div class="panel-help" style="padding:30px;border-bottom:2px solid #ccc;position:absolute;width:100%;background:#fff;z-index:9999;display:none;">
	       <b>Commands:</b>
	       <br>
	       <i>/today</i> : shout today's earnings <br>
	       <i>/yesterday</i> : shout yesterday's earnings <br>
	       <i>/mtd</i> : shout month-to-date earnings <br>
	       <i>/alltime</i> : shout alltime earnings <br><br>
	       <b>Options:</b><br>
	       Double click on a user to start a private chat <br>
	       Double click user's tab to close chat
       </div>

                                   <!-- Panel Body -->
                                   <div class="row">

                                   <div class="col-md-9 col-sm-9" style="padding-right:0px !important">
                                   <div class="panel-body" id="msg-body" style="padding-left:30px;height:315px;overflow-y:scroll;border:none !important;">
                                    
                                    <div class="msg-list" ng-repeat="msg in messages | filter: {id: context}:strict">
									  
                                      <i class="octicon octicon-comment-discussion"></i> @if(Request::segment(1) == 'admin') <a href="{{ url('json/chat/delete') }}/@{{msg.msgid}}"><i class="octicon octicon-trashcan text-danger delmessage"></i></a> @endif <b>@{{msg.name}}:</b><small style="float:right;" ng-bind-html="formatTime(msg.timestamp)"></small><br>
                                      <span class="pl15 mmsg" ng-bind-html="msg.msg" ng-class="{'admin':msg.role=='admin','system':msg.role=='system'}"></span><br><br>
                                      
									</div>
                                   </div>

                                   </div>

                                     <div class="col-md-3 col-sm-3 hidden-phone" style="height:300px;overflow-y:auto;border:none !important;padding-left:0px;">
                                    <table class="table mbn tc-med-1 tc-bold-last users">
                                                <thead>
                                                    <tr class="hidden">
                                                        <th>#</th>
                                                        <th>First Name</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr ng-repeat="user in users | orderBy: '+role'" ng-dblclick="addChat(user.id,user.name)">
                                                        <td ng-class="{'me':user.id==myID,'admin':user.role=='admin'}">
                                                            <span class="octicon octicon-primitive-dot text-success fs14 mr10"></span>@{{ user.name }}</td>
                                                    </tr>
                                                </tbody>
                                            </table>

                                   </div>
                                   </div>

                                   <div class="panel-editbox" style="display: block;">
                                    
                                    <div class="row">
                                        <div class="col-md-9">

                                            <div class="input-group">
                                                <input class="form-control sendbox" type="text" ng-model="msg" ng-enter="sendmsg()" @if($disabled == 0) placeholder="Type your message.." @else placeholder="Your chat is disabled" disabled @endif style="text-align:left;">
                                                <span class="input-group-addon" onclick="$('.smileys').toggle();" style="cursor:pointer;" title="Smileys" data-toggle="tooltip" data-placement="left"><i class="icon-smile"></i>
                                                  </span>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <button ng-click="sendmsg()" ng-bind="btntext" class="btn btn-primary sendbtn" style="width:100%;">Send</button>
                                        </div>
                                    </div>
                                   </div>

     <!-- Panel Footer -->
     <div class="panel-footer smileys" style="display:none;">

     <div class="emoticon-smile" onclick="$('.sendbox').val($('.sendbox').val() + ':)');$('.sendbox').trigger('input');" style="cursor:pointer;"></div>

<div class="emoticon-sad" onclick="$('.sendbox').val($('.sendbox').val() + ':(');$('.sendbox').trigger('input');" style="cursor:pointer;"></div>

<div class="emoticon-laughing" onclick="$('.sendbox').val($('.sendbox').val() + ':D');$('.sendbox').trigger('input');" style="cursor:pointer;"></div>

<div class="emoticon-cool" onclick="$('.sendbox').val($('.sendbox').val() + '8)');$('.sendbox').trigger('input');" style="cursor:pointer;"></div>

<div class="emoticon-wink" onclick="$('.sendbox').val($('.sendbox').val() + ';)');$('.sendbox').trigger('input');" style="cursor:pointer;"></div>

<div class="emoticon-crying" onclick="$('.sendbox').val($('.sendbox').val() + ';(');$('.sendbox').trigger('input');" style="cursor:pointer;"></div>

<div class="emoticon-sweat" onclick="$('.sendbox').val($('.sendbox').val() + '(sweat)');$('.sendbox').trigger('input');" style="cursor:pointer;"></div>

<div class="emoticon-straight-face" onclick="$('.sendbox').val($('.sendbox').val() + ':|');$('.sendbox').trigger('input');" style="cursor:pointer;"></div>

<div class="emoticon-kiss" onclick="$('.sendbox').val($('.sendbox').val() + ':*');$('.sendbox').trigger('input');" style="cursor:pointer;"></div>

<div class="emoticon-tongue" onclick="$('.sendbox').val($('.sendbox').val() + ':P');$('.sendbox').trigger('input');" style="cursor:pointer;"></div>

<div class="emoticon-sleepy" onclick="$('.sendbox').val($('.sendbox').val() + '|-)');$('.sendbox').trigger('input');" style="cursor:pointer;"></div>

<div class="emoticon-love" onclick="$('.sendbox').val($('.sendbox').val() + '(love)');$('.sendbox').trigger('input');" style="cursor:pointer;"></div>

<div class="emoticon-grin" onclick="$('.sendbox').val($('.sendbox').val() + '(grin)');$('.sendbox').trigger('input');" style="cursor:pointer;"></div>

<div class="emoticon-yawn" onclick="$('.sendbox').val($('.sendbox').val() + '|-()');$('.sendbox').trigger('input');" style="cursor:pointer;"></div>

<div class="emoticon-vomit" onclick="$('.sendbox').val($('.sendbox').val() + '(puke)');$('.sendbox').trigger('input');" style="cursor:pointer;"></div>

<div class="emoticon-angry" onclick="$('.sendbox').val($('.sendbox').val() + ':@');$('.sendbox').trigger('input');" style="cursor:pointer;"></div>

<div class="emoticon-nerd" onclick="$('.sendbox').val($('.sendbox').val() + '8-|');$('.sendbox').trigger('input');" style="cursor:pointer;"></div>

<div class="emoticon-mouth-shut" onclick="$('.sendbox').val($('.sendbox').val() + ':x');$('.sendbox').trigger('input');" style="cursor:pointer;"></div>

<div class="emoticon-phone" onclick="$('.sendbox').val($('.sendbox').val() + '(call)');$('.sendbox').trigger('input');" style="cursor:pointer;"></div>

<div class="emoticon-silly" onclick="$('.sendbox').val($('.sendbox').val() + '(silly)');$('.sendbox').trigger('input');" style="cursor:pointer;"></div>

<div class="emoticon-smug" onclick="$('.sendbox').val($('.sendbox').val() + '(smug)');$('.sendbox').trigger('input');" style="cursor:pointer;"></div>

<div class="emoticon-secret" onclick="$('.sendbox').val($('.sendbox').val() + '(secret)');$('.sendbox').trigger('input');" style="cursor:pointer;"></div>

<div class="emoticon-idea" onclick="$('.sendbox').val($('.sendbox').val() + '(idea)');$('.sendbox').trigger('input');" style="cursor:pointer;"></div>

<div class="emoticon-loser" onclick="$('.sendbox').val($('.sendbox').val() + '(loser)');$('.sendbox').trigger('input');" style="cursor:pointer;"></div>

<div class="emoticon-music" onclick="$('.sendbox').val($('.sendbox').val() + '(music)');$('.sendbox').trigger('input');" style="cursor:pointer;"></div>

<div class="emoticon-sick" onclick="$('.sendbox').val($('.sendbox').val() + '(sick)');$('.sendbox').trigger('input');" style="cursor:pointer;"></div>

<div class="emoticon-phew" onclick="$('.sendbox').val($('.sendbox').val() + '(phew)');$('.sendbox').trigger('input');" style="cursor:pointer;"></div>

<div class="emoticon-sleep" onclick="$('.sendbox').val($('.sendbox').val() + '(sleep)');$('.sendbox').trigger('input');" style="cursor:pointer;"></div>

<div class="emoticon-hug" onclick="$('.sendbox').val($('.sendbox').val() + '(hug)');$('.sendbox').trigger('input');" style="cursor:pointer;"></div>

<div class="emoticon-thinking" onclick="$('.sendbox').val($('.sendbox').val() + '(thinking)');$('.sendbox').trigger('input');" style="cursor:pointer;"></div>

<div class="emoticon-dizzy" onclick="$('.sendbox').val($('.sendbox').val() + '(dizzy)');$('.sendbox').trigger('input');" style="cursor:pointer;"></div>

<div class="emoticon-drool" onclick="$('.sendbox').val($('.sendbox').val() + '(drool)');$('.sendbox').trigger('input');" style="cursor:pointer;"></div>

<div class="emoticon-bye" onclick="$('.sendbox').val($('.sendbox').val() + '(bye)');$('.sendbox').trigger('input');" style="cursor:pointer;"></div>

<div class="emoticon-scared" onclick="$('.sendbox').val($('.sendbox').val() + '(scared)');$('.sendbox').trigger('input');" style="cursor:pointer;"></div>

<div class="emoticon-smoke" onclick="$('.sendbox').val($('.sendbox').val() + '(ci)');$('.sendbox').trigger('input');" style="cursor:pointer;"></div>

<div class="emoticon-rainning" onclick="$('.sendbox').val($('.sendbox').val() + '(rain)');$('.sendbox').trigger('input');" style="cursor:pointer;"></div>

<div class="emoticon-dead" onclick="$('.sendbox').val($('.sendbox').val() + '(dead)');$('.sendbox').trigger('input');" style="cursor:pointer;"></div>

<div class="emoticon-zombie" onclick="$('.sendbox').val($('.sendbox').val() + '(zombie)');$('.sendbox').trigger('input');" style="cursor:pointer;"></div>

<div class="emoticon-cold" onclick="$('.sendbox').val($('.sendbox').val() + '(cold)');$('.sendbox').trigger('input');" style="cursor:pointer;"></div>

<div class="emoticon-devil" onclick="$('.sendbox').val($('.sendbox').val() + '(devil)');$('.sendbox').trigger('input');" style="cursor:pointer;"></div>

<div class="emoticon-clown" onclick="$('.sendbox').val($('.sendbox').val() + '(clown)');$('.sendbox').trigger('input');" style="cursor:pointer;"></div>

<div class="emoticon-dog" onclick="$('.sendbox').val($('.sendbox').val() + '(dog)');$('.sendbox').trigger('input');" style="cursor:pointer;"></div>

<div class="emoticon-cat" onclick="$('.sendbox').val($('.sendbox').val() + '(cat)');$('.sendbox').trigger('input');" style="cursor:pointer;"></div>

<div class="emoticon-heart" onclick="$('.sendbox').val($('.sendbox').val() + '(heart)');$('.sendbox').trigger('input');" style="cursor:pointer;"></div>

<div class="emoticon-win" onclick="$('.sendbox').val($('.sendbox').val() + '(win)');$('.sendbox').trigger('input');" style="cursor:pointer;"></div>

<div class="emoticon-bomb" onclick="$('.sendbox').val($('.sendbox').val() + '(bomb)');$('.sendbox').trigger('input');" style="cursor:pointer;"></div>

<div class="emoticon-wave" onclick="$('.sendbox').val($('.sendbox').val() + '(wave)');$('.sendbox').trigger('input');" style="cursor:pointer;"></div>

<div class="emoticon-sun" onclick="$('.sendbox').val($('.sendbox').val() + '(sun)');$('.sendbox').trigger('input');" style="cursor:pointer;"></div>

<div class="emoticon-night" onclick="$('.sendbox').val($('.sendbox').val() + '(night)');$('.sendbox').trigger('input');" style="cursor:pointer;"></div>

<div class="emoticon-stars" onclick="$('.sendbox').val($('.sendbox').val() + '(stars)');$('.sendbox').trigger('input');" style="cursor:pointer;"></div>

<div class="emoticon-gift" onclick="$('.sendbox').val($('.sendbox').val() + '(gift)');$('.sendbox').trigger('input');" style="cursor:pointer;"></div>

<div class="emoticon-flower" onclick="$('.sendbox').val($('.sendbox').val() + '(flower)');$('.sendbox').trigger('input');" style="cursor:pointer;"></div>

<div class="emoticon-poo" onclick="$('.sendbox').val($('.sendbox').val() + '(shit)');$('.sendbox').trigger('input');" style="cursor:pointer;"></div>

<div class="emoticon-drinks" onclick="$('.sendbox').val($('.sendbox').val() + '(drinks)');$('.sendbox').trigger('input');" style="cursor:pointer;"></div>

<div class="emoticon-balloon" onclick="$('.sendbox').val($('.sendbox').val() + '(balloon)');$('.sendbox').trigger('input');" style="cursor:pointer;"></div>

<div class="emoticon-clock" onclick="$('.sendbox').val($('.sendbox').val() + '(clock)');$('.sendbox').trigger('input');" style="cursor:pointer;"></div>

<div class="emoticon-mail" onclick="$('.sendbox').val($('.sendbox').val() + '(mail)');$('.sendbox').trigger('input');" style="cursor:pointer;"></div>

<div class="emoticon-rainbow" onclick="$('.sendbox').val($('.sendbox').val() + '(rainbow)');$('.sendbox').trigger('input');" style="cursor:pointer;"></div>

<div class="emoticon-ghost" onclick="$('.sendbox').val($('.sendbox').val() + '(ghost)');$('.sendbox').trigger('input');" style="cursor:pointer;"></div>

<div class="emoticon-ninja" onclick="$('.sendbox').val($('.sendbox').val() + '(ninja)');$('.sendbox').trigger('input');" style="cursor:pointer;"></div>

<div class="emoticon-pig" onclick="$('.sendbox').val($('.sendbox').val() + '(pig)');$('.sendbox').trigger('input');" style="cursor:pointer;"></div>

     </div>

                                </div>
                        </div>