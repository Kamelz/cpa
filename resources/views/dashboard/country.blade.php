<!DOCTYPE html>
<html ng-app="ngDashboard" ng-controller="PageController">

<head>
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <title>AdGodMedia - Country Breakdown</title>
    <meta name="keywords" content="{{ $gset->keywords }}" />
    <meta name="description" content="{{ $gset->description }}">

    <meta name="csrf-token" content="{{ csrf_token() }}" />

    <meta name="author" content="AdminDesigns">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- Font CSS (Via CDN) -->
    <link rel='stylesheet' type='text/css' href='//fonts.googleapis.com/css?family=Open+Sans:400,600,700'>
    <link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Roboto:300,400,500,700">

    <!-- Theme CSS -->
    <link rel="stylesheet" type="text/css" href="{{ asset('panel/assets/skin/default_skin/css/theme.css') }}">

    <!-- Custom CSS -->
    <link rel="stylesheet" type="text/css" href="{{ asset('panel/assets/skin/default_skin/css/custom.css') }}">

    <!-- Admin Panels CSS -->
    <link rel="stylesheet" type="text/css" href="{{ asset('panel/assets/admin-tools/admin-plugins/admin-panels/adminpanels.css') }}">

    <link rel="stylesheet" type="text/css" href="//cloud.github.com/downloads/lafeber/world-flags-sprite/flags16.css" />


    <link rel="stylesheet" type="text/css" href="{{ asset('panel/vendor/plugins/datepicker/css/bootstrap-datetimepicker.min.css') }}">

    <link href="{{ asset('panel/vendor/plugins/datatables/media/css/dataTables.bootstrap.css') }}" rel="stylesheet" type="text/css">

    <!-- Favicon -->
    <link rel="shortcut icon" href="{{ asset('favicon.ico') }}">

    <script type="text/javascript" src="{{ asset('panel/vendor/jquery/jquery-1.11.1.min.js') }}"></script>
    <script src="//ajax.googleapis.com/ajax/libs/angularjs/1.2.26/angular.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.3.14/angular-sanitize.min.js"></script>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->

@include('ganalytics') </head>

<body class="dashboard-page sb-l-o sb-r-c">

    <!-- Start: Main -->
    <div id="main">

    @include('dashboard.header')

    @include('dashboard.sidebar')



        <!-- Start: Content-Wrapper -->
        <section id="content_wrapper">

            <!-- Start: Topbar -->
            <header id="topbar">
                <div class="topbar-left">
                    <ol class="breadcrumb">
                        <li class="crumb-active">
                            <a href="{{ url('dashboard/countrybreakdown') }}">Country Breakdown</a>
                        </li>
                    </ol>
                </div>
            </header>
            <!-- End: Topbar -->

            <!-- Begin: Content -->
            <section id="content" class="animated fadeIn table-layout">

            <div class="tray tray-center p25 va-t posr">
              
<div class="panel" id="spy3">
<div class="panel-heading">
<span class="panel-icon"><i class="fa fa-pencil"></i>
</span>
<span class="panel-title"> Country Heatmap</span><span class="panel-controls">{{ date('Y-m-d',strtotime($from)) }} to {{ date('Y-m-d',strtotime($to)) }}</span>
</div>
<div class="panel-body dark">
<div id="WidgetMap" style="height: 320px; width: 100%;"></div>
</div>
</div>

<div class="panel panel-visible" id="spy1">
<div class="panel-heading">
<div class="panel-title hidden-xs">
<span class="glyphicon glyphicon-tasks"></span>Breakdown</div>
</div>
<div class="panel-body pn">
<table class="table table-striped table-hover" id="datatable" cellspacing="0" width="100%">
<thead>
<tr>
<th>Country</th>
<th>Clicks</th>
<th>Leads</th>
<th>Earnings</th>
</tr>
</thead>
<tbody class="f16">
@foreach ($countries as $country)
<tr>
  <td><span class="flag {{ strtolower($country->country) }}"></span> {{ $codes->where('option',$country->country)->first()->value }}</td>
  <td>{{ formatnum($country->clicks) }}</td>
  <td>{{ formatnum($country->leads) }}</td>
  <td>${{ formatnum($country->payout,2) }}</td>
</tr>
@endforeach
</tbody>
<tfoot></tfoot>
</table>
</div>
</div>

</div>

<aside class="tray tray-right tray290 va-t pn" data-tray-height="match" style="height: 1100px;">

<div class="p20 admin-form">
<h4 class="mt5 text-muted fw500"> Filter</h4>
<hr class="short">
<h6 class="fw400">Quick Select</h6>
<div class="section mb15">
<label class="field select">
<select class="quickfilter form-control">
<option value="" selected="">Select..</option>
<option value="today">Today</option>
<option value="yesterday">Yesterday</option>
<option value="-7 days">Last 7 Days</option>
<option value="this month">This Month</option>
<option value="last month">Last Month</option>
<option value="ytd">YTD</option>
<option value="alltime">All Time</option>
</select>
<i class="arrow double"></i>
</label>
</div>

<form action="{{ url('dashboard/countrybreakdown') }}" method="post">
<input type="hidden" name="_token" value="{{ csrf_token() }}">

<h6 class="fw400">Date Range</h6>
<div class="section row">
<div class="col-md-12">

<div class="input-group mb20">
<input id="date1" class="form-control" placeholder="2015-03-21" name="fromDate" type="text">
<span class="input-group-addon octicon octicon-calendar"></span>
</div>

<div class="input-group mb20">
<input id="date2" class="form-control" placeholder="2015-03-21" name="toDate" type="text">
<span class="input-group-addon octicon octicon-calendar"></span>
</div>

</div>
</div>

<div class="section row">
<div class="col-sm-12">
<button class="btn btn-default btn-sm ph25" type="submit">Update</button>
</div>
</div>
</div>

</form>
</aside>

            </section>
            <!-- End: Content -->

        </section>
        <!-- End: Content-Wrapper -->

    </div>
    <!-- End: Main -->

    <!-- BEGIN: PAGE SCRIPTS -->

    <!-- jQuery -->
    <script type="text/javascript" src="{{ asset('panel/vendor/jquery/jquery_ui/jquery-ui.min.js') }}"></script>

    <!-- Bootstrap -->
    <script type="text/javascript" src="{{ asset('panel/assets/js/bootstrap/bootstrap.min.js') }}"></script>

    <!-- Theme Javascript -->
    <script type="text/javascript" src="{{ asset('panel/assets/js/utility/utility.js') }}"></script>
    <script type="text/javascript" src="{{ asset('panel/assets/js/main.js') }}"></script>

        <!-- Load Additional Jvectormaps -->
    <script type="text/javascript" src="{{ asset('panel/assets/js/utility/jvectormap/assets/jquery-jvectormap-world-mill-en.js') }}"></script>

    <!-- Admin Panels  -->
    <script type="text/javascript" src="{{ asset('panel/assets/admin-tools/admin-plugins/admin-panels/json2.js') }}"></script>
    <script type="text/javascript" src="{{ asset('panel/assets/admin-tools/admin-plugins/admin-panels/jquery.ui.touch-punch.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('panel/assets/admin-tools/admin-plugins/admin-panels/adminpanels.js') }}"></script>

    <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.8.3/moment.js"></script>

    <script type="text/javascript" src="{{ asset('panel/vendor/plugins/datepicker/js/bootstrap-datetimepicker.min.js') }}"></script>

    <script src="{{ asset('panel/vendor/plugins/datatables/media/js/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('panel/vendor/plugins/datatables/media/js/dataTables.bootstrap.js') }}"></script>

     <script>

soundManager.setup({
  url: '{{ asset('panel/assets/swf') }}/',
  onready: function() {
    lead_snd = soundManager.createSound({
        url: '{{ asset('panel/assets/audio/chaching.mp3') }}'
      });
  }
});

var app = angular.module('ngDashboard', ['ngSanitize']);
app.controller('PageController', function($scope,$http,$interval) {
  
  $scope.earnings = '${{ formatnum($today->payout,2) }}';
  $scope.clicks = '{{ formatnum($today->clicks) }}';
  $scope.leads = '{{ formatnum($today->leads) }}';
  $scope.epc = '${{ formatnum(division($today->payout,$today->clicks),2) }}';

var serverdate = new Date({{ date("Y, m, d, H, i, s") }});
function displaytime(){
    serverdate.setSeconds(serverdate.getSeconds() + 1);
    var hh=serverdate.getHours();
    var m=serverdate.getMinutes();
    var s=serverdate.getSeconds();
    
    var dd = "AM";
    var h = hh;
    if (h >= 12) {
        h = hh-12;
        dd = "PM";
    }
    if (h == 0) {
        h = 12;
    }
    m=checkTime(m);
    s=checkTime(s);
    var output = h+":"+m+" "+dd;
    $scope.serverTime = output;
}

// Add leading zero in time
function checkTime(i){if (i<10){i="0" + i;}return i;}

var getEarn = function() {
    $http.get( "{{ url('dashboard/earnings') }}").success(function(data, status, headers, config) {
        if ($scope.leads < data.leads) { lead_snd.play(); }
        if (data.clicks == undefined) { $interval.cancel(makeitrain); return; }
        $scope.clicks = data.clicks;
        $scope.leads = data.leads;
        $scope.earnings = data.earnings;
        $scope.epc = data.epc;
    });
};

$interval(displaytime, 1000);
makeitrain = $interval(getEarn, 60000);
});

</script>

    <script type="text/javascript">
        jQuery(document).ready(function() {

            "use strict";

            // Init Theme Core      
            Core.init({
                sbm: "sb-l-c",
            });


            $('#date1').datetimepicker({format: "YYYY-MM-DD"});
            $('#date2').datetimepicker({format: "YYYY-MM-DD"});

            $('.quickfilter').change( function() {
                var d = new Date();
                if ($(this).val() == 'today') { 
                    var from = moment().format('YYYY')+'-'+moment().format('MM')+'-'+moment().format('DD'); 
                    var to = moment().format('YYYY')+'-'+moment().format('MM')+'-'+moment().format('DD'); 

                } else if ($(this).val() == '-7 days') {
                  var from = moment().subtract(7,'days').format('YYYY')+'-'+moment().subtract(7,'days').format('MM')+'-'+moment().subtract(7,'days').format('DD');
                  var to = moment().format('YYYY')+'-'+moment().format('MM')+'-'+moment().format('DD');
                } else if ($(this).val() == 'this month') {
                    var from = moment().startOf('month').format('YYYY')+'-'+moment().startOf('month').format('MM')+'-'+moment().startOf('month').format('DD');
                    var to = moment().format('YYYY')+'-'+moment().format('MM')+'-'+moment().format('DD');
                } else if ($(this).val() == 'yesterday') {
                    var from = moment().subtract(1,'days').format('YYYY')+'-'+moment().subtract(1,'days').format('MM')+'-'+moment().subtract(1,'days').format('DD');
                    var to = moment().subtract(1,'days').format('YYYY')+'-'+moment().subtract(1,'days').format('MM')+'-'+moment().subtract(1,'days').format('DD');
                } else if ($(this).val() == 'last month') {
                    var from = moment().subtract(1,'months').startOf('month').format('YYYY')+'-'+moment().subtract(1,'months').startOf('month').format('MM')+'-'+moment().subtract(1,'months').startOf('month').format('DD');
                    var to = moment().subtract(1,'months').endOf('month').format('YYYY')+'-'+moment().subtract(1,'months').endOf('month').format('MM')+'-'+moment().subtract(1,'months').endOf('month').format('DD');
                } else if ($(this).val() == 'ytd') {
                    var from = moment().startOf('year').format('YYYY')+'-'+moment().startOf('year').format('MM')+'-'+moment().startOf('year').format('DD');
                    var to = moment().format('YYYY')+'-'+moment().format('MM')+'-'+moment().format('DD'); 
                } else if ($(this).val() == 'alltime') {
                    var from = '2015-01-01';
                    var to = moment().format('YYYY')+'-'+moment().format('MM')+'-'+moment().format('DD'); 
                }
                    $('#date1').val(from); 
                    $('#date2').val(to); 
            });

            $('#datatable').dataTable({"sDom": 't<"dt-panelfooter clearfix"ip>',"aaSorting": []});


            // Init Widget Demo JS
            // demoHighCharts.init();

            // Because we are using Admin Panels we use the OnFinish 
            // callback to activate the demoWidgets. It's smoother if
            // we let the panels be moved and organized before 
            // filling them with content from various plugins

            // Init plugins used on this page
            // HighCharts, JvectorMap, Admin Panels

            // Init Admin Panels on widgets inside the ".admin-panels" container
            $('.admin-panels').adminpanel({
                grid: '.admin-grid',
                draggable: false,
                preserveGrid: true,
                mobile: true,
                callback: function() {
                    bootbox.confirm('<h3>A Custom Callback!</h3>', function() {});
                },
                onFinish: function() {
                    $('.admin-panels').addClass('animated fadeIn').removeClass('fade-onload');

                    // Init the rest of the plugins now that the panels
                    // have had a chance to be moved and organized.
                    // It's less taxing to organize empty panels
                    demoHighCharts.init();

                    // We also refresh any "in-view" waypoints to ensure
                    // the correct position is being calculated after the 
                    // Admin Panels plugin moved everything
                    //Waypoint.refreshAll();

                },
                onSave: function() {
                    $(window).trigger('resize');
                }
            });

            $('.ann-tab').click(function() {
                $('.ann-tab').removeClass('active');
                $(this).addClass('active');

                $('.ann-body').addClass('hidden');
                $($(this).attr('href')).removeClass('hidden')
            });

            var earn = {@foreach ($countries as $country)
                            "{{$country->country}}": {{ $country->payout }},
                          @endforeach "undefined": 0};

            
            $('#WidgetMap').vectorMap({
                map: 'world_mill_en',
                backgroundColor: 'transparent',
                regionStyle: {
                    initial: {
                        fill: '#f7c65f'
                    },
                    hover: {
                        fill: '#6e7072'
                    }
                },
                onRegionLabelShow: function(e, el, code) {
                    if (earn[code] == undefined) earn[code] = 0;
                    el.html(el.html()+' (Earnings: $'+earn[code]+')');
                },
                series: {
                    regions: [{
                        scale: ['#f7c65f', '#ec6f5a'],
                        normalizeFunction: 'polynomial',
                        values: earn
                    }]
                }
            });


        });
    </script>

    <!-- END: PAGE SCRIPTS -->

</body>

</html>
