<!DOCTYPE html>
<html ng-app="ngDashboard" ng-controller="PageController">

<head>
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <title>AdGodMedia - Create Gateway Locker</title>
    <meta name="keywords" content="{{ $gset->keywords }}" />
    <meta name="description" content="{{ $gset->description }}">

    <meta name="csrf-token" content="{{ csrf_token() }}" />

    <meta name="author" content="AdminDesigns">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- Font CSS (Via CDN) -->
    <link rel='stylesheet' type='text/css' href='//fonts.googleapis.com/css?family=Open+Sans:400,600,700'>
    <link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Roboto:300,400,500,700">

    @if (isset($gfonts))
    <link rel="stylesheet" href="//fonts.googleapis.com/css?family={{ $gfonts }}" name="gfonts" type="text/css" />
    @endif

    <!-- Theme CSS -->
    <link rel="stylesheet" type="text/css" href="{{ asset('panel/assets/skin/default_skin/css/theme.css') }}">

    <!-- Custom CSS -->
    <link rel="stylesheet" type="text/css" href="{{ asset('panel/assets/skin/default_skin/css/custom.css') }}">

    <!-- Admin Panels CSS -->
    <link rel="stylesheet" type="text/css" href="{{ asset('panel/assets/admin-tools/admin-plugins/admin-panels/adminpanels.css') }}">

    <link rel="stylesheet" href="{{ asset('panel/css/igrowl.min.css') }}">

    <link href="{{ asset('panel/vendor/plugins/colorpicker/css/bootstrap-colorpicker.min.css') }}" rel="stylesheet" type="text/css">

    <!-- Favicon -->
    <link rel="shortcut icon" href="{{ asset('favicon.ico') }}">

    <script type="text/javascript" src="{{ asset('panel/vendor/jquery/jquery-1.11.1.min.js') }}"></script>
    <script src="//ajax.googleapis.com/ajax/libs/angularjs/1.2.26/angular.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.3.14/angular-sanitize.min.js"></script>

    <script type="text/javascript" src="{{ asset('panel/assets/js/angular.ngcss.min.js') }}"></script>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->

<script type="text/javascript">
            //# Setup the required globals
            var ngDashboard = angular.module("ngDashboard", ["ngCss"]),
                sScopeAccess = "local"
            ;

            //# Define Angular page .controller
            ngDashboard.controller("PageController", ["$scope", "ngCss", function ($scope, ngCss) {
                //# Import ngCss into our $scope (for use in the button's ng-click)
                $scope.ngCss = ngCss;

                $scope.gatewayname = "{{ $gateway->name }}";
                $scope.redirecturl = "{{ $gateway->redirect }}";
                $scope.earnings = '{{ $today->payout }}';

                var serverdate = new Date({{ date("Y, m, d, H, i, s") }});
                function displaytime(){
                    serverdate.setSeconds(serverdate.getSeconds() + 1);
                    var hh=serverdate.getHours();
                    var m=serverdate.getMinutes();
                    var s=serverdate.getSeconds();
                    
                    var dd = "AM";
                    var h = hh;
                    if (h >= 12) {
                        h = hh-12;
                        dd = "PM";
                    }
                    if (h == 0) {
                        h = 12;
                    }
                    m=checkTime(m);
                    s=checkTime(s);
                    var output = h+":"+m+" "+dd;
                    $scope.serverTime = output;
                }

                function checkTime(i){if (i<10){i="0" + i;}return i;}

                displaytime();

                $scope.title ="{{ $gateway->title }}";
                $scope.inst = "{{ $gateway->inst }}";

                $scope.loadmethod = "{{ $gateway->load_method }}";

                $scope.offers = ["Win an XBox 360 & Kinect","Download Xvid Media Player","Win one of ten $50 Tesco Gift Card","Get A Years Supply of Walkers Crisps","Play to win $500 to spend at IKEA","$500 Gift Card to spend at Argos","Sign Up Today And Win A MacBook Air"];
                $scope.numOffers = {{ $gateway->numOffers }};

                $scope.noredirect = @if (empty($gateway->redirect)) true @else false @endif;
                $scope.custom = @if ($gateway->customcss == 'true') true @else false @endif;
                $scope.allowclose = @if ($gateway->allow_close == 0) false @else true @endif;

                //# Collect the $scope used by #myCss
                ngCss.getScope("myCss", function($cssScope, isIsolate) {
                    //# Setup the $cssScope

                    $cssScope.css = {
                        bg: '{{ $gateway->bgimg }}',
                        width: {{ $css['width'] }},
                        height: {{ $css['height'] }},
                        bradius: {{ $css['border_radius'] }},
                        tfont: '{{ $css['title_font'] }}',
                        tcolor: '{{ $css['title_color'] }}',
                        tfontsize: '{{ $css['title_font_size'] }}',
                        ttopmargin: '{{ $css['title_top_margin'] }}',
                        ifont: '{{ $css['instruction_font'] }}',
                        icolor: '{{ $css['instruction_color'] }}',
                        ifontsize: '{{ $css['instruction_font_size'] }}',
                        itopmargin: '{{ $css['instruction_top_margin'] }}',
                        ofont: '{{ $css['offers_font'] }}',
                        ocolor: '{{ $css['offers_color'] }}',
                        ofontsize:'{{ $css['offers_font_size'] }}',
                        otopmargin: '{{ $css['offers_top_margin'] }}',
                        oalignment: '{{ $css['offers_alignment'] }}',
                    };

                    $scope.savegateway = function() {
                        $.post( "{{ url('dashboard/gateway/create') }}", { 
                            _token: $('meta[name="csrf-token"]').attr('content'),
                            gateway_name: $scope.gatewayname,
                            redirect_url: $scope.redirecturl,
                            load_method: $scope.loadmethod,
                            num_offers: $scope.numOffers,
                            width: $cssScope.css.width,
                            height: $cssScope.css.height,
                            border_radius: $cssScope.css.bradius,
                            title_font: $cssScope.css.tfont,
                            title_color: $cssScope.css.tcolor,
                            title_font_size: $cssScope.css.tfontsize,
                            title_top_margin: $cssScope.css.ttopmargin,
                            instruction_font: $cssScope.css.ifont,
                            instruction_color: $cssScope.css.icolor,
                            instruction_font_size: $cssScope.css.ifontsize,
                            instruction_top_margin: $cssScope.css.itopmargin,
                            offers_font: $cssScope.css.ofont,
                            offers_color: $cssScope.css.ocolor,
                            offers_font_size: $cssScope.css.ofontsize,
                            offers_top_margin: $cssScope.css.otopmargin,
                            offers_alignment: $cssScope.css.oalignment,
                            title: $scope.title,
                            instruction: $scope.inst,
                            bgimage: $cssScope.css.bg,
                            noredirect: $scope.redirect,
                            custom: $scope.custom,
                            custom_css: $('.formatcss').val(),
                            validcss: $('#validcss').val(),
                            allow_close: $scope.allowclose,
                            gateway_id: {{ $gateway_id }}
                          })
                        .done(function( data ) {
                            $.each( data, function( key, value ) {
                                    if (value.title == 'Success') {
                                      $.iGrowl({title:'Success',message: value.message});
                                      window.location.href = '{{ url('dashboard/gateways') }}';
                                    } else {
                                      $.iGrowl({title:'Whoops!',message: value.message, type: 'notice'});
                                    }
                            });
                        });
                    };

                    //# Copy the $cssScope into our own $scope
                    $scope.$cssScope = $cssScope
                });

            }]);
</script>

<style type="text/css" id="myCss" ng-css="{ script: false, live: true, async: true, prelink: undefined }">
.widget {
    height: @{{css.height}}px;
    width: @{{css.width}}px;
    color: #fff;
    top: 187px;
    transform: transformY(-50%);
    background-image: url('@{{css.bg}}');
    position:fixed;
    text-align: center;
    background-color:#000;
    border-radius: @{{css.bradius}}px;
    padding-left: 30px;
    padding-right: 30px
}
.widget h2 {
  font-family: '@{{css.tfont}}',sans-serif;
  color: @{{css.tcolor}};
  font-size: @{{css.tfontsize}}px;
  margin-top: @{{css.ttopmargin}}px;
}
.widget p {
  font-family: '@{{css.ifont}}',sans-serif;
  color: @{{css.icolor}};
  font-size: @{{css.ifontsize}}px;
  margin-top: @{{css.itopmargin}}px;
}
.widget ul {
  list-style-type: none;
  margin-top: @{{css.otopmargin}}px;
  text-align: @{{css.oalignment}};
  padding-left: 40px;
  padding-right: 40px;
}
.widget ul a {
  font-family: '@{{css.ofont}}',sans-serif;
  color: @{{css.ocolor}};
  font-size: @{{css.ofontsize}}px;
}
.none {

}
</style>

@include('ganalytics') </head>

<body class="dashboard-page sb-l-o sb-r-c sb-l-m">

    <!-- Start: Main -->
    <div id="main">

    @include('dashboard.header')

    @include('dashboard.sidebar')



        <!-- Start: Content-Wrapper -->
        <section id="content_wrapper">

            <!-- Start: Topbar -->
            <header id="topbar">
                <div class="topbar-left">
                    <ol class="breadcrumb">
                        <li class="crumb-active">
                            <a href="{{ url('dashboard/gateway/create') }}">Create Gateway</a>
                        </li>
                    </ol>
                </div>
            </header>
            <!-- End: Topbar -->

            <!-- Begin: Content -->
            <section id="content" class="animated fadeIn pn">

<div class="tray tray-top p20 va-t posr">
  <div class="row">
    <div class="col-md-4 admin-form">
        <div class="panel" id="spy3">
        <div class="panel-heading">
        <span class="panel-title"> General Settings</span>
        <span class="panel-controls"><a href="#" class="panel-control-loader"></a><a href="#" class="panel-control-collapse"></a></span>
        </div>
        <div class="panel-body dark">

            <div class="row"><div class="col-md-12"><h5>Gateway Name</h5></div></div>

            <div class="row">
              <div class="col-md-12">
                  <div class="form-group">
                  <input type="text" placeholder="My Locker" class="form-control" ng-model="gatewayname">
                  </div>
              </div>
            </div>

            <div class="row"><div class="col-md-12"><h5>Redirect Url</h5></div></div>

            <div class="row"><div class="col-md-12">
                    <div class="form-group">
                    <input type="text" class="form-control" placeholder="http://domain.com/unlock" ng-model="redirecturl">
                    </div>
                      <div class="checkbox-custom mb5">
                    <input type="checkbox" id="checkboxDefault4" ng-model="redirect">
                    <label for="checkboxDefault4">No redirect <span class="text-muted">(just close)</span></label>
                    </div>
            </div></div>

            <div class="row"><div class="col-md-12"><h5>Load Method</h5></div></div>

            <div class="row"><div class="col-md-12">
              <select name="load_method" id="" class="form-control" ng-model="loadmethod">
                <option value="onload">On Load</option>
                <option value="onclick">On Click</option>
              </select>
            </div></div>

        </div>
        </div>

        <div class="panel sort-disable" data-animate="fadeIn" id="p2" data-panel-color="false" data-panel-fullscreen="false" data-panel-remove="false" data-panel-title="false">
<div class="panel-heading">
<span class="panel-title"> Appearance</span>
<span class="panel-controls"><a href="#" class="panel-control-loader"></a><a href="#" class="panel-control-collapse"></a></span></div>
<div class="panel-body">

            <div class="row"><div class="col-md-12"><h5>Title</h5></div></div>

            <div class="row"><div class="col-md-12">
                    <div class="form-group">
                    <input class="form-control" id="disabledInput" ng-model="title">
                    </div>
            </div></div>

            <div class="row"><div class="col-md-12"><h5>Instructions</h5></div></div>

            <div class="row"><div class="col-md-12">
                  <div class="form-group">
                  <textarea class="form-control" id="textArea1" rows="4" ng-model="inst"></textarea>
                  </div>
            </div></div>

            
            <div class="row">
              <div class="col-md-12"><h5>Background Image</h5></div>
            </div>

            <div class="row">
              <div class="col-md-6">
                  <div class="form-group">
                  <input type="text" ng-model="$cssScope.css.bg" class="form-control">
                  </div>
              </div>
              <div class="col-md-6">
                  <div class="form-group">
                    <div class="section">
                    <label class="field select">
                    <select name="" id="" ng-model="$cssScope.css.bg" class="form-control">
                      <option value="//i.imgur.com/R5UHBqR.gif" selected>Blank</option>
                      <option value="//i.imgur.com/OEpPERA.jpg">Grey</option>
                    </select>
                    <i class="arrow"></i>
                    </label>
                    </div>
                  </div>
              </div>
            </div>

            <div class="row">
              <div class="col-md-6"><h5>Width</h5></div>
              <div class="col-md-6"><h5>Height</h5></div>
            </div>

            <div class="row">
            <div class="col-md-6">
              <div class="form-group">
              <div class="input-group">
                <input class="form-control" ng-model="$cssScope.css.width">
                <div class="input-group-addon">px</div>
              </div>
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
              <div class="input-group">
                <input class="form-control" ng-model="$cssScope.css.height">
                <div class="input-group-addon">px</div>
              </div>
              </div>
            </div>
            </div>

            <div class="row"><div class="col-md-12"><h5>Border Radius</h5></div></div>

            <div class="row"><div class="col-md-12">
                  <div class="form-group">
                  <div class="input-group">
                  <input class="form-control" ng-model="$cssScope.css.bradius">
                  <div class="input-group-addon">px</div>
                  </div>
                  </div>
            </div></div>

            <div class="row"><div class="col-md-12">

                <div class="checkbox-custom mb5">
                    <input type="checkbox" id="checkboxDefault9" ng-model="allowclose">
                    <label for="checkboxDefault9">Allow widget to be closed</label>
                    </div>

            </div></div>

            <div class="row">
              <div class="col-md-6"><h5>Title Color</h5></div>
              <div class="col-md-6"><h5>Title Font Size</h5></div>
            </div>

            <div class="row">
            <div class="col-md-6">
              <div class="form-group">
              <div id="title-color" class="input-group cursor">
                  <span class="input-group-addon"><i></i>
                  </span>
                  <input type="text" value="" class="form-control" ng-model="$cssScope.css.tcolor" />
              </div>
              </div>
            </div>

            <div class="col-md-6">
                <div class="form-group">
                <div class="input-group">
                  <input class="form-control" ng-model="$cssScope.css.tfontsize">
                  <div class="input-group-addon">px</div>
                </div>
                </div>
            </div>
            </div>

            <div class="row"><div class="col-md-6"><h5>Title Font</h5></div><div class="col-md-6"><h5>Title Top Margin</h5></div></div>

            <div class="row">
            <div class="col-md-6">

            <div class="form-group">
                  <div class="section">
                  <label class="field select">
                  <select id="tfont" name="country" ng-model="$cssScope.css.tfont" class="form-control fontchooser">
                  <option value="Georgia">Georgia</option>
                  <option value="Palatino Linotype">Palatino Linotype</option>
                  <option value="Times New Roman">Times New Roman</option>
                  <option value="Arial" selected="">Arial</option>
                  <option value="Arial Black">Arial Black</option>
                  <option value="Comic Sans MS">Comic Sans MS</option>
                  <option value="Impact">Impact</option>
                  <option value="Lucida Sans Unicode">Lucida Sans Unicode</option>
                  <option value="Tahoma">Tahoma</option>
                  <option value="Trebuchet MS">Trebuchet MS</option>
                  <option value="Verdana">Verdana</option>
                  <option value="Courier New">Courier New</option>
                  <option value="Lucida Console">Lucida Console</option>
                  <option disabled>--------------------------------</option>
                  <option value="Ubuntu">Ubuntu</option>
                  <option value="Open Sans">Open Sans</option>
                  <option value="Lato">Lato</option>
                  <option value="Josefin Slab">Josefin Slab</option>
                  <option value="Droid Sans">Droid Sans</option>
                  <option value="Arvo">Arvo</option>
                  <option value="Vollkorn">Vollkorn</option>
                  <option value="Delius">Delius</option>
                  <option value="Abril Fatface">Abril Fatface</option>
                  <option value="Modak">Modak</option>
                  <option value="Dekko">Dekko</option>
                  <option value="Ranga">Ranga</option>
                  <option value="Cambay">Cambay</option>
                  <option value="Wire One">Wire One</option>
                  <option value="Yanone">Yanone</option>
                  <option value="Yellowtail">Yellowtail</option>
                  <option value="Yesteryear">Yesteryear</option>
                  <option value="Allerta Stencil">Allerta Stencil</option>
                  <option value="Roboto Slab">Roboto Slab</option>
                  <option value="Oswald">Oswald</option>
                  <option value="Raleway">Raleway</option>
                  <option value="Stalemate">Stalemate</option>
                  <option value="Jura">Jura</option>
                  <option value="Fjord One">Fjord One</option>
                  <option value="Amaranth">Amaranth</option>
                  </select>
                  <i class="arrow"></i>
                  </label>
                  </div>
            </div>

            </div>

            <div class="col-md-6">
                <div class="form-group">
                <div class="input-group">
                  <input class="form-control" ng-model="$cssScope.css.ttopmargin">
                  <div class="input-group-addon">px</div>
                </div>
                </div>
            </div>
            </div>

            <div class="row"><div class="col-md-6"><h5>Instructions Font Color</h5></div><div class="col-md-6"><h5>Instructions Font Size</h5></div></div>

            <div class="row">
            <div class="col-md-6">
            <div class="form-group">
            <div id="inst-color" class="input-group cursor">
                <span class="input-group-addon"><i></i>
                </span>
                <input type="text" value="" class="form-control" ng-model="$cssScope.css.icolor" />
            </div>
            </div>
            </div>

            <div class="col-md-6">
                <div class="form-group">
                <div class="input-group">
                  <input class="form-control" ng-model="$cssScope.css.ifontsize">
                  <div class="input-group-addon">px</div>
                </div>
                </div>
            </div>

            </div>

            <div class="row"><div class="col-md-6"><h5>Instructions Font</h5></div><div class="col-md-6"><h5>Inst. Top Margin</h5></div></div>

            <div class="row">
              <div class="col-md-6">
                  <div class="form-group">
                        <div class="section">
                        <label class="field select">
                        <select id="ifont" name="country" ng-model="$cssScope.css.ifont" class="form-control fontchooser">
                        <option value="Georgia">Georgia</option>
                        <option value="Palatino Linotype">Palatino Linotype</option>
                        <option value="Times New Roman">Times New Roman</option>
                        <option value="Arial" selected="">Arial</option>
                        <option value="Arial Black">Arial Black</option>
                        <option value="Comic Sans MS">Comic Sans MS</option>
                        <option value="Impact">Impact</option>
                        <option value="Lucida Sans Unicode">Lucida Sans Unicode</option>
                        <option value="Tahoma">Tahoma</option>
                        <option value="Trebuchet MS">Trebuchet MS</option>
                        <option value="Verdana">Verdana</option>
                        <option value="Courier New">Courier New</option>
                        <option value="Lucida Console">Lucida Console</option>
                        <option disabled>--------------------------------</option>
                        <option value="Ubuntu">Ubuntu</option>
                        <option value="Open Sans">Open Sans</option>
                        <option value="Lato">Lato</option>
                        <option value="Josefin Slab">Josefin Slab</option>
                        <option value="Droid Sans">Droid Sans</option>
                        <option value="Arvo">Arvo</option>
                        <option value="Vollkorn">Vollkorn</option>
                        <option value="Delius">Delius</option>
                        <option value="Abril Fatface">Abril Fatface</option>
                        <option value="Modak">Modak</option>
                        <option value="Dekko">Dekko</option>
                        <option value="Ranga">Ranga</option>
                        <option value="Cambay">Cambay</option>
                        <option value="Wire One">Wire One</option>
                        <option value="Yanone">Yanone</option>
                        <option value="Yellowtail">Yellowtail</option>
                        <option value="Yesteryear">Yesteryear</option>
                        <option value="Allerta Stencil">Allerta Stencil</option>
                        <option value="Roboto Slab">Roboto Slab</option>
                        <option value="Oswald">Oswald</option>
                        <option value="Raleway">Raleway</option>
                        <option value="Stalemate">Stalemate</option>
                        <option value="Jura">Jura</option>
                        <option value="Fjord One">Fjord One</option>
                        <option value="Amaranth">Amaranth</option>
                        </select>
                        <i class="arrow"></i>
                        </label>
                        </div>
                  </div>
              </div>

              <div class="col-md-6">
                  <div class="form-group">
                  <div class="input-group">
                    <input class="form-control" ng-model="$cssScope.css.itopmargin">
                    <div class="input-group-addon">px</div>
                  </div>
                  </div>
              </div>
            </div>

            <div class="row"><div class="col-md-6"><h5>Offers Color</h5></div><div class="col-md-6"><h5>Offer Font Size</h5></div></div>

             
             <div class="row">
             <div class="col-md-6">
            <div class="form-group">
            <div id="offers-color" class="input-group cursor">
                <span class="input-group-addon"><i></i>
                </span>
                <input type="text" value="" class="form-control" ng-model="$cssScope.css.ocolor" />
            </div>
            </div>
            </div>

            <div class="col-md-6">
                <div class="form-group">
                <div class="input-group">
                  <input class="form-control" ng-model="$cssScope.css.ofontsize">
                  <div class="input-group-addon">px</div>
                </div>
                </div>
            </div>

            </div>

            <div class="row"><div class="col-md-6"><h5>Offers Font</h5></div><div class="col-md-6"><h5>Offers Top Margin</h5></div></div>

            <div class="row">
            <div class="col-md-6">
                  <div class="form-group">
                        <div class="section">
                        <label class="field select">
                        <select id="ofont" name="country" ng-model="$cssScope.css.ofont" class="form-control fontchooser">
                        <option value="Georgia">Georgia</option>
                        <option value="Palatino Linotype">Palatino Linotype</option>
                        <option value="Times New Roman">Times New Roman</option>
                        <option value="Arial" selected="">Arial</option>
                        <option value="Arial Black">Arial Black</option>
                        <option value="Comic Sans MS">Comic Sans MS</option>
                        <option value="Impact">Impact</option>
                        <option value="Lucida Sans Unicode">Lucida Sans Unicode</option>
                        <option value="Tahoma">Tahoma</option>
                        <option value="Trebuchet MS">Trebuchet MS</option>
                        <option value="Verdana">Verdana</option>
                        <option value="Courier New">Courier New</option>
                        <option value="Lucida Console">Lucida Console</option>
                        <option disabled>--------------------------------</option>
                        <option value="Ubuntu">Ubuntu</option>
                        <option value="Open Sans">Open Sans</option>
                        <option value="Lato">Lato</option>
                        <option value="Josefin Slab">Josefin Slab</option>
                        <option value="Droid Sans">Droid Sans</option>
                        <option value="Arvo">Arvo</option>
                        <option value="Vollkorn">Vollkorn</option>
                        <option value="Delius">Delius</option>
                        <option value="Abril Fatface">Abril Fatface</option>
                        <option value="Modak">Modak</option>
                        <option value="Dekko">Dekko</option>
                        <option value="Ranga">Ranga</option>
                        <option value="Cambay">Cambay</option>
                        <option value="Wire One">Wire One</option>
                        <option value="Yanone">Yanone</option>
                        <option value="Yellowtail">Yellowtail</option>
                        <option value="Yesteryear">Yesteryear</option>
                        <option value="Allerta Stencil">Allerta Stencil</option>
                        <option value="Roboto Slab">Roboto Slab</option>
                        <option value="Oswald">Oswald</option>
                        <option value="Raleway">Raleway</option>
                        <option value="Stalemate">Stalemate</option>
                        <option value="Jura">Jura</option>
                        <option value="Fjord One">Fjord One</option>
                        <option value="Amaranth">Amaranth</option>
                        </select>
                        <i class="arrow"></i>
                        </label>
                        </div>
                  </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                <div class="input-group">
                  <input class="form-control" ng-model="$cssScope.css.otopmargin">
                  <div class="input-group-addon">px</div>
                </div>
                </div>
            </div>
            </div>

            <div class="row"><div class="col-md-6"><h5>Offers Alignment</h5></div><div class="col-md-6"><h5>Number of Offers</h5></div></div>

            <div class="row">

              <div class="col-md-6">
                      <div class="form-group">
                      <div class="section">
                      <label class="field select">
                      <select ng-model="$cssScope.css.oalignment" class="form-control">
                      <option value="left">Left</option>
                      <option value="center">Center</option>
                      <option value="right">Right</option>
                      </select>
                      <i class="arrow"></i>
                      </label>
                      </div>
                      </div>
              </div>

              <div class="col-md-6">
                      <div class="form-group">
                      <div class="section">
                      <label class="field select">
                      <select ng-model="numOffers" class="form-control">
                      <option value="1">1</option>
                      <option value="2">2</option>
                      <option value="3">3</option>
                      <option value="4">4</option>
                      <option value="5">5</option>
                      <option value="6">6</option>
                      <option value="7">7</option>
                      </select>
                      <i class="arrow"></i>
                      </label>
                      </div>
                      </div>
              </div>
            </div>

</div>
</div>

<!--
<div class="panel sort-disable" data-animate="fadeIn" id="p2" data-panel-color="false" data-panel-fullscreen="false" data-panel-remove="false" data-panel-title="false">
<div class="panel-heading">
<span class="panel-title"> Advanced</span>
<span class="panel-controls"><a href="#" class="panel-control-loader"></a><a href="#" class="panel-control-collapse"></a></span></div>
<div class="panel-body">
<h5>Custom CSS</h5>
<div class="alert alert-micro alert-border-left alert-primary light alert-dismissable">
  This feature is recommended for advanced users only. Please note changes made here does not reflect in live preview.
</div>
<div class="checkbox-custom mb5">
<input type="checkbox" id="checkboxDefault3" ng-model="custom">
<label for="checkboxDefault3">Enable Custom CSS</label>
</div><br>
<div id="editor" style="height: 300px;">
@if ($gateway->customcss == 'true')
  {{ $gateway->css }}
@else
.widget {
    height: 525px;
    width: 280px;
    color: #fff;
    background: url('//i.imgur.com/R5UHBqR.gif');
    position:fixed;
    text-align: center;
}
@endif
</div>
<input type="hidden" id="validcss" value="true" />
<textarea style="display:none;" class="formatcss"></textarea>
</div>
</div>
-->

<div class="tray-bin p10 bg-light dark admin-filter-panels">
              <a href="" class="btn btn-success btn-block" ng-click="savegateway()"> @if ($gateway_id == 0) Create Gateway @else Save Changes @endif</a>
              </div>
    </div>

    <div class="col-md-8" style="padding-left:10%;">
    <h5 style="position:fixed;" class="pt20"><span class="octicon octicon-pulse"></span> Live Preview</h5>
      <div class="widget">
        <h2 ng-bind="title"></h2>
        <p ng-bind="inst"></p>
        <ul class="offers">
          <li ng-repeat="offer in offers|limitTo:numOffers"><a href="#">@{{offer}}</a></li>
        </ul>
      </div>
    </div>
  </div>
</div>

            </section>
            <!-- End: Content -->

        </section>
        <!-- End: Content-Wrapper -->

    </div>
    <!-- End: Main -->

    <!-- BEGIN: PAGE SCRIPTS -->

    <!-- jQuery -->
    <script type="text/javascript" src="{{ asset('panel/vendor/jquery/jquery_ui/jquery-ui.min.js') }}"></script>

    <!-- Bootstrap -->
    <script type="text/javascript" src="{{ asset('panel/assets/js/bootstrap/bootstrap.min.js') }}"></script>

    <!-- Chart Plugins -->
    <!--<script type="text/javascript" src="{{ asset('panel/vendor/plugins/highcharts/highcharts.js') }}"></script>-->
    <!--<script type="text/javascript" src="{{ asset('panel/vendor/plugins/circles/circles.js') }}"></script>
    <script type="text/javascript" src="{{ asset('panel/vendor/plugins/raphael/raphael.js') }}"></script>-->

    <!-- Holder js  -->
    <!--<script type="text/javascript" src="{{ asset('panel/assets/js/bootstrap/holder.min.js') }}"></script>-->

    <!-- Theme Javascript -->
    <script type="text/javascript" src="{{ asset('panel/assets/js/utility/utility.js') }}"></script>
    <script type="text/javascript" src="{{ asset('panel/assets/js/main.js') }}"></script>

    <!-- Admin Panels  -->
    <script type="text/javascript" src="{{ asset('panel/assets/admin-tools/admin-plugins/admin-panels/json2.js') }}"></script>
    <script type="text/javascript" src="{{ asset('panel/assets/admin-tools/admin-plugins/admin-panels/jquery.ui.touch-punch.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('panel/assets/admin-tools/admin-plugins/admin-panels/adminpanels.js') }}"></script>

    <script type="text/javascript" src="{{ asset('panel/assets/js/igrowl.min.js') }}"></script>

    <script src="{{ asset('panel/vendor/plugins/colorpicker/js/bootstrap-colorpicker.min.js') }}"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/ace/1.1.9/ace.js"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/ace/1.1.9/mode-curly.js"></script>

    <script type="text/javascript">
        jQuery(document).ready(function() {

            "use strict";

            // Init Theme Core      
            Core.init({
                sbm: "sb-l-c",
            });

            $('.fontchooser').change(function() {

                var tempArr = [$('#tfont').val(),$('#ifont').val(),$('#ofont').val()];

                var gfonts = [];

                $.each(tempArr, function(index,value) {
                  if ($.inArray(value, ["Georgia","Palatino Linotype","Times New Roman","Arial","Arial Black","Comic Sans MS","Impact","Lucida Sans Unicode","Tahoma","Trebuchet MS","Verdana","Courier New","Lucida Console"]) == -1) {
                      value.replace(" ", "+");
                      gfonts.push(value);
                  }
                });

                $('link[name="gfonts"').remove();

                $('head').append('<link rel="stylesheet" href="//fonts.googleapis.com/css?family='+gfonts.join('|')+'" name="gfonts" type="text/css" />');
               
                console.log(gfonts);
            });

            /*
            var editor = ace.edit("editor");
            editor.setTheme("ace/theme/monokai");
            editor.getSession().setMode("ace/mode/css");
            editor.getSession().on("changeAnnotation", function(){

                var annot = editor.getSession().getAnnotations();

                if (jQuery.isEmptyObject(annot)) { $('#validcss').val('true'); } else { $('#validcss').val('false'); }

            });

          editor.getSession().on('change', function () {
               $('.formatcss').val(editor.getSession().getValue());
           });

           $('.formatcss').val(editor.getSession().getValue());
           */

            $('#title-color').colorpicker({color: '{{ $css['title_color'] }}', format: 'hex'}).on('changeColor', function(event) {
                  $('#title-color input').trigger('input');
            });

            $('#inst-color').colorpicker({color: '{{ $css['instruction_color'] }}', format: 'hex'}).on('changeColor', function(event) {
                  $('#inst-color input').trigger('input');
            });

            $('#offers-color').colorpicker({color: '{{ $css['offers_color'] }}', format: 'hex'}).on('changeColor', function(event) {
                  $('#offers-color input').trigger('input');
            });

            $('.admin-panels').adminpanel({
                grid: '.admin-grid',
                draggable: false,
                preserveGrid: true,
                mobile: true,
                callback: function() {
                    bootbox.confirm('<h3>A Custom Callback!</h3>', function() {});
                },
                onFinish: function() {
                    $('.admin-panels').addClass('animated fadeIn').removeClass('fade-onload');

                },
                onSave: function() {
                    $(window).trigger('resize');
                }
            });

            $('.ann-tab').click(function() {
                $('.ann-tab').removeClass('active');
                $(this).addClass('active');

                $('.ann-body').addClass('hidden');
                $($(this).attr('href')).removeClass('hidden')
            });


        });
    </script>

</body>

</html>
