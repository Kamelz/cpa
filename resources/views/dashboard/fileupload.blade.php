<!DOCTYPE html>
<html ng-app="ngDashboard" ng-Controller="PageController">

<head>
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <title>AdGodMedia - Upload File</title>
    <meta name="keywords" content="{{ $gset->keywords }}" />
    <meta name="description" content="{{ $gset->description }}">

    <meta name="csrf-token" content="{{ csrf_token() }}" />

    <meta name="author" content="AdminDesigns">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- Font CSS (Via CDN) -->
    <link rel='stylesheet' type='text/css' href='//fonts.googleapis.com/css?family=Open+Sans:400,600,700'>
    <link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Roboto:300,400,500,700">

    <!-- Theme CSS -->
    <link rel="stylesheet" type="text/css" href="{{ asset('panel/assets/skin/default_skin/css/theme.css') }}">

    <!-- Custom CSS -->
    <link rel="stylesheet" type="text/css" href="{{ asset('panel/assets/skin/default_skin/css/custom.css') }}">

    <!-- Admin Panels CSS -->
    <link rel="stylesheet" type="text/css" href="{{ asset('panel/assets/admin-tools/admin-plugins/admin-panels/adminpanels.css') }}">


    <link rel="stylesheet" href="{{ asset('panel/css/igrowl.min.css') }}">

    <link href="{{ asset('panel/vendor/plugins/dropzone/css/dropzone.css') }}" rel="stylesheet" type="text/css">

    <!-- Favicon -->
    <link rel="shortcut icon" href="{{ asset('favicon.ico') }}">

    <script type="text/javascript" src="{{ asset('panel/vendor/jquery/jquery-1.11.1.min.js') }}"></script>
    <script src="//ajax.googleapis.com/ajax/libs/angularjs/1.2.26/angular.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.3.14/angular-sanitize.min.js"></script>


    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->

@include('ganalytics') </head>

<body class="dashboard-page sb-l-o sb-r-c">

    <!-- Start: Main -->
    <div id="main">

    @include('dashboard.header')

    @include('dashboard.sidebar')



        <!-- Start: Content-Wrapper -->
        <section id="content_wrapper">

            <!-- Start: Topbar -->
            <header id="topbar">
                <div class="topbar-left">
                    <ol class="breadcrumb">
                        <li class="crumb-active">
                            <a href="{{ url('dashboard/file/upload') }}">Upload Files</a>
                        </li>
                    </ol>
                </div>
            </header>
            <!-- End: Topbar -->

            <!-- Begin: Content -->
            <section id="content" class="animated fadeIn pn table-layout">

            <aside class="tray tray-left p20 tray350 va-t" data-tray-height="match" style="height: 1140px;">
              
<b>Maximum file size allowed</b><br>
<ul><li>15mb</li></ul>
<b>You are not allowed to upload</b>
<ul>
<li>adult-related files</li>
<li>pornography</li>
<li>pornography logins / password</li>
<li>files with sexual filenames</li>
<li>or anything remotely adult-related</li>
</ul>
<b>You can only upload these file types</b>
<br>
<span class="label label-default">txt</span>
<span class="label label-default">csv</span>
<span class="label label-default">doc(x)</span>
<span class="label label-default">xls(x)</span>
<span class="label label-default">rtf</span>
<span class="label label-default">ppt(x)</span>
<span class="label label-default">pdf</span>
<span class="label label-default">swf</span>
<span class="label label-default">flv</span>
<span class="label label-default">psd</span>
<span class="label label-default">avi</span>
<span class="label label-default">wmv</span>
<span class="label label-default">mov</span>
<span class="label label-default">jp(e)g</span>
<span class="label label-default">gif</span>
<span class="label label-default">png</span>
<span class="label label-default">zip</span>
<span class="label label-default">tif</span>
<span class="label label-default">rar</span>

<br><br>

<a href="{{ url('dashboard/files') }}" class="btn btn-primary btn-rounded btn-block" data-test="rotateInDownRight">Manage Files</a>

</aside>

<div class="tray tray-center p20 va-t posr">

<form action="{{ url('dashboard/file/upload') }}" class="dropzone dropzone-sm pt50" id="dropZone">
      <input type="hidden" name="_token" value="{{ csrf_token() }}">
<div class="dz-message text-center" data-dz-message><span><i class="octicon octicon-cloud-upload text-primary fs100"></i><br><h2><span class="main-text"><b>Drop Files Here</b> to upload</span> <br /><span class="sub-text">(or click)</span></span></h2></div>
</form>

<div class="row"><div class="col md-12 previewFL dropzone-previews"></div></div>

</div>

            </section>
            <!-- End: Content -->

        </section>
        <!-- End: Content-Wrapper -->

    </div>
    <!-- End: Main -->

    <!-- BEGIN: PAGE SCRIPTS -->

    <!-- jQuery -->
    <script type="text/javascript" src="{{ asset('panel/vendor/jquery/jquery_ui/jquery-ui.min.js') }}"></script>

    <!-- Bootstrap -->
    <script type="text/javascript" src="{{ asset('panel/assets/js/bootstrap/bootstrap.min.js') }}"></script>

    <!-- Theme Javascript -->
    <script type="text/javascript" src="{{ asset('panel/assets/js/utility/utility.js') }}"></script>
    <script type="text/javascript" src="{{ asset('panel/assets/js/main.js') }}"></script>

    <!-- Admin Panels  -->
    <script type="text/javascript" src="{{ asset('panel/assets/admin-tools/admin-plugins/admin-panels/json2.js') }}"></script>
    <script type="text/javascript" src="{{ asset('panel/assets/admin-tools/admin-plugins/admin-panels/jquery.ui.touch-punch.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('panel/assets/admin-tools/admin-plugins/admin-panels/adminpanels.js') }}"></script>


    <script type="text/javascript" src="{{ asset('panel/assets/js/igrowl.min.js') }}"></script>

    <script src="{{ asset('panel/vendor/plugins/dropzone/dropzone.min.js') }}"></script>

    <script>

soundManager.setup({
  url: '{{ asset('panel/assets/swf') }}/',
  onready: function() {
    lead_snd = soundManager.createSound({
        url: '{{ asset('panel/assets/audio/chaching.mp3') }}'
      });
  }
});

var app = angular.module('ngDashboard', ['ngSanitize']);
app.controller('PageController', function($scope,$http,$interval) {
  
  $scope.earnings = '${{ formatnum($today->payout,2) }}';
  $scope.clicks = '{{ formatnum($today->clicks) }}';
  $scope.leads = '{{ formatnum($today->leads) }}';
  $scope.epc = '${{ formatnum(division($today->payout,$today->clicks),2) }}';

var serverdate = new Date({{ date("Y, m, d, H, i, s") }});
function displaytime(){
    serverdate.setSeconds(serverdate.getSeconds() + 1);
    var hh=serverdate.getHours();
    var m=serverdate.getMinutes();
    var s=serverdate.getSeconds();
    
    var dd = "AM";
    var h = hh;
    if (h >= 12) {
        h = hh-12;
        dd = "PM";
    }
    if (h == 0) {
        h = 12;
    }
    m=checkTime(m);
    s=checkTime(s);
    var output = h+":"+m+" "+dd;
    $scope.serverTime = output;
}

// Add leading zero in time
function checkTime(i){if (i<10){i="0" + i;}return i;}

var getEarn = function() {
    $http.get( "{{ url('dashboard/earnings') }}").success(function(data, status, headers, config) {
        if ($scope.leads < data.leads) { lead_snd.play(); }
        if (data.clicks == undefined) { $interval.cancel(makeitrain); return; }
        $scope.clicks = data.clicks;
        $scope.leads = data.leads;
        $scope.earnings = data.earnings;
        $scope.epc = data.epc;
    });
};

$interval(displaytime, 1000);
makeitrain = $interval(getEarn, 60000);
});

</script>

    <script type="text/javascript">
        jQuery(document).ready(function() {

            "use strict";

            // Init Theme Core      
            Core.init({
                sbm: "sb-l-c",
            });

            Dropzone.options.dropZone = {
                paramName: "file", // The name that will be used to transfer the file
                maxFilesize: 15, // Max file size - in MB
                addRemoveLinks: true, // Toggle remove links
                dictDefaultMessage: 'Upload Files',
                dictResponseError: 'Server not Configured',
                previewsContainer: '.previewFL',
                acceptedFiles: '.txt,.csv,.doc,.docx,.xls,.xlsx,.rtf,.ppt,.pptx,.pdf,.swf,.flv,.psd,.avi,.wmv,.mov,.jpg,.jpeg,.gif,.png,.zip,.tif,.rar',
                  init: function() {
                      this.on("addedfile", function(file) {
                        if (!file.type.match(/image.*/)) {
                          // This is not an image, so Dropzone doesn't create a thumbnail.
                          // Create a default thumbnail:
                          this.emit("thumbnail", file, "{{ asset('panel/assets/img/uploadicon.jpg') }}");
                        }
                      });
                      this.on("success", function(file, response) {
                        if (response['title'] == 'Success') {
                        $.iGrowl({title:'Success',message: response['message']});
                        } else {
                          $.iGrowl({title:'Whoops!',message: response['message'],type:'notice'});
                          this.removeFile(file);
                        }
                      });
                      this.on("error", function(file, message) {
                          $.iGrowl({title:'Whoops!',message: message,type:'notice'});
                          this.removeFile(file);
                      });
                  }
            };

            $('.admin-panels').adminpanel({
                grid: '.admin-grid',
                draggable: false,
                preserveGrid: true,
                mobile: true,
                callback: function() {
                    bootbox.confirm('<h3>A Custom Callback!</h3>', function() {});
                },
                onFinish: function() {
                    $('.admin-panels').addClass('animated fadeIn').removeClass('fade-onload');

                },
                onSave: function() {
                    $(window).trigger('resize');
                }
            });

            $('.ann-tab').click(function() {
                $('.ann-tab').removeClass('active');
                $(this).addClass('active');

                $('.ann-body').addClass('hidden');
                $($(this).attr('href')).removeClass('hidden')
            });


        });
    </script>

    <!-- END: PAGE SCRIPTS -->

</body>

</html>
