<!DOCTYPE html>
<html ng-app="ngDashboard" ng-Controller="PageController">

<head>
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <title>AdGodMedia - Leaderboard</title>
    <meta name="keywords" content="{{ $gset->keywords }}" />
    <meta name="description" content="{{ $gset->description }}">

    <meta name="csrf-token" content="{{ csrf_token() }}" />

    <meta name="author" content="AdminDesigns">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- Font CSS (Via CDN) -->
    <link rel='stylesheet' type='text/css' href='//fonts.googleapis.com/css?family=Open+Sans:400,600,700'>
    <link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Roboto:300,400,500,700">

    <!-- Theme CSS -->
    <link rel="stylesheet" type="text/css" href="{{ asset('panel/assets/skin/default_skin/css/theme.css') }}">

    <!-- Custom CSS -->
    <link rel="stylesheet" type="text/css" href="{{ asset('panel/assets/skin/default_skin/css/custom.css') }}">

    <!-- Admin Panels CSS -->
    <link rel="stylesheet" type="text/css" href="{{ asset('panel/assets/admin-tools/admin-plugins/admin-panels/adminpanels.css') }}">

    <link rel="stylesheet" href="{{ asset('panel/css/igrowl.min.css') }}">

    <!-- Favicon -->
    <link rel="shortcut icon" href="{{ asset('favicon.ico') }}">

    <script type="text/javascript" src="{{ asset('panel/vendor/jquery/jquery-1.11.1.min.js') }}"></script>
    <script src="//ajax.googleapis.com/ajax/libs/angularjs/1.2.26/angular.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.3.14/angular-sanitize.min.js"></script>


    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->

@include('ganalytics') </head>

<body class="dashboard-page sb-l-o sb-r-c">

    <!-- Start: Main -->
    <div id="main">

    @include('dashboard.header')

    @include('dashboard.sidebar')



        <!-- Start: Content-Wrapper -->
        <section id="content_wrapper">

            <!-- Start: Topbar -->
            <header id="topbar">
                <div class="topbar-left">
                    <ol class="breadcrumb">
                        <li class="crumb-active">
                            <a href="{{ url('dashboard/leaderboard') }}">Leaderboard</a>
                        </li>
                    </ol>
                </div>
            </header>
            <!-- End: Topbar -->

            <!-- Begin: Content -->
            <section id="content" class="animated fadeIn pn">
<div class="row p20">
  <div class="col-md-12">
    
<div class="panel">
<div class="panel-heading">
<ul class="nav panel-tabs-border panel-tabs">
<li class="active">
<a href="#tab1_1" data-toggle="tab">Today</a>
</li>
<li>
<a href="#tab1_2" data-toggle="tab">This Month</a>
</li>
</ul>
</div>
<div class="panel-body pn green-top">
<input type="hidden" name="_token" value="{{ csrf_token() }}">
<div class="tab-content">
<div id="tab1_1" class="tab-pane active">
<table class="table table-bordered">
  <thead>
    <th>Rank</th>
    <th>Username</th>
    <th>Earnings</th>
  </thead>
  <tbody>
    @foreach ($toptoday as $key => $day)
    <?php
    if (isset($settings->where('user_id',$day->user_id)->where('option','hide_username')->first()->value)) {
        $huser = 1;
    } else { $huser = 0; }

    if (isset($settings->where('user_id',$day->user_id)->where('option','hide_earnings')->first()->value)) {
        $hearn = 1;
    } else { $hearn = 0; }
    ?>
    <tr><td>{{ $key + 1 }}</td><td> @if($huser == 1 && $day->user_id != $myid) [hidden] @else {{ $users->where('id',$day->user_id)->first()->chathandle }} @endif </td><td>@if($hearn == 1 && $day->user_id != $myid) [hidden] @else ${{ $day->pay }} @endif </td></tr>
    @endforeach
  </tbody>
</table>
</div>
<div id="tab1_2" class="tab-pane">

<table class="table table-bordered">
  <thead>
    <th>Rank</th>
    <th>Username</th>
    <th>Earnings</th>
  </thead>
  <tbody>
    @foreach ($topmtd as $key => $day)
    <?php
    if (isset($settings->where('user_id',$day->user_id)->where('option','hide_username')->first()->value)) {
        $huser = 1;
    } else { $huser = 0; }

    if (isset($settings->where('user_id',$day->user_id)->where('option','hide_earnings')->first()->value)) {
        $hearn = 1;
    } else { $hearn = 0; }
    ?>
    <tr><td>{{ $key + 1 }}</td><td> @if($huser == 1 && $day->user_id != $myid) [hidden] @else {{ $users->where('id',$day->user_id)->first()->chathandle }} @endif </td><td>@if($hearn == 1 && $day->user_id != $myid) [hidden] @else ${{ $day->pay }} @endif </td></tr>
    @endforeach
  </tbody>
</table>

</div>

<div class="row p40">
<div class="col-md-3">
<div class="checkbox-custom fill checkbox-primary">
<input type="checkbox" @if($hideusername == 1) checked="" @endif id="hideusername" data-pk="{{ $myid }}">
<label for="hideusername">Hide my username</label>
</div>
</div>
<div class="col-md-3">
<div class="checkbox-custom fill checkbox-primary">
<input type="checkbox" @if($hidearnings == 1) checked="" @endif id="hidearnings" data-pk="{{ $myid }}">
<label for="hidearnings">Hide my earnings</label>
</div>
</div>
</div>


</div>
</div>
</div>

  </div>
</div>

            </section>
            <!-- End: Content -->

        </section>

    </div>
    <!-- End: Main -->

    <!-- BEGIN: PAGE SCRIPTS -->

    <!-- jQuery -->
    <script type="text/javascript" src="{{ asset('panel/vendor/jquery/jquery_ui/jquery-ui.min.js') }}"></script>

    <!-- Bootstrap -->
    <script type="text/javascript" src="{{ asset('panel/assets/js/bootstrap/bootstrap.min.js') }}"></script>

    <!-- Theme Javascript -->
    <script type="text/javascript" src="{{ asset('panel/assets/js/utility/utility.js') }}"></script>
    <script type="text/javascript" src="{{ asset('panel/assets/js/main.js') }}"></script>

    <!-- Admin Panels  -->
    <script type="text/javascript" src="{{ asset('panel/assets/admin-tools/admin-plugins/admin-panels/json2.js') }}"></script>
    <script type="text/javascript" src="{{ asset('panel/assets/admin-tools/admin-plugins/admin-panels/jquery.ui.touch-punch.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('panel/assets/admin-tools/admin-plugins/admin-panels/adminpanels.js') }}"></script>

    <script type="text/javascript" src="{{ asset('panel/assets/js/igrowl.min.js') }}"></script>

    <script>

soundManager.setup({
  url: '{{ asset('panel/assets/swf') }}/',
  onready: function() {
    lead_snd = soundManager.createSound({
        url: '{{ asset('panel/assets/audio/chaching.mp3') }}'
      });
  }
});

var app = angular.module('ngDashboard', ['ngSanitize']);
app.controller('PageController', function($scope,$http,$interval) {
  
  $scope.earnings = '${{ formatnum($today->payout,2) }}';
  $scope.clicks = '{{ formatnum($today->clicks) }}';
  $scope.leads = '{{ formatnum($today->leads) }}';
  $scope.epc = '${{ formatnum(division($today->payout,$today->clicks),2) }}';

var serverdate = new Date({{ date("Y, m, d, H, i, s") }});
function displaytime(){
    serverdate.setSeconds(serverdate.getSeconds() + 1);
    var hh=serverdate.getHours();
    var m=serverdate.getMinutes();
    var s=serverdate.getSeconds();
    
    var dd = "AM";
    var h = hh;
    if (h >= 12) {
        h = hh-12;
        dd = "PM";
    }
    if (h == 0) {
        h = 12;
    }
    m=checkTime(m);
    s=checkTime(s);
    var output = h+":"+m+" "+dd;
    $scope.serverTime = output;
}

// Add leading zero in time
function checkTime(i){if (i<10){i="0" + i;}return i;}

var getEarn = function() {
    $http.get( "{{ url('dashboard/earnings') }}").success(function(data, status, headers, config) {
        if ($scope.leads < data.leads) { lead_snd.play(); }
        if (data.clicks == undefined) { $interval.cancel(makeitrain); return; }
        $scope.clicks = data.clicks;
        $scope.leads = data.leads;
        $scope.earnings = data.earnings;
        $scope.epc = data.epc;
    });
};

$interval(displaytime, 1000);
makeitrain = $interval(getEarn, 60000);
});

</script>

    <script type="text/javascript">
        jQuery(document).ready(function() {

            "use strict";

            // Init Theme Core      
            Core.init({
                sbm: "sb-l-c",
            });

            $('#hideusername').click(function() {
                if ($(this).is(':checked')) { var status = 1; } else { var status = 0; }
                $.post('{{ url('leaderboard/settings') }}', { _token: $('[name="_token"]').val(), id: $(this).data('pk'), status: status, field: 'hide_username' }).done(function() {
                    $.iGrowl({title:'Success',message: "Settings saved",type: 'success'});
                });
            });

            $('#hidearnings').click(function() {
                if ($(this).is(':checked')) { var status = 1; } else { var status = 0; }
                $.post('{{ url('leaderboard/settings') }}', { _token: $('[name="_token"]').val(), id: $(this).data('pk'), status: status, field: 'hide_earnings' }).done(function() {
                    $.iGrowl({title:'Success',message: "Settings saved",type: 'success'});
                });
            })

            
            $('.admin-panels').adminpanel({
                grid: '.admin-grid',
                draggable: false,
                preserveGrid: true,
                mobile: true,
                callback: function() {
                    bootbox.confirm('<h3>A Custom Callback!</h3>', function() {});
                },
                onFinish: function() {
                    $('.admin-panels').addClass('animated fadeIn').removeClass('fade-onload');

                },
                onSave: function() {
                    $(window).trigger('resize');
                }
            });

            $('.ann-tab').click(function() {
                $('.ann-tab').removeClass('active');
                $(this).addClass('active');

                $('.ann-body').addClass('hidden');
                $($(this).attr('href')).removeClass('hidden')
            });


        });
    </script>

</body>

</html>
