<!DOCTYPE html>
<html ng-app="ngDashboard" ng-controller="PageController">

<head>
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <title>AdGodMedia - Live Stats</title>
    <meta name="keywords" content="{{ $gset->keywords }}" />
    <meta name="description" content="{{ $gset->description }}">

    <meta name="csrf-token" content="{{ csrf_token() }}" />

    <meta name="author" content="AdminDesigns">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- Font CSS (Via CDN) -->
    <link rel='stylesheet' type='text/css' href='//fonts.googleapis.com/css?family=Open+Sans:400,600,700'>
    <link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Roboto:300,400,500,700">

    <!-- Theme CSS -->
    <link rel="stylesheet" type="text/css" href="{{ asset('panel/assets/skin/default_skin/css/theme.css') }}">

    <!-- Custom CSS -->
    <link rel="stylesheet" type="text/css" href="{{ asset('panel/assets/skin/default_skin/css/custom.css') }}">

    <!-- Admin Panels CSS -->
    <link rel="stylesheet" type="text/css" href="{{ asset('panel/assets/admin-tools/admin-plugins/admin-panels/adminpanels.css') }}">

    <link rel="stylesheet" type="text/css" href="{{ asset('panel/css/flags16.css') }}" />

    <!-- Favicon -->
    <link rel="shortcut icon" href="{{ asset('favicon.ico') }}">

    <script type="text/javascript" src="{{ asset('panel/vendor/jquery/jquery-1.11.1.min.js') }}"></script>
    <script src="//ajax.googleapis.com/ajax/libs/angularjs/1.2.26/angular.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/angular-filter/0.4.7/angular-filter.js"></script>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->

@include('ganalytics') </head>

<body class="dashboard-page sb-l-o sb-r-c">

    <!-- Start: Main -->
    <div id="main">

    @include('dashboard.header')

    @include('dashboard.sidebar')



        <!-- Start: Content-Wrapper -->
        <section id="content_wrapper">

            <!-- Start: Topbar -->
            <header id="topbar">
                <div class="topbar-left">
                    <ol class="breadcrumb">
                        <span class="octicon octicon-primitive-dot fs15 mr5" style="color:#70ca63!important;"></span>
                        <li class="crumb-active">
                            <a href="{{ url('dashboard/live') }}"> Live Stats</a>
                        </li>
                    </ol>
                </div>
            </header>
            <!-- End: Topbar -->

            <!-- Begin: Content -->
            <section id="content" class="animated fadeIn table-layout">

<div class="row">
	<div class="col-md-7">
		<div class="panel" id="spy3">
		<div class="panel-heading">
		<span class="panel-title">Live Visitors</span>
		</div>
		<div class="panel-body dark">
		<div class="livemap" style="height: 320px; width: 100%;"></div>
		</div>
		</div>

		<div class="panel panel-visible" id="spy1">
		<div class="panel-heading">
		<span class="panel-title">Content</span>
		</div>
		<div class="panel-body pn">
		<table class="table table-bordered table-hover" cellspacing="0" width="100%">
		<thead>
		<tr>
		<th>Content</th>
		<th>Visitors</th>
		</tr>
		</thead>
		<tbody class="f16">
		<tr ng-repeat="content in contents | orderBy: '-v'"><td ng-cloak>@{{ content.c }}</td><td ng-cloak>@{{ content.v }}</td></tr>
		</tbody>
		</table>
		</div>
		</div>

	</div>
	<div class="col-md-5">
		<div class="panel bg-info of-h mb10">
	        <div class="pn pl20 p5">
	            <div class="icon-bg"> <i class="icon-group"></i> </div>
	            <h2 class="mt15 lh15"> <b ng-bind="total"></b> </h2>
	            <h5 class="text-muted">Online Visitors</h5>
	        </div>
	    </div>

		<div class="panel panel-visible" id="spy1">
		<div class="panel-heading">
		<span class="panel-title">Countries</span>
		</div>
		<div class="panel-body pn">
		<table class="table table-bordered table-hover" cellspacing="0" width="100%">
		<thead>
		<tr>
		<th>Country</th>
		<th>Visitors</th>
		</tr>
		</thead>
		<tbody>
		<tr ng-repeat="visitor in countries | orderBy: '-v'"><td class="f16" ng-cloak><span class="flag @{{ visitor.c }}"></span> @{{ visitor.t }}</td><td ng-cloak>@{{ visitor.v }}</td></tr>
		</tbody>
		</table>
		</div>
		</div>
	</div>
</div>                    

            </section>
            <!-- End: Content -->

        </section>
        <!-- End: Content-Wrapper -->

    </div>
    <!-- End: Main -->

    <!-- BEGIN: PAGE SCRIPTS -->

    <!-- jQuery -->
    <script type="text/javascript" src="{{ asset('panel/vendor/jquery/jquery_ui/jquery-ui.min.js') }}"></script>

    <!-- Bootstrap -->
    <script type="text/javascript" src="{{ asset('panel/assets/js/bootstrap/bootstrap.min.js') }}"></script>

    <!-- Theme Javascript -->
    <script type="text/javascript" src="{{ asset('panel/assets/js/utility/utility.js') }}"></script>
    <script type="text/javascript" src="{{ asset('panel/assets/js/main.js') }}"></script>

        <!-- Load Additional Jvectormaps -->
    <script type="text/javascript" src="{{ asset('panel/assets/js/utility/jvectormap/assets/jquery-jvectormap-world-mill-en.js') }}"></script>

    <!-- Admin Panels  -->
    <script type="text/javascript" src="{{ asset('panel/assets/admin-tools/admin-plugins/admin-panels/json2.js') }}"></script>
    <script type="text/javascript" src="{{ asset('panel/assets/admin-tools/admin-plugins/admin-panels/jquery.ui.touch-punch.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('panel/assets/admin-tools/admin-plugins/admin-panels/adminpanels.js') }}"></script>

    <script type="text/javascript">
        jQuery(document).ready(function() {

            "use strict";

            // Init Theme Core      
            Core.init({
                sbm: "sb-l-c",
            });

            $('.admin-panels').adminpanel({
                grid: '.admin-grid',
                draggable: false,
                preserveGrid: true,
                mobile: true,
                callback: function() {
                    bootbox.confirm('<h3>A Custom Callback!</h3>', function() {});
                },
                onFinish: function() {
                    $('.admin-panels').addClass('animated fadeIn').removeClass('fade-onload');

                },
                onSave: function() {
                    $(window).trigger('resize');
                }
            });

            $('.ann-tab').click(function() {
                $('.ann-tab').removeClass('active');
                $(this).addClass('active');

                $('.ann-body').addClass('hidden');
                $($(this).attr('href')).removeClass('hidden')
            });

            	var chart = $('.livemap').vectorMap({
    	                map: 'world_mill_en',
                    backgroundColor: 'transparent',
                    regionStyle: {
                        initial: {
                            fill: '#f7c65f'
                        },
                        hover: {
                            fill: '#6e7072'
                        }
                    },
                    onRegionLabelShow: function(e, el, code) {
                        if (angular.element("html").scope().datamap[code] == undefined) angular.element("html").scope().datamap[code] = 0;
                        el.html(el.html()+'<br>'+angular.element("html").scope().datamap[code]+' Visitor(s)');
                    },
                    series: {
                        regions: [{
                            scale: ['#f7c65f', '#ec6f5a'],
                            normalizeFunction: 'polynomial',
                            values: []
                        }]
                    }
      					
                }); 


        });
    </script>

<script>

soundManager.setup({
  url: '{{ asset('panel/assets/swf') }}/',
  onready: function() {
    lead_snd = soundManager.createSound({
        url: '{{ asset('panel/assets/audio/chaching.mp3') }}'
      });
  }
});

var app = angular.module('ngDashboard',['angular.filter'])
  .controller('PageController', function($scope,$location,$http,$interval) { 

  	$scope.countries = [];
  	$scope.contents = [];

  	$scope.datamap = {};

  	$scope.total = 'Loading..';

  var data = function(){   
    $http({method: 'GET', url: '{{ url("dashboard/livedata") }}'}).success(function(result){

	// Reset variables
	$scope.countries = result.visitors;
	$scope.total = 0;
	$scope.contents = result.content;
	$scope.datamap = {};

	$scope.countries.forEach(function(fruit, i) {

  			var c = fruit.c;
  			var v = fruit.v;
  			console.log(v);

  			$scope.datamap[c] = v;

  			$scope.total = $scope.total + v;
	});

	console.log($scope.datamap);

	  $('.livemap').vectorMap('get', 'mapObject').series.regions[0].clear();
      $('.livemap').vectorMap('get', 'mapObject').series.regions[0].setValues($scope.datamap);
    }); 
  }

  data();

  $scope.earnings = '${{ formatnum($today->payout,2) }}';
  $scope.clicks = '{{ formatnum($today->clicks) }}';
  $scope.leads = '{{ formatnum($today->leads) }}';
  $scope.epc = '${{ formatnum(division($today->payout,$today->clicks),2) }}';

var serverdate = new Date({{ date("Y, m, d, H, i, s") }});
function displaytime(){
    serverdate.setSeconds(serverdate.getSeconds() + 1);
    var hh=serverdate.getHours();
    var m=serverdate.getMinutes();
    var s=serverdate.getSeconds();
    
    var dd = "AM";
    var h = hh;
    if (h >= 12) {
        h = hh-12;
        dd = "PM";
    }
    if (h == 0) {
        h = 12;
    }
    m=checkTime(m);
    s=checkTime(s);
    var output = h+":"+m+" "+dd;
    $scope.serverTime = output;
}

// Add leading zero in time
function checkTime(i){if (i<10){i="0" + i;}return i;}

var getEarn = function() {
    $http.get( "{{ url('dashboard/earnings') }}").success(function(data, status, headers, config) {
        if ($scope.leads < data.leads) { lead_snd.play(); }
        if (data.clicks == undefined) { $interval.cancel(makeitrain); return; }
        $scope.clicks = data.clicks;
        $scope.leads = data.leads;
        $scope.earnings = data.earnings;
        $scope.epc = data.epc;
    });
};

$interval(displaytime, 1000);
makeitrain = $interval(getEarn, 60000);
$interval(data, 10000);

 });
</script>

    <!-- END: PAGE SCRIPTS -->

</body>

</html>
