<!DOCTYPE html>
<html ng-app="ngDashboard" ng-Controller="PageController">

<head>
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <title>AdGodMedia - Manage Notes & Links</title>
    <meta name="keywords" content="{{ $gset->keywords }}" />
    <meta name="description" content="{{ $gset->description }}">

    <meta name="csrf-token" content="{{ csrf_token() }}" />

    <meta name="author" content="AdminDesigns">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- Font CSS (Via CDN) -->
    <link rel='stylesheet' type='text/css' href='//fonts.googleapis.com/css?family=Open+Sans:400,600,700'>
    <link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Roboto:300,400,500,700">

    <!-- Theme CSS -->
    <link rel="stylesheet" type="text/css" href="{{ asset('panel/assets/skin/default_skin/css/theme.css') }}">

    <!-- Custom CSS -->
    <link rel="stylesheet" type="text/css" href="{{ asset('panel/assets/skin/default_skin/css/custom.css') }}">

    <!-- Admin Panels CSS -->
    <link rel="stylesheet" type="text/css" href="{{ asset('panel/assets/admin-tools/admin-plugins/admin-panels/adminpanels.css') }}">

    <link rel="stylesheet" href="{{ asset('panel/css/igrowl.min.css') }}">

    <!-- Favicon -->
    <link rel="shortcut icon" href="{{ asset('favicon.ico') }}">

    <script type="text/javascript" src="{{ asset('panel/vendor/jquery/jquery-1.11.1.min.js') }}"></script>
    <script src="//ajax.googleapis.com/ajax/libs/angularjs/1.2.26/angular.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.3.14/angular-sanitize.min.js"></script>


    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->

@include('ganalytics') </head>

<body class="dashboard-page sb-l-o sb-r-c">

    <!-- Start: Main -->
    <div id="main">

    @include('dashboard.header')

    @include('dashboard.sidebar')



        <!-- Start: Content-Wrapper -->
        <section id="content_wrapper">

            <!-- Start: Topbar -->
            <header id="topbar">
                <div class="topbar-left">
                    <ol class="breadcrumb">
                        <li class="crumb-active">
                            <a href="{{ url('dashboard/notes-links') }}">Links & Notes</a>
                        </li>
                    </ol>
                </div>
            </header>
            <!-- End: Topbar -->

            <!-- Begin: Content -->
            <section id="content" class="animated fadeIn pn">

<div class="row p10">
<div class="col-md-12">
<div class="panel" id="spy7">
<div class="panel-heading">
<span class="panel-title">
<span class="octicon octicon-list-unordered"></span> My Links & Notes</span>
</div>
<div class="panel-body pn">
<table class="table table-bordered">
<thead>
<tr>
<th>Title</th>
<th>Type</th>
<th>Clicks</th>
<th>Unlocks</th>
<th>Earnings</th>
<th>Date Added</th>
<th>Actions</th>
</tr>
</thead>
<tbody>
@foreach ($notes as $note)
<tr>
  <td>{{ $note->title }}</td>
  <td><span class="label label-primary">Note Locker</span></td>
  <td>{{ $note->clicks }}</td>
  <td>{{ $note->leads }}</td>
  <td>${{ $note->earnings }}</td>
  <td>{{ date('d M Y',strtotime($note->created_at)) }}</td>
  <td><a href="{{ url('dashboard/note/get/'.$note->id) }}" data-toggle="modal" class="btn btn-sm btn-success mr5">Get Link</a><a href="{{ url('dashboard/note/edit/'.$note->id) }}" class="btn btn-sm btn-primary mr5"><span class="octicon octicon-pencil"></span> Edit</a> <a href="{{ url('dashboard/note/delete/'.$note->id) }}" class="btn btn-sm btn-default" data-toggle="modal"><span class="octicon octicon-trashcan"></span> Delete</a></td>
</tr>
@endforeach
@foreach ($links as $link)
<tr>
  <td>{{ $link->title }}</td>
  <td><span class="label label-success">Link Locker</span></td>
  <td>{{ $link->clicks }}</td>
  <td>{{ $link->leads }}</td>
  <td>${{ $link->earnings }}</td>
  <td>{{ date('d M Y',strtotime($link->created_at)) }}</td>
  <td><a href="{{ url('dashboard/link/get/'.$link->id) }}" data-toggle="modal" class="btn btn-sm btn-success mr5">Get Link</a><a href="{{ url('dashboard/link/edit/'.$link->id) }}" class="btn btn-sm btn-primary mr5"><span class="octicon octicon-pencil"></span> Edit</a> <a href="{{ url('dashboard/link/delete/'.$link->id) }}" class="btn btn-sm btn-default" data-toggle="modal"><span class="octicon octicon-trashcan"></span> Delete</a></td>
</tr>
@endforeach
@if ($num < 1) <tr><td colspan="7">No link or note created yet!</td></tr> @endif
</tbody>
</table>
</div>
</div>
              </div>
            </div>

<div class="row">
  <div class="col-md-6 pl25">
    <a href="{{ url('dashboard/note/create') }}" class="btn btn-block btn-primary">Create New Note Locker</a>
  </div>
  <div class="col-md-6 pr25">
    <a href="{{ url('dashboard/link/create') }}" class="btn btn-block btn-primary">Create New Link Locker</a>
  </div>
</div>

            </section>
            <!-- End: Content -->

        </section>

    </div>
    <!-- End: Main -->

    <!-- BEGIN: PAGE SCRIPTS -->

    <!-- jQuery -->
    <script type="text/javascript" src="{{ asset('panel/vendor/jquery/jquery_ui/jquery-ui.min.js') }}"></script>

    <!-- Bootstrap -->
    <script type="text/javascript" src="{{ asset('panel/assets/js/bootstrap/bootstrap.min.js') }}"></script>

    <!-- Theme Javascript -->
    <script type="text/javascript" src="{{ asset('panel/assets/js/utility/utility.js') }}"></script>
    <script type="text/javascript" src="{{ asset('panel/assets/js/main.js') }}"></script>

    <!-- Admin Panels  -->
    <script type="text/javascript" src="{{ asset('panel/assets/admin-tools/admin-plugins/admin-panels/json2.js') }}"></script>
    <script type="text/javascript" src="{{ asset('panel/assets/admin-tools/admin-plugins/admin-panels/jquery.ui.touch-punch.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('panel/assets/admin-tools/admin-plugins/admin-panels/adminpanels.js') }}"></script>

    <script type="text/javascript" src="{{ asset('panel/assets/js/igrowl.min.js') }}"></script>

    <script>

soundManager.setup({
  url: '{{ asset('panel/assets/swf') }}/',
  onready: function() {
    lead_snd = soundManager.createSound({
        url: '{{ asset('panel/assets/audio/chaching.mp3') }}'
      });
  }
});

var app = angular.module('ngDashboard', ['ngSanitize']);
app.controller('PageController', function($scope,$http,$interval) {
  
  $scope.earnings = '${{ formatnum($today->payout,2) }}';
  $scope.clicks = '{{ formatnum($today->clicks) }}';
  $scope.leads = '{{ formatnum($today->leads) }}';
  $scope.epc = '${{ formatnum(division($today->payout,$today->clicks),2) }}';

var serverdate = new Date({{ date("Y, m, d, H, i, s") }});
function displaytime(){
    serverdate.setSeconds(serverdate.getSeconds() + 1);
    var hh=serverdate.getHours();
    var m=serverdate.getMinutes();
    var s=serverdate.getSeconds();
    
    var dd = "AM";
    var h = hh;
    if (h >= 12) {
        h = hh-12;
        dd = "PM";
    }
    if (h == 0) {
        h = 12;
    }
    m=checkTime(m);
    s=checkTime(s);
    var output = h+":"+m+" "+dd;
    $scope.serverTime = output;
}

// Add leading zero in time
function checkTime(i){if (i<10){i="0" + i;}return i;}

var getEarn = function() {
    $http.get( "{{ url('dashboard/earnings') }}").success(function(data, status, headers, config) {
        if ($scope.leads < data.leads) { lead_snd.play(); }
        if (data.clicks == undefined) { $interval.cancel(makeitrain); return; }
        $scope.clicks = data.clicks;
        $scope.leads = data.leads;
        $scope.earnings = data.earnings;
        $scope.epc = data.epc;
    });
};

$interval(displaytime, 1000);
makeitrain = $interval(getEarn, 60000);
});

</script>

    <script type="text/javascript">
        jQuery(document).ready(function() {

            "use strict";

            // Init Theme Core      
            Core.init({
                sbm: "sb-l-c",
            });

            
            @foreach ($errors->all() as $error)
                $.iGrowl({title:'Notice',message: "{{{ $error }}}",type: 'notice'});
            @endforeach

            @if ($successmsg)
                console.log('juk');
                $.iGrowl({title:'Success',message: "{{ $successmsg }}",type: 'success'});
            @endif

            $('.admin-panels').adminpanel({
                grid: '.admin-grid',
                draggable: false,
                preserveGrid: true,
                mobile: true,
                callback: function() {
                    bootbox.confirm('<h3>A Custom Callback!</h3>', function() {});
                },
                onFinish: function() {
                    $('.admin-panels').addClass('animated fadeIn').removeClass('fade-onload');
                },
                onSave: function() {
                    $(window).trigger('resize');
                }
            });

            $('.ann-tab').click(function() {
                $('.ann-tab').removeClass('active');
                $(this).addClass('active');

                $('.ann-body').addClass('hidden');
                $($(this).attr('href')).removeClass('hidden')
            });


        });
    </script>

</body>

</html>
