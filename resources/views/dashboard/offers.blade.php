<!DOCTYPE html>
<html ng-app="ngDashboard" ng-controller="PageController">

<head>
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <title>AdGodMedia - Promote Offers</title>
    <meta name="keywords" content="{{ $gset->keywords }}" />
    <meta name="description" content="{{ $gset->description }}">

    <meta name="csrf-token" content="{{ csrf_token() }}" />

    <meta name="author" content="AdminDesigns">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- Font CSS (Via CDN) -->
    <link rel='stylesheet' type='text/css' href='//fonts.googleapis.com/css?family=Open+Sans:400,600,700'>
    <link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Raleway:300,400,500,700">

    <!-- Theme CSS -->
    <link rel="stylesheet" type="text/css" href="{{ asset('panel/assets/skin/default_skin/css/theme.css') }}">

    <!-- Custom CSS -->
    <link rel="stylesheet" type="text/css" href="{{ asset('panel/assets/skin/default_skin/css/custom.css') }}">

    <!-- Admin Panels CSS -->
    <link rel="stylesheet" type="text/css" href="{{ asset('panel/assets/admin-tools/admin-plugins/admin-panels/adminpanels.css') }}">


    <link rel="stylesheet" href="{{ asset('panel/css/igrowl.min.css') }}">

    <link href="{{ asset('panel/vendor/plugins/datatables/media/css/dataTables.bootstrap.css') }}" rel="stylesheet" type="text/css">

    <!-- Favicon -->
    <link rel="shortcut icon" href="{{ asset('favicon.ico') }}">

    <script type="text/javascript" src="{{ asset('panel/vendor/jquery/jquery-1.11.1.min.js') }}"></script>
    <script src="//ajax.googleapis.com/ajax/libs/angularjs/1.2.26/angular.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.3.14/angular-sanitize.min.js"></script>


    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->

@include('ganalytics') </head>

<body class="dashboard-page sb-l-o sb-r-c">

    <!-- Start: Main -->
    <div id="main">

    @include('dashboard.header')

    @include('dashboard.sidebar')



        <!-- Start: Content-Wrapper -->
        <section id="content_wrapper">

            <!-- Start: Topbar -->
            <header id="topbar">
                <div class="topbar-left">
                    <ol class="breadcrumb">
                        <li class="crumb-active">
                            <a href="{{ url('dashboard/offers') }}">Promote Offers @if ($scountry)  - {{ $scountry }} @endif </a>
                        </li>
                    </ol>
                </div>
            </header>
            <!-- End: Topbar -->

            <!-- Begin: Content -->
            <section id="content" class="animated fadeIn admin-form">

                    <div class="tray-bin p10 bg-light dark admin-filter-panels">
        <form action="{{ url('dashboard/offers') }}" method="get">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <div class="row p10">
                <div class="col-md-4">
                    Country: <select name="country" id="" class="form-control">
                        <option value="">All Countries</option>
                        @foreach ($countries as $country)
                        <option value="{{ $country->option }}" @if($scountry == $country->option) selected @endif >{{ $country->value }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="col-md-4">
                    Sort: <select name="sort" id="" class="form-control">
                        <option value="">None</option>
                        <option value="epc" @if($sort == 'epc') selected @endif >EPC</option>
                        <option value="conv" @if($sort == 'conv') selected @endif >Conversion Rate</option>
                        <option value="payout" @if($sort == 'payout') selected @endif >Payout</option>
                    </select>
                </div>
                <div class="col-md-4">
                    &nbsp; <button type="submit" class="btn btn-block btn-primary">Submit</button>
                </div>
            </div> 
            </form>     
        </div>

            <div class="row">
              <div class="col-md-12">
                <div class="panel">
                  <div class="panel-heading"><div class="panel-title"><span class="octicon octicon-dashboard"></span> Offers</div></div>
                  <div class="panel-body pn">
                    <table class="table table-striped table-hover display" cellspacing="0" width="100%">
<thead>
<tr>
<th>ID</th>
<th>Offer Name</th>
<th>Country</th>
<th>Payout</th>
<th>CR%</th>
<th>EPC</th>
</tr>
</thead>
<tbody class="f16">

@foreach ($offers as $offer)
<?php $payout = $offer->payout * ($gset->offerrate / 100); ?>
<tr>
    <td>{{ $offer->id }}</td>
    <td><a href="{{ url('dashboard/offer/details/'.$offer->id) }}" data-toggle="modal">{{ $offer->title }}</a></td>
    <td>{{ $offer->countries }}</td>
    <td>${{ formatnum($payout,2) }}</td>
    <td>{{ $offer->conv }}</td>
    <td>${{ $offer->epc }}</td>
</tr>
@endforeach
<tfoot>
<tr>
    <td colspan="2" class="p30" style="border-right:none;"><?php echo $offers->render(); ?></td>
    <td colspan="10" class="text-right p20" style="border-left:none;"><b>{{ $offers->total() }}</b> Offer(s)</td>
</tr>
</tfoot>
</tbody>
</table>
                  </div>
                </div>
              </div>
            </div>

            </section>
            <!-- End: Content -->

        </section>

    </div>
    <!-- End: Main -->

    <!-- BEGIN: PAGE SCRIPTS -->

    <!-- jQuery -->
    <script type="text/javascript" src="{{ asset('panel/vendor/jquery/jquery_ui/jquery-ui.min.js') }}"></script>

    <!-- Bootstrap -->
    <script type="text/javascript" src="{{ asset('panel/assets/js/bootstrap/bootstrap.min.js') }}"></script>

    <!-- Theme Javascript -->
    <script type="text/javascript" src="{{ asset('panel/assets/js/utility/utility.js') }}"></script>
    <script type="text/javascript" src="{{ asset('panel/assets/js/main.js') }}"></script>

    <script type="text/javascript" src="{{ asset('panel/assets/admin-tools/admin-plugins/admin-panels/json2.js') }}"></script>
    <script type="text/javascript" src="{{ asset('panel/assets/admin-tools/admin-plugins/admin-panels/jquery.ui.touch-punch.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('panel/assets/admin-tools/admin-plugins/admin-panels/adminpanels.js') }}"></script>

    <script type="text/javascript" src="{{ asset('panel/assets/js/igrowl.min.js') }}"></script>

    <script src="{{ asset('panel/vendor/plugins/datatables/media/js/jquery.dataTables.js') }}"></script>
<script src="{{ asset('panel/vendor/plugins/datatables/media/js/dataTables.bootstrap.js') }}"></script>

<script>

soundManager.setup({
  url: '{{ asset('panel/assets/swf') }}/',
  onready: function() {
    lead_snd = soundManager.createSound({
        url: '{{ asset('panel/assets/audio/chaching.mp3') }}'
      });
  }
});

var app = angular.module('ngDashboard', ['ngSanitize']);
app.controller('PageController', function($scope,$http,$interval) {
  
  $scope.earnings = '${{ formatnum($today->payout,2) }}';
  $scope.clicks = '{{ formatnum($today->clicks) }}';
  $scope.leads = '{{ formatnum($today->leads) }}';
  $scope.epc = '${{ formatnum(division($today->payout,$today->clicks),2) }}';

var serverdate = new Date({{ date("Y, m, d, H, i, s") }});
function displaytime(){
    serverdate.setSeconds(serverdate.getSeconds() + 1);
    var hh=serverdate.getHours();
    var m=serverdate.getMinutes();
    var s=serverdate.getSeconds();
    
    var dd = "AM";
    var h = hh;
    if (h >= 12) {
        h = hh-12;
        dd = "PM";
    }
    if (h == 0) {
        h = 12;
    }
    m=checkTime(m);
    s=checkTime(s);
    var output = h+":"+m+" "+dd;
    $scope.serverTime = output;
}

// Add leading zero in time
function checkTime(i){if (i<10){i="0" + i;}return i;}

var getEarn = function() {
    $http.get( "{{ url('dashboard/earnings') }}").success(function(data, status, headers, config) {
        if ($scope.leads < data.leads) { lead_snd.play(); }
        if (data.clicks == undefined) { $interval.cancel(makeitrain); return; }
        $scope.clicks = data.clicks;
        $scope.leads = data.leads;
        $scope.earnings = data.earnings;
        $scope.epc = data.epc;
    });
};

$interval(displaytime, 1000);
makeitrain = $interval(getEarn, 60000);
});

</script>

    <script type="text/javascript">
        jQuery(document).ready(function() {

            "use strict";

            // Init Theme Core      
            Core.init({
                sbm: "sb-l-c",
            });

            $("[id^=disablec]").change(function() {
                if ($(this).is(':checked')) { var status = 1; } else { var status = 0; }
                $.post('{{ url('dashboard/offer/toggle') }}', { _token: $('[name="_token"]').val(), id: $(this).data('pk'), status: status });
            });

            $("[id^=favoritec]").change(function() {
                if ($(this).is(':checked')) { var status = 1; } else { var status = 0; }
                $.post('{{ url('dashboard/offer/favorite') }}', { _token: $('[name="_token"]').val(), id: $(this).data('pk'), status: status });
            });

            $('.admin-panels').adminpanel({
                grid: '.admin-grid',
                draggable: false,
                preserveGrid: true,
                mobile: true,
                callback: function() {
                    bootbox.confirm('<h3>A Custom Callback!</h3>', function() {});
                },
                onFinish: function() {
                    $('.admin-panels').addClass('animated fadeIn').removeClass('fade-onload');

                    // Init the rest of the plugins now that the panels
                    // have had a chance to be moved and organized.
                    // It's less taxing to organize empty panels
                    demoHighCharts.init();

                    // We also refresh any "in-view" waypoints to ensure
                    // the correct position is being calculated after the 
                    // Admin Panels plugin moved everything
                    //Waypoint.refreshAll();

                },
                onSave: function() {
                    $(window).trigger('resize');
                }
            });

            $('.ann-tab').click(function() {
                $('.ann-tab').removeClass('active');
                $(this).addClass('active');

                $('.ann-body').addClass('hidden');
                $($(this).attr('href')).removeClass('hidden')
            });


        });
    </script>

    <!-- END: PAGE SCRIPTS -->

</body>

</html>
