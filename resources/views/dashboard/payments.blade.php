<!DOCTYPE html>
<html ng-app="ngDashboard" ng-controller="PageController">

<head>
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <title>AdGodMedia - Payments</title>
    <meta name="keywords" content="{{ $gset->keywords }}" />
    <meta name="description" content="{{ $gset->description }}">

    <meta name="csrf-token" content="{{ csrf_token() }}" />

    <meta name="author" content="AdminDesigns">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- Font CSS (Via CDN) -->
    <link rel='stylesheet' type='text/css' href='//fonts.googleapis.com/css?family=Open+Sans:400,600,700'>
    <link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Raleway:300,400,500,700">

    <!-- Theme CSS -->
    <link rel="stylesheet" type="text/css" href="{{ asset('panel/assets/skin/default_skin/css/theme.css') }}">

    <!-- Custom CSS -->
    <link rel="stylesheet" type="text/css" href="{{ asset('panel/assets/skin/default_skin/css/custom.css') }}">

    <!-- Admin Panels CSS -->
    <link rel="stylesheet" type="text/css" href="{{ asset('panel/assets/admin-tools/admin-plugins/admin-panels/adminpanels.css') }}">

    <link rel="stylesheet" href="{{ asset('panel/css/igrowl.min.css') }}">

    <!-- Favicon -->
    <link rel="shortcut icon" href="{{ asset('favicon.ico') }}">

    <script type="text/javascript" src="{{ asset('panel/vendor/jquery/jquery-1.11.1.min.js') }}"></script>
    <script src="//ajax.googleapis.com/ajax/libs/angularjs/1.2.26/angular.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.3.14/angular-sanitize.min.js"></script>


    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->

@include('ganalytics') </head>

<body class="dashboard-page sb-l-o sb-r-c">

    <!-- Start: Main -->
    <div id="main">

    @include('dashboard.header')

    @include('dashboard.sidebar')



        <!-- Start: Content-Wrapper -->
        <section id="content_wrapper">

            <!-- Start: Topbar -->
            <header id="topbar">
                <div class="topbar-left">
                    <ol class="breadcrumb">
                        <li class="crumb-active">
                            <a href="{{ url('dashboard/payments') }}">Payments</a>
                        </li>
                    </ol>
                </div>
            </header>
            <!-- End: Topbar -->

            <!-- Begin: Content -->
            <section id="content" class="animated fadeIn">

<div class="row">
  <div class="col-md-3">
          <div class="panel panel-tile bg-primary light of-h mb10">
          <div class="panel-body pn pl20 p5">
          <div class="icon-bg">
          <i class="fa fa-comments-o"></i>
          </div>
          <h2 class="mt15 lh15">
          <b>${{ formatnum($balancearn,2) }}</b>
          </h2>
          <h5 class="text-muted">Current Balance</h5>
          </div>
          </div>
  </div>
    <div class="col-md-3">
          <div class="panel panel-tile bg-alert light of-h mb10">
          <div class="panel-body pn pl20 p5">
          <div class="icon-bg">
          <i class="fa fa-comments-o"></i>
          </div>
          <h2 class="mt15 lh15">
          <b>${{ formatnum($pending,2) }}</b>
          </h2>
          <h5 class="text-muted">Pending Payments</h5>
          </div>
          </div>
  </div>
    <div class="col-md-3">
          <div class="panel panel-tile bg-info light of-h mb10">
          <div class="panel-body pn pl20 p5">
          <div class="icon-bg">
          <i class="fa fa-comments-o"></i>
          </div>
          <h2 class="mt15 lh15">
          <b>${{ formatnum($paid,2) }}</b>
          </h2>
          <h5 class="text-muted">Paid Earnings</h5>
          </div>
          </div>
  </div>
    <div class="col-md-3">
          <div class="panel panel-tile bg-warning light of-h mb10">
          <div class="panel-body pn pl20 p5">
          <div class="icon-bg">
          <i class="fa fa-comments-o"></i>
          </div>
          <h2 class="mt15 lh15">
          <b>${{ formatnum($alltime,2) }}</b>
          </h2>
          <h5 class="text-muted">Total Earnings</h5>
          </div>
          </div>
  </div>
</div>

<br>

<div class="row">
  <div class="col-md-8">

  <div class="panel">
  <form action="{{ url('dashboard/payment') }}" method="post">
  <input type="hidden" name="_token" value="{{ csrf_token() }}">

    <div class="panel-heading"><div class="panel-title">Request Payout</div></div>
    <div class="panel-body">
      <div class="row pb10">
        <div class="col-md-4 p10">
          Amount
        </div>
        <div class="col-md-8">
          <div class="input-group">
          <span class="input-group-addon">$
          </span>
          <input class="form-control" type="text" name="amount">
          </div>
        </div>
      </div>
      <div class="row pb10">
        <div class="col-md-4 p10">
          Payment will be sent by
        </div>
        <div class="col-md-8">
          <input type="text" readonly value="{{ date('d/m/Y',strtotime($paydate)) }}" class="form-control">
          <input type="hidden" name="date" value="{{ $paydate }}">
        </div>
      </div>
      <div class="row pb10">
        <div class="col-md-4 p10">
          Payment Method
        </div>
          <div class="col-md-8">
            <input type="text" readonly value="{{ $profile->payment_method }}" class="form-control">
          </div>
      </div>
      <div class="row">
        <div class="col-md-4"></div>
        <div class="col-md-8">
          <button type="submit" class="btn btn-primary pl25 pr25">Submit</button>
        </div>
      </div>
    </div>

    </form>
  </div>

      <div class="panel">
        <div class="panel-heading"><div class="panel-title"><span class="octicon octicon-credit-card"></span> Payments</div></div>
        <div class="panel-body pn">
        <div class="table-responsive">
        <table class="table table-bordered">
          <thead>
            <th>Amount</th>
            <th>Method</th>
            <th>Generated</th>
            <th>Paid</th>
            <th>Status</th>
            <th>Fee</th>
          </thead>
          <tbody>
          @forelse ($payments as $payment)
          <tr>
            <td>${{ $payment->amount }}</td>
            <td>{{ $payment->payment_method }}</td>
            <td>{{ date('d/m/Y',strtotime($payment->request_date)) }}</td>
            <td>@if (is_null($payment->payment_date)) - @else {{ date('d/m/Y',strtotime($payment->payment_date)) }} @endif</td>
            <td>@if ($payment->status == 'Pending') <span class="label label-default">{{ $payment->status }}</span> @elseif ($payment->status == 'Denied') <span class="label label-danger">{{ $payment->status }}</span> @else <span class="label label-success">{{ $payment->status }}</span> @endif</td>
            <td>${{ $payment->fee }}</td>
          </tr>
          @empty
          <tr><td colspan="5">No payments yet</td></tr>
          @endforelse
          </tbody>
        </table>
      </div>
        </div>
      </div>
  </div>
  <div class="col-md-4">
    <div class="panel">
<div class="panel-heading">
<span class="panel-title">Info</span>
</div>
<div class="panel-body">
<blockquote class="blockquote-success">
<p>Payment Schedule</p>
<span class="label label-default">{{ $profile->payment_cycle }}</span>
</blockquote>
<blockquote class="blockquote-success">
<p>Payment Method</p>
<span class="label label-default">{{ $profile->payment_method }}</span><a href="{{ url('dashboard/account') }}" class="btn btn-default btn-sm btn-primary btn-rounded ml15">Change</a>
</blockquote>
<p>{{ $myschedule->value }}</p>
<p>If you need a better payment schedule, please contact us</p>
</div>
</div>
  </div>
</div>

            </section>
            <!-- End: Content -->

        </section>
    </div>
    <!-- End: Main -->

    <!-- BEGIN: PAGE SCRIPTS -->

    <!-- jQuery -->
    <script type="text/javascript" src="{{ asset('panel/vendor/jquery/jquery_ui/jquery-ui.min.js') }}"></script>

    <!-- Bootstrap -->
    <script type="text/javascript" src="{{ asset('panel/assets/js/bootstrap/bootstrap.min.js') }}"></script>

    <!-- Theme Javascript -->
    <script type="text/javascript" src="{{ asset('panel/assets/js/utility/utility.js') }}"></script>
    <script type="text/javascript" src="{{ asset('panel/assets/js/main.js') }}"></script>

    <!-- Admin Panels  -->
    <script type="text/javascript" src="{{ asset('panel/assets/admin-tools/admin-plugins/admin-panels/json2.js') }}"></script>
    <script type="text/javascript" src="{{ asset('panel/assets/admin-tools/admin-plugins/admin-panels/jquery.ui.touch-punch.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('panel/assets/admin-tools/admin-plugins/admin-panels/adminpanels.js') }}"></script>


    <script type="text/javascript" src="{{ asset('panel/assets/js/igrowl.min.js') }}"></script>

    <script>

soundManager.setup({
  url: '{{ asset('panel/assets/swf') }}/',
  onready: function() {
    lead_snd = soundManager.createSound({
        url: '{{ asset('panel/assets/audio/chaching.mp3') }}'
      });
  }
});

var app = angular.module('ngDashboard', ['ngSanitize']);
app.controller('PageController', function($scope,$http,$interval) {
  
  $scope.earnings = '${{ formatnum($today->payout,2) }}';
  $scope.clicks = '{{ formatnum($today->clicks) }}';
  $scope.leads = '{{ formatnum($today->leads) }}';
  $scope.epc = '${{ formatnum(division($today->payout,$today->clicks),2) }}';

var serverdate = new Date({{ date("Y, m, d, H, i, s") }});
function displaytime(){
    serverdate.setSeconds(serverdate.getSeconds() + 1);
    var hh=serverdate.getHours();
    var m=serverdate.getMinutes();
    var s=serverdate.getSeconds();
    
    var dd = "AM";
    var h = hh;
    if (h >= 12) {
        h = hh-12;
        dd = "PM";
    }
    if (h == 0) {
        h = 12;
    }
    m=checkTime(m);
    s=checkTime(s);
    var output = h+":"+m+" "+dd;
    $scope.serverTime = output;
}

// Add leading zero in time
function checkTime(i){if (i<10){i="0" + i;}return i;}

var getEarn = function() {
    $http.get( "{{ url('dashboard/earnings') }}").success(function(data, status, headers, config) {
        if ($scope.leads < data.leads) { lead_snd.play(); }
        if (data.clicks == undefined) { $interval.cancel(makeitrain); return; }
        $scope.clicks = data.clicks;
        $scope.leads = data.leads;
        $scope.earnings = data.earnings;
        $scope.epc = data.epc;
    });
};

$interval(displaytime, 1000);
makeitrain = $interval(getEarn, 60000);
});

</script>

    <script type="text/javascript">
        jQuery(document).ready(function() {

            "use strict";

            // Init Theme Core      
            Core.init({
                sbm: "sb-l-c",
            });

            @if ($error)
                $.iGrowl({title:'Notice',message: "{{{ $error }}}",type: 'notice'});
            @endif

            @if ($successmsg)
                $.iGrowl({title:'Success',message: "{{ $successmsg }}",type: 'success'});
            @endif

            $('.admin-panels').adminpanel({
                grid: '.admin-grid',
                draggable: false,
                preserveGrid: true,
                mobile: true,
                callback: function() {
                    bootbox.confirm('<h3>A Custom Callback!</h3>', function() {});
                },
                onFinish: function() {
                    $('.admin-panels').addClass('animated fadeIn').removeClass('fade-onload');

                },
                onSave: function() {
                    $(window).trigger('resize');
                }
            });

            $('.ann-tab').click(function() {
                $('.ann-tab').removeClass('active');
                $(this).addClass('active');

                $('.ann-body').addClass('hidden');
                $($(this).attr('href')).removeClass('hidden')
            });


        });
    </script>

    <!-- END: PAGE SCRIPTS -->

</body>

</html>
