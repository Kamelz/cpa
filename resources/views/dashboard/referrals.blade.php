<!DOCTYPE html>
<html ng-app="ngDashboard" ng-controller="PageController">

<head>
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <title>AdGodMedia - Referrals</title>
    <meta name="keywords" content="{{ $gset->keywords }}" />
    <meta name="description" content="{{ $gset->description }}">

    <meta name="csrf-token" content="{{ csrf_token() }}" />

    <meta name="author" content="AdminDesigns">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- Font CSS (Via CDN) -->
    <link rel='stylesheet' type='text/css' href='//fonts.googleapis.com/css?family=Open+Sans:400,600,700'>
    <link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Raleway:300,400,500,700">

    <!-- Theme CSS -->
    <link rel="stylesheet" type="text/css" href="{{ asset('panel/assets/skin/default_skin/css/theme.css') }}">

    <!-- Custom CSS -->
    <link rel="stylesheet" type="text/css" href="{{ asset('panel/assets/skin/default_skin/css/custom.css') }}">

    <!-- Admin Panels CSS -->
    <link rel="stylesheet" type="text/css" href="{{ asset('panel/assets/admin-tools/admin-plugins/admin-panels/adminpanels.css') }}">

    <link rel="stylesheet" href="{{ asset('panel/css/igrowl.min.css') }}">

    <!-- Favicon -->
    <link rel="shortcut icon" href="{{ asset('favicon.ico') }}">

    <script type="text/javascript" src="{{ asset('panel/vendor/jquery/jquery-1.11.1.min.js') }}"></script>
    <script src="//ajax.googleapis.com/ajax/libs/angularjs/1.2.26/angular.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.3.14/angular-sanitize.min.js"></script>


    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->

@include('ganalytics') </head>

<body class="dashboard-page sb-l-o sb-r-c">

    <!-- Start: Main -->
    <div id="main">

    @include('dashboard.header')

    @include('dashboard.sidebar')



        <!-- Start: Content-Wrapper -->
        <section id="content_wrapper">

            <!-- Start: Topbar -->
            <header id="topbar">
                <div class="topbar-left">
                    <ol class="breadcrumb">
                        <li class="crumb-active">
                            <a href="{{ url('dashboard/referrals') }}">Referral Center</a>
                        </li>
                    </ol>
                </div>
            </header>
            <!-- End: Topbar -->

            <!-- Begin: Content -->
            <section id="content" class="animated fadeIn">

<div class="row">
  <div class="col-md-12">
      <div class="panel">
        <div class="panel-heading"><div class="panel-title"><span class="icon-group"></span> Referrals</div></div>
        <div class="panel-body pn">
        <div class="table-responsive">
        <table class="table table-bordered">
          <thead>
            <th>Username</th>
            <th>Earnings</th>
          </thead>
          <tbody>
          @forelse ($users as $user)
            <tr>
              <td><b>{{ $user->chathandle }}</b></td>
              <td>${{ $refearn->where('referral_id',$user->id)->first()->pay or 0 }}</td>
            </tr>
        @empty
            <tr>
                <td colspan="2">No referrals</td>
            </tr>
        @endforelse
          </tbody>
        </table>
      </div>
        </div>
      </div>
  </div>
</div>

<div class="row">
  <div class="col-md-12">
    <div class="panel">
      <div class="panel-body">
            <h2>Refer & Earn Big</h2>
    <p>Refer new publishers to AdGodMedia and earn <b>5%</b> of their lifetime earnings. Get started by using your Unique URL and referral banners!</p>
            <div class="alert alert-default">
                  <div class="input-group">
                  <span class="input-group-addon"><i class="octicon octicon-link"></i></span>
                  <input class="form-control" type="text" value="{{ url('ref/'.$me) }}">
                  </div>
            </div>


            <div class="row">
              <div class="col-md-8">
                            <div class="alert alert-default text-center">
                                <img src="{{ asset('banners/banner1_468x60.png') }}" alt=""><br>

                                <div class="input-group mt20">
                                <span class="input-group-addon">HTML</i>
                                </span>
                                <input class="form-control" type="text" value="{{{ '<a href="'.url('ref/'.$me).'"><img src="'.asset('banners/banner1_468x60.png').'"  alt="Make Money With Your Files"  /></a>' }}}">
                                </div>
                            </div>

                            <div class="alert alert-default text-center">
                              <img src="{{ asset('banners/banner2_468x60.png') }}" alt=""><br>

                                <div class="input-group mt20">
                                <span class="input-group-addon">HTML</i>
                                </span>
                                <input class="form-control" type="text" value="{{{ '<a href="'.url('ref/'.$me).'"><img src="'.asset('banners/banner2_468x60.png').'"  alt="Make Money With Your Files"  /></a>' }}}">
                                </div>
                            </div>

                            <div class="alert alert-default text-center">
                              <img src="{{ asset('banners/banner4.gif') }}" alt=""><br>

                                <div class="input-group mt20">
                                <span class="input-group-addon">HTML</i>
                                </span>
                                <input class="form-control" type="text" value="{{{ '<a href="'.url('ref/'.$me).'"><img src="'.asset('banners/banner4.gif').'"  alt="Make Money With Your Files"  /></a>' }}}">
                                </div>
                            </div>

                            <div class="alert alert-default text-center">
                              <img src="{{ asset('banners/banner5.png') }}" alt=""><br>

                                <div class="input-group mt20">
                                <span class="input-group-addon">HTML</i>
                                </span>
                                <input class="form-control" type="text" value="{{{ '<a href="'.url('ref/'.$me).'"><img src="'.asset('banners/banner5.png').'"  alt="Make Money With Your Files"  /></a>' }}}">
                                </div>
                            </div>

              </div>
              <div class="col-md-4">
                          <div class="alert alert-default text-center">
                             <img src="{{ asset('banners/banner11.png') }}" alt=""><br>

                                <div class="input-group mt20">
                                <span class="input-group-addon">HTML</i>
                                </span>
                                <input class="form-control" type="text" value="{{{ '<a href="'.url('ref/'.$me).'"><img src="'.asset('banners/banner11.png').'"  alt="Make Money With Your Files"  /></a>' }}}">
                                </div>
              </div>
            </div>

            <div class="row">
              <div class="col-md-12">

                              <div class="alert alert-default text-center">
                              <img src="{{ asset('banners/banner7.gif') }}" alt=""><br>

                                <div class="input-group mt20">
                                <span class="input-group-addon">HTML</i>
                                </span>
                                <input class="form-control" type="text" value="{{{ '<a href="'.url('ref/'.$me).'"><img src="'.asset('banners/banner7.gif').'"  alt="Make Money With Your Files"  /></a>' }}}">
                                </div>
                            </div>

                            <div class="alert alert-default text-center">
                             <img src="{{ asset('banners/banner8.gif') }}" alt=""><br>

                                <div class="input-group mt20">
                                <span class="input-group-addon">HTML</i>
                                </span>
                                <input class="form-control" type="text" value="{{{ '<a href="'.url('ref/'.$me).'"><img src="'.asset('banners/banner8.gif').'"  alt="Make Money With Your Files"  /></a>' }}}">
                                </div>
                            </div>

                            <div class="alert alert-default text-center">
                              <img src="{{ asset('banners/banner9.gif') }}" alt=""><br>

                                <div class="input-group mt20">
                                <span class="input-group-addon">HTML</i>
                                </span>
                                <input class="form-control" type="text" value="{{{ '<a href="'.url('ref/'.$me).'"><img src="'.asset('banners/banner9.gif').'"  alt="Make Money With Your Files"  /></a>' }}}">
                                </div>
                            </div>

                            <div class="alert alert-default text-center">
                              <img src="{{ asset('banners/banner10.gif') }}" alt=""><br>

                                <div class="input-group mt20">
                                <span class="input-group-addon">HTML</i>
                                </span>
                                <input class="form-control" type="text" value="{{{ '<a href="'.url('ref/'.$me).'"><img src="'.asset('banners/banner10.gif').'"  alt="Make Money With Your Files"  /></a>' }}}">
                                </div>
                            </div>
              </div>
            </div>
      </div>
    </div>
  </div>
</div>

            </section>
            <!-- End: Content -->

        </section>

    </div>
    <!-- End: Main -->

    <!-- BEGIN: PAGE SCRIPTS -->

    <!-- jQuery -->
    <script type="text/javascript" src="{{ asset('panel/vendor/jquery/jquery_ui/jquery-ui.min.js') }}"></script>

    <!-- Bootstrap -->
    <script type="text/javascript" src="{{ asset('panel/assets/js/bootstrap/bootstrap.min.js') }}"></script>

    <!-- Theme Javascript -->
    <script type="text/javascript" src="{{ asset('panel/assets/js/utility/utility.js') }}"></script>
    <script type="text/javascript" src="{{ asset('panel/assets/js/main.js') }}"></script>
 
    <!-- Admin Panels  -->
    <script type="text/javascript" src="{{ asset('panel/assets/admin-tools/admin-plugins/admin-panels/json2.js') }}"></script>
    <script type="text/javascript" src="{{ asset('panel/assets/admin-tools/admin-plugins/admin-panels/jquery.ui.touch-punch.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('panel/assets/admin-tools/admin-plugins/admin-panels/adminpanels.js') }}"></script>

    <script type="text/javascript" src="{{ asset('panel/assets/js/igrowl.min.js') }}"></script>

    <script>

soundManager.setup({
  url: '{{ asset('panel/assets/swf') }}/',
  onready: function() {
    lead_snd = soundManager.createSound({
        url: '{{ asset('panel/assets/audio/chaching.mp3') }}'
      });
  }
});

var app = angular.module('ngDashboard', ['ngSanitize']);
app.controller('PageController', function($scope,$http,$interval) {
  
  $scope.earnings = '${{ formatnum($today->payout,2) }}';
  $scope.clicks = '{{ formatnum($today->clicks) }}';
  $scope.leads = '{{ formatnum($today->leads) }}';
  $scope.epc = '${{ formatnum(division($today->payout,$today->clicks),2) }}';

var serverdate = new Date({{ date("Y, m, d, H, i, s") }});
function displaytime(){
    serverdate.setSeconds(serverdate.getSeconds() + 1);
    var hh=serverdate.getHours();
    var m=serverdate.getMinutes();
    var s=serverdate.getSeconds();
    
    var dd = "AM";
    var h = hh;
    if (h >= 12) {
        h = hh-12;
        dd = "PM";
    }
    if (h == 0) {
        h = 12;
    }
    m=checkTime(m);
    s=checkTime(s);
    var output = h+":"+m+" "+dd;
    $scope.serverTime = output;
}

// Add leading zero in time
function checkTime(i){if (i<10){i="0" + i;}return i;}

var getEarn = function() {
    $http.get( "{{ url('dashboard/earnings') }}").success(function(data, status, headers, config) {
        if ($scope.leads < data.leads) { lead_snd.play(); }
        if (data.clicks == undefined) { $interval.cancel(makeitrain); return; }
        $scope.clicks = data.clicks;
        $scope.leads = data.leads;
        $scope.earnings = data.earnings;
        $scope.epc = data.epc;
    });
};

$interval(displaytime, 1000);
makeitrain = $interval(getEarn, 60000);
});

</script>

    <script type="text/javascript">
        jQuery(document).ready(function() {

            "use strict";

            // Init Theme Core      
            Core.init({
                sbm: "sb-l-c",
            });

            $('.admin-panels').adminpanel({
                grid: '.admin-grid',
                draggable: false,
                preserveGrid: true,
                mobile: true,
                callback: function() {
                    bootbox.confirm('<h3>A Custom Callback!</h3>', function() {});
                },
                onFinish: function() {
                    $('.admin-panels').addClass('animated fadeIn').removeClass('fade-onload');

                },
                onSave: function() {
                    $(window).trigger('resize');
                }
            });

            $('.ann-tab').click(function() {
                $('.ann-tab').removeClass('active');
                $(this).addClass('active');

                $('.ann-body').addClass('hidden');
                $($(this).attr('href')).removeClass('hidden')
            });


        });
    </script>

    <!-- END: PAGE SCRIPTS -->

</body>

</html>
