<div id="sidebar-right-tab1" class="tab-pane active">

<h5 class="title-divider text-muted mb20"> Referral Program</h5>
<div class="row">
    <div class="col-xs-12">
        <div class="text-success">
            <p class="pl5 fw700">Refer users to AdGodMedia and get {{ $gset->referralrate }}% referral bonus!</p>
        </div>
    </div>
</div>

<h5 class="title-divider text-muted mt30 mb10">My Referral Link</h5>
<div class="row">
    <div class="col-xs-12">
        <a href="{{ url('ref/'.$me) }}" class="pl5">{{ url('ref/'.$me) }}</a>
    </div>
</div>


<h5 class="title-divider text-muted mt30 mb10">Commission Stats</h5>
<div class="row">
    <div class="col-xs-5">
        <h5 class="text-muted mn pl5">Today</h5>
    </div>
    <div class="col-xs-7 text-right">
        <h4 class="text-success-dark mn">${{ formatnum($today,2) }}</h4>
    </div>
</div>

<div class="row">
    <div class="col-xs-5">
        <h5 class="text-muted mn pl5">This Month</h5>
    </div>
    <div class="col-xs-7 text-right">
        <h4 class="text-success-dark mn">${{ formatnum($mtd,2) }}</h4>
    </div>
</div>

<div class="row">
    <div class="col-xs-5">
        <h5 class="text-muted mn pl5">All Time</h5>
    </div>
    <div class="col-xs-7 text-right">
        <h4 class="text-success-dark mn">${{ formatnum($alltime,2) }}</h4>
    </div>
</div>

<h5 class="title-divider text-muted mt25 mb10">My Referrals</h5>
@foreach ($referrals as $ref)
<div class="row">
    <div class="col-xs-5">
        <h5 class="text-muted mn pl5">ID: {{ $ref->referral_id }}</h5>
    </div>
    <div class="col-xs-7 text-right">
        <h4 class="text-success-dark mn"> ${{ formatnum($ref->pay,2) }} </h4>
    </div>
</div>
@endforeach

</div>