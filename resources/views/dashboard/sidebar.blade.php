         <!-- Start: Sidebar -->
        <aside id="sidebar_left" class="nano nano-primary">
            <div class="nano-content">               

                <!-- sidebar menu -->
                <ul class="nav sidebar-menu">
                    <li class="sidebar-label pt20">Earnings Today</li>
                    <li>
                        <a href="{{ url('dashboard') }}" data-toggle="tooltip" title="Updated every 60 seconds">
                            <span class="octicon octicon-radio-tower"></span>
                            <span class="sidebar-title text-success fs20" ng-bind="earnings">$0.00</span>
                        </a>
                    </li>
                    <li class="sidebar-label pt20">Dashboard</li>
                    <li>
                        <a href="{{ url('dashboard') }}">
                            <span class="octicon octicon-home"></span>
                            <span class="sidebar-title">Home</span>
                        </a>
                    </li>

                    <li class="sidebar-label pt20">Statistics</li>
                    <li>
                        <a href="{{ url('dashboard/overview') }}">
                            <span class="octicon octicon-eye"></span>
                            <span class="sidebar-title">Overview</span>
                        </a>
                    </li>
                    <li>
                        <a href="#" class="accordion-toggle">
                            <span class="octicon octicon-database"></span>
                            <span class="sidebar-title">Breakdowns</span>
                            <span class="caret"></span>
                        </a>
                        <ul class="nav sub-nav">
                            <li>
                                <a href="{{ url('dashboard/timescalebreakdown') }}">
                                    <span class="octicon octicon-clock"></span> Time Breakdown</a>
                            </li>
                            <li>
                                <a href="{{ url('dashboard/countrybreakdown') }}">
                                    <span class="octicon octicon-globe"></span> Country Breakdown</a>
                            </li>
                            <li>
                                <a href="{{ url('dashboard/lockerbreakdown') }}">
                                    <span class="octicon octicon-lock"></span> Locker Breakdown</a>
                            </li>
                            <li>
                                <a href="{{ url('dashboard/reversals') }}">
                                    <span class="octicon octicon-history"></span> Reversed Leads</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="{{ url('dashboard/live') }}">
                            <span class="octicon octicon-flame"></span>
                            <span class="sidebar-title">Live Stats</span>
                        </a>
                    </li>

                    <!-- sidebar resources -->
                    <li class="sidebar-label pt20">Lockers</li>
                    <li>
                        <a href="#" class="accordion-toggle">
                            <span class="octicon octicon-file-directory"></span>
                            <span class="sidebar-title">Files</span>
                            <span class="caret"></span>
                        </a>
                        <ul class="nav sub-nav">
                            <li>
                                <a href="{{ url('dashboard/files') }}">
                                    <span class="octicon octicon-file-submodule"></span> Manage Files</a>
                            </li>
                            <li>
                                <a href="{{ url('dashboard/file/upload') }}">
                                    <span class="octicon octicon-cloud-upload"></span> Upload Files</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="#" class="accordion-toggle">
                            <span class="octicon octicon-browser"></span>
                            <span class="sidebar-title">Gateways</span>
                            <span class="caret"></span>
                        </a>
                        <ul class="nav sub-nav">
                            <li>
                                <a href="{{ url('dashboard/gateways') }}">
                                    <span class="octicon octicon-versions"></span> Manage Lockers</a>
                            </li>
                            <li>
                                <a href="{{ url('dashboard/gateway/create') }}">
                                    <span class="octicon octicon-diff-added"></span> Create Locker</a>
                            </li>
                        </ul>
                    </li>
                     <li>
                        <a href="#" class="accordion-toggle">
                            <span class="octicon octicon-link"></span>
                            <span class="sidebar-title">Notes & Links</span>
                            <span class="caret"></span>
                        </a>
                        <ul class="nav sub-nav">
                            <li>
                                <a href="{{ url('dashboard/notes-links') }}">
                                    <span class="octicon octicon-clippy"></span> Manage Notes & Links</a>
                            </li>
                            <li>
                                <a href="{{ url('dashboard/note/create') }}">
                                    <span class="octicon octicon-file-text"></span> Create Note Locker</a>
                            </li>
                            <li>
                                <a href="{{ url('dashboard/link/create') }}">
                                    <span class="octicon octicon-link"></span> Create Link Locker</a>
                            </li>
                        </ul>
                    </li>
                    <li class="sidebar-label pt20">Network</li>
                    <!--
                    <li>
                        <a href="{{ url('dashboard/leaderboard') }}">
                            <span class="octicon octicon-list-ordered"></span>
                            <span class="sidebar-title">Leaderboard</span>
                        </a>
                    </li>
                    -->
                    <li>
                        <a href="{{ url('dashboard/promotions') }}">
                            <span class="octicon octicon-gift"></span>
                            <span class="sidebar-title">Promotions</span>
                        </a>
                    </li>
                    <li class="sidebar-label pt20">Offers</li>
                    <li>
                        <a href="{{ url('dashboard/offers') }}">
                            <span class="octicon octicon-gift"></span>
                            <span class="sidebar-title">Promote offers</span>
                        </a>
                    </li>					

                    <li class="sidebar-label pt20">Account</li>
                    <li>
                        <a href="{{ url('dashboard/account') }}">
                            <span class="octicon octicon-settings"></span>
                            <span class="sidebar-title">Settings</span>
                        </a>
                    </li>
                    <li>
                        <a href="{{ url('dashboard/payments') }}">
                            <span class="octicon octicon-credit-card"></span>
                            <span class="sidebar-title">Payments</span>
                        </a>
                    </li>
                    <li>
                        <a href="{{ url('dashboard/referrals') }}">
                            <span class="icon-group"></span>
                            <span class="sidebar-title">Referrals</span>
                        </a>
                    </li>
                    <li>
                        <a class="accordion-toggle" href="#">
                            <span class="octicon octicon-tools"></span>
                            <span class="sidebar-title">Extra Tools</span>
                            <span class="caret"></span>
                        </a>
                        <ul class="nav sub-nav">
                            <li>
                                <a href="{{ url('dashboard/offers/optimize') }}">
                                    <span class="octicon octicon-diff"></span> Offers Optimization</a>
                            </li>
                            <li>
                                <a href="{{ url('dashboard/countries/minpayout') }}">
                                    <span class="octicon octicon-diff-ignored"></span> Country Min Payout</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="{{ url('dashboard/tickets') }}">
                            <span class="octicon octicon-megaphone"></span>
                            <span class="sidebar-title">Support</span>
                        </a>
                    </li>

                    <!-- sidebar progress bars -->
                    <!--
                    <li class="sidebar-label pt25 pb10">Last Login</li>
                    <li class="sidebar-stat mb10">
                        <a href="#projectOne" class="fs11">
                            <span class="icon-info-circled text-info"></span>
                            <span class="sidebar-title text-muted">117.193.110.195</span>
                            <span class="pull-right mr20 text-muted">Yesterday</span>
                        </a>
                    </li>
                    -->
                </ul>
            </div>
        </aside>