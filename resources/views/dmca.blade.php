<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>AdGodMedia - DMCA</title>
    <meta name="keywords" content="{{ $gset->keywords }}" />
    <meta name="description" content="{{ $gset->description }}">
    <link rel="stylesheet" href="{{ asset('site/css/foundation.css') }}" />
    <!-- Favicon -->
    <link rel="shortcut icon" href="{{ asset('favicon.ico') }}">
    <!-- Google Font -->
    <link href='//fonts.googleapis.com/css?family=Open+Sans:400,700|Raleway:700,400' rel='stylesheet' type='text/css'>
    <!-- Custom CSS-->
    <link rel="stylesheet" href="{{ asset('site/css/app.css') }}" />
    <script src="{{ asset('site/js/vendor/modernizr.js') }}"></script>

    <link rel="stylesheet" href="{{ asset('site/css/animate.min.css') }}">

    <link rel="stylesheet" href="{{ asset('site/css/font-awesome.min.css') }}">

    @include('ganalytics')

</head>
<body>

@include('_header')

<section class="pagehead">
	<div class="overlay">
	<div class="row">
		<h1 class="white" style="font-weight:bold;">DMCA</h1>
	</div>
	</div>
</section>

<section class="content">
	<div class="row">
		<div class="large-12 columns">
Digital Millennium Copyright Act (DMCA) is a United States copyright law that allows specific files to be removed by their owners if they are being used without proper permission. In order to file a complaint, you should take the following steps:
<br><br>
1. Gather the URL(s) of the offending file(s).
<br><br>
2. Make sure you are the proper owner of the file and that the file does not have permission to be used on this site. In order for the DMCA to succeed, <b>you must send an email from a domain that is legitimately owned by the offended company</b>. DMCA complaints from Gmail, Hotmail, or other web mail accounts will not be accepted.
<br><br>
3. Prepare an email to be sent to <a href="mailto:dmca@adgodmedia.com">dmca@adgodmedia.com</a> as well as <a href="mailto:admin@adgodmedia.com">admin@adgodmedia.com</a>
<br><br>
4. Make the subject of the email “DMCA – {Company Name}” (eg. “DMCA – Microsoft”)
<br><br>
5. Include the URL(s) in step one in the content of the email. The email should include the Identification of copyrighted works, Copyright infringing material location(s), and Statement of authority.
<br><br>
Once the email is sent, the file will be taken down as soon as possible. We are very sorry for this inconvenience!
		</div>
	</div>
</section>

<br><br><br>

@include('_footer')

  <script src="{{ asset('site/js/vendor/jquery.js') }}"></script>
  <script src="{{ asset('site/js/foundation.min.js') }}"></script>

  <script src="{{ asset('site/js/wow.min.js') }}"></script>

  <script src="{{ asset('site/js/jquery-scrollto.js') }}"></script>
  <script src="{{ asset('site/js/jquery.sticky.js') }}"></script>

  <script>
    $(document).foundation();

    new WOW().init();

    var isLarge, isMedium, isSmall;

isSmall = function() {
  return matchMedia(Foundation.media_queries['small']).matches && !matchMedia(Foundation.media_queries.medium).matches;
};

isMedium = function() {
  return matchMedia(Foundation.media_queries['medium']).matches && !matchMedia(Foundation.media_queries.large).matches;
};

isLarge = function() {
  return matchMedia(Foundation.media_queries['large']).matches;
};

if (!isSmall()) {

  $(window).bind("load", function() {  
		$('.vidplay').load('video.php<?php if(isset($_GET['v'])) { echo "?v=".$_GET['v']; } ?>');
	});

}    

	// Testing
	$('header img').click(function() {
	  	$('.testing').toggle();
	});

// display the first div by default.
$("#accordion div").first().css('display', 'block');


// Get all the links.
var link = $("#accordion a");

// On clicking of the links do something.
link.on('click', function(e) {

    e.preventDefault();

    var a = $(this).attr("href");

    $(a).slideDown('fast');

    //$(a).slideToggle('fast');
    $("#accordion div").not(a).slideUp('fast');
    
});

$.fn.animateNumbers = function(stop, commas, duration, ease) {
        return this.each(function() {
            var $this = $(this);
            var start = parseInt($this.text().replace(/,/g, ""));
			commas = (commas === undefined) ? true : commas;
            $({value: start}).animate({value: stop}, {
            	duration: duration == undefined ? 1000 : duration,
            	easing: ease == undefined ? "swing" : ease,
            	step: function() {
            		$this.text(Math.floor(this.value));
					if (commas) { $this.text($this.text().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")); }
            	},
            	complete: function() {
            	   if (parseInt($this.text()) !== stop) {
            	       $this.text(stop);
					   if (commas) { $this.text($this.text().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")); }
            	   }
            	}
            });
        });
    };

$(window).scroll(function() {
$(".stats span").each(function() {
      if($(window).height() + $(window).scrollTop() - $(this).offset().top > 0) {
            $("#num-2").animateNumbers(3144, true, 2000);
		    $("#num-1").animateNumbers(109586, true, 2000);
		    $("#num-3").animateNumbers(1510, true, 2000);
		    $("#num-4").animateNumbers(482, true, 2000);
      }
    });
});

$('.pad').click(function() {
	Foundation.utils.S('.pad').removeClass('active');
	Foundation.utils.S(this).addClass('active');
	var num = Foundation.utils.S(this).attr('data-img');
	element = Foundation.utils.S('.features img');
	element.attr('src','img/'+num+'.png').addClass('animated zoomIn');
	window.setTimeout( function(){
          element.removeClass('animated zoomIn');
      }, 1300);
});

$(document).ready(function(){
	$("header").sticky({topSpacing:0});
});

	// Testing
  </script>
	
</body>
</html>