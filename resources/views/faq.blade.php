<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>AdGodMedia - FAQ</title>
    <meta name="keywords" content="{{ $gset->keywords }}" />
    <meta name="description" content="{{ $gset->description }}">
    <link rel="stylesheet" href="{{ asset('site/css/foundation.css') }}" />
    <!-- Favicon -->
    <link rel="shortcut icon" href="{{ asset('favicon.ico') }}">
    <!-- Google Font -->
    <link href='//fonts.googleapis.com/css?family=Open+Sans:400,700|Raleway:700,400' rel='stylesheet' type='text/css'>
    <!-- Custom CSS-->
    <link rel="stylesheet" href="{{ asset('site/css/app.css') }}" />
    <script src="{{ asset('site/js/vendor/modernizr.js') }}"></script>

    <link rel="stylesheet" href="{{ asset('site/css/animate.min.css') }}">

    <link rel="stylesheet" href="{{ asset('site/css/font-awesome.min.css') }}">

    @include('ganalytics')

</head>
<body>

@include('_header')

<section class="pagehead">
    <div class="overlay">
    <div class="row">
        <h1 class="white" style="font-weight:bold;">Frequently Asked Questions</h1>
    </div>
    </div>
</section>

<section class="content">
    <div class="row text-justify">
        <div class="large-12 columns">
            <ul class="large-block-grid-2">
                <li>
                <i class="fa fa-question-circle green"></i><b> How much does AdGodMedia Pay?</b><br>
                The average rate is around $1,000 for 1000 downloads. You can earn on average anywhere between 30 cents up to 15 dollars for each user who gains access to your content depending on what country the user is coming from and the offer selected. Most leads pay from around 1 dollar for the United States, United Kingdom, Canada, and Australia. Other countries vary between 25 - 60 cents usually.<br><br>
                <i class="fa fa-question-circle green"></i><b> What are AdGodMedia payment methods?</b><br>
                Payments will be made via Paypal, Payza, Wire Transfer, Skrills or Western Union. We also support Bitcoin payments.<br><br>
                <i class="fa fa-question-circle green"></i><b> What type of files are allowed?</b><br>
                All file types are accepted for upload. Pornography and copyright files are NOT allowed.br <br><br>
                <i class="fa fa-question-circle green"></i><b> Can I get paid quicker?</b><br>
                You can use the quick payout feature to get your payment sent sooner. A 20% fee is taken and payments are usually sent within 24 hours.
                <br><br>
                <i class="fa fa-question-circle green"></i><b> What type of websites can I use the content gateway on?</b><br>
                All types of websites are able to be monetized using our content gateway. Pornography and copyright content is NOT allowed and all earnings can be forefeited if found!
                <br><br>
                </li>
                <li>
                <i class="fa fa-question-circle green"></i><b> What is your schedule for payments?</b><br>
                You are able to request a payout at anytime once you reach the 25 USD limit. Earnings will be reviewed by our fraud department at the end of each month. Payments will be sent Net7 and net15 (7,15 days after each month). <br><br>
                <i class="fa fa-question-circle green"></i><b> What makes you better than the others?</b><br>
                We strive to make sure we are offering the highest payouts in the industry. We have a dedicated team working with our advertisers 24/7 always looking for the newest, highest performing advertisements to ensure you are getting the most money for your content possible. We have the most sophiscated system around and are constantly making changes and finding new advertisements to make sure you are earning the most money in the industry - If you want to get paid the most money for your content using the most sophisticated content locking site out there then join The AdGodMedia Network today!
                <br><br>
                <i class="fa fa-question-circle green"></i><b> How does the file gateway work?</b><br>
                You can upload files that are protected. Users have to complete an offer in order to access your file.
                <br><br>
                <i class="fa fa-question-circle green"></i><b> What if I have more questions?</b><br>
                Please use the contact form above and we will get back to you quickly with any questions you have.
                </li>
            </ul>
        </div>
    </div>
</section>

<section class="content cta" style="padding-bottom:60px;">

<div class="row">

    <div class="large-8 medium-8 small-12 columns text-right small-text-center">
        <h2 style="margin:0px;">Join <span style="color:#0AB600;">AdGodMedia</span> &amp; Start Earning Today!</h2>
    </div>
    <div class="large-4 medium-4 small-12 columns text-center">
        <button class="button success medium">APPLY</button>
    </div>
</div>

</section>

@include('_footer')

  <script src="{{ asset('site/js/vendor/jquery.js') }}"></script>
  <script src="{{ asset('site/js/foundation.min.js') }}"></script>

  <script src="{{ asset('site/js/wow.min.js') }}"></script>

  <script src="{{ asset('site/js/jquery-scrollto.js') }}"></script>
  <script src="{{ asset('site/js/jquery.sticky.js') }}"></script>

  <script>
    $(document).foundation();

    new WOW().init();

    var isLarge, isMedium, isSmall;

isSmall = function() {
  return matchMedia(Foundation.media_queries['small']).matches && !matchMedia(Foundation.media_queries.medium).matches;
};

isMedium = function() {
  return matchMedia(Foundation.media_queries['medium']).matches && !matchMedia(Foundation.media_queries.large).matches;
};

isLarge = function() {
  return matchMedia(Foundation.media_queries['large']).matches;
};

if (!isSmall()) {

  $(window).bind("load", function() {  
        $('.vidplay').load('video.php<?php if(isset($_GET['v'])) { echo "?v=".$_GET['v']; } ?>');
    });

}    

    // Testing
    $('header img').click(function() {
        $('.testing').toggle();
    });

// display the first div by default.
$("#accordion div").first().css('display', 'block');


// Get all the links.
var link = $("#accordion a");

// On clicking of the links do something.
link.on('click', function(e) {

    e.preventDefault();

    var a = $(this).attr("href");

    $(a).slideDown('fast');

    //$(a).slideToggle('fast');
    $("#accordion div").not(a).slideUp('fast');
    
});

$.fn.animateNumbers = function(stop, commas, duration, ease) {
        return this.each(function() {
            var $this = $(this);
            var start = parseInt($this.text().replace(/,/g, ""));
            commas = (commas === undefined) ? true : commas;
            $({value: start}).animate({value: stop}, {
                duration: duration == undefined ? 1000 : duration,
                easing: ease == undefined ? "swing" : ease,
                step: function() {
                    $this.text(Math.floor(this.value));
                    if (commas) { $this.text($this.text().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")); }
                },
                complete: function() {
                   if (parseInt($this.text()) !== stop) {
                       $this.text(stop);
                       if (commas) { $this.text($this.text().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")); }
                   }
                }
            });
        });
    };

$(window).scroll(function() {
$(".stats span").each(function() {
      if($(window).height() + $(window).scrollTop() - $(this).offset().top > 0) {
            $("#num-2").animateNumbers(3144, true, 2000);
            $("#num-1").animateNumbers(109586, true, 2000);
            $("#num-3").animateNumbers(1510, true, 2000);
            $("#num-4").animateNumbers(482, true, 2000);
      }
    });
});

$('.pad').click(function() {
    Foundation.utils.S('.pad').removeClass('active');
    Foundation.utils.S(this).addClass('active');
    var num = Foundation.utils.S(this).attr('data-img');
    element = Foundation.utils.S('.features img');
    element.attr('src','img/'+num+'.png').addClass('animated zoomIn');
    window.setTimeout( function(){
          element.removeClass('animated zoomIn');
      }, 1300);
});

$(document).ready(function(){
    $("header").sticky({topSpacing:0});
});

    // Testing
  </script>
    
</body>
</html>