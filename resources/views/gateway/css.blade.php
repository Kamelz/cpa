@foreach ($gfonts as $gfont)
@import url(//fonts.googleapis.com/css?family={{$gfont}});
@endforeach

.ucwidget {
    height: {{ $css['height'] }}px;
    width: {{ $css['width'] }}px;
    color: #fff;
    background-image: url('{{ $gateway->bgimg }}');
    text-align: center;
    background-color:#000;
    border-radius: {{ $css['border_radius'] }}px;
    position: relative;
  	margin: 20px auto;
}
.ucwidget h2 {
  font-family: '{{ $css['title_font'] }}',sans-serif;
  color: {{ $css['title_color'] }};
  font-size: {{ $css['title_font_size'] }}px;
  padding-top: {{ $css['title_top_margin'] }}px;
  margin: 10px auto;
}
.ucwidget p {
  font-family: '{{ $css['instruction_font'] }}',sans-serif;
  color: {{ $css['instruction_color'] }};
  font-size: {{ $css['instruction_font_size'] }}px !important;
  font-size: 14px;
  padding-top: {{ $css['instruction_top_margin'] }}px;
  margin: 10px auto;
}
.ucwidget ul {
  list-style-type: none;
  padding-top: {{ $css['offers_top_margin'] }}px;
  text-align: {{ $css['offers_alignment'] }};
  padding-left: 40px;
  padding-right: 40px;
  margin-bottom: 0;
  margin-top: 0px;
}
.ucwidget ul a {
  font-family: '{{ $css['offers_font'] }}',sans-serif;
  color: {{ $css['offers_color'] }} !important;
  font-size: {{ $css['offers_font_size'] }}px;
  text-decoration: none;
}

.mfp-bg {
background-image: url({{ asset('gateway/overlay.png') }});
}

#moreoffers {
  font-size: 70%;
  color: {{ $css['offers_color'] }};
  text-decoration: none;
  font-family: arial;
  text-decoration: underline;
}