var js;
var html_doc = document.getElementsByTagName('head')[0];

function loader() {
    if (typeof jQuery != 'undefined') {
        magnific();
        return;
    }
    js = document.createElement('script');
    js.setAttribute('type', 'text/javascript');
    js.setAttribute('src', "//ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js");
    html_doc.appendChild(js);
    js.setAttribute("onload", "magnific()");
    js.onreadystatechange = function() {
        if (js.readyState == 'loaded' || js.readyState == 'complete') {
            magnific();
        }
    };
}

loader();

function magnific() {
    js = document.createElement('script');
    js.setAttribute('type', 'text/javascript');
    js.setAttribute('src', "{{ asset('panel/vendor/plugins/magnific/jquery.magnific-popup.js') }}");
    html_doc.appendChild(js);
    js.setAttribute("onload", "magnificcss()");
    js.onreadystatechange = function() {
        if (js.readyState == 'loaded' || js.readyState == 'complete') {
            magnificcss();
        }
    }
}

function magnificcss() {
    js = document.createElement('link');
    js.setAttribute('rel', 'stylesheet');
    js.setAttribute('type', 'text/css');
    js.setAttribute('href', "{{ asset('panel/vendor/plugins/magnific/magnific-popup.css') }}");
    html_doc.appendChild(js);
    js.setAttribute("onload", "lockercss()");
    js.onreadystatechange = function() {
        if (js.readyState == 'loaded' || js.readyState == 'complete') {
            lockercss();
        }
    }
}

function lockercss() {
    js = document.createElement('link');
    js.setAttribute('rel', 'stylesheet');
    js.setAttribute('type', 'text/css');
    js.setAttribute('href', "{{ url('gateway/css/'.$gateway->id) }}");
    html_doc.appendChild(js);
    js.setAttribute("onload", "locker()");
    js.onreadystatechange = function() {
        if (js.readyState == 'loaded' || js.readyState == 'complete') {
            locker();
        }
    }
}

function locker() {

    @if($gateway->load_method == "onload")
    loadlocker();
    @endif
}

function loadlocker() {

    if ($(".ucwidget").length == 0) {
        $('body').append('<div class="ucwidget mfp-hide" id="ucwidget"><h2>{{ $gateway->title }}</h2><p>{{ $gateway->inst }}</p><ul> @foreach ($offers as $offer) <li><a href="{{ url('gotooffer'.'?offer='.$offer->campid.'&pub='.$gateway->user_id.'&nt='.$offer->network.'&locker=gateway&lid='.$gateway->id.'&ip='.$ip.'&cnty='.$location->isoCode.'&site_referrer='.$site_referrer.'&session='.$sessid) }}" target="_blank">{{ $offer->title }}</a></li> @endforeach </ul><!--<a href="#" id="moreoffers">Load More Offers</a>--></div>');
    }

    var set = 1;

    $('a#moreoffers').click(function() {
       $(this).text('Please Wait..');
          $.get("{{ url('moreoffers') }}", { _token: '{{ csrf_token() }}', user: {{ $gateway->user_id}}, country: '{{ $location->isoCode }}', skip: set, num: {{ $num }} })
          .done(function(data) {
              $('.ucwidget ul').empty();
              $.each(data, function(index,value) {

                  $('.ucwidget ul').append('<li><a href="{{ url('gotooffer') }}?offer=' + value.campid +'&pub={{ $gateway->user_id }}&nt=' + value.network + '&locker=gateway&lid={{ $gateway->id }}&ip={{ $ip }}&cnty={{ $location->isoCode }}&site_referrer={{ $site_referrer }}&session={{ $sessid }}" target="_blank">' + value.title + '</a></li>');

              });
              set++;
              $('a#moreoffers').text('Load More Offers');
      });
    });

    $.magnificPopup.open({items: {src: '#ucwidget'},type:'inline',@if ($gateway->allow_close == 0) modal:true @else showCloseBtn: false @endif}, 0);
    var completionCheck = setInterval(function(){
            $.post('{{ url('check/'.$sessid) }}', { _token: '{{ csrf_token() }}', user: {{ $gateway->user_id }}, country: '{{ $location->isoCode }}', ip: {{ $ip }}, ctype: 'gateway', id: {{ $gateway->id }}, content: '{{ $gateway->name }}' } )
            .done(function(result) {
                if(result != 0) { $('#offers').html('Congratulations! <a href="{{ url("gotocontent"."?session=".$sessid."&ip=".$ip."&country=".$location->isoCode) }}">Click here</a> to get your content'); clearInterval(completionCheck); }
            });
     }, 10000);
}