<!DOCTYPE html>
<html>
	<head>
		
	<title>Adgodmedia|Content Locking & Incentive CPA Network</title>
	<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
		<meta name="description" content="{{ $gset->description }}">
		
		<!-- Put favicon.ico and apple-touch-icon(s).png in the images folder -->
	    <link rel="shortcut icon" href="{{ asset('site/images/favicon1.ico') }}" >
		    	
		<link href='https://fonts.googleapis.com/css?family=Oswald:400,100,300,500,700%7CLato:400,300,700,900&subset=latin,latin-ext' rel='stylesheet' type='text/css'>
		
		<!-- Plugins CSS files -->
		<link rel="stylesheet" href="{{ asset('site/css/assets.css')}}">

		<!-- REVOLUTION SLIDER STYLES -->
		<link rel="stylesheet" href="{{ asset('site/css/pe-icon-7-stroke.css') }}" type="text/css">
		<link rel="stylesheet" href="{{ asset('site/css/settings.css') }}" type="text/css">
		<link rel="stylesheet" href="{{ asset('site/css/navigation.css') }}" type="text/css">
		
		<!-- Template CSS files -->
		<link rel="stylesheet" href="{{ asset('site/css/style.css') }}">
		<link rel="stylesheet" href="{{ asset('site/css/shortcodes.css') }}">
		<link id="theme_css" rel="stylesheet" href="{{ asset('site/css/light.css') }}">
		<link id="skin_css" rel="stylesheet" href="{{ asset('site/css/skins/default.css') }}">
		@include('ganalytics')
	</head>
	<body>
			<!-- site preloader start -->
		<div class="page-loader"></div>
		<!-- site preloader end -->
		
		<div class="pageWrapper">
			
			<!-- Header start -->
			<div class="top-bar main-bg no-border">
				<div class="container">
					
					<ul class="top-info f-left">
					    <li><i class="fa fa-envelope white"></i><b class="white">Email:</b> <a href="#">support@adgodmedia.com</a></li>
					    <li><i class="fa fa-phone white"></i><b class="white">Phone:</b> +1 (888) 000-0000</li>
					    <li class="dropdown language-selector">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown" data-close-others="true"><i class="fa fa-flag white"></i>EN</a>
							<ul class="dropdown-menu">
								<li>
									<a href="#">AR</a>
								</li>
								<li class="active">
									<a href="#">EN</a>
								</li>
								<li>
									<a href="#">FR</a>
								</li>
								<li>
									<a href="#">RU</a>
								</li>
							</ul>
						</li>
				    </ul>
					
				    <div class="f-right social-list">
					    <span class="lbl-txt">Follow Us On:</span>
					    <a href="#" data-toggle="tooltip" data-placement="bottom" data-original-title="Facebook"><i class="fa fa-facebook ic-facebook no-border"></i></a>
						<a href="#" data-toggle="tooltip" data-placement="bottom" data-original-title="Twitter"><i class="fa fa-twitter ic-twitter no-border"></i></a>
						<a href="#" data-toggle="tooltip" data-placement="bottom" data-original-title="Linkedin"><i class="fa fa-linkedin ic-linkedin no-border"></i></a>
						<a href="#" data-toggle="tooltip" data-placement="bottom" data-original-title="Dribbble"><i class="fa fa-dribbble ic-dribbble no-border"></i></a>
						<a href="#" data-toggle="tooltip" data-placement="bottom" data-original-title="Google Plus"><i class="fa fa-google-plus ic-gplus no-border"></i></a>
				    </div>

				</div>
			</div>
			<header class="top-head header-1 skew">
		    	<div class="container">
			    	<!-- Logo start -->
			    	<div class="logo">
				    	<a href="{{ url('/') }}"><img alt="" src="{{ asset('site/images/logo1.png') }}" /></a>
				    </div>
				    <!-- Logo end -->
				    
					<div class="responsive-nav">
						<!-- top navigation menu start -->
						<nav class="top-nav">
							<ul>
								<li class="selected"><a href="http://adgodmedia.com/"><span>Home</span></a>
										
								</li>
								<li><a href="http://adgodmedia.com/publishers"><span>Publisher</span></a>
								</li>
																<li><a href="http://adgodmedia.com/advertisers"><span>Advertiser</span></a>
								</li>
																<li><a href="http://adgodmedia.com/about"><span>About Us</span></a>
								</li>
																<li><a href="http://www.adgodmedia.com/contactus"><span>Contact Us</span></a>
								</li>
																<li><a href="http://www.adgodmedia.com/auth/register"><span>Sign Up</span></a>
								</li>								<li><a href="http://www.adgodmedia.com/auth/login"><span>Sign In</span></a>
								</li>
								
							</ul>
						</nav>
					
					  
					</div>
		    	</div>
		    </header>
		    <!-- Header start -->
		    
		    <!-- Content start -->
	    	<div class="pageContent">
	    		
	    		<div id="slider_wrapper" class="rev_slider_wrapper fullwidthbanner-container">
					<div id="slider" class="rev_slider fullwidthabanner">
						<ul>	
							
							<!-- SLIDE  -->
							<li data-index="rs-243" data-transition="fade" data-slotamount="default" data-easein="default" data-easeout="default" data-masterspeed="1500" data-saveperformance="off">
								<!-- MAIN IMAGE -->
								<img src="{{ asset('site/images/slide-1.jpg') }}" alt="" data-bgposition="center center" data-bgfit="none" data-bgrepeat="repeat" data-bgparallax="5" class="rev-slidebg" data-no-retina>
								<!-- LAYERS -->

								<!-- LAYER NR. 1 -->
								<div class="tp-caption tp-resizeme rs-parallaxlevel-1" 
									data-x="['right','right','center','center']" data-hoffset="['0','0','15','15']" 
									data-y="['middle','middle','bottom','bottom']" data-voffset="['0','0','0','0']" 
									data-width="none"
									data-height="none"
									data-whitespace="nowrap"
									data-transform_idle="o:1;"
									data-transform_in="x:right;s:1500;e:Power3.easeOut;" 
									data-transform_out="opacity:0;s:1500;e:Power4.easeIn;s:1500;e:Power4.easeIn;" 
									data-start="1000" 
									data-responsive_offset="on" 									
									style="z-index: 5;"><img src="{{ asset('site/images/man.png') }}" alt="" data-no-retina> 
								</div>

								<!-- LAYER NR. 7 -->
								<div class="tp-caption white light-font uppercase tp-resizeme rs-parallaxlevel-0" 
									data-x="['left','left','center','center']" data-hoffset="['30','30','0','0']" 
									data-y="['top','top','top','top']" data-voffset="['230','230','120','130']" 
									data-fontsize="['25','25','20','15']"
									data-lineheight="['25','25','20','15']"
									data-width="none"
									data-height="none"
									data-whitespace="nowrap"
									data-transform_idle="o:1;"
									data-transform_in="x:-50px;opacity:0;s:1000;e:Power2.easeOut;" 
									data-transform_out="opacity:0;s:1500;e:Power4.easeIn;s:1500;e:Power4.easeIn;" 
									data-start="1000" 
									data-splitin="none" 
									data-splitout="none" 
									data-responsive_offset="on" 									
									style="z-index: 11; white-space: nowrap;">Next Generation Content Locking CPA Network 
								</div>

								<!-- LAYER NR. 8 -->
								<h2 class="tp-caption tp-resizeme heavy white uppercase rs-parallaxlevel-0" 
									data-x="['left','left','center','center']" data-hoffset="['30','30','0','0']" 
									data-y="['top','top','top','top']" data-voffset="['275','275','190','190']" 
									data-fontsize="['80','80','50','30']"
									data-width="none"
									data-height="none"
									data-whitespace="nowrap"
									data-transform_idle="o:1;"
									data-transform_in="x:-50px;opacity:0;s:1000;e:Power2.easeOut;" 
									data-transform_out="opacity:0;s:1500;e:Power4.easeIn;s:1500;e:Power4.easeIn;" 
									data-start="1250" 
									data-splitin="none" 
									data-splitout="none" 
									data-responsive_offset="on" 									
									style="z-index: 12; white-space: nowrap;">IT Is <span class="main-color">ADGODMEDIA</span>
								</h2>

								<!-- LAYER NR. 9 -->
								<h3 class="tp-caption tp-resizeme uppercase white light-font rs-parallaxlevel-0" 
									data-x="['left','left','center','center']" data-hoffset="['30','30','0','0']" 
									data-y="['top','top','top','top']" data-voffset="['340','340','300','300']" 
									data-fontsize="['22','22','20','15']"
									data-lineheight="['25','25','20','15']"
									data-whitespace="nowrap"
									data-transform_idle="o:1;"
									data-transform_in="x:-50px;opacity:0;s:1000;e:Power2.easeOut;" 
									data-transform_out="opacity:0;s:1500;e:Power4.easeIn;s:1500;e:Power4.easeIn;" 
									data-start="1500" 
									data-splitin="none" 
									data-splitout="none" 
									data-responsive_offset="on" 									
									style="z-index: 13; white-space: nowrap;letter-spacing: 2px">A Revolutionary Content Locking CPA Network
								</h3>

								<!-- LAYER NR. 10 -->
								<div class="tp-caption rs-parallaxlevel-0" 
									data-x="['left','left','left','center']" data-hoffset="['30','30','30','30']" 
									data-y="['bottom','bottom','bottom','bottom']" data-voffset="['110','110','110','110']" 
									data-whitespace="nowrap"
									data-transform_idle="o:1;"
									data-transform_hover="o:1;rX:0;rY:0;rZ:0;z:0;s:300;e:Linear.easeNone;"
									data-transform_in="x:-50px;opacity:0;s:1000;e:Power2.easeOut;" 
									data-transform_out="opacity:0;s:1500;e:Power4.easeIn;s:1500;e:Power4.easeIn;" 
									data-start="1750" 
									data-splitin="none" 
									data-splitout="none" 
									data-actions='[{"event":"click","action":"scrollbelow","offset":"px"}]'
									data-responsive_offset="on" 
									style="z-index: 14; white-space: nowrap;"><a href="http://www.adgodmedia.com/auth/register" class="btn main-bg btn-lg left-icon"><span><i class="fa fa-key"></i>Sign Up Now</span></a>
								</div>

								<!-- LAYER NR. 10 -->
								<div class="tp-caption rs-parallaxlevel-0" 
									data-x="['left','left','right','center']" data-hoffset="['226','226','50','0']" 
									data-y="['bottom','bottom','bottom','bottom']" data-voffset="['110','110','110','110']" 
									data-whitespace="nowrap"
									data-transform_idle="o:1;"
									data-transform_in="x:-50px;opacity:0;s:1000;e:Power2.easeOut;" 
									data-transform_out="opacity:0;s:1500;e:Power4.easeIn;s:1500;e:Power4.easeIn;" 
									data-start="2000" 
									data-splitin="none" 
									data-splitout="none" 
									data-actions='[{"event":"click","action":"scrollbelow","offset":"px"}]'
									data-responsive_offset="on" 
									style="z-index: 14; white-space: nowrap;"><a href="http://www.adgodmedia.com/auth/login" class="btn btn-default btn-lg left-icon"><span><i class="fa fa-lock"></i>Login Now</span></a>
								</div>

							</li>

							<!-- SLIDE  -->
							<li data-index="rs-2432" data-transition="fade" data-slotamount="default" data-easein="default" data-easeout="default" data-masterspeed="1500" data-saveperformance="off">
								<!-- MAIN IMAGE -->
								<img src="{{ asset('site/images/slider1.jpg') }}" alt="" data-bgposition="center center" data-bgrepeat="no-repeat" data-bgfit="cover" data-bgparallax="10" class="rev-slidebg" data-no-retina>
								<!-- LAYERS -->
	
								<!-- LAYER NR. 1 -->
								<h2 class="tp-caption tp-resizeme heavy white uppercase rs-parallaxlevel-0" 
									data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']"  
									data-y="['middle','middle','middle','middle']" data-voffset="['0','0','0','0']" 
									data-fontsize="['70','70','50','40']"
									data-lineheight="['70','70','70','50']"
									data-width="none"
									data-height="none"
									data-whitespace="nowrap"
									data-transform_idle="o:1;"
									data-transform_in="x:[105%];z:0;rX:45deg;rY:0deg;rZ:90deg;sX:1;sY:1;skX:0;skY:0;s:2000;e:Power4.easeInOut;" 
									data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;" 
									data-mask_in="x:0px;y:0px;s:inherit;e:inherit;" 
									data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;" 
									data-start="1250" 
									data-splitin="none" 
									data-splitout="none" 
									data-responsive_offset="on" 
									style="z-index: 12; white-space: nowrap;"><span class="main-color">Earn Huge Money With ADGODMEDIA</span>
								</h2>
								
									
												

								<!-- LAYER NR. 2 -->
								<div class="tp-caption tp-resizeme uppercase heavy white light-font rs-parallaxlevel-0" 
									data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" 
									data-y="['middle','middle','middle','middle']" data-voffset="['52','52','52','51']" 
									data-width="['auto','auto','500','300']"
									data-fontsize="['20','18','15','15']"
									data-lineheight="['25','25','20','20']"
									data-height="none"
									data-whitespace="['nowrap','nowrap','normal','normal']"
									data-transform_idle="o:1;"
									data-transform_in="y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:2000;e:Power4.easeInOut;" 
									data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;" 
									data-mask_in="x:0px;y:[100%];s:inherit;e:inherit;" 
									data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;" 
									data-start="1500" 
									data-splitin="none" 
									data-splitout="none" 
									data-responsive_offset="on" 
									style="z-index: 6; white-space: nowrap;letter-spacing: 3px">Just try it out, Surprises are waiting, Don't miss. 
									
								</div>

								
								
								<!-- LAYER NR. 3 -->
								<div class="tp-caption tp-resizeme red rs-parallaxlevel-0" 
									data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" 
									data-y="['middle','middle','middle','middle']" data-voffset="['-80','-80','-80','-80']" 
									data-fontsize="['70','50','35','25']"
									data-lineheight="['75','75','35','25']"
									data-width="none"
									data-height="none"
									data-whitespace="nowrap"
									data-transform_idle="o:1;"
									data-style_hover="cursor:default;"
									data-transform_in="y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:1500;e:Power4.easeInOut;" 
									data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;" 
									data-mask_in="x:0px;y:[100%];s:inherit;e:inherit;" 
									data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;" 
									data-start="2000" 
									data-splitin="none" 
									data-splitout="none" 
									data-responsive_offset="on" 
									style="z-index: 7; white-space: nowrap;"><i class="fa fa-heart"></i> 
								</div>

							</li>


						</ul>
						<div class="tp-bannertimer tp-bottom" style="visibility: hidden !important;"></div>	
					</div>
				</div>
				

				<div class="md-padding p-b-0 gry-bg">
					<div class="container">
						<div class="heading main centered">
							<h3 class="uppercase lg-title">Why Choose <span class="main-color">ADGODMEDIA</span></h3>
							<p>Our Main Goal is to provide maximum support of our publisher to Earn money</p>
						</div>
						<div class="row">
							<div class="col-md-4 side-cell right-icons">
								<div class="icon-box simple">
									<i class="fa fa-money md-icon icon-bg main-bg border3px"></i>
									<div class="icon-simple-desc">
										<h4 class="uppercase">Highest Payouts</h4>
										<p>We try our best to provide the highest rates in the industry for your traffic. Our strong relationship with advertisers allows us access to the best campaigns you will find..</p>
									</div>
								</div>
								<div class="icon-box simple">
									<i class="fa fa-cc-mastercard md-icon icon-bg main-bg border3px"></i>
									<div class="icon-simple-desc">
										<h4 class="uppercase">Flexible Payments</h4>
										<p>We currently offer Paypal, Payza, skrill, Wire Transfer,Payoneer and Neteller for payout options. Need something else? Our team will do the best to accommodate your needs.</p>
									</div>
								</div>
								<div class="icon-box simple">
									<i class="fa fa-life-ring md-icon icon-bg main-bg border3px"></i>
									<div class="icon-simple-desc">
										<h4 class="uppercase">Community Forum</h4>
										<p>Learn new tactics and techniques from our active community forum. An account to the forums will automatically be created for you when your AdGodMedia Application is approved. Earn reputation points and have fun!</p>
									</div>
								</div>
							</div>
							<div class="col-md-4 t-center over-hidden">
								<img alt="" src="{{ asset('site/images/mobile.jpg') }}" class="fx" data-animate="fadeInUp" data-animation-delay="200">
							</div>
							<div class="col-md-4 side-cell">
								<div class="icon-box simple">
									<i class="fa fa-line-chart md-icon icon-bg main-bg border3px"></i>
									<div class="icon-simple-desc">
										<h4 class="uppercase">In-Depth Reporting</h4>
										<p>We provide breakdowns on all your traffic including countries and more. Our platform allows you to fully analyze your traffic..</p>
									</div>
								</div>
								<div class="icon-box simple">
									<i class="fa fa-shield md-icon icon-bg main-bg border3px"></i>
									<div class="icon-simple-desc">
										<h4 class="uppercase">Reliable & Trustworthy</h4>
										<p>Reliability is a something we highly value at AdGodMedia. Due to the scalibilty of our platform we won't suffer from downtimes in high traffic periods and guarantee we track every click</p>
									</div>
								</div>
								<div class="icon-box simple">
									<i class="fa fa-file md-icon icon-bg main-bg border3px"></i>
									<div class="icon-simple-desc">
										<h4 class="uppercase">Upload Contents</h4>
										<p>Use our simple but efficient File Manager to upload your files with just a few clicks. Once your file has been uploaded, you get to immediately start sharing your links and earning for every download.</p>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="md-padding">
										
					<div class="container">
						
						<div class="heading main centered">
							<h3 class="uppercase lg-title">Our Network <span class="main-color">Status</span></h3>
							
						</div>

						<div class="row icon-boxes-3">
							<div class="col-md-3">
								<div class="icon-box-3 circle">
									<div class="icon-desc main-bg">
										<i class="fa fa-users"></i>
										<div class="box-number heavy white"><span class="odometer count-title" data-value="1223" data-timer="800"></span></div>
										<h4 class="white">Happy Publishers</h4>
									</div>
								</div>
							</div>
							<div class="col-md-3">
								<div class="icon-box-3 circle">
									<div class="icon-desc dark-bg">
										<i class="fa fa-rss-square"></i>
										<div class="box-number heavy white"><span class="odometer count-title" data-value="3576" data-timer="1500"></span></div>
										<h4 class="white">Our Networks Offers</h4>
									</div>
								</div>
							</div>
							<div class="col-md-3">
								<div class="icon-box-3 circle">
									<div class="icon-desc red-bg">
										<i class="fa fa-money"></i>
										<div class="box-number heavy white"><span class="odometer count-title" data-value="25511" data-timer="1500"></span></div>
										<h4 class="white">Total Payment($)</h4>
									</div>
								</div>
							</div>
							<div class="col-md-3">
								<div class="icon-box-3 circle">
									<div class="icon-desc pink-bg">
										<i class="fa fa-user-secret"></i>
										<div class="box-number heavy white"><span class="odometer count-title" data-value="78" data-timer="1500"></span></div>
										<h4 class="white">Happy Advertisers</h4>
									</div>
								</div>
							</div>
						</div>

					</div>
				</div>

				<div class="sm-padding pattern-line-1 main-bg">
					<div class="container">
						<div class="cta_btn">
							
							<div class="f-left"> 
								<h2 class="cta_heading uppercase white">Get Started Earn money with CPA Marketing </h2>
								<h4 class="cta_heading white">Real Time reporting ,Content locker,Link locker ,Note locker and many more.</h4>
							</div>    
							
							<a class="btn btn-xl btn-outlined btn-white f-right" href="http://www.adgodmedia.com/auth/register">Join Now</a>
							
						</div>
					</div>
				</div>

				<div class="md-padding">
										
					<div class="container">
						
						<div class="heading main centered">
							<h3 class="uppercase lg-title">Our <span class="main-color">Team</span></h3>
							<p>Our Main Goal Is provide Maximum Support </p>
						</div>

						<div class="row">
							<div class="col-md-3">
								<div class="team-box box-3 main-bg">
									<div class="team-over">
										<h4 class="team-name">Devid Dee</h4>
										<h5 class="uppercase">Director</h5>
										<div class="top-team">
											<div class="front face"></div>										
											<div class="back face main-bg">
												<p>All About Director</p>
												<div class="social-list tbl">
													<a href="#" data-toggle="tooltip" data-placement="top" data-original-title="Facebook"><i class="fa fa-facebook ic-facebook sm-icon"></i></a>
													<a href="#" data-toggle="tooltip" data-placement="top" data-original-title="Twitter"><i class="fa fa-twitter ic-twitter sm-icon"></i></a>
													<a href="#" data-toggle="tooltip" data-placement="top" data-original-title="Linkedin"><i class="fa fa-linkedin ic-linkedin sm-icon"></i></a>
													<a href="#" data-toggle="tooltip" data-placement="top" data-original-title="Dribbble"><i class="fa fa-dribbble ic-dribbble sm-icon"></i></a>
												</div>
											</div>
										</div>
										<div class="bottom-team"></div>
									</div>
									<img alt="" src="{{ asset('site/images/1.jpg') }}" class="tm-img" />
								</div>
							</div>
							<div class="col-md-3">
								<div class="team-box box-3 main-bg">
									<div class="team-over">
										<h4 class="team-name">Tom Hook</h4>
										<h5 class="uppercase">Affiliate Manager</h5>
										<div class="top-team">
											<div class="front face"></div>										
											<div class="back face main-bg">
												<p>All About Affiliate Manager.</p>
												<div class="social-list tbl">
													<a href="#" data-toggle="tooltip" data-placement="top" data-original-title="Facebook"><i class="fa fa-facebook ic-facebook sm-icon"></i></a>
													<a href="#" data-toggle="tooltip" data-placement="top" data-original-title="Twitter"><i class="fa fa-twitter ic-twitter sm-icon"></i></a>
													<a href="#" data-toggle="tooltip" data-placement="top" data-original-title="Linkedin"><i class="fa fa-linkedin ic-linkedin sm-icon"></i></a>
													<a href="#" data-toggle="tooltip" data-placement="top" data-original-title="Dribbble"><i class="fa fa-dribbble ic-dribbble sm-icon"></i></a>
												</div>
											</div>
										</div>
										<div class="bottom-team"></div>
									</div>
									<img alt="" src="{{ asset('site/images/3.jpg') }}" class="tm-img" />
								</div>
							</div>
							<div class="col-md-3">
								<div class="team-box box-3 main-bg">
									<div class="team-over">
										<h4 class="team-name">Elina Vee</h4>
										<h5 class="uppercase">Affiliate Manager</h5>
										<div class="top-team">
											<div class="front face"></div>										
											<div class="back face main-bg">
												<p>All About Affiliate manager.</p>
												<div class="social-list tbl">
													<a href="#" data-toggle="tooltip" data-placement="top" data-original-title="Facebook"><i class="fa fa-facebook ic-facebook sm-icon"></i></a>
													<a href="#" data-toggle="tooltip" data-placement="top" data-original-title="Twitter"><i class="fa fa-twitter ic-twitter sm-icon"></i></a>
													<a href="#" data-toggle="tooltip" data-placement="top" data-original-title="Linkedin"><i class="fa fa-linkedin ic-linkedin sm-icon"></i></a>
													<a href="#" data-toggle="tooltip" data-placement="top" data-original-title="Dribbble"><i class="fa fa-dribbble ic-dribbble sm-icon"></i></a>
												</div>
											</div>
										</div>
										<div class="bottom-team"></div>
									</div>
									<img alt="" src="{{ asset('site/images/2.jpg') }}" class="tm-img" />
								</div>
							</div>
							<div class="col-md-3">
								<div class="team-box box-3 main-bg">
									<div class="team-over">
										<h4 class="team-name">Clark Dow</h4>
										<h5 class="uppercase">Designer</h5>
										<div class="top-team">
											<div class="front face"></div>										
											<div class="back face main-bg">
												<p>All About Designer.</p>
												<div class="social-list tbl">
													<a href="#" data-toggle="tooltip" data-placement="top" data-original-title="Facebook"><i class="fa fa-facebook ic-facebook sm-icon"></i></a>
													<a href="#" data-toggle="tooltip" data-placement="top" data-original-title="Twitter"><i class="fa fa-twitter ic-twitter sm-icon"></i></a>
													<a href="#" data-toggle="tooltip" data-placement="top" data-original-title="Linkedin"><i class="fa fa-linkedin ic-linkedin sm-icon"></i></a>
													<a href="#" data-toggle="tooltip" data-placement="top" data-original-title="Dribbble"><i class="fa fa-dribbble ic-dribbble sm-icon"></i></a>
												</div>
											</div>
										</div>
										<div class="bottom-team"></div>
									</div>
									<img alt="" src="{{ asset('site/images/4.jpg') }}" class="tm-img" />
								</div>
							</div>
						</div>

					</div>
				</div>
				
		    </div>			    
	    	<!-- Content start -->

	    	<!-- Footer start -->
		    <footer id="footWrapper">
		    	
		    	<!-- footer top bar start -->
		    	<div class="footer-top">
		    		<div class="container">
						<p class="lg-txt f-left">
							We Always care of you! Earn Money As A proud Publisher of <span class="main-color heavy">ADGODMEDIA</span> CPA Network
						</p>
						<a class="f-right btn uppercase main-gradient skew-btn" href="http://www.adgodmedia.com/auth/register"><span>Sign Up Now</span></a>
		    		</div>
		    	</div>
		    	<!-- footer top bar start -->


			    <!-- footer bottom bar start -->
			    <div class="footer-bottom">
				    <div class="container">
			    		<div class="row">
				    		<!-- footer copyrights left cell -->
				    		<div class="copyrights col-md-5">© Copyrights <b class="main-color">adgodmedia.com</b> 2017. All rights reserved.</div>
				    						    		
				    		<!-- footer bottom menu start -->
						    <div class="col-md-7 last">
						    	<ul class="footer-menu f-right">
								    <li><a href="http://www.adgodmedia.com">Home</a></li>
								    <li><a href="http://www.adgodmedia.com/terms-and-conditions">Terms & Conditions</a></li>
								    <li><a href="http://www.adgodmedia.com/blog">Blog</a></li>
								    <li><a href="http://www.adgodmedia.com/auth/register">Signup</a></li>
								    <li><a href="http://www.adgodmedia.com/auth/login">Login</a></li>
								    <li><a href="http://www.adgodmedia.com/contactus">Contact</a></li>
							    </ul>
						    </div>
						    <!-- footer bottom menu end -->
						    
			    		</div>
				    </div>
			    </div>
			    <!-- footer bottom bar end -->

			</footer>
			<!-- Footer end -->

		</div>

		<!-- Back to top Link -->
	    <a id="to-top" href="#"><span class="fa fa fa-angle-up"></span></a>

		<!-- Load JS plugins -->
 		<script type="text/javascript" src="{{ asset('site/js/jquery-1.12.0.min.js') }}"></script>
 		<script type="text/javascript" src="{{ asset('site/js/assets.js') }}"></script>

 		<!-- SLIDER REVOLUTION  -->
		<script type="text/javascript" src="{{ asset('site/js/jquery.themepunch.tools.min.js') }}"></script>
		<script type="text/javascript" src="{{ asset('site/js/jquery.themepunch.revolution.min.js') }}"></script>
		
		<!-- SLIDER REVOLUTION 5.0 EXTENSIONS  
			(Load Extensions only on Local File Systems !  +
			The following part can be removed on Server for On Demand Loading) -->
		<script type="text/javascript" src="{{ asset('site/js/revolution.extension.actions.min.js') }}"></script>
		<script type="text/javascript" src="{{ asset('site/js/revolution.extension.carousel.min.js') }}"></script>
		<script type="text/javascript" src="{{ asset('site/js/revolution.extension.kenburn.min.js') }}"></script>
		<script type="text/javascript" src="{{ asset('site/js/revolution.extension.layeranimation.min.js') }}"></script>
		<script type="text/javascript" src="{{ asset('site/js/revolution.extension.migration.min.js') }}"></script>
		<script type="text/javascript" src="{{ asset('site/js/revolution.extension.navigation.min.js') }}"></script>
		<script type="text/javascript" src="{{ asset('site/js/revolution.extension.parallax.min.js') }}"></script>
		<script type="text/javascript" src="{{ asset('site/js/revolution.extension.slideanims.min.js') }}"></script>
		<script type="text/javascript" src="{{ asset('site/js/revolution.extension.video.min.js') }}"></script>
		<!-- END SLIDER REVOLUTION 5.0 EXTENSIONS -->
		<script type="text/javascript">
				var tpj=jQuery;			
				var revapi70;
				tpj(window).load(function() {
					if(tpj("#slider").revolution == undefined){
						revslider_showDoubleJqueryError("#slider");
					}else{
						revapi70 = tpj("#slider").show().revolution({
							sliderType:"standard",

							jsFileLocation:"{{ asset('site/js/') }}",
							sliderLayout:"fullwidth",
							dottedOverlay:"none",
							delay:9000,
							navigation: {
								keyboardNavigation:"off",
								keyboard_direction: "horizontal",
								mouseScrollNavigation:"off",
								onHoverStop:"off",
								touch:{
									touchenabled:"on",
									swipe_threshold: 75,
									swipe_min_touches: 1,
									swipe_direction: "horizontal",
									drag_block_vertical: false
								}
								,
								arrows: {
									style:"zeus",
									enable:true,
									hide_onmobile:true,
									hide_under:600,
									hide_onleave:true,
									hide_delay:200,
									hide_delay_mobile:1200,
									tmp:'<div class="tp-title-wrap">  	<div class="tp-arr-imgholder"></div> </div>',
									left: {
										h_align:"left",
										v_align:"center",
										h_offset:30,
										v_offset:0
									},
									right: {
										h_align:"right",
										v_align:"center",
										h_offset:30,
										v_offset:0
									}
								}
							},
							responsiveLevels:[1240,1024,778,480],
							visibilityLevels:[1240,1024,778,480],
							gridwidth:[1240,1024,778,480],
							gridheight:[600,600,600,600],
							lazyType:"smart",
							parallax: {
								type:"mouse+scroll",
								origo:"slidercenter",
								speed:2000,
								levels:[1,2,3,20,25,30,35,40,45,50],
								disable_onmobile:"on"
							},
							shadow:0,
							spinner:"spinner2",
							autoHeight:"off",
							disableProgressBar:"on",
							hideThumbsOnMobile:"off",
							hideSliderAtLimit:0,
							hideCaptionAtLimit:0,
							hideAllCaptionAtLilmit:0,
							debugMode:false,
							fallbacks: {
								simplifyAll:"off",
								disableFocusListener:false,
							}
						});
					}
				});	/*ready*/
		</script>

 		<!-- general script file -->
		<script type="text/javascript" src="{{ asset('site/js/script.js') }}"></script>

	</body>
</html>