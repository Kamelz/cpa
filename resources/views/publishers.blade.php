<!DOCTYPE html>
<html lang="en">
<head>
    <title>AdGodMedia - Publishers</title>
	
	@include('_head')

    @include('ganalytics')

</head>
<body>

@include('_header')

<div class="md-padding p-b-0 gry-bg">
<div class="container">
<div class="heading main centered">
<h3 class="uppercase lg-title"> <span class="main-color">Publisher</span></h3>
<br>
<h1>Get Started Quickly</h1>

<br>
	<p>AdGodMedia is a network which prides itself for best in class customer support. We will take care of individual publishers needs and help them to make more money. Sign up right now with the top-rated CPA network!</p>						

	
	<h2 class="uppercase lg-title">Our Benefits</h2>
	
	</div>
	
		<div class="row">
							<div class="col-md-4 side-cell right-icons">
								<div class="icon-box simple">
									<i class="fa fa-clock-o md-icon icon-bg main-bg border3px"></i>
									<div class="icon-simple-desc">
										<h4 class="uppercase">Faster Payouts</h4>
										<p>Each publisher gets started on Net-30 schedule. We analyze your traffic and you will quickly be moved to Net-15 or weekly payments.</p>
									</div>
								</div>
								<div class="icon-box simple">
									<i class="fa fa-cc-mastercard md-icon icon-bg main-bg border3px"></i>
									<div class="icon-simple-desc">
										<h4 class="uppercase">Flexible Payments</h4>
										<p>We currently offer Paypal, Payza, skrill, Wire Transfer,Payoneer and Neteller for payout options. Need something else? Our team will do the best to accommodate your needs.</p>
									</div>
								</div>
								<div class="icon-box simple">
									<i class="fa fa-cogs md-icon icon-bg main-bg border3px"></i>
									<div class="icon-simple-desc">
										<h4 class="uppercase">High Offer Performance</h4>
										<p>We hand pick every offer running on AdGodMedia platform which convert and pay well. Meaning, you get maximum payout for the traffic you run with us.</p>
									</div>
								</div>
							</div>
							<div class="col-md-4 t-center over-hidden">
								<img alt=""src="{{ asset('site/images/mobile.jpg') }}" class="fx" data-animate="fadeInUp" data-animation-delay="200">
							</div>
							<div class="col-md-4 side-cell">
								<div class="icon-box simple">
									<i class="fa fa-line-chart md-icon icon-bg main-bg border3px"></i>
									<div class="icon-simple-desc">
										<h4 class="uppercase">In-Depth Reporting</h4>
										<p>We provide breakdowns on all your traffic including countries and more. Our platform allows you to fully analyze your traffic..</p>
									</div>
								</div>
								<div class="icon-box simple">
									<i class="fa fa-shield md-icon icon-bg main-bg border3px"></i>
									<div class="icon-simple-desc">
										<h4 class="uppercase">Reliable & Trustworthy</h4>
										<p>Reliability is a something we highly value at AdGodMedia. Due to the scalibilty of our platform we won't suffer from downtimes in high traffic periods and guarantee we track every click</p>
									</div>
								</div>
								<div class="icon-box simple">
									<i class="fa fa-file md-icon icon-bg main-bg border3px"></i>
									<div class="icon-simple-desc">
										<h4 class="uppercase">Upload Contents</h4>
										<p>Use our simple but efficient File Manager to upload your files with just a few clicks. Once your file has been uploaded, you get to immediately start sharing your links and earning for every download.</p>
									</div>
								</div>
							</div>
						</div>
						
						
						
						
				
						
</div>
</div>






@include('_footer')

@include('_footer_js')
	
</body>
</html>