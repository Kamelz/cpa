<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "//www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="//www.w3.org/1999/xhtml" xmlns:fb="//ogp.me/ns/fb#">
<head>
  <title>Download File: {{ $cname }}</title>
<meta name='description' content='Download File: {{ $cname }}'/>
<meta property='og:title' content='Download: {{ $cname }}'/>
<meta name="robots" content="noindex">
<meta property='og:type' content='article'/>
<meta property='og:url' content='//adgodmedia.com/{{ Request::path() }}'/>
<meta property='og:site_name' content='AdGodMedia'/>

<link rel='icon' type='image/png' href="{{ asset('favicon.ico') }}">

<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">

<link href="{{ asset('templates/default/bootstrap.css') }}" rel="stylesheet" media="screen">
<link href="{{ asset('templates/default/style.css') }}" rel="stylesheet" media="screen">

<link href='//fonts.googleapis.com/css?family=Roboto:300,700|Droid+Sans' rel='stylesheet' type='text/css'>

<script type="text/javascript" src="{{ asset('templates/default/jquery-1.11.1.min.js') }}"></script>

@include('ganalytics')

</head>

<div id="wrap">

<nav class="navbari navbari-default navbari-fixed open-hover" role="navigation">
<div class="container">
  <div class="navbari-header">
        <a class="navbari-brand" href="#"><i class="icon-download-cloud-1"></i> Download File - {{ $cname }}</a>
  </div>
  <div class="collapse navbari-collapse navbari-ex1-collapse">
    <ul class="nav navbari-nav navbari-right">
      <li><a href="//adgodmedia.com"> adgodmedia.com </a></li>
    </ul>
  </div>
</div>
</nav>

  
<div class="container">
<div class="col-sm-12 col-lg-12">
<div class="row">

  <div class="col-md-8" style="z-index:200;">
    <div class="panel panel-default panel-body greyback">
      <h4>{{ $cname }}</h4>
      <hr>
      <div id="downloadbox" style="max-height:320px; min-height:320px;">
        <center><img id="placeholdoffer" src="{{ asset('templates/default/speed.png') }}" /></center>
        <div id="verify">
          <div class="col-sm-12 col-lg-12">
            <center><h5>Before you can download this file, please verify you are human!</h5></center>
            <div id="offer_html"></div>
          </div>
        </div>
      </div>
      <center>
        @if ($content->scope == 'file')
        <div class="col-md-4"><h3>{{ $content->fakesize }}</h3>File Size</div>
        <div class="col-md-4"><h3 id="uploaddate">{{ date('Y-m-d',strtotime($content->created_at)) }}</h3>Upload Date</div>
        <div class="col-md-4"><h3>{{ $content->fake_leads + $content->leads }}</h3>Downloads</div>
        @else
        <div class="col-md-6"><h3 id="uploaddate">{{ date('Y-m-d',strtotime($content->created_at)) }}</h3>Date Created</div>
        <div class="col-md-6"><h3>{{ $content->leads }}</h3>Unlocks</div>
        @endif
      </center>
    </div>
    <div class="theme_custom_html" id="theme_custom_html"></div>
  </div>
  
    
    <div class="col-md-4">
      <div class="panel panel-default panel-body greyback">
          <h5>Download File</h5>
        <button class="btn btn-success btn-block" data-toggle="modal" data-mfp-src="#offers">Download</button>
        </div>
       
        <div class="panel panel-default panel-body greyback">
          <h5>File URL</h5>
        
          <input type="text" id="shareurl" class="form-control" readonly="readonly" value="" onclick="this.select()" />
      <hr>
          <span>
          <a href="#" class="btn btn-primary btn-block sharefb u_share">Share on Facebook</a>
      <a href="#" class="btn btn-primary btn-block sharetw u_share">Share on Twitter</a>
            </span>
       </div>
       
       
       <div class="panel panel-default panel-body greyback">
          <h5>Report Content</h5>
                    If you think that this download doesn't comply with adgodmedia.com terms, please report it. <br /><br />
          <button class="btn btn-default btn-block" data-toggle="modal" data-mfp-src="#report">Report</button>
       </div>
     
    </div>
    
</div>
</div>
</div> 
</div>

<div class="white-popup mfp-hide" id="report">
  <h4>Report Content</h4>
  <hr />
  <div id="msgsent"></div>
  <div class="row">
    <div class="col-md-2">Name:</div>
    <div class="col-md-10"><input type="text" class="form-control" name="rname" /></div>
  </div>
  <br />
  <div class="row">
    <div class="col-md-2">Email:</div>
    <div class="col-md-10"><input type="text" class="form-control" name="remail" /></div>
  </div>
  <br />
  <div class="row">
    <div class="col-md-2">Message:</div>
    <div class="col-md-10"><textarea name="rmessage" cols="30" rows="5" class="form-control"></textarea></div>
  </div>
  <br />
  <div class="row">
    <div class="col-md-2"></div>
    <div class="col-md-10"><button id="rsubmit">Submit</button></div>
  </div>
</div>

<div class="white-popup mfp-hide" id="offers">
  <h4>Download File - {{ $cname }}</h4><hr />
  <div class="inst">Please complete a quick survey below before downloading:</div>
  <table class="table">
   <tbody id="offerstable">
      @foreach ($offers as $offer)
     <tr><td><a href="{{ url('gotooffer'.'?offer='.$offer->campid.'&pub='.$content->user_id.'&nt='.$offer->network.'&locker='.$content->scope.'&lid='.$content->id.'&ip='.$ip.'&cnty='.$location->isoCode.'&site_referrer='.$site_referrer.'&session='.$sessid) }}" target="_blank">{{ $offer->title }}</a></td></tr>
     @endforeach
   </tbody>
   <!--<tfoot>
     <tr><td style="line-height:0.5;background:#ddd;"><a href="#" id="moreoffers" style="font-size:70%;color:#666;">Load More Offers</a></td></tr>
   </tfoot>-->
  </table>
  <div class="checker">
    <div class="spinner">
      <div class="bounce1"></div>
      <div class="bounce2"></div>
      <div class="bounce3"></div>
    </div>
    Checking completion
  </div>

  <div class="row help">
    <div class="col-md-4">
      <img src="{{ asset('templates/default/step1.png') }}" alt="" />
      <span>Input valid information into any survey above!</span>
    </div>
    <div class="col-md-4">
      <img src="{{ asset('templates/default/step2.png') }}" alt="" />
      <span>We will keep checking your survey status for authentication!s</span>
    </div>
    <div class="col-md-4">
      <img src="{{ asset('templates/default/step3.png') }}" alt="" />
      <span>Once complete, your file download will <b>automatically</b> start!</span>
    </div>
  </div>
</div>


<footer class="text-center">
  <p><a href="//www.adgodmedia.com" target="_blank">adgodmedia.com</a> |  <a href="{{ url('download-tos') }}" target="_blank" >Terms & Conditions</a> | <a href="{{ url('dmca') }}" target="_blank" >DMCA</a></p>
  <p>Provided by <a href="{{ url('/') }}">adgodmedia.com</a> File Sharing. Copyright &copy;{{ date('Y') }}</p>
</footer>


<script type="text/javascript" src="{{ asset('templates/default/script.js') }}"></script>

<script>
   jQuery(document).ready(function() {

      var set = 1;

      var completionCheck = setInterval(function(){
            $.post('{{ url('check/'.$sessid) }}', { _token: '{{ csrf_token() }}', user: {{ $content->user_id }}, country: '{{ $location->isoCode }}', ip: {{ $ip }}, ctype: '{{ $content->scope }}', id: {{ $content->id }}, content: '{{ $cname }}' } )
            .done(function(result) {
                if(result != 0) { $('#offers').html('Congratulations! <a href="{{ url("gotocontent"."?session=".$sessid."&ip=".$ip."&country=".$location->isoCode) }}">Click here</a> to get your content'); clearInterval(completionCheck); }
            });
     }, 10000);

    $('a#moreoffers').click(function() {
          $(this).text('Please Wait..');
          $.get("{{ url('moreoffers') }}", { _token: '{{ csrf_token() }}', user: {{ $content->user_id}}, country: '{{ $location->isoCode }}', skip: set, num: 5 })
          .done(function(data) {
              $('#offerstable').empty();
              $.each(data, function(index,value) {

                  $('#offerstable').append('<tr><td><a href="{{ url('gotooffer') }}?offer=' + value.campid +'&pub={{ $content->user_id }}&nt=' + value.network + '&locker={{ $content->scope }}&lid={{ $content->id }}&ip={{ $ip }}&cnty={{ $location->isoCode }}&site_referrer={{ $site_referrer }}&session={{ $sessid }}" target="_blank">' + value.title + '</a></td></tr>');

              });
              set++;
              $('a#moreoffers').text('Load More Offers');
          });
      });

      $('button#rsubmit').click(function() {
        $.post("{{ url('content/report') }}", { _token: '{{ csrf_token() }}', name: $('[name="rname"]').val(), email: $('[name="remail"]').val(), message: $('[name="rmessage"]').val(), ip: {{ $ip }}, country: "{{ $location->isoCode }}", lockerid: {{ $content->id }}, lockertype: "{{ $content->scope }}", lockername: "{{ $cname }}" })
        .done(function(data) {
                $('#msgsent').html('');
                $.each( data, function( key, value ) {
                    if (value.title == 'Success') {
                      $('#msgsent').html('<div class="alert alert-info">'+value.message+'</div>');
                    } else {
                      $('#msgsent').append('<div class="alert alert-danger">'+value.message+'</div>');
                    }
                });
        });
      });

   });
</script>

</body>
</html>