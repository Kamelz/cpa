<!DOCTYPE html>
<html lang="en">
<head>
    <title>AdGodMedia - Terms & Conditions</title>
   
    @include('_head')
    @include('ganalytics')

</head>
<body>

@include('_header')

<div class="md-padding p-b-0 gry-bg">
<div class="container">
<div class="heading main centered">
<h3 class="uppercase lg-title"> <span class="main-color">Terms & Conditions</span></h3>
<br>

</div>
<p>The following enlists the terms of services from adgodmedia.com and its mirrors for its members. The publisher must have fully read and understood to all the Terms of Service, negligence will not be accepted as a reason by the Publisher for breaking the Terms of Service. <br><br>

In accordance with this Agreement, AdGodMedia, ("AdGodMedia", adgodmedia.com) and Publisher shall agree to the following Terms of Service for the receipt of Advertising Materials ("Creative") from Advertising Customers ("Customers"). This includes the serving, tracking and reporting of each Campaign made available on the AdGodMedia (the "Network") to the Publisher and Publisher Websites ("Websites").<br><br>
</p>
<h4>Account</h4>

AdGodMedia reserves the right to refuse service to any new or existing Publisher, at its sole discretion, with or without cause. Approval of membership in the network is limited to websites which follows the adgodmedia.com compliance guidelines. AdGodMedia reserves the right to withhold approval of membership in AdGodMedia based on the Website primary language.
<br><br>
AdGodMedia reserves the right, in its sole discretion and without liability, to reject, omit or exclude any Publisher or Website for any reason at any time, with or without notice to the Publisher and regardless of whether the Publisher or Website was previously accepted. Publishers may not apply for multiple accounts; doing so will result in rejection from the service.
<br><br>
<h4>Publisher and Website Approval</h4>

Registration with AdGodMedia shall not confer any right on Publisher to market or promote any Programs made available by AdGodMedia on the Site on behalf of its clients (the "Advertisers"). All prospective publishers need official approval from AdGodMedia before they can become Publishers. Only approved Publishers are permitted to use the Site. AdGodMedia reserves the right to withhold or refuse approval for any reason, whatsoever.
<br><br>
<h4>Representation</h4>

Publisher represents and warrants that:
<br>
- It is the owner or is licensed to use the entire contents and subject matter contained in the Website.
<br>
- The Website does not violate any law or regulation governing false or deceptive advertising, sweepstakes, gambling, comparative advertising, or trade disparagement
<br>
- The Website is free of any "worm", "virus" or other device that could impair or injure any person or entity.
<br>
- The Website does not contain any misrepresentation, or content that is defamatory or violates any rights of privacy or publicity.
<br>
- Publisher is generally familiar with the nature of the Internet and will comply with all laws and regulations that may apply to the internet and digital media.
<br>
- The Website does not and will not infringe any copyright, trademark, patent or other proprietary right. Publisher grants AdGodMedia and the customer the right and license to transmit the Creative to the Website if it does not infringe on the above.
<br><br>
<h4>Publishers Eligibility Requirements</h4>

Publisher, all websites, affiliated websites, and e-mail distribution lists (collectively the "Media") must meet the following criteria:
<br>
- All Publishers that wish to send e-mail advertisements must have permission based opt-in databases with functional unsubscribe mechanisms.
<br>
- Publisher websites must be content-based, not simply a collection of links or advertisements, nor can the sites be centred on making money off of the Advertisers.
<br>
- Publisher websites must have a top-level domain name.
<br>
- Unless otherwise approved in writing or by electronic means by AdGodMedia.
<br>
- Publisher websites must be in good order, fully functional and lack security vulnerabilities. Pop-ups are prohibited.
<br>
- Websites and Publishers must not promote or reference software piracy (warez, cracking, etc.), hacking, phreaking, emulators, ROM's, illegal activity or free winner based content.
<br>
- Websites must not contain, promote, have links to profanity, sexually explicit materials, hate material, promote violence, discrimination based on race, sex, religion, nationality, disability, sexual orientation, age, family status, terrorist material or any other materials deemed unsuitable or harmful to the reputation of AdGodMedia and its third party partners.
<br>
- Websites must not promote illegal activities or violations of the intellectual property rights of others such but not limited to copyrighted software, music and television/film material.
<br>
- Websites must not promote activities generally understood as Internet abuse, including but not limited to, the sending of unsolicited bulk electronic mail or instant messaging bombing tactics.
<br>
- Websites must not be advertised or promoted through the use of unsolicited bulk email, or allow website members or customers to engage in similar activities through Publisher's Website, including those activities prohibited by this agreement.
<br><br>
<h4>Payment</h4>

- AdGodMedia, shall pay any amounts due to the publisher on or about the 30th of every month for the revenue generated between the 1st to 31st of each previous month. Payment sheduled may be shortened based on publisher performance and at AdGodMedia descrition.
<br>
- AdGodMedia reserves the right to reduce any payments owed to Publisher as a consequence of any offsets taken by Advertisers for the following; invalid events, technical errors, tracking discrepancies, poor lead-quality and the like. AdGodMedia shall compile, calculate and electronically deliver data required to determine Publishers billing and compensation.
<br>
- AdGodMedia will issue to the Publisher any positive balance in the Publishers Account for Transactions reported for the previous relevant period. AdGodMedia shall have no obligation to make payment of any Commissions for which AdGodMedia has not received payment from the relevant Merchant of all monies due to AdGodMedia (including for all Commissions owed by such Merchant to all of such Merchant's Publishers) until such payment has been received to AdGodMedia.
<br>
- AdGodMedia determines the actual payments to your account. Any concerns or questions regarding your payout needs to be submitted to AdGodMedia in writing within three business days of you receiving the payment that is in question. If no question is raised within the three business days then your pay-out will be considered fair and accurate by you the Publisher, this cannot be reversed after the three business days.
<br>
- All amounts will be paid in US dollars ($). No payments will be issued for any amounts less than the "Payment Threshold" of "Payment Method" set by AdGodMedia.
<br>
- AdGodMedia will not pay for any Events that occur before a Program is initiated, or after a program terminates. Invoices submitted to AdGodMedia and payments made to Publisher shall be based on the Events as reported by AdGodMedia only. AdGodMedia will not be responsible to compensate Publishers for events that are not recorded due to Publisher's error or for non-payment by the Advertiser.
<br>
- AdGodMedia also reserves the right to deduct commission from any future payments for leads reversed/charged back after they were paid in a previous payment.
<br><br>
<h4>Termination</h4>

AdGodMedia reserves the right to terminate any Publisher's relationship with AdGodMedia at any time, with or without a cause. Termination notice may be provided by multiple means including email or any other public means and will be effective immediately. Upon receipt of such termination notice, Publisher agrees to immediately remove from his/her website AdGodMedia's HTML/JavaScript code for serving "Creative" from adgodmedia.com. The Publisher will be paid, in the next scheduled payment cycle, all legitimate earnings due up to the time of termination. Upon termination, and in the event that blatant fraudulent activities have been documented in the AdGodMedia server logs, no past or future payments will be made by AdGodMedia to the publisher.
<br><br>
<h4>Registration & Account Details</h4>

The Publisher agrees to submit valid information to AdGodMedia at all times. This includes Account Registration, Account Updates, Website Listings, and other areas where publisher details are collected. If information such as full name, address, websites listed, etc, are determined to be false or in question, AdGodMedia will suspend the respective Publisher and freeze all earnings due.
<br><br>
<h4>Intellectual Property & Reverse Engineering</h4>

The Publisher, or any user of the site, may NOT reverse engineer or decompile (whether in whole or in part) any software used in the Site and/or the Services (except to the extent expressly permitted by applicable law). You may NOT replicate any part of our platform including but not limited to AdGodMedia's content/link/file lockers, Publisher Platform, and other tools and services, doing so may result in legal action and account termination by AdGodMedia.
<br>
By accepting these terms, you acknowledge and agree that all content presented to you on this site is protected by copyrights, trademarks, service marks, patents or other proprietary rights and laws, and is the sole property of adgodmedia.com and/or its Affiliates.
<br>
You are only permitted to use the content as expressly authorized by us or the specific content provider. Except for a single copy made for personal use only, you may not copy, reproduce, modify, republish, upload, post, transmit, or distribute any documents or information from this site in any form or by any means without prior written permission from us or the specific content provider, and you are solely responsible for obtaining permission before reusing any copyrighted material that is available on this site. Any unauthorized use of the materials appearing on this site may violate copyright, trademark and other applicable laws and could result in criminal or civil penalties.
<br><br>
<h4>Technical Support</h4>

Publisher may optionally request that our support team access their server and/or website to solve technical issues. Publisher may also request technical recommendations and advice. AdGodMedia is not responsible for any damage that occurs as a result of these requests, advice, or implementations and AdGodMedia is not legally liable.
<br><br>
<h4>Referral Program</h4>

- You may refer others to join AdGodMedia as a new Publisher, using a referral link found in the Publisher Dashboard. In order for a referred publisher to constitute as a valid referral the following must be true:
<br>
- Referred publisher must be new to AdGodMedia, applying for an account for the first time. Referred publisher must not be a business partner and must not live with you.
<br>
- Referred publisher account must not be directly or indirectly under your control. Referred publisher must not qualify as an affiliate or advertising network
<br>
- Referrals must register using your unique referral link to our Publisher application page. Referrals cannot be credited to your account retroactively or manually. Referrals must also be in good standing at the time your referral commissions are paid, in order for you to earn valid commission on that referral.
<br><br>
<h4>Promotions</h4>

When available, performance bonuses can be activated through the applicable manager. Network level accounts are not eligible for promotion bonuses. Once the promotion is activated by the user, all leads generated within the specified time period will be tallied and awarded the respective bonus amount. AdGodMedia reserves the right to adjust or deny all bonuses due to fraud, calculation errors, or other matters deemed relevant by us.
<br><br>
<h4>Website and Channel Content</h4>

AdGodMedia reserves the absolute right to refuse to affiliate with any Publisher. AdGodMedia does not accept Websites that provide or produce adult content (without prior consent). AdGodMedia does not accept Websites that engage in, promote or facilitate illegal or legally questionable activities such as pirating and hacking. AdGodMedia does not accept Websites that are: under construction, hosted by a free service, personal home pages, or do not own the domain they are under. This Agreement is voidable by AdGodMedia immediately if Publisher fails to disclose, conceals or misrepresents itself in any way. In addition, AdGodMedia may in its complete discretion refuse to serve any Website that it deems appropriate. To insure compliance with this Agreement, any Publishers that change their content after approval for membership MUST notify adgodmedia.com of the changes in writing IMMEDIATELY. We prefer you notify us ahead of time of any major changes in content or design.
<br>
- In order to be eligible to become a Publisher on our network, your content must not promote, advocate, facilitate or otherwise include any of the following:
<br>
- AdGodMedia Publisher platform makes Incent-Friendly campaigns clearly noticed, incent content should only use these campaigns.
<br>
- Your Publisher Website(s) cannot contain pornographic, obscene, sexually explicit or related content.
<br>
- Your Publisher Website(s) cannot contain hacking, cracking, virus, Trojan, misleading content, use of the word "Free" or related content.
<br>
- Your Publisher Website(s) cannot promote illegal substance or (e.g., illegal narcotics/substances, how to build a bomb or terrorism related device, counterfeiting money, etc.)
<br>
- Your Publisher Website(s) cannot contain racial, ethnic, political, hate or otherwise objectionable content.
<br>
- Your Publisher Website(s) must contain distinct and legitimate content, substance and material, not simply a list of links or advertisements. Further, the applicable Publisher Website(s) must serve a purpose substantially or completely separate and distinct from merely being designed to earn money solely from Company's Advertisers or third party advertisers. Your Publisher Website(s) must each be represented by a legitimate second-level domain name (e.g. yoursite.com is acceptable; however, a shared server, e.g., sharedsite.com/your site, is not acceptable). Your Publisher Website(s) must be written in English and contain no other language in its content. Your Publisher Website(s) must be fully functional at all levels; no "under construction" website(s) or sections are permissible.
<br><br>
<h4>Spam Indemnification</h4>

Publisher agrees to indemnify and hold AdGodMedia, its Advertisers and their respective affiliates, employees, officers, agents, directors and representatives of "AdGodMedia" are harmless from all allegations, claims, actions, causes of action, lawsuits, damages, liabilities, obligations, costs and expenses (including without limitation reasonable attorneys' fees, costs related to in-house counsel time, court costs and witness fees) (collectively "Losses") arising out of or in connection with AdGodMedia's use of the email list provided by Publisher (including, but not limited to alleged violations of the Can-Spam Act of 2003).
<br>
Publisher warrants that it will not send any commercial email to any person who has requested not to receive email from the Publisher and/or Advertiser and that they are in full compliance with the Can-Spam Act. Publisher also understands that upon doing so it automatically forfeits the right and claim to any revenue generated for its account, and Publisher's account will be immediately terminated. Publisher further agrees that all of Publisher's business will be in compliance with all applicable laws.
<br>
Publisher also agrees to load the latest Suppression List for Campaigns that make this file noticeably available on the Campaign Details page. Email promotion can only occur once an email list has been checked against the Suppression List. All matching emails in the Suppression List MUST be removed from the mailing.
<br><br>

<h4>Fraud and Deception</h4>

AdGodMedia audits every Publisher's traffic on a daily basis. Publishers that produce or commit fraudulent activities, including false clicks, false impressions, and incentivized clicks (that have not been previously authorized in writing), will have their account permanently suspended from our network and will not be compensated for fraudulent traffic. If fraud is suspected or detected, Publisher's account will be made inactive pending further investigation.
<br><br>

<h4>Publisher Account Flag</h4>

If AdGodMedia suspects any of the following suspicious activities, it can hold the member's account.<br><br>

- Generate click-through/conversion rates that are much higher than industry averages and where solid justification is not evident to the reasonable satisfaction of AdGodMedia
<br>
- Generate significant amount of leads with common behavioural characteristics to spammers
<br>
- Generate fraudulent leads as determined by the Advertisers;
<br>
- Use fake redirects, automated software, and/or fraud to generate events from the Programs.
<br>
- Multiple completions from the same user agents/hostnames.
<br><br>
If the Publisher fraudulently adds leads or clicks or inflates leads or clicks by fraudulent traffic generation (such as pre-population of forms or mechanisms not approved by AdGodMedia or use of sites in co-registration campaigns that have not been approved by AdGodMedia), as determined solely by AdGodMedia, Publisher will forfeit its entire commission for all programs and its account will be terminated. If Publisher is notified that fraudulent activities may be occurring on its Media, and Publisher fails to take prompt action to stop the fraudulent activities, then, in addition to any other remedies available to AdGodMedia, Publisher shall be responsible for all costs and legal fees arising from these fraudulent activities. In addition, in the event that Publisher has already received payment for fraudulent activities, AdGodMedia reserves the right to seek credit or remedy from future earnings or to demand reimbursement from Publisher. AdGodMedia also retains the right to share relevant account information with third parties to verify or investigate fraudulent activity.
<br><br>

<h4>Code</h4>

AdGodMedia ad codes must not be modified from original format without consent from AdGodMedia Publisher cannot alter, copy, modify, take, sell, reuse, or divulge any AdGodMedia computer code, except as is necessary to partake in the AdGodMedia Network, provided, however, with the prior approval of AdGodMedia, a Publisher may, in certain instances, modify the AdGodMedia computer code for purposes of inserting certain pre-approved language above or below an advertisement served by adgodmedia.com. Requests for language approval should be sent to your AdGodMedia affiliate/performance manager.
<br><br>

<h4>Data Reporting (Stats)</h4>

AdGodMedia is the sole owner of all website, campaign, and aggregate web user data collected by adgodmedia.com. Publisher only has access to campaign data that is collected through the use of their inventory. Customers only have access to website and web user data that is collected as part of Customer's campaign.
<br><br>

<h4>Use of Leads</h4>

Publisher hereby acknowledges that the collection of the Leads is being done solely for the benefit of AdGodMedia or its Advertiser. Therefore, other than providing the Leads to AdGodMedia for delivery to the Advertisers, Publisher may not use, sell, transfer or assign or attempt to monetize the Leads for its own purposes. All right, title and interest in the Leads shall vest exclusively in AdGodMedia or its Advertisers.
<br><br>

<h4>Scrubbing Leads</h4>

Each Program shall have its own criteria for determining the validity of a lead (the "Lead Requirements"), which shall be detailed in the section entitled "Campaign Details" included in the on-site offer summary and the offer-specific instructions emailed by an AdGodMedia Affiliate/Performance Manager. AdGodMedia shall only pay for net or billable leads ("Billable Leads"), which are determined by taking the gross leads from a Program and deducting
<br><br>

<h4>Invalid Leads and Returned Leads</h4>

"Invalid Leads" are those leads that do not meet the Lead Requirements or AdGodMedia's general quality requirements. Examples include, but are not limited to, leads that are missing data, do not meet filter requirements, are incorrectly formatted or do not meet certain phone, email and CASS (postal address) validations.
<br><br>
"Returned Leads" are those that have met the Lead Requirements, but are rejected due to inaccurate data or the inability to verify user information. Examples include, but are not limited to, leads for which the registrant is not at the phone number or address submitted, "unsubscribes" prior to being contacted, is on the "Do Not Call List", replies with "Did Not Request This Information" or the lead is a duplicate in the Advertiser's database.
<br><br>
The AdGodMedia proprietary lead system is responsible for detecting and tracking Invalid Leads. The Advertiser is generally responsible for detecting and tracking Returned Leads. Billable Leads will generally be determined by the 25th of the subsequent month and will be displayed on the AdGodMedia site when available. Accordingly, any statistics regarding Billable Leads appearing on the Site during the month in which the Program is running are preliminary and are subject to adjustment as provided herein.
<br><br>

<h4>Lead Quality Requirements</h4>

AdGodMedia routinely audits leads to uphold our quality standards. We evaluate leads based on IP location and header details passed in transit during redirects through our tracking links. While we employ many automated fraud checks, some manual quality checks are required. AdGodMedia reserves the right to revoke or decline any lead for lack of quality.
<br><br>

<h4>Contact Information</h4>

To insure timely payment, Publishers are responsible for maintaining the correct contact and payment information associated with their account. Payment Profile information must be updated by the last day of the month to be reflected in the next payment. This must be done online using the Publisher's account. Any and all bank/service fees associated with returned or cancelled payments due to any error in the Publisher contact or payment information are Publisher's responsibility, and will be deducted from re-payment.
<br><br>

<h4>Third Party Publishers</h4>

AdGodMedia may terminate this agreement with publisher if a Third Party Publisher is engaging in fraudulent and/or prohibited conduct as is stated in this agreement. AdGodMedia may withhold all payments to publisher that are associated with the Fraudulent and/or prohibited conduct
<br><br>

<h4>Relationship of Parties</h4>

For purposes of this Agreement, each party shall be and act as an independent contractor. This Agreement does not constitute, create, or give effect to any employer/employee or franchiser/franchisee relationship, nor any joint venture, partnership, limited partnership, or agency among the parties, and the parties hereby acknowledge that no other facts of relations exist that would constitute, create, or give to effect any such relationship between them. Neither party has any right or authority to assume or create any obligation or responsibility on behalf of the other party except as may from time to time be provided otherwise by written agreement signed by both parties.
<br><br>

<h4>Assignment</h4>

This Agreement shall be binding upon and inure to the benefit of the parties hereto, their subsidiaries, and their respective successors and assigns, provided that neither party may assign any of its rights or privileges hereunder without the prior written consent of the other party except to a successor in ownership (for example, by merger or acquisition) of all or substantially all of the assets of the assigning party, and which successor shall expressly assume in writing the performance of all the terms and conditions of this Agreement to be performed by the assigning party. Any attempt at assignment in derogation of the foregoing shall be held null and void.
<br><br>

<h4>Indemnification</h4>

Publishers will at all times indemnify and hold harmless the AdGodMedia Indemnified Parties from and against any and all Losses arising out of any arising out of the Publisher's breach of any representation, warranty or obligation hereunder, or any alleged breach of any representation, warranty or obligation to any other party.
<br><br>

<h4>Limitation of Liability</h4>

AdGodMedia shall not have any liability to the publisher for lost profits or other consequential, special, indirect or incidental damages, based upon a claim of any type or nature (including, but not limited to, contract, tort, including negligence, warranty or strict liability.
<br><br>

<h4>Representations</h4>

Each party represents and warrants that it has the authority to enter into this Agreement and sufficient rights to grant any licenses granted hereunder, and that any material provided by it to the other party for display on the other party's site will not infringe on any copyright, trademark or other proprietary right of any third party.
<br><br>

<h4>Severability</h4>

If any provision of this Agreement is held to be ineffective, unenforceable or illegal for any reason, such decision shall not affect the validity of any or all of the remaining portions thereof.
<br><br>

<h4>Governing Law</h4>

This Agreement will be governed by and construed under the laws of England and Wales without regard to the conflicts of law provisions thereof. Any action relating to this Agreement must be brought in the courts of England and Wales and Publisher irrevocably consents to the jurisdiction of such courts.
<br><br>

<h4>Confidentiality</h4>

Each party acknowledges that it will not disclose the confidential information of the other party, except to its employees and professional advisors and except as required by law.
<br><br>

<h4>Applicability</h4>

In This Agreement, including all attachments which are incorporated herein by reference, constitutes the entire agreement between the parties with respect to the subject matter hereof, and supersedes and replaces all prior and contemporaneous understandings or agreements, written or oral, regarding such subject matter. Applicable sections shall survive expiration or early termination of this Agreement. Nothing in this Agreement shall be deemed to create a partnership or joint venture between the parties and neither AdGodMedia nor Publisher shall hold itself out as the agent of the other, except for that specified in this Agreement. Neither party shall be liable to the other for delays or failures in performance resulting from causes beyond the reasonable control of that party, including, but not limited to, acts of God, labour disputes or disturbances, material shortages or rationing, riots, acts of war, governmental regulations, communication or utility failures, or casualties. Failure by either party to enforce any provision of this Agreement shall not be deemed a waiver of future enforcement of that or any other provision.
<br><br>

Any waiver, amendment or other modification of any provision of this Agreement shall be effective only if in writing and signed by the parties. If for any reason a court of competent jurisdiction finds any provision of this Agreement to be unenforceable, that provision of the Agreement shall be enforced to the maximum extent permissible so as to affect the intent of the parties, and the remainder of this Agreement shall continue in full force and effect.
<br><br>

Headings used in this Agreement are for ease of reference only and shall not be used to interpret any aspect of this Agreement. In addition to terms that are negotiated and documented separately from this Agreement, terms that are automatically generated through the interactive use of the AdGodMedia website Publisher interface are explicitly bound by this Agreement.
<br><br>

<h4>Force Majeure</h4>

Neither party shall be held liable or responsible to the other party nor be deemed to have defaulted under or have defaulted under or breached this Agreement for failure or delay in fulfilling or performing any term of this Agreement when such failure or delay is caused by or results from causes beyond the reasonable control of the affected party, including but not limited to fire, floods, failure of communications systems or networks, embargoes, war, acts of war (whether war is declared or not), acts of terrorism, insurrections, riots, civil commotion, strikes, lockouts or other labor disturbances, acts of God or acts, omissions or delays in acting by any governmental authority or the other party; provided, however, that the party so affected shall use reasonable commercial efforts to avoid or remove such causes of non-performance, and shall continue performance hereunder with reasonable dispatch whenever such causes are removed. Either party shall provide the other party with prompt written notice of any delay or failure to perform that occurs by reason of force majeure. The parties shall mutually seek a resolution of the delay of the failure to perform as noted above.
<br><br>

<h4>Entire Agreement</h4>

This Agreement constitutes the entire agreement and supersedes all prior agreements of the parties with respect to the transactions set forth herein. AdGodMedia reserves the right to modify these terms and conditions at its sole discretion. Publishers are entitled to review these terms and conditions periodically.
<br><br>

<h4>Public Release</h4>

Publisher shall not release any information regarding Campaigns, Creatives, or Publishers relationship with AdGodMedia or its customers, including, without limitation, in press releases or promotional or merchandising materials, without the prior written consent of adgodmedia.com. AdGodMedia shall have the right to reference and refer to its work for, and relationship with, Publisher for marketing and promotional purposes. No press releases or general public announcements shall be made without the mutual consent of AdGodMedia and Publisher.
<br><br>

<h4>Remedy</h4>

If any Publisher violates or refuses to partake in their responsibilities, or commits fraudulent activity against us, AdGodMedia reserves the right to withhold payment and take appropriate legal action to cover its damages.
<br><br>

<h4>Audit</h4>

AdGodMedia shall have the sole responsibility for calculation of Publisher earnings, including Impressions and click through numbers. In the event Publisher disagrees with any such calculation, a written request should be sent no later than 3 days to AdGodMedia. AdGodMedia will provide Publisher with an explanation or adjustment of the numbers which shall be final and binding.
<br><br>

<h4>Modifications</h4>

AdGodMedia reserves the right to change any condition of this contract at any time, notification may be provided to Publishers but is not required.
<br><br>

<h4>Privacy</h4>

Publisher shall support AdGodMedia's commitment to protect the privacy of the online community; such commitment is set forth in AdGodMedia Privacy Policy located on adgodmedia.com, which is hereby incorporated into this Agreement.
<br><br>

<h4>Solicitors' Fees</h4>

AdGodMedia under this agreement is entitled to recover all reasonable legal' fees and collection fees associated with this agreement due to any intentional wrong doing or breach of this agreement by you the publisher.
<br><br>

<h4>Waiver</h4>

No waiver by either party of any breach of any provision hereof shall be deemed a waiver of any subsequent or prior breach of the same or any other provision. Ability to Enter into Agreement: By executing this Agreement, Publisher warrants that Publisher (or Authorized Representative of Publisher) is at least 18 years of age, and that there is no legal reason that Publisher cannot enter into a binding contract.
</div>
</div>


<br><br><br>

@include('_footer')
@include('_footer_js')

</body>
</html>