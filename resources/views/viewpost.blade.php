<!DOCTYPE html>
<html lang="en">
<head>
	
<link rel="stylesheet" href="{{ asset('site/css/foundation.css') }}" />
    <!-- Favicon -->
    <link rel="shortcut icon" href="{{ asset('favicon.ico') }}">
    <!-- Google Font -->
    <link href='//fonts.googleapis.com/css?family=Open+Sans:400,700|Raleway:700,400' rel='stylesheet' type='text/css'>
    <!-- Custom CSS-->
    <link rel="stylesheet" href="{{ asset('site/css/app.css') }}" />
    <script src="{{ asset('site/js/modernizr.js') }}"></script>

    <link rel="stylesheet" href="{{ asset('site/css/animate.min.css') }}">

    <link rel="stylesheet" href="{{ asset('site/css/font-awesome.min.css') }}">

    @include('_head')

    @include('ganalytics')
	

</head>
<body>

@include('_header')

<div class="md-padding p-b-0 gry-bg">
<div class="container">
<div class="heading main centered">
<h3 class="uppercase lg-title"> <span class="main-color">Blog</span></h3>
</div>

<div class="row">
	<div class="large-9 columns">
		
    <h2>{{ $post->title }}</h2><br>
    <?php echo $post->content; ?>
    <br>
    <h4>Tags</h4>
    <?php $tags = explode(',',$post->tags);
    foreach ($tags as $tag) { echo '<span class="tag">'.$tag.'</span>'; }
    ?> 

	</div>
	<div class="large-3 columns">
		<h4>Recent Posts</h4>
			@foreach ($recentposts as $recent)
			<a href="{{ url('blog'.'/'.$recent->slug) }}">{{ $recent->title }}</a><br><br>
			@endforeach
	</div>
</div>

<br><br>
</div>
</div>


<br><br>


@include('_footer')
@include('_footer_js')
  
  <script src="{{ asset('site/js/jsde/foundation.min.js') }}"></script>

  <script src="{{ asset('site/js/jsde/wow.min.js') }}"></script>

  <script src="{{ asset('site/js/jsde/jquery-scrollto.js') }}"></script>
  <script src="{{ asset('site/js/jsde/jquery.sticky.js') }}"></script>
	
</body>
</html>